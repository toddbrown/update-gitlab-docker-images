export const imageVersionSplit = /(registry.gitlab.com\S+)(:)(v\d+\.\d+\.\d+)/g
export const versionReplace = /registry.gitlab.com\S+:(v\d+\.\d+\.\d+)/gm
