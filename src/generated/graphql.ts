import { GraphQLClient } from "graphql-request"
import * as Dom from "graphql-request/dist/types.dom"
import gql from "graphql-tag"
export type Maybe<T> = T | null
export type Exact<T extends { [key: string]: unknown }> = {
  [K in keyof T]: T[K]
}
export type MakeOptional<T, K extends keyof T> = Omit<T, K> &
  { [SubKey in K]?: Maybe<T[SubKey]> }
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> &
  { [SubKey in K]: Maybe<T[SubKey]> }
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string
  String: string
  Boolean: boolean
  Int: number
  Float: number
  /**
   * A `AlertManagementHttpIntegrationID` is a global ID. It is encoded as a string.
   *
   * An example `AlertManagementHttpIntegrationID` is: `"gid://gitlab/AlertManagement::HttpIntegration/1"`.
   *
   */
  AlertManagementHttpIntegrationID: any
  /**
   * A `AnalyticsDevopsAdoptionEnabledNamespaceID` is a global ID. It is encoded as a string.
   *
   * An example `AnalyticsDevopsAdoptionEnabledNamespaceID` is: `"gid://gitlab/Analytics::DevopsAdoption::EnabledNamespace/1"`.
   *
   */
  AnalyticsDevopsAdoptionEnabledNamespaceID: any
  /**
   * A `AwardableID` is a global ID. It is encoded as a string.
   *
   * An example `AwardableID` is: `"gid://gitlab/Awardable/1"`.
   *
   */
  AwardableID: any
  /** Represents non-fractional signed whole numeric values. Since the value may exceed the size of a 32-bit integer, it's encoded as a string. */
  BigInt: any
  /**
   * A `BoardID` is a global ID. It is encoded as a string.
   *
   * An example `BoardID` is: `"gid://gitlab/Board/1"`.
   *
   */
  BoardID: any
  /**
   * A `BoardsEpicBoardID` is a global ID. It is encoded as a string.
   *
   * An example `BoardsEpicBoardID` is: `"gid://gitlab/Boards::EpicBoard/1"`.
   *
   */
  BoardsEpicBoardID: any
  /**
   * A `BoardsEpicListID` is a global ID. It is encoded as a string.
   *
   * An example `BoardsEpicListID` is: `"gid://gitlab/Boards::EpicList/1"`.
   *
   */
  BoardsEpicListID: any
  /**
   * A `CiBuildID` is a global ID. It is encoded as a string.
   *
   * An example `CiBuildID` is: `"gid://gitlab/Ci::Build/1"`.
   *
   */
  CiBuildID: any
  /**
   * A `CiPipelineID` is a global ID. It is encoded as a string.
   *
   * An example `CiPipelineID` is: `"gid://gitlab/Ci::Pipeline/1"`.
   *
   */
  CiPipelineID: any
  /**
   * A `ClustersAgentID` is a global ID. It is encoded as a string.
   *
   * An example `ClustersAgentID` is: `"gid://gitlab/Clusters::Agent/1"`.
   *
   */
  ClustersAgentID: any
  /**
   * A `ClustersAgentTokenID` is a global ID. It is encoded as a string.
   *
   * An example `ClustersAgentTokenID` is: `"gid://gitlab/Clusters::AgentToken/1"`.
   *
   */
  ClustersAgentTokenID: any
  /**
   * A `ClustersClusterID` is a global ID. It is encoded as a string.
   *
   * An example `ClustersClusterID` is: `"gid://gitlab/Clusters::Cluster/1"`.
   *
   */
  ClustersClusterID: any
  /**
   * A `ComplianceManagementFrameworkID` is a global ID. It is encoded as a string.
   *
   * An example `ComplianceManagementFrameworkID` is: `"gid://gitlab/ComplianceManagement::Framework/1"`.
   *
   */
  ComplianceManagementFrameworkID: any
  /**
   * A `ContainerRepositoryID` is a global ID. It is encoded as a string.
   *
   * An example `ContainerRepositoryID` is: `"gid://gitlab/ContainerRepository/1"`.
   *
   */
  ContainerRepositoryID: any
  /**
   * A `DastProfileID` is a global ID. It is encoded as a string.
   *
   * An example `DastProfileID` is: `"gid://gitlab/Dast::Profile/1"`.
   *
   */
  DastProfileID: any
  /**
   * A `DastScannerProfileID` is a global ID. It is encoded as a string.
   *
   * An example `DastScannerProfileID` is: `"gid://gitlab/DastScannerProfile/1"`.
   *
   */
  DastScannerProfileID: any
  /**
   * A `DastSiteProfileID` is a global ID. It is encoded as a string.
   *
   * An example `DastSiteProfileID` is: `"gid://gitlab/DastSiteProfile/1"`.
   *
   */
  DastSiteProfileID: any
  /**
   * A `DastSiteTokenID` is a global ID. It is encoded as a string.
   *
   * An example `DastSiteTokenID` is: `"gid://gitlab/DastSiteToken/1"`.
   *
   */
  DastSiteTokenID: any
  /**
   * A `DastSiteValidationID` is a global ID. It is encoded as a string.
   *
   * An example `DastSiteValidationID` is: `"gid://gitlab/DastSiteValidation/1"`.
   *
   */
  DastSiteValidationID: any
  /** Date represented in ISO 8601 */
  Date: any
  /**
   * A `DesignManagementDesignAtVersionID` is a global ID. It is encoded as a string.
   *
   * An example `DesignManagementDesignAtVersionID` is: `"gid://gitlab/DesignManagement::DesignAtVersion/1"`.
   *
   */
  DesignManagementDesignAtVersionID: any
  /**
   * A `DesignManagementDesignID` is a global ID. It is encoded as a string.
   *
   * An example `DesignManagementDesignID` is: `"gid://gitlab/DesignManagement::Design/1"`.
   *
   */
  DesignManagementDesignID: any
  /**
   * A `DesignManagementVersionID` is a global ID. It is encoded as a string.
   *
   * An example `DesignManagementVersionID` is: `"gid://gitlab/DesignManagement::Version/1"`.
   *
   */
  DesignManagementVersionID: any
  /**
   * A `DiffNoteID` is a global ID. It is encoded as a string.
   *
   * An example `DiffNoteID` is: `"gid://gitlab/DiffNote/1"`.
   *
   */
  DiffNoteID: any
  /**
   * A `DiscussionID` is a global ID. It is encoded as a string.
   *
   * An example `DiscussionID` is: `"gid://gitlab/Discussion/1"`.
   *
   */
  DiscussionID: any
  /**
   * Duration between two instants, represented as a fractional number of seconds.
   *
   * For example: 12.3334
   *
   */
  Duration: any
  /**
   * A `EnvironmentID` is a global ID. It is encoded as a string.
   *
   * An example `EnvironmentID` is: `"gid://gitlab/Environment/1"`.
   *
   */
  EnvironmentID: any
  /**
   * A `EpicID` is a global ID. It is encoded as a string.
   *
   * An example `EpicID` is: `"gid://gitlab/Epic/1"`.
   *
   */
  EpicID: any
  /**
   * A `EpicTreeSortingID` is a global ID. It is encoded as a string.
   *
   * An example `EpicTreeSortingID` is: `"gid://gitlab/EpicTreeSorting/1"`.
   *
   */
  EpicTreeSortingID: any
  /**
   * A `GitlabErrorTrackingDetailedErrorID` is a global ID. It is encoded as a string.
   *
   * An example `GitlabErrorTrackingDetailedErrorID` is: `"gid://gitlab/Gitlab::ErrorTracking::DetailedError/1"`.
   *
   */
  GitlabErrorTrackingDetailedErrorID: any
  /**
   * A global identifier.
   *
   * A global identifier represents an object uniquely across the application.
   * An example of such an identifier is `"gid://gitlab/User/1"`.
   *
   * Global identifiers are encoded as strings.
   *
   */
  GlobalID: any
  /**
   * A `GroupID` is a global ID. It is encoded as a string.
   *
   * An example `GroupID` is: `"gid://gitlab/Group/1"`.
   *
   */
  GroupID: any
  /** An ISO 8601-encoded date */
  ISO8601Date: any
  /**
   * A `IncidentManagementEscalationPolicyID` is a global ID. It is encoded as a string.
   *
   * An example `IncidentManagementEscalationPolicyID` is: `"gid://gitlab/IncidentManagement::EscalationPolicy/1"`.
   *
   */
  IncidentManagementEscalationPolicyID: any
  /**
   * A `IncidentManagementEscalationRuleID` is a global ID. It is encoded as a string.
   *
   * An example `IncidentManagementEscalationRuleID` is: `"gid://gitlab/IncidentManagement::EscalationRule/1"`.
   *
   */
  IncidentManagementEscalationRuleID: any
  /**
   * A `IncidentManagementOncallParticipantID` is a global ID. It is encoded as a string.
   *
   * An example `IncidentManagementOncallParticipantID` is: `"gid://gitlab/IncidentManagement::OncallParticipant/1"`.
   *
   */
  IncidentManagementOncallParticipantID: any
  /**
   * A `IncidentManagementOncallRotationID` is a global ID. It is encoded as a string.
   *
   * An example `IncidentManagementOncallRotationID` is: `"gid://gitlab/IncidentManagement::OncallRotation/1"`.
   *
   */
  IncidentManagementOncallRotationID: any
  /**
   * A `IssuableID` is a global ID. It is encoded as a string.
   *
   * An example `IssuableID` is: `"gid://gitlab/Issuable/1"`.
   *
   */
  IssuableID: any
  /**
   * A `IssueID` is a global ID. It is encoded as a string.
   *
   * An example `IssueID` is: `"gid://gitlab/Issue/1"`.
   *
   */
  IssueID: any
  /**
   * A `IterationID` is a global ID. It is encoded as a string.
   *
   * An example `IterationID` is: `"gid://gitlab/Iteration/1"`.
   *
   */
  IterationID: any
  /**
   * A `IterationsCadenceID` is a global ID. It is encoded as a string.
   *
   * An example `IterationsCadenceID` is: `"gid://gitlab/Iterations::Cadence/1"`.
   *
   */
  IterationsCadenceID: any
  /** Represents untyped JSON */
  JSON: any
  /**
   * A `CommitStatusID` is a global ID. It is encoded as a string.
   *
   * An example `CommitStatusID` is: `"gid://gitlab/CommitStatus/1"`.
   *
   */
  JobID: any
  /** JSON object as raw string */
  JsonString: any
  /**
   * A `LabelID` is a global ID. It is encoded as a string.
   *
   * An example `LabelID` is: `"gid://gitlab/Label/1"`.
   *
   */
  LabelID: any
  /**
   * A `ListID` is a global ID. It is encoded as a string.
   *
   * An example `ListID` is: `"gid://gitlab/List/1"`.
   *
   */
  ListID: any
  /**
   * A `MergeRequestID` is a global ID. It is encoded as a string.
   *
   * An example `MergeRequestID` is: `"gid://gitlab/MergeRequest/1"`.
   *
   */
  MergeRequestID: any
  /**
   * A `MetricsDashboardAnnotationID` is a global ID. It is encoded as a string.
   *
   * An example `MetricsDashboardAnnotationID` is: `"gid://gitlab/Metrics::Dashboard::Annotation/1"`.
   *
   */
  MetricsDashboardAnnotationID: any
  /**
   * A `MilestoneID` is a global ID. It is encoded as a string.
   *
   * An example `MilestoneID` is: `"gid://gitlab/Milestone/1"`.
   *
   */
  MilestoneID: any
  /**
   * A `NamespaceID` is a global ID. It is encoded as a string.
   *
   * An example `NamespaceID` is: `"gid://gitlab/Namespace/1"`.
   *
   */
  NamespaceID: any
  /**
   * A `NoteID` is a global ID. It is encoded as a string.
   *
   * An example `NoteID` is: `"gid://gitlab/Note/1"`.
   *
   */
  NoteID: any
  /**
   * A `NoteableID` is a global ID. It is encoded as a string.
   *
   * An example `NoteableID` is: `"gid://gitlab/Noteable/1"`.
   *
   */
  NoteableID: any
  /**
   * A `PackagesConanFileMetadatumID` is a global ID. It is encoded as a string.
   *
   * An example `PackagesConanFileMetadatumID` is: `"gid://gitlab/Packages::Conan::FileMetadatum/1"`.
   *
   */
  PackagesConanFileMetadatumID: any
  /**
   * A `PackagesConanMetadatumID` is a global ID. It is encoded as a string.
   *
   * An example `PackagesConanMetadatumID` is: `"gid://gitlab/Packages::Conan::Metadatum/1"`.
   *
   */
  PackagesConanMetadatumID: any
  /**
   * A `PackagesMavenMetadatumID` is a global ID. It is encoded as a string.
   *
   * An example `PackagesMavenMetadatumID` is: `"gid://gitlab/Packages::Maven::Metadatum/1"`.
   *
   */
  PackagesMavenMetadatumID: any
  /**
   * A `PackagesNugetMetadatumID` is a global ID. It is encoded as a string.
   *
   * An example `PackagesNugetMetadatumID` is: `"gid://gitlab/Packages::Nuget::Metadatum/1"`.
   *
   */
  PackagesNugetMetadatumID: any
  /**
   * A `PackagesPackageFileID` is a global ID. It is encoded as a string.
   *
   * An example `PackagesPackageFileID` is: `"gid://gitlab/Packages::PackageFile/1"`.
   *
   */
  PackagesPackageFileID: any
  /**
   * A `PackagesPackageID` is a global ID. It is encoded as a string.
   *
   * An example `PackagesPackageID` is: `"gid://gitlab/Packages::Package/1"`.
   *
   */
  PackagesPackageID: any
  /**
   * A `PackagesPypiMetadatumID` is a global ID. It is encoded as a string.
   *
   * An example `PackagesPypiMetadatumID` is: `"gid://gitlab/Packages::Pypi::Metadatum/1"`.
   *
   */
  PackagesPypiMetadatumID: any
  /**
   * A `PathLockID` is a global ID. It is encoded as a string.
   *
   * An example `PathLockID` is: `"gid://gitlab/PathLock/1"`.
   *
   */
  PathLockID: any
  /** String or integer. */
  PayloadAlertFieldPathSegment: any
  /**
   * A `ProjectID` is a global ID. It is encoded as a string.
   *
   * An example `ProjectID` is: `"gid://gitlab/Project/1"`.
   *
   */
  ProjectID: any
  /**
   * A `PrometheusServiceID` is a global ID. It is encoded as a string.
   *
   * An example `PrometheusServiceID` is: `"gid://gitlab/PrometheusService/1"`.
   *
   */
  PrometheusServiceID: any
  /**
   * A `ReleasesLinkID` is a global ID. It is encoded as a string.
   *
   * An example `ReleasesLinkID` is: `"gid://gitlab/Releases::Link/1"`.
   *
   */
  ReleasesLinkID: any
  /**
   * A `SnippetID` is a global ID. It is encoded as a string.
   *
   * An example `SnippetID` is: `"gid://gitlab/Snippet/1"`.
   *
   */
  SnippetID: any
  /**
   * A `TerraformStateID` is a global ID. It is encoded as a string.
   *
   * An example `TerraformStateID` is: `"gid://gitlab/Terraform::State/1"`.
   *
   */
  TerraformStateID: any
  /**
   * Time represented in ISO 8601.
   *
   * For example: "2021-03-09T14:58:50+00:00".
   *
   * See `https://www.iso.org/iso-8601-date-and-time-format.html`.
   *
   */
  Time: any
  /**
   * A `TodoID` is a global ID. It is encoded as a string.
   *
   * An example `TodoID` is: `"gid://gitlab/Todo/1"`.
   *
   */
  TodoID: any
  /**
   * A `TodoableID` is a global ID. It is encoded as a string.
   *
   * An example `TodoableID` is: `"gid://gitlab/Todoable/1"`.
   *
   */
  TodoableID: any
  /** A regexp containing patterns sourced from user input */
  UntrustedRegexp: any
  Upload: any
  /**
   * A `UserID` is a global ID. It is encoded as a string.
   *
   * An example `UserID` is: `"gid://gitlab/User/1"`.
   *
   */
  UserID: any
  /**
   * A `VulnerabilitiesExternalIssueLinkID` is a global ID. It is encoded as a string.
   *
   * An example `VulnerabilitiesExternalIssueLinkID` is: `"gid://gitlab/Vulnerabilities::ExternalIssueLink/1"`.
   *
   */
  VulnerabilitiesExternalIssueLinkID: any
  /**
   * A `VulnerabilitiesScannerID` is a global ID. It is encoded as a string.
   *
   * An example `VulnerabilitiesScannerID` is: `"gid://gitlab/Vulnerabilities::Scanner/1"`.
   *
   */
  VulnerabilitiesScannerID: any
  /**
   * A `VulnerabilityID` is a global ID. It is encoded as a string.
   *
   * An example `VulnerabilityID` is: `"gid://gitlab/Vulnerability/1"`.
   *
   */
  VulnerabilityID: any
}

/** Represents the access level of a relationship between a User and object that it is related to */
export type AccessLevel = {
  __typename?: "AccessLevel"
  /** Integer representation of access level. */
  integerValue?: Maybe<Scalars["Int"]>
  /** String representation of access level. */
  stringValue?: Maybe<AccessLevelEnum>
}

/** Access level to a resource */
export enum AccessLevelEnum {
  /** No access. */
  NoAccess = "NO_ACCESS",
  /** Minimal access. */
  MinimalAccess = "MINIMAL_ACCESS",
  /** Guest access. */
  Guest = "GUEST",
  /** Reporter access. */
  Reporter = "REPORTER",
  /** Developer access. */
  Developer = "DEVELOPER",
  /** Maintainer access. */
  Maintainer = "MAINTAINER",
  /** Owner access. */
  Owner = "OWNER",
}

/** Autogenerated input type of AddAwardEmoji */
export type AddAwardEmojiInput = {
  /** The global ID of the awardable resource. */
  awardableId: Scalars["AwardableID"]
  /** The emoji name. */
  name: Scalars["String"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of AddAwardEmoji */
export type AddAwardEmojiPayload = {
  __typename?: "AddAwardEmojiPayload"
  /** The award emoji after mutation. */
  awardEmoji?: Maybe<AwardEmoji>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

/** Autogenerated input type of AddProjectToSecurityDashboard */
export type AddProjectToSecurityDashboardInput = {
  /** ID of the project to be added to Instance Security Dashboard. */
  id: Scalars["ProjectID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of AddProjectToSecurityDashboard */
export type AddProjectToSecurityDashboardPayload = {
  __typename?: "AddProjectToSecurityDashboardPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** Project that was added to the Instance Security Dashboard. */
  project?: Maybe<Project>
}

/** Autogenerated input type of AdminSidekiqQueuesDeleteJobs */
export type AdminSidekiqQueuesDeleteJobsInput = {
  /** Delete jobs matching user in the context metadata */
  user?: Maybe<Scalars["String"]>
  /** Delete jobs matching project in the context metadata */
  project?: Maybe<Scalars["String"]>
  /** Delete jobs matching root_namespace in the context metadata */
  rootNamespace?: Maybe<Scalars["String"]>
  /** Delete jobs matching subscription_plan in the context metadata */
  subscriptionPlan?: Maybe<Scalars["String"]>
  /** Delete jobs matching caller_id in the context metadata */
  callerId?: Maybe<Scalars["String"]>
  /** Delete jobs matching remote_ip in the context metadata */
  remoteIp?: Maybe<Scalars["String"]>
  /** Delete jobs matching related_class in the context metadata */
  relatedClass?: Maybe<Scalars["String"]>
  /** Delete jobs matching feature_category in the context metadata */
  featureCategory?: Maybe<Scalars["String"]>
  /** Delete jobs matching client_id in the context metadata */
  clientId?: Maybe<Scalars["String"]>
  /** The name of the queue to delete jobs from. */
  queueName: Scalars["String"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of AdminSidekiqQueuesDeleteJobs */
export type AdminSidekiqQueuesDeleteJobsPayload = {
  __typename?: "AdminSidekiqQueuesDeleteJobsPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** Information about the status of the deletion request. */
  result?: Maybe<DeleteJobsResponse>
}

/** Describes an alert from the project's Alert Management */
export type AlertManagementAlert = Noteable & {
  __typename?: "AlertManagementAlert"
  /** Assignees of the alert. */
  assignees?: Maybe<UserCoreConnection>
  /** Timestamp the alert was created. */
  createdAt?: Maybe<Scalars["Time"]>
  /** Description of the alert. */
  description?: Maybe<Scalars["String"]>
  /** Alert details. */
  details?: Maybe<Scalars["JSON"]>
  /** The URL of the alert detail page. */
  detailsUrl: Scalars["String"]
  /** All discussions on this noteable. */
  discussions: DiscussionConnection
  /** Timestamp the alert ended. */
  endedAt?: Maybe<Scalars["Time"]>
  /** Environment for the alert. */
  environment?: Maybe<Environment>
  /** Number of events of this alert. */
  eventCount?: Maybe<Scalars["Int"]>
  /** List of hosts the alert came from. */
  hosts?: Maybe<Array<Scalars["String"]>>
  /** Internal ID of the alert. */
  iid: Scalars["ID"]
  /** Issue attached to the alert. */
  issue?: Maybe<Issue>
  /**
   * Internal ID of the GitLab issue attached to the alert. Deprecated in 13.10: Use issue field.
   * @deprecated Use issue field. Deprecated in 13.10.
   */
  issueIid?: Maybe<Scalars["ID"]>
  /** URL for metrics embed for the alert. */
  metricsDashboardUrl?: Maybe<Scalars["String"]>
  /** Monitoring tool the alert came from. */
  monitoringTool?: Maybe<Scalars["String"]>
  /** All notes on this noteable. */
  notes: NoteConnection
  /** The alert condition for Prometheus. */
  prometheusAlert?: Maybe<PrometheusAlert>
  /** Runbook for the alert as defined in alert details. */
  runbook?: Maybe<Scalars["String"]>
  /** Service the alert came from. */
  service?: Maybe<Scalars["String"]>
  /** Severity of the alert. */
  severity?: Maybe<AlertManagementSeverity>
  /** Timestamp the alert was raised. */
  startedAt?: Maybe<Scalars["Time"]>
  /** Status of the alert. */
  status?: Maybe<AlertManagementStatus>
  /** Title of the alert. */
  title?: Maybe<Scalars["String"]>
  /** To-do items of the current user for the alert. */
  todos?: Maybe<TodoConnection>
  /** Timestamp the alert was last updated. */
  updatedAt?: Maybe<Scalars["Time"]>
}

/** Describes an alert from the project's Alert Management */
export type AlertManagementAlertAssigneesArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Describes an alert from the project's Alert Management */
export type AlertManagementAlertDiscussionsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Describes an alert from the project's Alert Management */
export type AlertManagementAlertNotesArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Describes an alert from the project's Alert Management */
export type AlertManagementAlertTodosArgs = {
  action?: Maybe<Array<TodoActionEnum>>
  authorId?: Maybe<Array<Scalars["ID"]>>
  projectId?: Maybe<Array<Scalars["ID"]>>
  groupId?: Maybe<Array<Scalars["ID"]>>
  state?: Maybe<Array<TodoStateEnum>>
  type?: Maybe<Array<TodoTargetEnum>>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** The connection type for AlertManagementAlert. */
export type AlertManagementAlertConnection = {
  __typename?: "AlertManagementAlertConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<AlertManagementAlertEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<AlertManagementAlert>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type AlertManagementAlertEdge = {
  __typename?: "AlertManagementAlertEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<AlertManagementAlert>
}

/** Values for sorting alerts */
export enum AlertManagementAlertSort {
  /** Updated at descending order. */
  UpdatedDesc = "UPDATED_DESC",
  /** Updated at ascending order. */
  UpdatedAsc = "UPDATED_ASC",
  /** Created at descending order. */
  CreatedDesc = "CREATED_DESC",
  /** Created at ascending order. */
  CreatedAsc = "CREATED_ASC",
  /** Start time by ascending order. */
  StartedAtAsc = "STARTED_AT_ASC",
  /** Start time by descending order. */
  StartedAtDesc = "STARTED_AT_DESC",
  /** End time by ascending order. */
  EndedAtAsc = "ENDED_AT_ASC",
  /** End time by descending order. */
  EndedAtDesc = "ENDED_AT_DESC",
  /** Created time by ascending order. */
  CreatedTimeAsc = "CREATED_TIME_ASC",
  /** Created time by descending order. */
  CreatedTimeDesc = "CREATED_TIME_DESC",
  /** Created time by ascending order. */
  UpdatedTimeAsc = "UPDATED_TIME_ASC",
  /** Created time by descending order. */
  UpdatedTimeDesc = "UPDATED_TIME_DESC",
  /** Events count by ascending order. */
  EventCountAsc = "EVENT_COUNT_ASC",
  /** Events count by descending order. */
  EventCountDesc = "EVENT_COUNT_DESC",
  /** Severity from less critical to more critical. */
  SeverityAsc = "SEVERITY_ASC",
  /** Severity from more critical to less critical. */
  SeverityDesc = "SEVERITY_DESC",
  /** Status by order: Ignored > Resolved > Acknowledged > Triggered. */
  StatusAsc = "STATUS_ASC",
  /** Status by order: Triggered > Acknowledged > Resolved > Ignored. */
  StatusDesc = "STATUS_DESC",
}

/** Represents total number of alerts for the represented categories */
export type AlertManagementAlertStatusCountsType = {
  __typename?: "AlertManagementAlertStatusCountsType"
  /** Number of alerts with status ACKNOWLEDGED for the project */
  acknowledged?: Maybe<Scalars["Int"]>
  /** Total number of alerts for the project. */
  all?: Maybe<Scalars["Int"]>
  /** Number of alerts with status IGNORED for the project */
  ignored?: Maybe<Scalars["Int"]>
  /** Number of alerts with status TRIGGERED or ACKNOWLEDGED for the project. */
  open?: Maybe<Scalars["Int"]>
  /** Number of alerts with status RESOLVED for the project */
  resolved?: Maybe<Scalars["Int"]>
  /** Number of alerts with status TRIGGERED for the project */
  triggered?: Maybe<Scalars["Int"]>
}

/** Filters the alerts based on given domain */
export enum AlertManagementDomainFilter {
  /** Alerts for operations domain. */
  Operations = "operations",
  /** Alerts for threat monitoring domain. */
  ThreatMonitoring = "threat_monitoring",
}

/** An endpoint and credentials used to accept alerts for a project */
export type AlertManagementHttpIntegration = AlertManagementIntegration & {
  __typename?: "AlertManagementHttpIntegration"
  /** Whether the endpoint is currently accepting alerts. */
  active?: Maybe<Scalars["Boolean"]>
  /** URL at which Prometheus metrics can be queried to populate the metrics dashboard. */
  apiUrl?: Maybe<Scalars["String"]>
  /** ID of the integration. */
  id: Scalars["ID"]
  /** Name of the integration. */
  name?: Maybe<Scalars["String"]>
  /** Extract alert fields from payload example for custom mapping. */
  payloadAlertFields?: Maybe<Array<AlertManagementPayloadAlertField>>
  /** The custom mapping of GitLab alert attributes to fields from the payload_example. */
  payloadAttributeMappings?: Maybe<
    Array<AlertManagementPayloadAlertMappingField>
  >
  /** The example of an alert payload. */
  payloadExample?: Maybe<Scalars["JsonString"]>
  /** Token used to authenticate alert notification requests. */
  token?: Maybe<Scalars["String"]>
  /** Type of integration. */
  type: AlertManagementIntegrationType
  /** Endpoint which accepts alert notifications. */
  url?: Maybe<Scalars["String"]>
}

/** The connection type for AlertManagementHttpIntegration. */
export type AlertManagementHttpIntegrationConnection = {
  __typename?: "AlertManagementHttpIntegrationConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<AlertManagementHttpIntegrationEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<AlertManagementHttpIntegration>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type AlertManagementHttpIntegrationEdge = {
  __typename?: "AlertManagementHttpIntegrationEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<AlertManagementHttpIntegration>
}

export type AlertManagementIntegration = {
  /** Whether the endpoint is currently accepting alerts. */
  active?: Maybe<Scalars["Boolean"]>
  /** URL at which Prometheus metrics can be queried to populate the metrics dashboard. */
  apiUrl?: Maybe<Scalars["String"]>
  /** ID of the integration. */
  id: Scalars["ID"]
  /** Name of the integration. */
  name?: Maybe<Scalars["String"]>
  /** Token used to authenticate alert notification requests. */
  token?: Maybe<Scalars["String"]>
  /** Type of integration. */
  type: AlertManagementIntegrationType
  /** Endpoint which accepts alert notifications. */
  url?: Maybe<Scalars["String"]>
}

/** The connection type for AlertManagementIntegration. */
export type AlertManagementIntegrationConnection = {
  __typename?: "AlertManagementIntegrationConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<AlertManagementIntegrationEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<AlertManagementIntegration>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type AlertManagementIntegrationEdge = {
  __typename?: "AlertManagementIntegrationEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<AlertManagementIntegration>
}

/** Values of types of integrations */
export enum AlertManagementIntegrationType {
  /** Prometheus integration. */
  Prometheus = "PROMETHEUS",
  /** Integration with any monitoring tool. */
  Http = "HTTP",
}

/** Parsed field from an alert used for custom mappings */
export type AlertManagementPayloadAlertField = {
  __typename?: "AlertManagementPayloadAlertField"
  /** Human-readable label of the payload path. */
  label?: Maybe<Scalars["String"]>
  /** Path to value inside payload JSON. */
  path?: Maybe<Array<Scalars["PayloadAlertFieldPathSegment"]>>
  /** Type of the parsed value. */
  type?: Maybe<AlertManagementPayloadAlertFieldType>
}

/** Field that are available while modifying the custom mapping attributes for an HTTP integration */
export type AlertManagementPayloadAlertFieldInput = {
  /** A GitLab alert field name. */
  fieldName: AlertManagementPayloadAlertFieldName
  /** Path to value inside payload JSON. */
  path: Array<Scalars["PayloadAlertFieldPathSegment"]>
  /** Human-readable label of the payload path. */
  label?: Maybe<Scalars["String"]>
  /** Type of the parsed value. */
  type: AlertManagementPayloadAlertFieldType
}

/** Values for alert field names used in the custom mapping */
export enum AlertManagementPayloadAlertFieldName {
  /** The title of the incident. */
  Title = "TITLE",
  /** A high-level summary of the problem. */
  Description = "DESCRIPTION",
  /** The time of the incident. */
  StartTime = "START_TIME",
  /** The resolved time of the incident. */
  EndTime = "END_TIME",
  /** The affected service. */
  Service = "SERVICE",
  /** The name of the associated monitoring tool. */
  MonitoringTool = "MONITORING_TOOL",
  /** One or more hosts, as to where this incident occurred. */
  Hosts = "HOSTS",
  /** The severity of the alert. */
  Severity = "SEVERITY",
  /** The unique identifier of the alert. This can be used to group occurrences of the same alert. */
  Fingerprint = "FINGERPRINT",
  /** The name of the associated GitLab environment. */
  GitlabEnvironmentName = "GITLAB_ENVIRONMENT_NAME",
}

/** Values for alert field types used in the custom mapping */
export enum AlertManagementPayloadAlertFieldType {
  /** Array field type. */
  Array = "ARRAY",
  /** DateTime field type. */
  Datetime = "DATETIME",
  /** String field type. */
  String = "STRING",
}

/** Parsed field (with its name) from an alert used for custom mappings */
export type AlertManagementPayloadAlertMappingField = {
  __typename?: "AlertManagementPayloadAlertMappingField"
  /** A GitLab alert field name. */
  fieldName?: Maybe<AlertManagementPayloadAlertFieldName>
  /** Human-readable label of the payload path. */
  label?: Maybe<Scalars["String"]>
  /** Path to value inside payload JSON. */
  path?: Maybe<Array<Scalars["PayloadAlertFieldPathSegment"]>>
  /** Type of the parsed value. */
  type?: Maybe<AlertManagementPayloadAlertFieldType>
}

/** An endpoint and credentials used to accept Prometheus alerts for a project */
export type AlertManagementPrometheusIntegration =
  AlertManagementIntegration & {
    __typename?: "AlertManagementPrometheusIntegration"
    /** Whether the endpoint is currently accepting alerts. */
    active?: Maybe<Scalars["Boolean"]>
    /** URL at which Prometheus metrics can be queried to populate the metrics dashboard. */
    apiUrl?: Maybe<Scalars["String"]>
    /** ID of the integration. */
    id: Scalars["ID"]
    /** Name of the integration. */
    name?: Maybe<Scalars["String"]>
    /** Token used to authenticate alert notification requests. */
    token?: Maybe<Scalars["String"]>
    /** Type of integration. */
    type: AlertManagementIntegrationType
    /** Endpoint which accepts alert notifications. */
    url?: Maybe<Scalars["String"]>
  }

/** Alert severity values */
export enum AlertManagementSeverity {
  /** Critical severity */
  Critical = "CRITICAL",
  /** High severity */
  High = "HIGH",
  /** Medium severity */
  Medium = "MEDIUM",
  /** Low severity */
  Low = "LOW",
  /** Info severity */
  Info = "INFO",
  /** Unknown severity */
  Unknown = "UNKNOWN",
}

/** Alert status values */
export enum AlertManagementStatus {
  /** Investigation has not started. */
  Triggered = "TRIGGERED",
  /** Someone is actively investigating the problem. */
  Acknowledged = "ACKNOWLEDGED",
  /** No further work is required. */
  Resolved = "RESOLVED",
  /** No action will be taken on the alert. */
  Ignored = "IGNORED",
}

/** Autogenerated input type of AlertSetAssignees */
export type AlertSetAssigneesInput = {
  /** The project the alert to mutate is in. */
  projectPath: Scalars["ID"]
  /** The IID of the alert to mutate. */
  iid: Scalars["String"]
  /** The usernames to assign to the alert. Replaces existing assignees by default. */
  assigneeUsernames: Array<Scalars["String"]>
  /** The operation to perform. Defaults to REPLACE. */
  operationMode?: Maybe<MutationOperationMode>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of AlertSetAssignees */
export type AlertSetAssigneesPayload = {
  __typename?: "AlertSetAssigneesPayload"
  /** The alert after mutation. */
  alert?: Maybe<AlertManagementAlert>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The issue created after mutation. */
  issue?: Maybe<Issue>
  /** The to-do item after mutation. */
  todo?: Maybe<Todo>
}

/** Autogenerated input type of AlertTodoCreate */
export type AlertTodoCreateInput = {
  /** The project the alert to mutate is in. */
  projectPath: Scalars["ID"]
  /** The IID of the alert to mutate. */
  iid: Scalars["String"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of AlertTodoCreate */
export type AlertTodoCreatePayload = {
  __typename?: "AlertTodoCreatePayload"
  /** The alert after mutation. */
  alert?: Maybe<AlertManagementAlert>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The issue created after mutation. */
  issue?: Maybe<Issue>
  /** The to-do item after mutation. */
  todo?: Maybe<Todo>
}

/** Data associated with configuring API fuzzing scans in GitLab CI */
export type ApiFuzzingCiConfiguration = {
  __typename?: "ApiFuzzingCiConfiguration"
  /** All available scan modes. */
  scanModes?: Maybe<Array<ApiFuzzingScanMode>>
  /** All default scan profiles. */
  scanProfiles?: Maybe<Array<ApiFuzzingScanProfile>>
}

/** Autogenerated input type of ApiFuzzingCiConfigurationCreate */
export type ApiFuzzingCiConfigurationCreateInput = {
  /** Full path of the project. */
  projectPath: Scalars["ID"]
  /** File path or URL to the file that defines the API surface for scanning. Must be in the format specified by the `scanMode` argument. */
  apiSpecificationFile: Scalars["String"]
  /** CI variable containing the password for authenticating with the target API. */
  authPassword?: Maybe<Scalars["String"]>
  /** CI variable containing the username for authenticating with the target API. */
  authUsername?: Maybe<Scalars["String"]>
  /** The mode for API fuzzing scans. */
  scanMode: ApiFuzzingScanMode
  /** Name of a default profile to use for scanning. Ex: Quick-10. */
  scanProfile?: Maybe<Scalars["String"]>
  /** URL for the target of API fuzzing scans. */
  target: Scalars["String"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of ApiFuzzingCiConfigurationCreate */
export type ApiFuzzingCiConfigurationCreatePayload = {
  __typename?: "ApiFuzzingCiConfigurationCreatePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** A YAML snippet that can be inserted into the project's `.gitlab-ci.yml` to set up API fuzzing scans. */
  configurationYaml?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The location at which the project's `.gitlab-ci.yml` file can be edited in the browser. */
  gitlabCiYamlEditPath?: Maybe<Scalars["String"]>
}

/** All possible ways to specify the API surface for an API fuzzing scan. */
export enum ApiFuzzingScanMode {
  /** The API surface is specified by a HAR file. */
  Har = "HAR",
  /** The API surface is specified by a OPENAPI file. */
  Openapi = "OPENAPI",
  /** The API surface is specified by a POSTMAN file. */
  Postman = "POSTMAN",
}

/** An API Fuzzing scan profile. */
export type ApiFuzzingScanProfile = {
  __typename?: "ApiFuzzingScanProfile"
  /** A short description of the profile. */
  description?: Maybe<Scalars["String"]>
  /** The unique name of the profile. */
  name?: Maybe<Scalars["String"]>
  /** A syntax highlit HTML representation of the YAML. */
  yaml?: Maybe<Scalars["String"]>
}

/** Describes a rule for who can approve merge requests. */
export type ApprovalRule = {
  __typename?: "ApprovalRule"
  /** ID of the rule. */
  id: Scalars["GlobalID"]
  /** Name of the rule. */
  name?: Maybe<Scalars["String"]>
  /** Type of the rule. */
  type?: Maybe<ApprovalRuleType>
}

/** The kind of an approval rule. */
export enum ApprovalRuleType {
  /** A `regular` approval rule. */
  Regular = "REGULAR",
  /** A `code_owner` approval rule. */
  CodeOwner = "CODE_OWNER",
  /** A `report_approver` approval rule. */
  ReportApprover = "REPORT_APPROVER",
  /** A `any_approver` approval rule. */
  AnyApprover = "ANY_APPROVER",
}

/** Assignee ID wildcard values */
export enum AssigneeWildcardId {
  /** No assignee is assigned. */
  None = "NONE",
  /** An assignee is assigned. */
  Any = "ANY",
}

/** User availability status */
export enum AvailabilityEnum {
  /** Not Set */
  NotSet = "NOT_SET",
  /** Busy */
  Busy = "BUSY",
}

/** An emoji awarded by a user */
export type AwardEmoji = {
  __typename?: "AwardEmoji"
  /** The emoji description. */
  description: Scalars["String"]
  /** The emoji as an icon. */
  emoji: Scalars["String"]
  /** The emoji name. */
  name: Scalars["String"]
  /** The emoji in Unicode. */
  unicode: Scalars["String"]
  /** The Unicode version for this emoji. */
  unicodeVersion: Scalars["String"]
  /** The user who awarded the emoji. */
  user: UserCore
}

/** Autogenerated input type of AwardEmojiAdd */
export type AwardEmojiAddInput = {
  /** The global ID of the awardable resource. */
  awardableId: Scalars["AwardableID"]
  /** The emoji name. */
  name: Scalars["String"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of AwardEmojiAdd */
export type AwardEmojiAddPayload = {
  __typename?: "AwardEmojiAddPayload"
  /** The award emoji after mutation. */
  awardEmoji?: Maybe<AwardEmoji>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

/** The connection type for AwardEmoji. */
export type AwardEmojiConnection = {
  __typename?: "AwardEmojiConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<AwardEmojiEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<AwardEmoji>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type AwardEmojiEdge = {
  __typename?: "AwardEmojiEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<AwardEmoji>
}

/** Autogenerated input type of AwardEmojiRemove */
export type AwardEmojiRemoveInput = {
  /** The global ID of the awardable resource. */
  awardableId: Scalars["AwardableID"]
  /** The emoji name. */
  name: Scalars["String"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of AwardEmojiRemove */
export type AwardEmojiRemovePayload = {
  __typename?: "AwardEmojiRemovePayload"
  /** The award emoji after mutation. */
  awardEmoji?: Maybe<AwardEmoji>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

/** Autogenerated input type of AwardEmojiToggle */
export type AwardEmojiToggleInput = {
  /** The global ID of the awardable resource. */
  awardableId: Scalars["AwardableID"]
  /** The emoji name. */
  name: Scalars["String"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of AwardEmojiToggle */
export type AwardEmojiTogglePayload = {
  __typename?: "AwardEmojiTogglePayload"
  /** The award emoji after mutation. */
  awardEmoji?: Maybe<AwardEmoji>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** Indicates the status of the emoji. True if the toggle awarded the emoji, and false if the toggle removed the emoji. */
  toggledOn: Scalars["Boolean"]
}

export type BaseService = Service & {
  __typename?: "BaseService"
  /** Indicates if the service is active. */
  active?: Maybe<Scalars["Boolean"]>
  /** Class name of the service. */
  type?: Maybe<Scalars["String"]>
}

export type Blob = Entry & {
  __typename?: "Blob"
  /** Flat path of the entry. */
  flatPath: Scalars["String"]
  /** ID of the entry. */
  id: Scalars["ID"]
  /** LFS ID of the blob. */
  lfsOid?: Maybe<Scalars["String"]>
  /** Blob mode in numeric format. */
  mode?: Maybe<Scalars["String"]>
  /** Name of the entry. */
  name: Scalars["String"]
  /** Path of the entry. */
  path: Scalars["String"]
  /** Last commit SHA for the entry. */
  sha: Scalars["String"]
  /** Type of tree entry. */
  type: EntryType
  /** Web path of the blob. */
  webPath?: Maybe<Scalars["String"]>
  /** Web URL of the blob. */
  webUrl?: Maybe<Scalars["String"]>
}

/** The connection type for Blob. */
export type BlobConnection = {
  __typename?: "BlobConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<BlobEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<Blob>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type BlobEdge = {
  __typename?: "BlobEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<Blob>
}

/** Represents how the blob content should be displayed */
export type BlobViewer = {
  __typename?: "BlobViewer"
  /** Shows whether the blob should be displayed collapsed. */
  collapsed: Scalars["Boolean"]
  /** Content file type. */
  fileType: Scalars["String"]
  /** Shows whether the blob content is loaded asynchronously. */
  loadAsync: Scalars["Boolean"]
  /** Loading partial name. */
  loadingPartialName: Scalars["String"]
  /** Error rendering the blob content. */
  renderError?: Maybe<Scalars["String"]>
  /** Shows whether the blob is too large to be displayed. */
  tooLarge: Scalars["Boolean"]
  /** Type of blob viewer. */
  type: BlobViewersType
}

/** Types of blob viewers */
export enum BlobViewersType {
  /** Rich blob viewers type. */
  Rich = "rich",
  /** Simple blob viewers type. */
  Simple = "simple",
  /** Auxiliary blob viewers type. */
  Auxiliary = "auxiliary",
}

/** Represents a project or group issue board */
export type Board = {
  __typename?: "Board"
  /** The board assignee. */
  assignee?: Maybe<UserCore>
  /** Timestamp of when the board was created. */
  createdAt: Scalars["Time"]
  /** Epics associated with board issues. */
  epics?: Maybe<BoardEpicConnection>
  /** Whether or not backlog list is hidden. */
  hideBacklogList?: Maybe<Scalars["Boolean"]>
  /** Whether or not closed list is hidden. */
  hideClosedList?: Maybe<Scalars["Boolean"]>
  /** ID (global ID) of the board. */
  id: Scalars["ID"]
  /** The board iteration. */
  iteration?: Maybe<Iteration>
  /** Labels of the board. */
  labels?: Maybe<LabelConnection>
  /** Lists of the board. */
  lists?: Maybe<BoardListConnection>
  /** The board milestone. */
  milestone?: Maybe<Milestone>
  /** Name of the board. */
  name?: Maybe<Scalars["String"]>
  /** Timestamp of when the board was last updated. */
  updatedAt: Scalars["Time"]
  /** Web path of the board. */
  webPath: Scalars["String"]
  /** Web URL of the board. */
  webUrl: Scalars["String"]
  /** Weight of the board. */
  weight?: Maybe<Scalars["Int"]>
}

/** Represents a project or group issue board */
export type BoardEpicsArgs = {
  issueFilters?: Maybe<BoardIssueInput>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Represents a project or group issue board */
export type BoardLabelsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Represents a project or group issue board */
export type BoardListsArgs = {
  id?: Maybe<Scalars["ListID"]>
  issueFilters?: Maybe<BoardIssueInput>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** The connection type for Board. */
export type BoardConnection = {
  __typename?: "BoardConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<BoardEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<Board>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type BoardEdge = {
  __typename?: "BoardEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<Board>
}

/** Represents an epic on an issue board */
export type BoardEpic = Noteable &
  CurrentUserTodos &
  Eventable & {
    __typename?: "BoardEpic"
    /** Author of the epic. */
    author: UserCore
    /** A list of award emojis associated with the epic. */
    awardEmoji?: Maybe<AwardEmojiConnection>
    /** Children (sub-epics) of the epic. */
    children?: Maybe<EpicConnection>
    /** Timestamp of when the epic was closed. */
    closedAt?: Maybe<Scalars["Time"]>
    /** Indicates if the epic is confidential. */
    confidential?: Maybe<Scalars["Boolean"]>
    /** Timestamp of when the epic was created. */
    createdAt?: Maybe<Scalars["Time"]>
    /** To-do items for the current user. */
    currentUserTodos: TodoConnection
    /** Number of open and closed descendant epics and issues. */
    descendantCounts?: Maybe<EpicDescendantCount>
    /** Total weight of open and closed issues in the epic and its descendants. */
    descendantWeightSum?: Maybe<EpicDescendantWeights>
    /** Description of the epic. */
    description?: Maybe<Scalars["String"]>
    /** The GitLab Flavored Markdown rendering of `description` */
    descriptionHtml?: Maybe<Scalars["String"]>
    /** All discussions on this noteable. */
    discussions: DiscussionConnection
    /** Number of downvotes the epic has received. */
    downvotes: Scalars["Int"]
    /** Due date of the epic. */
    dueDate?: Maybe<Scalars["Time"]>
    /** Fixed due date of the epic. */
    dueDateFixed?: Maybe<Scalars["Time"]>
    /** Inherited due date of the epic from milestones. */
    dueDateFromMilestones?: Maybe<Scalars["Time"]>
    /** Indicates if the due date has been manually set. */
    dueDateIsFixed?: Maybe<Scalars["Boolean"]>
    /** A list of events associated with the object. */
    events?: Maybe<EventConnection>
    /** Group to which the epic belongs. */
    group: Group
    /** Indicates if the epic has children. */
    hasChildren: Scalars["Boolean"]
    /** Indicates if the epic has direct issues. */
    hasIssues: Scalars["Boolean"]
    /** Indicates if the epic has a parent epic. */
    hasParent: Scalars["Boolean"]
    /** Current health status of the epic. */
    healthStatus?: Maybe<EpicHealthStatus>
    /** ID of the epic. */
    id: Scalars["ID"]
    /** Internal ID of the epic. */
    iid: Scalars["ID"]
    /** A list of issues associated with the epic. */
    issues?: Maybe<EpicIssueConnection>
    /** Labels assigned to the epic. */
    labels?: Maybe<LabelConnection>
    /** All notes on this noteable. */
    notes: NoteConnection
    /** Parent epic of the epic. */
    parent?: Maybe<Epic>
    /** List of participants for the epic. */
    participants?: Maybe<UserCoreConnection>
    /** Internal reference of the epic. Returned in shortened format by default. */
    reference: Scalars["String"]
    /** URI path of the epic-issue relationship. */
    relationPath?: Maybe<Scalars["String"]>
    /** The relative position of the epic in the epic tree. */
    relativePosition?: Maybe<Scalars["Int"]>
    /** Start date of the epic. */
    startDate?: Maybe<Scalars["Time"]>
    /** Fixed start date of the epic. */
    startDateFixed?: Maybe<Scalars["Time"]>
    /** Inherited start date of the epic from milestones. */
    startDateFromMilestones?: Maybe<Scalars["Time"]>
    /** Indicates if the start date has been manually set. */
    startDateIsFixed?: Maybe<Scalars["Boolean"]>
    /** State of the epic. */
    state: EpicState
    /** Indicates the currently logged in user is subscribed to the epic. */
    subscribed: Scalars["Boolean"]
    /** Title of the epic. */
    title?: Maybe<Scalars["String"]>
    /** The GitLab Flavored Markdown rendering of `title` */
    titleHtml?: Maybe<Scalars["String"]>
    /** Timestamp of when the epic was updated. */
    updatedAt?: Maybe<Scalars["Time"]>
    /** Number of upvotes the epic has received. */
    upvotes: Scalars["Int"]
    /** Number of user discussions in the epic. */
    userDiscussionsCount: Scalars["Int"]
    /** Number of user notes of the epic. */
    userNotesCount: Scalars["Int"]
    /** Permissions for the current user on the resource */
    userPermissions: EpicPermissions
    /** User preferences for the epic on the issue board. */
    userPreferences?: Maybe<BoardEpicUserPreferences>
    /** Web path of the epic. */
    webPath: Scalars["String"]
    /** Web URL of the epic. */
    webUrl: Scalars["String"]
  }

/** Represents an epic on an issue board */
export type BoardEpicAwardEmojiArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Represents an epic on an issue board */
export type BoardEpicChildrenArgs = {
  timeframe?: Maybe<Timeframe>
  iid?: Maybe<Scalars["ID"]>
  iids?: Maybe<Array<Scalars["ID"]>>
  state?: Maybe<EpicState>
  search?: Maybe<Scalars["String"]>
  sort?: Maybe<EpicSort>
  authorUsername?: Maybe<Scalars["String"]>
  labelName?: Maybe<Array<Scalars["String"]>>
  milestoneTitle?: Maybe<Scalars["String"]>
  iidStartsWith?: Maybe<Scalars["String"]>
  includeDescendantGroups?: Maybe<Scalars["Boolean"]>
  confidential?: Maybe<Scalars["Boolean"]>
  myReactionEmoji?: Maybe<Scalars["String"]>
  not?: Maybe<NegatedEpicFilterInput>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Represents an epic on an issue board */
export type BoardEpicCurrentUserTodosArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
  state?: Maybe<TodoStateEnum>
}

/** Represents an epic on an issue board */
export type BoardEpicDiscussionsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Represents an epic on an issue board */
export type BoardEpicEventsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Represents an epic on an issue board */
export type BoardEpicIssuesArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Represents an epic on an issue board */
export type BoardEpicLabelsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Represents an epic on an issue board */
export type BoardEpicNotesArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Represents an epic on an issue board */
export type BoardEpicParticipantsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Represents an epic on an issue board */
export type BoardEpicReferenceArgs = {
  full?: Maybe<Scalars["Boolean"]>
}

/** The connection type for BoardEpic. */
export type BoardEpicConnection = {
  __typename?: "BoardEpicConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<BoardEpicEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<BoardEpic>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type BoardEpicEdge = {
  __typename?: "BoardEpicEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<BoardEpic>
}

/** Represents user preferences for a board epic */
export type BoardEpicUserPreferences = {
  __typename?: "BoardEpicUserPreferences"
  /** Indicates epic should be displayed as collapsed. */
  collapsed: Scalars["Boolean"]
}

export type BoardIssueInput = {
  /** Filter by label name. */
  labelName?: Maybe<Array<Maybe<Scalars["String"]>>>
  /** Filter by author username. */
  authorUsername?: Maybe<Scalars["String"]>
  /** Filter by reaction emoji applied by the current user. */
  myReactionEmoji?: Maybe<Scalars["String"]>
  /** List of IIDs of issues. For example ["1", "2"]. */
  iids?: Maybe<Array<Scalars["String"]>>
  /** Filter by milestone title. */
  milestoneTitle?: Maybe<Scalars["String"]>
  /** Filter by assignee username. */
  assigneeUsername?: Maybe<Array<Maybe<Scalars["String"]>>>
  /** Filter by release tag. */
  releaseTag?: Maybe<Scalars["String"]>
  /** Filter by epic ID. Incompatible with epicWildcardId. */
  epicId?: Maybe<Scalars["EpicID"]>
  /** Filter by iteration title. */
  iterationTitle?: Maybe<Scalars["String"]>
  /** Filter by weight. */
  weight?: Maybe<Scalars["String"]>
  /** Filter by a list of iteration IDs. Incompatible with iterationWildcardId. */
  iterationId?: Maybe<Array<Scalars["IterationID"]>>
  /** List of negated arguments. */
  not?: Maybe<NegatedBoardIssueInput>
  /** Search query for issue title or description. */
  search?: Maybe<Scalars["String"]>
  /** Filter by assignee wildcard. Incompatible with assigneeUsername. */
  assigneeWildcardId?: Maybe<AssigneeWildcardId>
  /** Filter by epic ID wildcard. Incompatible with epicId. */
  epicWildcardId?: Maybe<EpicWildcardId>
  /** Filter by iteration ID wildcard. */
  iterationWildcardId?: Maybe<IterationWildcardId>
  /** Filter by weight ID wildcard. Incompatible with weight. */
  weightWildcardId?: Maybe<WeightWildcardId>
}

/** Represents a list for an issue board */
export type BoardList = {
  __typename?: "BoardList"
  /** Assignee in the list. */
  assignee?: Maybe<UserCore>
  /** Indicates if the list is collapsed for this user. */
  collapsed?: Maybe<Scalars["Boolean"]>
  /** ID (global ID) of the list. */
  id: Scalars["ID"]
  /** Board issues. */
  issues?: Maybe<IssueConnection>
  /** Count of issues in the list. */
  issuesCount?: Maybe<Scalars["Int"]>
  /** Iteration of the list. */
  iteration?: Maybe<Iteration>
  /** Label of the list. */
  label?: Maybe<Label>
  /** The current limit metric for the list. */
  limitMetric?: Maybe<ListLimitMetric>
  /** Type of the list. */
  listType: Scalars["String"]
  /** Maximum number of issues in the list. */
  maxIssueCount?: Maybe<Scalars["Int"]>
  /** Maximum weight of issues in the list. */
  maxIssueWeight?: Maybe<Scalars["Int"]>
  /** Milestone of the list. */
  milestone?: Maybe<Milestone>
  /** Position of list within the board. */
  position?: Maybe<Scalars["Int"]>
  /** Title of the list. */
  title: Scalars["String"]
  /** Total weight of all issues in the list. */
  totalWeight?: Maybe<Scalars["Int"]>
}

/** Represents a list for an issue board */
export type BoardListIssuesArgs = {
  filters?: Maybe<BoardIssueInput>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** The connection type for BoardList. */
export type BoardListConnection = {
  __typename?: "BoardListConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<BoardListEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<BoardList>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** Autogenerated input type of BoardListCreate */
export type BoardListCreateInput = {
  /** Create the backlog list. */
  backlog?: Maybe<Scalars["Boolean"]>
  /** Global ID of an existing label. */
  labelId?: Maybe<Scalars["LabelID"]>
  /** Global ID of the issue board to mutate. */
  boardId: Scalars["BoardID"]
  /** Global ID of an existing milestone. */
  milestoneId?: Maybe<Scalars["MilestoneID"]>
  /** Global ID of an existing iteration. */
  iterationId?: Maybe<Scalars["IterationID"]>
  /** Global ID of an existing user. */
  assigneeId?: Maybe<Scalars["UserID"]>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of BoardListCreate */
export type BoardListCreatePayload = {
  __typename?: "BoardListCreatePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** Issue list in the issue board. */
  list?: Maybe<BoardList>
}

/** An edge in a connection. */
export type BoardListEdge = {
  __typename?: "BoardListEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<BoardList>
}

/** Autogenerated input type of BoardListUpdateLimitMetrics */
export type BoardListUpdateLimitMetricsInput = {
  /** The global ID of the list. */
  listId: Scalars["ListID"]
  /** The new limit metric type for the list. */
  limitMetric?: Maybe<ListLimitMetric>
  /** The new maximum issue count limit. */
  maxIssueCount?: Maybe<Scalars["Int"]>
  /** The new maximum issue weight limit. */
  maxIssueWeight?: Maybe<Scalars["Int"]>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of BoardListUpdateLimitMetrics */
export type BoardListUpdateLimitMetricsPayload = {
  __typename?: "BoardListUpdateLimitMetricsPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The updated list. */
  list?: Maybe<BoardList>
}

export type Branch = {
  __typename?: "Branch"
  /** Commit for the branch. */
  commit?: Maybe<Commit>
  /** Name of the branch. */
  name: Scalars["String"]
}

/** Autogenerated input type of BulkEnableDevopsAdoptionNamespaces */
export type BulkEnableDevopsAdoptionNamespacesInput = {
  /** List of Namespace IDs. */
  namespaceIds: Array<Scalars["NamespaceID"]>
  /** Display namespace ID. */
  displayNamespaceId?: Maybe<Scalars["NamespaceID"]>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of BulkEnableDevopsAdoptionNamespaces */
export type BulkEnableDevopsAdoptionNamespacesPayload = {
  __typename?: "BulkEnableDevopsAdoptionNamespacesPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Enabled namespaces after mutation. */
  enabledNamespaces?: Maybe<Array<DevopsAdoptionSegment>>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

/** Represents the total number of issues and their weights for a particular day */
export type BurnupChartDailyTotals = {
  __typename?: "BurnupChartDailyTotals"
  /** Number of closed issues as of this day. */
  completedCount: Scalars["Int"]
  /** Total weight of closed issues as of this day. */
  completedWeight: Scalars["Int"]
  /** Date for burnup totals. */
  date: Scalars["ISO8601Date"]
  /** Number of issues as of this day. */
  scopeCount: Scalars["Int"]
  /** Total weight of issues as of this day. */
  scopeWeight: Scalars["Int"]
}

export type CiApplicationSettings = {
  __typename?: "CiApplicationSettings"
  /** Whether to keep the latest jobs artifacts. */
  keepLatestArtifact?: Maybe<Scalars["Boolean"]>
}

export type CiBuildNeed = {
  __typename?: "CiBuildNeed"
  /** Name of the job we need to complete. */
  name?: Maybe<Scalars["String"]>
}

/** The connection type for CiBuildNeed. */
export type CiBuildNeedConnection = {
  __typename?: "CiBuildNeedConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<CiBuildNeedEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<CiBuildNeed>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type CiBuildNeedEdge = {
  __typename?: "CiBuildNeedEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<CiBuildNeed>
}

/** Autogenerated input type of CiCdSettingsUpdate */
export type CiCdSettingsUpdateInput = {
  /** Full Path of the project the settings belong to. */
  fullPath: Scalars["ID"]
  /** Indicates if the latest artifact should be kept for this project. */
  keepLatestArtifact?: Maybe<Scalars["Boolean"]>
  /** Indicates if merge pipelines are enabled for the project. */
  mergePipelinesEnabled?: Maybe<Scalars["Boolean"]>
  /** Indicates if merge trains are enabled for the project. */
  mergeTrainsEnabled?: Maybe<Scalars["Boolean"]>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of CiCdSettingsUpdate */
export type CiCdSettingsUpdatePayload = {
  __typename?: "CiCdSettingsUpdatePayload"
  /** The CI/CD settings after mutation. */
  ciCdSettings: ProjectCiCdSetting
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

export type CiConfig = {
  __typename?: "CiConfig"
  /** Linting errors. */
  errors?: Maybe<Array<Scalars["String"]>>
  /** Merged CI configuration YAML. */
  mergedYaml?: Maybe<Scalars["String"]>
  /** Stages of the pipeline. */
  stages?: Maybe<CiConfigStageConnection>
  /** Status of linting, can be either valid or invalid. */
  status?: Maybe<CiConfigStatus>
}

export type CiConfigStagesArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type CiConfigGroup = {
  __typename?: "CiConfigGroup"
  /** Jobs in group. */
  jobs?: Maybe<CiConfigJobConnection>
  /** Name of the job group. */
  name?: Maybe<Scalars["String"]>
  /** Size of the job group. */
  size?: Maybe<Scalars["Int"]>
}

export type CiConfigGroupJobsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** The connection type for CiConfigGroup. */
export type CiConfigGroupConnection = {
  __typename?: "CiConfigGroupConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<CiConfigGroupEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<CiConfigGroup>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type CiConfigGroupEdge = {
  __typename?: "CiConfigGroupEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<CiConfigGroup>
}

export type CiConfigJob = {
  __typename?: "CiConfigJob"
  /** Override a set of commands that are executed after the job. */
  afterScript?: Maybe<Array<Scalars["String"]>>
  /** Allow job to fail. */
  allowFailure?: Maybe<Scalars["Boolean"]>
  /** Override a set of commands that are executed before the job. */
  beforeScript?: Maybe<Array<Scalars["String"]>>
  /** Name of an environment to which the job deploys. */
  environment?: Maybe<Scalars["String"]>
  /** Limit when jobs are not created. */
  except?: Maybe<CiConfigJobRestriction>
  /** Name of the job group. */
  groupName?: Maybe<Scalars["String"]>
  /** Name of the job. */
  name?: Maybe<Scalars["String"]>
  /** Builds that must complete before the jobs run. */
  needs?: Maybe<CiConfigNeedConnection>
  /** Jobs are created when these conditions do not apply. */
  only?: Maybe<CiConfigJobRestriction>
  /** Shell script that is executed by a runner. */
  script?: Maybe<Array<Scalars["String"]>>
  /** Name of the job stage. */
  stage?: Maybe<Scalars["String"]>
  /** List of tags that are used to select a runner. */
  tags?: Maybe<Array<Scalars["String"]>>
  /** When to run the job. */
  when?: Maybe<Scalars["String"]>
}

export type CiConfigJobNeedsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** The connection type for CiConfigJob. */
export type CiConfigJobConnection = {
  __typename?: "CiConfigJobConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<CiConfigJobEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<CiConfigJob>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type CiConfigJobEdge = {
  __typename?: "CiConfigJobEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<CiConfigJob>
}

export type CiConfigJobRestriction = {
  __typename?: "CiConfigJobRestriction"
  /** The Git refs the job restriction applies to. */
  refs?: Maybe<Array<Scalars["String"]>>
}

export type CiConfigNeed = {
  __typename?: "CiConfigNeed"
  /** Name of the need. */
  name?: Maybe<Scalars["String"]>
}

/** The connection type for CiConfigNeed. */
export type CiConfigNeedConnection = {
  __typename?: "CiConfigNeedConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<CiConfigNeedEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<CiConfigNeed>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type CiConfigNeedEdge = {
  __typename?: "CiConfigNeedEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<CiConfigNeed>
}

export type CiConfigStage = {
  __typename?: "CiConfigStage"
  /** Groups of jobs for the stage. */
  groups?: Maybe<CiConfigGroupConnection>
  /** Name of the stage. */
  name?: Maybe<Scalars["String"]>
}

export type CiConfigStageGroupsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** The connection type for CiConfigStage. */
export type CiConfigStageConnection = {
  __typename?: "CiConfigStageConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<CiConfigStageEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<CiConfigStage>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type CiConfigStageEdge = {
  __typename?: "CiConfigStageEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<CiConfigStage>
}

/** Values for YAML processor result */
export enum CiConfigStatus {
  /** The configuration file is valid. */
  Valid = "VALID",
  /** The configuration file is not valid. */
  Invalid = "INVALID",
}

export type CiGroup = {
  __typename?: "CiGroup"
  /** Detailed status of the group. */
  detailedStatus?: Maybe<DetailedStatus>
  /** Jobs in group. */
  jobs?: Maybe<CiJobConnection>
  /** Name of the job group. */
  name?: Maybe<Scalars["String"]>
  /** Size of the group. */
  size?: Maybe<Scalars["Int"]>
}

export type CiGroupJobsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** The connection type for CiGroup. */
export type CiGroupConnection = {
  __typename?: "CiGroupConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<CiGroupEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<CiGroup>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type CiGroupEdge = {
  __typename?: "CiGroupEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<CiGroup>
}

export type CiJob = {
  __typename?: "CiJob"
  /** Indicates the job is active. */
  active: Scalars["Boolean"]
  /** Whether the job is allowed to fail. */
  allowFailure: Scalars["Boolean"]
  /** Artifacts generated by the job. */
  artifacts?: Maybe<CiJobArtifactConnection>
  /** Indicates the job can be canceled. */
  cancelable: Scalars["Boolean"]
  /** Path to the commit that triggered the job. */
  commitPath?: Maybe<Scalars["String"]>
  /** Coverage level of the job. */
  coverage?: Maybe<Scalars["Float"]>
  /** When the job was created. */
  createdAt: Scalars["Time"]
  /** Whether the job was created by a tag. */
  createdByTag: Scalars["Boolean"]
  /** Detailed status of the job. */
  detailedStatus?: Maybe<DetailedStatus>
  /** Duration of the job in seconds. */
  duration?: Maybe<Scalars["Int"]>
  /** When a job has finished running. */
  finishedAt?: Maybe<Scalars["Time"]>
  /** ID of the job. */
  id?: Maybe<Scalars["JobID"]>
  /** Whether the job has a manual action. */
  manualJob?: Maybe<Scalars["Boolean"]>
  /** Name of the job. */
  name?: Maybe<Scalars["String"]>
  /** References to builds that must complete before the jobs run. */
  needs?: Maybe<CiBuildNeedConnection>
  /** Pipeline the job belongs to. */
  pipeline?: Maybe<Pipeline>
  /** Indicates the job can be played. */
  playable: Scalars["Boolean"]
  /** When the job was enqueued and marked as pending. */
  queuedAt?: Maybe<Scalars["Time"]>
  /** How long the job was enqueued before starting. */
  queuedDuration?: Maybe<Scalars["Duration"]>
  /** Ref name of the job. */
  refName?: Maybe<Scalars["String"]>
  /** Path to the ref. */
  refPath?: Maybe<Scalars["String"]>
  /** Indicates the job can be retried. */
  retryable: Scalars["Boolean"]
  /** Schedule for the build. */
  scheduledAt?: Maybe<Scalars["Time"]>
  /** Type of pipeline scheduling. Value is `dag` if the pipeline uses the `needs` keyword, and `stage` otherwise. */
  schedulingType?: Maybe<Scalars["String"]>
  /** Short SHA1 ID of the commit. */
  shortSha: Scalars["String"]
  /** Stage of the job. */
  stage?: Maybe<CiStage>
  /** When the job was started. */
  startedAt?: Maybe<Scalars["Time"]>
  /** Status of the job. */
  status?: Maybe<CiJobStatus>
  /** Indicates the job is stuck. */
  stuck: Scalars["Boolean"]
  /** Tags for the current job. */
  tags?: Maybe<Array<Scalars["String"]>>
  /** Whether the job was triggered. */
  triggered?: Maybe<Scalars["Boolean"]>
  /** Permissions for the current user on the resource */
  userPermissions: JobPermissions
}

export type CiJobArtifactsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type CiJobNeedsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type CiJobArtifact = {
  __typename?: "CiJobArtifact"
  /** URL for downloading the artifact's file. */
  downloadPath?: Maybe<Scalars["String"]>
  /** File type of the artifact. */
  fileType?: Maybe<JobArtifactFileType>
}

/** The connection type for CiJobArtifact. */
export type CiJobArtifactConnection = {
  __typename?: "CiJobArtifactConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<CiJobArtifactEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<CiJobArtifact>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type CiJobArtifactEdge = {
  __typename?: "CiJobArtifactEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<CiJobArtifact>
}

/** The connection type for CiJob. */
export type CiJobConnection = {
  __typename?: "CiJobConnection"
  /** Total count of collection. */
  count: Scalars["Int"]
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<CiJobEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<CiJob>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type CiJobEdge = {
  __typename?: "CiJobEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<CiJob>
}

export enum CiJobStatus {
  /** A job that is created. */
  Created = "CREATED",
  /** A job that is waiting for resource. */
  WaitingForResource = "WAITING_FOR_RESOURCE",
  /** A job that is preparing. */
  Preparing = "PREPARING",
  /** A job that is pending. */
  Pending = "PENDING",
  /** A job that is running. */
  Running = "RUNNING",
  /** A job that is success. */
  Success = "SUCCESS",
  /** A job that is failed. */
  Failed = "FAILED",
  /** A job that is canceled. */
  Canceled = "CANCELED",
  /** A job that is skipped. */
  Skipped = "SKIPPED",
  /** A job that is manual. */
  Manual = "MANUAL",
  /** A job that is scheduled. */
  Scheduled = "SCHEDULED",
}

export type CiStage = {
  __typename?: "CiStage"
  /** Detailed status of the stage. */
  detailedStatus?: Maybe<DetailedStatus>
  /** Group of jobs for the stage. */
  groups?: Maybe<CiGroupConnection>
  /** Jobs for the stage. */
  jobs?: Maybe<CiJobConnection>
  /** Name of the stage. */
  name?: Maybe<Scalars["String"]>
}

export type CiStageGroupsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type CiStageJobsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** The connection type for CiStage. */
export type CiStageConnection = {
  __typename?: "CiStageConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<CiStageEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<CiStage>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type CiStageEdge = {
  __typename?: "CiStageEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<CiStage>
}

/** GitLab CI/CD configuration template. */
export type CiTemplate = {
  __typename?: "CiTemplate"
  /** Contents of the CI template. */
  content: Scalars["String"]
  /** Name of the CI template. */
  name: Scalars["String"]
}

export type ClusterAgent = {
  __typename?: "ClusterAgent"
  /** Timestamp the cluster agent was created. */
  createdAt?: Maybe<Scalars["Time"]>
  /** User object, containing information about the person who created the agent. */
  createdByUser?: Maybe<UserCore>
  /** ID of the cluster agent. */
  id: Scalars["ID"]
  /** Name of the cluster agent. */
  name?: Maybe<Scalars["String"]>
  /** The project this cluster agent is associated with. */
  project?: Maybe<Project>
  /** Tokens associated with the cluster agent. */
  tokens?: Maybe<ClusterAgentTokenConnection>
  /** Timestamp the cluster agent was updated. */
  updatedAt?: Maybe<Scalars["Time"]>
  /** Web path of the cluster agent. */
  webPath?: Maybe<Scalars["String"]>
}

export type ClusterAgentTokensArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** The connection type for ClusterAgent. */
export type ClusterAgentConnection = {
  __typename?: "ClusterAgentConnection"
  /** Total count of collection. */
  count: Scalars["Int"]
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<ClusterAgentEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<ClusterAgent>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** Autogenerated input type of ClusterAgentDelete */
export type ClusterAgentDeleteInput = {
  /** Global ID of the cluster agent that will be deleted. */
  id: Scalars["ClustersAgentID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of ClusterAgentDelete */
export type ClusterAgentDeletePayload = {
  __typename?: "ClusterAgentDeletePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

/** An edge in a connection. */
export type ClusterAgentEdge = {
  __typename?: "ClusterAgentEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<ClusterAgent>
}

export type ClusterAgentToken = {
  __typename?: "ClusterAgentToken"
  /** Cluster agent this token is associated with. */
  clusterAgent?: Maybe<ClusterAgent>
  /** Timestamp the token was created. */
  createdAt?: Maybe<Scalars["Time"]>
  /** The user who created the token. */
  createdByUser?: Maybe<UserCore>
  /** Description of the token. */
  description?: Maybe<Scalars["String"]>
  /** Global ID of the token. */
  id: Scalars["ClustersAgentTokenID"]
  /** Timestamp the token was last used. */
  lastUsedAt?: Maybe<Scalars["Time"]>
  /** Name given to the token. */
  name?: Maybe<Scalars["String"]>
}

/** The connection type for ClusterAgentToken. */
export type ClusterAgentTokenConnection = {
  __typename?: "ClusterAgentTokenConnection"
  /** Total count of collection. */
  count: Scalars["Int"]
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<ClusterAgentTokenEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<ClusterAgentToken>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** Autogenerated input type of ClusterAgentTokenCreate */
export type ClusterAgentTokenCreateInput = {
  /** Global ID of the cluster agent that will be associated with the new token. */
  clusterAgentId: Scalars["ClustersAgentID"]
  /** Description of the token. */
  description?: Maybe<Scalars["String"]>
  /** Name of the token. */
  name: Scalars["String"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of ClusterAgentTokenCreate */
export type ClusterAgentTokenCreatePayload = {
  __typename?: "ClusterAgentTokenCreatePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** Token secret value. Make sure you save it - you won't be able to access it again. */
  secret?: Maybe<Scalars["String"]>
  /** Token created after mutation. */
  token?: Maybe<ClusterAgentToken>
}

/** Autogenerated input type of ClusterAgentTokenDelete */
export type ClusterAgentTokenDeleteInput = {
  /** Global ID of the cluster agent token that will be deleted. */
  id: Scalars["ClustersAgentTokenID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of ClusterAgentTokenDelete */
export type ClusterAgentTokenDeletePayload = {
  __typename?: "ClusterAgentTokenDeletePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

/** An edge in a connection. */
export type ClusterAgentTokenEdge = {
  __typename?: "ClusterAgentTokenEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<ClusterAgentToken>
}

/** Represents the code coverage activity for a group */
export type CodeCoverageActivity = {
  __typename?: "CodeCoverageActivity"
  /** Average percentage of the different code coverage results available for the group. */
  averageCoverage?: Maybe<Scalars["Float"]>
  /** Number of different code coverage results available for the group. */
  coverageCount?: Maybe<Scalars["Int"]>
  /** Date when the code coverage was created. */
  date: Scalars["Date"]
  /** Number of projects with code coverage results for the group. */
  projectCount?: Maybe<Scalars["Int"]>
}

/** The connection type for CodeCoverageActivity. */
export type CodeCoverageActivityConnection = {
  __typename?: "CodeCoverageActivityConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<CodeCoverageActivityEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<CodeCoverageActivity>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type CodeCoverageActivityEdge = {
  __typename?: "CodeCoverageActivityEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<CodeCoverageActivity>
}

/** Represents the code coverage summary for a project */
export type CodeCoverageSummary = {
  __typename?: "CodeCoverageSummary"
  /** Average percentage of the different code coverage results available for the project. */
  averageCoverage?: Maybe<Scalars["Float"]>
  /** Number of different code coverage results available. */
  coverageCount?: Maybe<Scalars["Int"]>
  /** Latest date when the code coverage was created for the project. */
  lastUpdatedOn?: Maybe<Scalars["Date"]>
}

/** Represents a code quality degradation on the pipeline. */
export type CodeQualityDegradation = {
  __typename?: "CodeQualityDegradation"
  /** A description of the code quality degradation. */
  description: Scalars["String"]
  /** A unique fingerprint to identify the code quality degradation. For example, an MD5 hash. */
  fingerprint: Scalars["String"]
  /** The line on which the code quality degradation occurred. */
  line: Scalars["Int"]
  /** The relative path to the file containing the code quality degradation. */
  path: Scalars["String"]
  /** Status of the degradation (BLOCKER, CRITICAL, MAJOR, MINOR, INFO). */
  severity: CodeQualityDegradationSeverity
}

/** The connection type for CodeQualityDegradation. */
export type CodeQualityDegradationConnection = {
  __typename?: "CodeQualityDegradationConnection"
  /** Total count of collection. */
  count: Scalars["Int"]
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<CodeQualityDegradationEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<CodeQualityDegradation>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type CodeQualityDegradationEdge = {
  __typename?: "CodeQualityDegradationEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<CodeQualityDegradation>
}

export enum CodeQualityDegradationSeverity {
  /** Code Quality degradation has a status of blocker. */
  Blocker = "BLOCKER",
  /** Code Quality degradation has a status of critical. */
  Critical = "CRITICAL",
  /** Code Quality degradation has a status of major. */
  Major = "MAJOR",
  /** Code Quality degradation has a status of minor. */
  Minor = "MINOR",
  /** Code Quality degradation has a status of info. */
  Info = "INFO",
}

export type Commit = {
  __typename?: "Commit"
  /** Author of the commit. */
  author?: Maybe<UserCore>
  /** Commit authors gravatar. */
  authorGravatar?: Maybe<Scalars["String"]>
  /** Commit authors name. */
  authorName?: Maybe<Scalars["String"]>
  /** Timestamp of when the commit was authored. */
  authoredDate?: Maybe<Scalars["Time"]>
  /** Description of the commit message. */
  description?: Maybe<Scalars["String"]>
  /** The GitLab Flavored Markdown rendering of `description` */
  descriptionHtml?: Maybe<Scalars["String"]>
  /** ID (global ID) of the commit. */
  id: Scalars["ID"]
  /** Raw commit message. */
  message?: Maybe<Scalars["String"]>
  /** Pipelines of the commit ordered latest first. */
  pipelines?: Maybe<PipelineConnection>
  /** SHA1 ID of the commit. */
  sha: Scalars["String"]
  /** Short SHA1 ID of the commit. */
  shortId: Scalars["String"]
  /** Rendered HTML of the commit signature. */
  signatureHtml?: Maybe<Scalars["String"]>
  /** Title of the commit message. */
  title?: Maybe<Scalars["String"]>
  /** The GitLab Flavored Markdown rendering of `title` */
  titleHtml?: Maybe<Scalars["String"]>
  /** Web path of the commit. */
  webPath: Scalars["String"]
  /** Web URL of the commit. */
  webUrl: Scalars["String"]
}

export type CommitPipelinesArgs = {
  status?: Maybe<PipelineStatusEnum>
  ref?: Maybe<Scalars["String"]>
  sha?: Maybe<Scalars["String"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type CommitAction = {
  /** The action to perform, create, delete, move, update, chmod. */
  action: CommitActionMode
  /** Full path to the file. */
  filePath: Scalars["String"]
  /** Content of the file. */
  content?: Maybe<Scalars["String"]>
  /** Original full path to the file being moved. */
  previousPath?: Maybe<Scalars["String"]>
  /** Last known file commit ID. */
  lastCommitId?: Maybe<Scalars["String"]>
  /** Enables/disables the execute flag on the file. */
  executeFilemode?: Maybe<Scalars["Boolean"]>
  /** Encoding of the file. Default is text. */
  encoding?: Maybe<CommitEncoding>
}

/** Mode of a commit action */
export enum CommitActionMode {
  /** Create command. */
  Create = "CREATE",
  /** Delete command. */
  Delete = "DELETE",
  /** Move command. */
  Move = "MOVE",
  /** Update command. */
  Update = "UPDATE",
  /** Chmod command. */
  Chmod = "CHMOD",
}

/** The connection type for Commit. */
export type CommitConnection = {
  __typename?: "CommitConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<CommitEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<Commit>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** Autogenerated input type of CommitCreate */
export type CommitCreateInput = {
  /** Project full path the branch is associated with. */
  projectPath: Scalars["ID"]
  /** Name of the branch to commit into, it can be a new branch. */
  branch: Scalars["String"]
  /** If on a new branch, name of the original branch. */
  startBranch?: Maybe<Scalars["String"]>
  /** Raw commit message. */
  message: Scalars["String"]
  /** Array of action hashes to commit as a batch. */
  actions: Array<CommitAction>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of CommitCreate */
export type CommitCreatePayload = {
  __typename?: "CommitCreatePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** The commit after mutation. */
  commit?: Maybe<Commit>
  /** ETag path for the commit's pipeline. */
  commitPipelinePath?: Maybe<Scalars["String"]>
  /** Contents of the commit. */
  content?: Maybe<Array<Scalars["String"]>>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

/** An edge in a connection. */
export type CommitEdge = {
  __typename?: "CommitEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<Commit>
}

export enum CommitEncoding {
  /** Text encoding. */
  Text = "TEXT",
  /** Base64 encoding. */
  Base64 = "BASE64",
}

/** Represents a ComplianceFramework associated with a Project */
export type ComplianceFramework = {
  __typename?: "ComplianceFramework"
  /** Hexadecimal representation of compliance framework's label color. */
  color: Scalars["String"]
  /** Description of the compliance framework. */
  description: Scalars["String"]
  /** Compliance framework ID. */
  id: Scalars["ID"]
  /** Name of the compliance framework. */
  name: Scalars["String"]
  /** Full path of the compliance pipeline configuration stored in a project repository, such as `.gitlab/.compliance-gitlab-ci.yml@compliance/hipaa` **(ULTIMATE)**. */
  pipelineConfigurationFullPath?: Maybe<Scalars["String"]>
}

/** The connection type for ComplianceFramework. */
export type ComplianceFrameworkConnection = {
  __typename?: "ComplianceFrameworkConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<ComplianceFrameworkEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<ComplianceFramework>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type ComplianceFrameworkEdge = {
  __typename?: "ComplianceFrameworkEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<ComplianceFramework>
}

export type ComplianceFrameworkInput = {
  /** New name for the compliance framework. */
  name?: Maybe<Scalars["String"]>
  /** New description for the compliance framework. */
  description?: Maybe<Scalars["String"]>
  /** New color representation of the compliance framework in hex format. e.g. #FCA121. */
  color?: Maybe<Scalars["String"]>
  /** Full path of the compliance pipeline configuration stored in a project repository, such as `.gitlab/.compliance-gitlab-ci.yml@compliance/hipaa` **(ULTIMATE)**. */
  pipelineConfigurationFullPath?: Maybe<Scalars["String"]>
}

/** Composer metadata */
export type ComposerMetadata = {
  __typename?: "ComposerMetadata"
  /** Data of the Composer JSON file. */
  composerJson: PackageComposerJsonType
  /** Target SHA of the package. */
  targetSha: Scalars["String"]
}

/** Conan file metadata */
export type ConanFileMetadata = PackageFileMetadata & {
  __typename?: "ConanFileMetadata"
  /** Type of the Conan file. */
  conanFileType: ConanMetadatumFileTypeEnum
  /** Reference of the Conan package. */
  conanPackageReference?: Maybe<Scalars["String"]>
  /** Date of creation. */
  createdAt: Scalars["Time"]
  /** ID of the metadatum. */
  id: Scalars["PackagesConanFileMetadatumID"]
  /** Revision of the package. */
  packageRevision?: Maybe<Scalars["String"]>
  /** Revision of the Conan recipe. */
  recipeRevision: Scalars["String"]
  /** Date of most recent update. */
  updatedAt: Scalars["Time"]
}

/** Conan metadata */
export type ConanMetadata = {
  __typename?: "ConanMetadata"
  /** Date of creation. */
  createdAt: Scalars["Time"]
  /** ID of the metadatum. */
  id: Scalars["PackagesConanMetadatumID"]
  /** Channel of the Conan package. */
  packageChannel: Scalars["String"]
  /** Username of the Conan package. */
  packageUsername: Scalars["String"]
  /** Recipe of the Conan package. */
  recipe: Scalars["String"]
  /** Recipe path of the Conan package. */
  recipePath: Scalars["String"]
  /** Date of most recent update. */
  updatedAt: Scalars["Time"]
}

/** Conan file types */
export enum ConanMetadatumFileTypeEnum {
  /** A recipe file type. */
  RecipeFile = "RECIPE_FILE",
  /** A package file type. */
  PackageFile = "PACKAGE_FILE",
}

/** Autogenerated input type of ConfigureSast */
export type ConfigureSastInput = {
  /** Full path of the project. */
  projectPath: Scalars["ID"]
  /** SAST CI configuration for the project. */
  configuration: SastCiConfigurationInput
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of ConfigureSast */
export type ConfigureSastPayload = {
  __typename?: "ConfigureSastPayload"
  /** Branch that has the new/modified `.gitlab-ci.yml` file. */
  branch?: Maybe<Scalars["String"]>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** Redirect path to use when the response is successful. */
  successPath?: Maybe<Scalars["String"]>
}

/** Autogenerated input type of ConfigureSecretDetection */
export type ConfigureSecretDetectionInput = {
  /** Full path of the project. */
  projectPath: Scalars["ID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of ConfigureSecretDetection */
export type ConfigureSecretDetectionPayload = {
  __typename?: "ConfigureSecretDetectionPayload"
  /** Branch that has the new/modified `.gitlab-ci.yml` file. */
  branch?: Maybe<Scalars["String"]>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** Redirect path to use when the response is successful. */
  successPath?: Maybe<Scalars["String"]>
}

/** A tag expiration policy designed to keep only the images that matter most */
export type ContainerExpirationPolicy = {
  __typename?: "ContainerExpirationPolicy"
  /** This container expiration policy schedule. */
  cadence: ContainerExpirationPolicyCadenceEnum
  /** Timestamp of when the container expiration policy was created. */
  createdAt: Scalars["Time"]
  /** Indicates whether this container expiration policy is enabled. */
  enabled: Scalars["Boolean"]
  /** Number of tags to retain. */
  keepN?: Maybe<ContainerExpirationPolicyKeepEnum>
  /** Tags with names matching this regex pattern will expire. */
  nameRegex?: Maybe<Scalars["UntrustedRegexp"]>
  /** Tags with names matching this regex pattern will be preserved. */
  nameRegexKeep?: Maybe<Scalars["UntrustedRegexp"]>
  /** Next time that this container expiration policy will get executed. */
  nextRunAt?: Maybe<Scalars["Time"]>
  /** Tags older that this will expire. */
  olderThan?: Maybe<ContainerExpirationPolicyOlderThanEnum>
  /** Timestamp of when the container expiration policy was updated. */
  updatedAt: Scalars["Time"]
}

export enum ContainerExpirationPolicyCadenceEnum {
  /** Every day */
  EveryDay = "EVERY_DAY",
  /** Every week */
  EveryWeek = "EVERY_WEEK",
  /** Every two weeks */
  EveryTwoWeeks = "EVERY_TWO_WEEKS",
  /** Every month */
  EveryMonth = "EVERY_MONTH",
  /** Every three months */
  EveryThreeMonths = "EVERY_THREE_MONTHS",
}

export enum ContainerExpirationPolicyKeepEnum {
  /** 1 tag per image name */
  OneTag = "ONE_TAG",
  /** 5 tags per image name */
  FiveTags = "FIVE_TAGS",
  /** 10 tags per image name */
  TenTags = "TEN_TAGS",
  /** 25 tags per image name */
  TwentyFiveTags = "TWENTY_FIVE_TAGS",
  /** 50 tags per image name */
  FiftyTags = "FIFTY_TAGS",
  /** 100 tags per image name */
  OneHundredTags = "ONE_HUNDRED_TAGS",
}

export enum ContainerExpirationPolicyOlderThanEnum {
  /** 7 days until tags are automatically removed */
  SevenDays = "SEVEN_DAYS",
  /** 14 days until tags are automatically removed */
  FourteenDays = "FOURTEEN_DAYS",
  /** 30 days until tags are automatically removed */
  ThirtyDays = "THIRTY_DAYS",
  /** 90 days until tags are automatically removed */
  NinetyDays = "NINETY_DAYS",
}

/** A container repository */
export type ContainerRepository = {
  __typename?: "ContainerRepository"
  /** Can the current user delete the container repository. */
  canDelete: Scalars["Boolean"]
  /** Timestamp when the container repository was created. */
  createdAt: Scalars["Time"]
  /** The tags cleanup status for the container repository. */
  expirationPolicyCleanupStatus?: Maybe<ContainerRepositoryCleanupStatus>
  /** Timestamp when the cleanup done by the expiration policy was started on the container repository. */
  expirationPolicyStartedAt?: Maybe<Scalars["Time"]>
  /** ID of the container repository. */
  id: Scalars["ID"]
  /** URL of the container repository. */
  location: Scalars["String"]
  /** Name of the container repository. */
  name: Scalars["String"]
  /** Path of the container repository. */
  path: Scalars["String"]
  /** Project of the container registry. */
  project: Project
  /** Status of the container repository. */
  status?: Maybe<ContainerRepositoryStatus>
  /** Number of tags associated with this image. */
  tagsCount: Scalars["Int"]
  /** Timestamp when the container repository was updated. */
  updatedAt: Scalars["Time"]
}

/** Status of the tags cleanup of a container repository */
export enum ContainerRepositoryCleanupStatus {
  /** The tags cleanup is not scheduled. This is the default state. */
  Unscheduled = "UNSCHEDULED",
  /** The tags cleanup is scheduled and is going to be executed shortly. */
  Scheduled = "SCHEDULED",
  /** The tags cleanup has been partially executed. There are still remaining tags to delete. */
  Unfinished = "UNFINISHED",
  /** The tags cleanup is ongoing. */
  Ongoing = "ONGOING",
}

/** The connection type for ContainerRepository. */
export type ContainerRepositoryConnection = {
  __typename?: "ContainerRepositoryConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<ContainerRepositoryEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<ContainerRepository>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** Details of a container repository */
export type ContainerRepositoryDetails = {
  __typename?: "ContainerRepositoryDetails"
  /** Can the current user delete the container repository. */
  canDelete: Scalars["Boolean"]
  /** Timestamp when the container repository was created. */
  createdAt: Scalars["Time"]
  /** The tags cleanup status for the container repository. */
  expirationPolicyCleanupStatus?: Maybe<ContainerRepositoryCleanupStatus>
  /** Timestamp when the cleanup done by the expiration policy was started on the container repository. */
  expirationPolicyStartedAt?: Maybe<Scalars["Time"]>
  /** ID of the container repository. */
  id: Scalars["ID"]
  /** URL of the container repository. */
  location: Scalars["String"]
  /** Name of the container repository. */
  name: Scalars["String"]
  /** Path of the container repository. */
  path: Scalars["String"]
  /** Project of the container registry. */
  project: Project
  /** Status of the container repository. */
  status?: Maybe<ContainerRepositoryStatus>
  /** Tags of the container repository. */
  tags?: Maybe<ContainerRepositoryTagConnection>
  /** Number of tags associated with this image. */
  tagsCount: Scalars["Int"]
  /** Timestamp when the container repository was updated. */
  updatedAt: Scalars["Time"]
}

/** Details of a container repository */
export type ContainerRepositoryDetailsTagsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** An edge in a connection. */
export type ContainerRepositoryEdge = {
  __typename?: "ContainerRepositoryEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<ContainerRepository>
}

/** Values for sorting container repositories */
export enum ContainerRepositorySort {
  /** Updated at descending order. */
  UpdatedDesc = "UPDATED_DESC",
  /** Updated at ascending order. */
  UpdatedAsc = "UPDATED_ASC",
  /** Created at descending order. */
  CreatedDesc = "CREATED_DESC",
  /** Created at ascending order. */
  CreatedAsc = "CREATED_ASC",
  /** Name by ascending order. */
  NameAsc = "NAME_ASC",
  /** Name by descending order. */
  NameDesc = "NAME_DESC",
}

/** Status of a container repository */
export enum ContainerRepositoryStatus {
  /** Delete Scheduled status. */
  DeleteScheduled = "DELETE_SCHEDULED",
  /** Delete Failed status. */
  DeleteFailed = "DELETE_FAILED",
}

/** A tag from a container repository */
export type ContainerRepositoryTag = {
  __typename?: "ContainerRepositoryTag"
  /** Can the current user delete this tag. */
  canDelete: Scalars["Boolean"]
  /** Timestamp when the tag was created. */
  createdAt?: Maybe<Scalars["Time"]>
  /** Digest of the tag. */
  digest?: Maybe<Scalars["String"]>
  /** URL of the tag. */
  location: Scalars["String"]
  /** Name of the tag. */
  name: Scalars["String"]
  /** Path of the tag. */
  path: Scalars["String"]
  /** Revision of the tag. */
  revision?: Maybe<Scalars["String"]>
  /** Short revision of the tag. */
  shortRevision?: Maybe<Scalars["String"]>
  /** The size of the tag. */
  totalSize?: Maybe<Scalars["BigInt"]>
}

/** The connection type for ContainerRepositoryTag. */
export type ContainerRepositoryTagConnection = {
  __typename?: "ContainerRepositoryTagConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<ContainerRepositoryTagEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<ContainerRepositoryTag>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type ContainerRepositoryTagEdge = {
  __typename?: "ContainerRepositoryTagEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<ContainerRepositoryTag>
}

/** Autogenerated input type of CreateAlertIssue */
export type CreateAlertIssueInput = {
  /** The project the alert to mutate is in. */
  projectPath: Scalars["ID"]
  /** The IID of the alert to mutate. */
  iid: Scalars["String"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of CreateAlertIssue */
export type CreateAlertIssuePayload = {
  __typename?: "CreateAlertIssuePayload"
  /** The alert after mutation. */
  alert?: Maybe<AlertManagementAlert>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The issue created after mutation. */
  issue?: Maybe<Issue>
  /** The to-do item after mutation. */
  todo?: Maybe<Todo>
}

/** Autogenerated input type of CreateAnnotation */
export type CreateAnnotationInput = {
  /** The global ID of the environment to add an annotation to. */
  environmentId?: Maybe<Scalars["EnvironmentID"]>
  /** The global ID of the cluster to add an annotation to. */
  clusterId?: Maybe<Scalars["ClustersClusterID"]>
  /** Timestamp indicating starting moment to which the annotation relates. */
  startingAt: Scalars["Time"]
  /** Timestamp indicating ending moment to which the annotation relates. */
  endingAt?: Maybe<Scalars["Time"]>
  /** The path to a file defining the dashboard on which the annotation should be added. */
  dashboardPath: Scalars["String"]
  /** The description of the annotation. */
  description: Scalars["String"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of CreateAnnotation */
export type CreateAnnotationPayload = {
  __typename?: "CreateAnnotationPayload"
  /** The created annotation. */
  annotation?: Maybe<MetricsDashboardAnnotation>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

/** Autogenerated input type of CreateBoard */
export type CreateBoardInput = {
  /** Full path of the project with which the resource is associated. */
  projectPath?: Maybe<Scalars["ID"]>
  /** Full path of the group with which the resource is associated. */
  groupPath?: Maybe<Scalars["ID"]>
  /** The board name. */
  name?: Maybe<Scalars["String"]>
  /** Whether or not backlog list is hidden. */
  hideBacklogList?: Maybe<Scalars["Boolean"]>
  /** Whether or not closed list is hidden. */
  hideClosedList?: Maybe<Scalars["Boolean"]>
  /** ID of user to be assigned to the board. */
  assigneeId?: Maybe<Scalars["UserID"]>
  /** ID of milestone to be assigned to the board. */
  milestoneId?: Maybe<Scalars["MilestoneID"]>
  /** ID of iteration to be assigned to the board. */
  iterationId?: Maybe<Scalars["IterationID"]>
  /** Weight value to be assigned to the board. */
  weight?: Maybe<Scalars["Int"]>
  /** Labels of the issue. */
  labels?: Maybe<Array<Scalars["String"]>>
  /** IDs of labels to be added to the board. */
  labelIds?: Maybe<Array<Scalars["LabelID"]>>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of CreateBoard */
export type CreateBoardPayload = {
  __typename?: "CreateBoardPayload"
  /** The board after mutation. */
  board?: Maybe<Board>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

/** Autogenerated input type of CreateBranch */
export type CreateBranchInput = {
  /** Project full path the branch is associated with. */
  projectPath: Scalars["ID"]
  /** Name of the branch. */
  name: Scalars["String"]
  /** Branch name or commit SHA to create branch from. */
  ref: Scalars["String"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of CreateBranch */
export type CreateBranchPayload = {
  __typename?: "CreateBranchPayload"
  /** Branch after mutation. */
  branch?: Maybe<Branch>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

/** Autogenerated input type of CreateClusterAgent */
export type CreateClusterAgentInput = {
  /** Full path of the associated project for this cluster agent. */
  projectPath: Scalars["ID"]
  /** Name of the cluster agent. */
  name: Scalars["String"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of CreateClusterAgent */
export type CreateClusterAgentPayload = {
  __typename?: "CreateClusterAgentPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Cluster agent created after mutation. */
  clusterAgent?: Maybe<ClusterAgent>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

/** Autogenerated input type of CreateComplianceFramework */
export type CreateComplianceFrameworkInput = {
  /** Full path of the namespace to add the compliance framework to. */
  namespacePath: Scalars["ID"]
  /** Parameters to update the compliance framework with. */
  params: ComplianceFrameworkInput
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of CreateComplianceFramework */
export type CreateComplianceFrameworkPayload = {
  __typename?: "CreateComplianceFrameworkPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The created compliance framework. */
  framework?: Maybe<ComplianceFramework>
}

/** Autogenerated input type of CreateDiffNote */
export type CreateDiffNoteInput = {
  /** The global ID of the resource to add a note to. */
  noteableId: Scalars["NoteableID"]
  /** Content of the note. */
  body: Scalars["String"]
  /** The confidentiality flag of a note. Default is false. */
  confidential?: Maybe<Scalars["Boolean"]>
  /** The position of this note on a diff. */
  position: DiffPositionInput
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of CreateDiffNote */
export type CreateDiffNotePayload = {
  __typename?: "CreateDiffNotePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The note after mutation. */
  note?: Maybe<Note>
}

/** Autogenerated input type of CreateEpic */
export type CreateEpicInput = {
  /** The group the epic to mutate is in. */
  groupPath: Scalars["ID"]
  /** The title of the epic. */
  title?: Maybe<Scalars["String"]>
  /** The description of the epic. */
  description?: Maybe<Scalars["String"]>
  /** Indicates if the epic is confidential. */
  confidential?: Maybe<Scalars["Boolean"]>
  /** The start date of the epic. */
  startDateFixed?: Maybe<Scalars["String"]>
  /** The end date of the epic. */
  dueDateFixed?: Maybe<Scalars["String"]>
  /** Indicates start date should be sourced from start_date_fixed field not the issue milestones. */
  startDateIsFixed?: Maybe<Scalars["Boolean"]>
  /** Indicates end date should be sourced from due_date_fixed field not the issue milestones. */
  dueDateIsFixed?: Maybe<Scalars["Boolean"]>
  /** The IDs of labels to be added to the epic. */
  addLabelIds?: Maybe<Array<Scalars["ID"]>>
  /** The IDs of labels to be removed from the epic. */
  removeLabelIds?: Maybe<Array<Scalars["ID"]>>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of CreateEpic */
export type CreateEpicPayload = {
  __typename?: "CreateEpicPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** The created epic. */
  epic?: Maybe<Epic>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

/** Autogenerated input type of CreateImageDiffNote */
export type CreateImageDiffNoteInput = {
  /** The global ID of the resource to add a note to. */
  noteableId: Scalars["NoteableID"]
  /** Content of the note. */
  body: Scalars["String"]
  /** The confidentiality flag of a note. Default is false. */
  confidential?: Maybe<Scalars["Boolean"]>
  /** The position of this note on a diff. */
  position: DiffImagePositionInput
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of CreateImageDiffNote */
export type CreateImageDiffNotePayload = {
  __typename?: "CreateImageDiffNotePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The note after mutation. */
  note?: Maybe<Note>
}

/** Autogenerated input type of CreateIssue */
export type CreateIssueInput = {
  /** Description of the issue. */
  description?: Maybe<Scalars["String"]>
  /** Due date of the issue. */
  dueDate?: Maybe<Scalars["ISO8601Date"]>
  /** Indicates the issue is confidential. */
  confidential?: Maybe<Scalars["Boolean"]>
  /** Indicates discussion is locked on the issue. */
  locked?: Maybe<Scalars["Boolean"]>
  /** Type of the issue. */
  type?: Maybe<IssueType>
  /** Project full path the issue is associated with. */
  projectPath: Scalars["ID"]
  /** The IID (internal ID) of a project issue. Only admins and project owners can modify. */
  iid?: Maybe<Scalars["Int"]>
  /** Title of the issue. */
  title: Scalars["String"]
  /** The ID of the milestone to assign to the issue. On update milestone will be removed if set to null. */
  milestoneId?: Maybe<Scalars["MilestoneID"]>
  /** Labels of the issue. */
  labels?: Maybe<Array<Scalars["String"]>>
  /** The IDs of labels to be added to the issue. */
  labelIds?: Maybe<Array<Scalars["LabelID"]>>
  /** Timestamp when the issue was created. Available only for admins and project owners. */
  createdAt?: Maybe<Scalars["Time"]>
  /** The IID of a merge request for which to resolve discussions. */
  mergeRequestToResolveDiscussionsOf?: Maybe<Scalars["MergeRequestID"]>
  /** The ID of a discussion to resolve. Also pass `merge_request_to_resolve_discussions_of`. */
  discussionToResolve?: Maybe<Scalars["String"]>
  /** The array of user IDs to assign to the issue. */
  assigneeIds?: Maybe<Array<Scalars["UserID"]>>
  /** The desired health status. */
  healthStatus?: Maybe<HealthStatus>
  /** The weight of the issue. */
  weight?: Maybe<Scalars["Int"]>
  /** The ID of an epic to associate the issue with. */
  epicId?: Maybe<Scalars["EpicID"]>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of CreateIssue */
export type CreateIssuePayload = {
  __typename?: "CreateIssuePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The issue after mutation. */
  issue?: Maybe<Issue>
}

/** Autogenerated input type of CreateIteration */
export type CreateIterationInput = {
  /** Full path of the project with which the resource is associated. */
  projectPath?: Maybe<Scalars["ID"]>
  /** Full path of the group with which the resource is associated. */
  groupPath?: Maybe<Scalars["ID"]>
  /** The title of the iteration. */
  title?: Maybe<Scalars["String"]>
  /** The description of the iteration. */
  description?: Maybe<Scalars["String"]>
  /** The start date of the iteration. */
  startDate?: Maybe<Scalars["String"]>
  /** The end date of the iteration. */
  dueDate?: Maybe<Scalars["String"]>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of CreateIteration */
export type CreateIterationPayload = {
  __typename?: "CreateIterationPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The created iteration. */
  iteration?: Maybe<Iteration>
}

/** Autogenerated input type of CreateNote */
export type CreateNoteInput = {
  /** The global ID of the resource to add a note to. */
  noteableId: Scalars["NoteableID"]
  /** Content of the note. */
  body: Scalars["String"]
  /** The confidentiality flag of a note. Default is false. */
  confidential?: Maybe<Scalars["Boolean"]>
  /** The global ID of the discussion this note is in reply to. */
  discussionId?: Maybe<Scalars["DiscussionID"]>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of CreateNote */
export type CreateNotePayload = {
  __typename?: "CreateNotePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The note after mutation. */
  note?: Maybe<Note>
}

/** Autogenerated input type of CreateRequirement */
export type CreateRequirementInput = {
  /** Title of the requirement. */
  title?: Maybe<Scalars["String"]>
  /** Description of the requirement. */
  description?: Maybe<Scalars["String"]>
  /** Full project path the requirement is associated with. */
  projectPath: Scalars["ID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of CreateRequirement */
export type CreateRequirementPayload = {
  __typename?: "CreateRequirementPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** Requirement after mutation. */
  requirement?: Maybe<Requirement>
}

/** Autogenerated input type of CreateSnippet */
export type CreateSnippetInput = {
  /** Title of the snippet. */
  title: Scalars["String"]
  /** Description of the snippet. */
  description?: Maybe<Scalars["String"]>
  /** The visibility level of the snippet. */
  visibilityLevel: VisibilityLevelsEnum
  /** The project full path the snippet is associated with. */
  projectPath?: Maybe<Scalars["ID"]>
  /** The paths to files uploaded in the snippet description. */
  uploadedFiles?: Maybe<Array<Scalars["String"]>>
  /** Actions to perform over the snippet repository and blobs. */
  blobActions?: Maybe<Array<SnippetBlobActionInputType>>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of CreateSnippet */
export type CreateSnippetPayload = {
  __typename?: "CreateSnippetPayload"
  /**
   * The CAPTCHA site key which must be used to render a challenge for the user to solve to obtain a valid captchaResponse value. Included only when an operation was not completed because "NeedsCaptchaResponse" is true. Deprecated in 13.11: Use spam protection with HTTP headers instead.
   * @deprecated Use spam protection with HTTP headers instead. Deprecated in 13.11.
   */
  captchaSiteKey?: Maybe<Scalars["String"]>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /**
   * Indicates whether the operation was detected as possible spam and not completed. If CAPTCHA is enabled, the request must be resubmitted with a valid CAPTCHA response and spam_log_id included for the operation to be completed. Included only when an operation was not completed because "NeedsCaptchaResponse" is true. Deprecated in 13.11: Use spam protection with HTTP headers instead.
   * @deprecated Use spam protection with HTTP headers instead. Deprecated in 13.11.
   */
  needsCaptchaResponse?: Maybe<Scalars["Boolean"]>
  /** The snippet after mutation. */
  snippet?: Maybe<Snippet>
  /**
   * Indicates whether the operation was detected as definite spam. There is no option to resubmit the request with a CAPTCHA response. Deprecated in 13.11: Use spam protection with HTTP headers instead.
   * @deprecated Use spam protection with HTTP headers instead. Deprecated in 13.11.
   */
  spam?: Maybe<Scalars["Boolean"]>
  /**
   * The spam log ID which must be passed along with a valid CAPTCHA response for an operation to be completed. Included only when an operation was not completed because "NeedsCaptchaResponse" is true. Deprecated in 13.11: Use spam protection with HTTP headers instead.
   * @deprecated Use spam protection with HTTP headers instead. Deprecated in 13.11.
   */
  spamLogId?: Maybe<Scalars["Int"]>
}

/** Autogenerated input type of CreateTestCase */
export type CreateTestCaseInput = {
  /** The test case title. */
  title: Scalars["String"]
  /** The test case description. */
  description?: Maybe<Scalars["String"]>
  /** The IDs of labels to be added to the test case. */
  labelIds?: Maybe<Array<Scalars["ID"]>>
  /** The project full path to create the test case. */
  projectPath: Scalars["ID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of CreateTestCase */
export type CreateTestCasePayload = {
  __typename?: "CreateTestCasePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The test case created. */
  testCase?: Maybe<Issue>
}

/** Represents the current license */
export type CurrentLicense = {
  __typename?: "CurrentLicense"
  /** Date when the license was activated. */
  activatedAt?: Maybe<Scalars["Date"]>
  /** Number of billable users on the system. */
  billableUsersCount?: Maybe<Scalars["Int"]>
  /** Date, including grace period, when licensed features will be blocked. */
  blockChangesAt?: Maybe<Scalars["Date"]>
  /** Company of the licensee. */
  company?: Maybe<Scalars["String"]>
  /** Email of the licensee. */
  email?: Maybe<Scalars["String"]>
  /** Date when the license expires. */
  expiresAt?: Maybe<Scalars["Date"]>
  /** ID of the license. */
  id: Scalars["ID"]
  /** Date when the license was last synced. */
  lastSync?: Maybe<Scalars["Time"]>
  /** Highest number of billable users on the system during the term of the current license. */
  maximumUserCount?: Maybe<Scalars["Int"]>
  /** Name of the licensee. */
  name?: Maybe<Scalars["String"]>
  /** Name of the subscription plan. */
  plan: Scalars["String"]
  /** Date when the license started. */
  startsAt?: Maybe<Scalars["Date"]>
  /** Type of the license. */
  type: Scalars["String"]
  /** Number of paid users in the license. */
  usersInLicenseCount?: Maybe<Scalars["Int"]>
  /** Number of users over the paid users in the license. */
  usersOverLicenseCount?: Maybe<Scalars["Int"]>
}

export type CurrentUserTodos = {
  /** To-do items for the current user. */
  currentUserTodos: TodoConnection
}

export type CurrentUserTodosCurrentUserTodosArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
  state?: Maybe<TodoStateEnum>
}

/** Autogenerated input type of DastOnDemandScanCreate */
export type DastOnDemandScanCreateInput = {
  /** The project the site profile belongs to. */
  fullPath: Scalars["ID"]
  /** ID of the site profile to be used for the scan. */
  dastSiteProfileId: Scalars["DastSiteProfileID"]
  /** ID of the scanner profile to be used for the scan. */
  dastScannerProfileId?: Maybe<Scalars["DastScannerProfileID"]>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of DastOnDemandScanCreate */
export type DastOnDemandScanCreatePayload = {
  __typename?: "DastOnDemandScanCreatePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** URL of the pipeline that was created. */
  pipelineUrl?: Maybe<Scalars["String"]>
}

/** Represents a DAST Profile */
export type DastProfile = {
  __typename?: "DastProfile"
  /** The associated branch. */
  branch?: Maybe<DastProfileBranch>
  /** The associated scanner profile. */
  dastScannerProfile?: Maybe<DastScannerProfile>
  /** The associated site profile. */
  dastSiteProfile?: Maybe<DastSiteProfile>
  /** The description of the scan. */
  description?: Maybe<Scalars["String"]>
  /** Relative web path to the edit page of a profile. */
  editPath?: Maybe<Scalars["String"]>
  /** ID of the profile. */
  id: Scalars["DastProfileID"]
  /** The name of the profile. */
  name?: Maybe<Scalars["String"]>
}

/** Represents a DAST Profile Branch */
export type DastProfileBranch = {
  __typename?: "DastProfileBranch"
  /** Indicates whether or not the branch exists. */
  exists?: Maybe<Scalars["Boolean"]>
  /** The name of the branch. */
  name?: Maybe<Scalars["String"]>
}

/** The connection type for DastProfile. */
export type DastProfileConnection = {
  __typename?: "DastProfileConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<DastProfileEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<DastProfile>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** Autogenerated input type of DastProfileCreate */
export type DastProfileCreateInput = {
  /** The project the profile belongs to. */
  fullPath: Scalars["ID"]
  /** The name of the profile. */
  name: Scalars["String"]
  /** The description of the profile. Defaults to an empty string. */
  description?: Maybe<Scalars["String"]>
  /** The associated branch. */
  branchName?: Maybe<Scalars["String"]>
  /** ID of the site profile to be associated. */
  dastSiteProfileId: Scalars["DastSiteProfileID"]
  /** ID of the scanner profile to be associated. */
  dastScannerProfileId: Scalars["DastScannerProfileID"]
  /** Run scan using profile after creation. Defaults to false. */
  runAfterCreate?: Maybe<Scalars["Boolean"]>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of DastProfileCreate */
export type DastProfileCreatePayload = {
  __typename?: "DastProfileCreatePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** The created profile. */
  dastProfile?: Maybe<DastProfile>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The URL of the pipeline that was created. Requires `runAfterCreate` to be set to `true`. */
  pipelineUrl?: Maybe<Scalars["String"]>
}

/** Autogenerated input type of DastProfileDelete */
export type DastProfileDeleteInput = {
  /** ID of the profile to be deleted. */
  id: Scalars["DastProfileID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of DastProfileDelete */
export type DastProfileDeletePayload = {
  __typename?: "DastProfileDeletePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

/** An edge in a connection. */
export type DastProfileEdge = {
  __typename?: "DastProfileEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<DastProfile>
}

/** Autogenerated input type of DastProfileRun */
export type DastProfileRunInput = {
  /** Full path for the project the scanner profile belongs to. */
  fullPath: Scalars["ID"]
  /** ID of the profile to be used for the scan. */
  id: Scalars["DastProfileID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of DastProfileRun */
export type DastProfileRunPayload = {
  __typename?: "DastProfileRunPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** URL of the pipeline that was created. */
  pipelineUrl?: Maybe<Scalars["String"]>
}

/** Autogenerated input type of DastProfileUpdate */
export type DastProfileUpdateInput = {
  /** ID of the profile to be deleted. */
  id: Scalars["DastProfileID"]
  /** The project the profile belongs to. */
  fullPath: Scalars["ID"]
  /** The name of the profile. */
  name?: Maybe<Scalars["String"]>
  /** The description of the profile. Defaults to an empty string. */
  description?: Maybe<Scalars["String"]>
  /** The associated branch. */
  branchName?: Maybe<Scalars["String"]>
  /** ID of the site profile to be associated. */
  dastSiteProfileId?: Maybe<Scalars["DastSiteProfileID"]>
  /** ID of the scanner profile to be associated. */
  dastScannerProfileId?: Maybe<Scalars["DastScannerProfileID"]>
  /** Run scan using profile after update. Defaults to false. */
  runAfterUpdate?: Maybe<Scalars["Boolean"]>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of DastProfileUpdate */
export type DastProfileUpdatePayload = {
  __typename?: "DastProfileUpdatePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** The updated profile. */
  dastProfile?: Maybe<DastProfile>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The URL of the pipeline that was created. Requires the input argument `runAfterUpdate` to be set to `true` when calling the mutation, otherwise no pipeline will be created. */
  pipelineUrl?: Maybe<Scalars["String"]>
}

export enum DastScanTypeEnum {
  /** Passive DAST scan. This scan will not make active attacks against the target site. */
  Passive = "PASSIVE",
  /** Active DAST scan. This scan will make active attacks against the target site. */
  Active = "ACTIVE",
}

/** Represents a DAST scanner profile */
export type DastScannerProfile = {
  __typename?: "DastScannerProfile"
  /** Relative web path to the edit page of a scanner profile. */
  editPath?: Maybe<Scalars["String"]>
  /**
   * ID of the DAST scanner profile. Deprecated in 13.6: Use `id`.
   * @deprecated Use `id`. Deprecated in 13.6.
   */
  globalId: Scalars["DastScannerProfileID"]
  /** ID of the DAST scanner profile. */
  id: Scalars["DastScannerProfileID"]
  /** Name of the DAST scanner profile. */
  profileName?: Maybe<Scalars["String"]>
  /** List of security policy names that are referencing given project. */
  referencedInSecurityPolicies?: Maybe<Array<Scalars["String"]>>
  /** Indicates the type of DAST scan that will run. Either a Passive Scan or an Active Scan. */
  scanType?: Maybe<DastScanTypeEnum>
  /** Indicates if debug messages should be included in DAST console output. True to include the debug messages. */
  showDebugMessages: Scalars["Boolean"]
  /** The maximum number of minutes allowed for the spider to traverse the site. */
  spiderTimeout?: Maybe<Scalars["Int"]>
  /** The maximum number of seconds allowed for the site under test to respond to a request. */
  targetTimeout?: Maybe<Scalars["Int"]>
  /** Indicates if the AJAX spider should be used to crawl the target site. True to run the AJAX spider in addition to the traditional spider, and false to run only the traditional spider. */
  useAjaxSpider: Scalars["Boolean"]
}

/** The connection type for DastScannerProfile. */
export type DastScannerProfileConnection = {
  __typename?: "DastScannerProfileConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<DastScannerProfileEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<DastScannerProfile>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** Autogenerated input type of DastScannerProfileCreate */
export type DastScannerProfileCreateInput = {
  /** The project the scanner profile belongs to. */
  fullPath: Scalars["ID"]
  /** The name of the scanner profile. */
  profileName: Scalars["String"]
  /** The maximum number of minutes allowed for the spider to traverse the site. */
  spiderTimeout?: Maybe<Scalars["Int"]>
  /** The maximum number of seconds allowed for the site under test to respond to a request. */
  targetTimeout?: Maybe<Scalars["Int"]>
  /** Indicates the type of DAST scan that will run. Either a Passive Scan or an Active Scan. */
  scanType?: Maybe<DastScanTypeEnum>
  /** Indicates if the AJAX spider should be used to crawl the target site. True to run the AJAX spider in addition to the traditional spider, and false to run only the traditional spider. */
  useAjaxSpider?: Maybe<Scalars["Boolean"]>
  /** Indicates if debug messages should be included in DAST console output. True to include the debug messages. */
  showDebugMessages?: Maybe<Scalars["Boolean"]>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of DastScannerProfileCreate */
export type DastScannerProfileCreatePayload = {
  __typename?: "DastScannerProfileCreatePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /**
   * ID of the scanner profile. Deprecated in 13.6: Use `id`.
   * @deprecated Use `id`. Deprecated in 13.6.
   */
  globalId?: Maybe<Scalars["DastScannerProfileID"]>
  /** ID of the scanner profile. */
  id?: Maybe<Scalars["DastScannerProfileID"]>
}

/** Autogenerated input type of DastScannerProfileDelete */
export type DastScannerProfileDeleteInput = {
  /** Full path for the project the scanner profile belongs to. */
  fullPath: Scalars["ID"]
  /** ID of the scanner profile to be deleted. */
  id: Scalars["DastScannerProfileID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of DastScannerProfileDelete */
export type DastScannerProfileDeletePayload = {
  __typename?: "DastScannerProfileDeletePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

/** An edge in a connection. */
export type DastScannerProfileEdge = {
  __typename?: "DastScannerProfileEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<DastScannerProfile>
}

/** Autogenerated input type of DastScannerProfileUpdate */
export type DastScannerProfileUpdateInput = {
  /** The project the scanner profile belongs to. */
  fullPath: Scalars["ID"]
  /** ID of the scanner profile to be updated. */
  id: Scalars["DastScannerProfileID"]
  /** The name of the scanner profile. */
  profileName: Scalars["String"]
  /** The maximum number of minutes allowed for the spider to traverse the site. */
  spiderTimeout: Scalars["Int"]
  /** The maximum number of seconds allowed for the site under test to respond to a request. */
  targetTimeout: Scalars["Int"]
  /** Indicates the type of DAST scan that will run. Either a Passive Scan or an Active Scan. */
  scanType?: Maybe<DastScanTypeEnum>
  /** Indicates if the AJAX spider should be used to crawl the target site. True to run the AJAX spider in addition to the traditional spider, and false to run only the traditional spider. */
  useAjaxSpider?: Maybe<Scalars["Boolean"]>
  /** Indicates if debug messages should be included in DAST console output. True to include the debug messages. */
  showDebugMessages?: Maybe<Scalars["Boolean"]>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of DastScannerProfileUpdate */
export type DastScannerProfileUpdatePayload = {
  __typename?: "DastScannerProfileUpdatePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** ID of the scanner profile. */
  id?: Maybe<Scalars["DastScannerProfileID"]>
}

/** Represents a DAST Site Profile */
export type DastSiteProfile = {
  __typename?: "DastSiteProfile"
  /** Target authentication details. */
  auth?: Maybe<DastSiteProfileAuth>
  /** Relative web path to the edit page of a site profile. */
  editPath?: Maybe<Scalars["String"]>
  /** The URLs to skip during an authenticated scan. */
  excludedUrls?: Maybe<Array<Scalars["String"]>>
  /** ID of the site profile. */
  id: Scalars["DastSiteProfileID"]
  /** Normalized URL of the target to be scanned. */
  normalizedTargetUrl?: Maybe<Scalars["String"]>
  /** The name of the site profile. */
  profileName?: Maybe<Scalars["String"]>
  /** List of security policy names that are referencing given project. */
  referencedInSecurityPolicies?: Maybe<Array<Scalars["String"]>>
  /** Comma-separated list of request header names and values to be added to every request made by DAST. */
  requestHeaders?: Maybe<Scalars["String"]>
  /** The type of target to be scanned. */
  targetType?: Maybe<DastTargetTypeEnum>
  /** The URL of the target to be scanned. */
  targetUrl?: Maybe<Scalars["String"]>
  /** Permissions for the current user on the resource */
  userPermissions: DastSiteProfilePermissions
  /** The current validation status of the site profile. */
  validationStatus?: Maybe<DastSiteProfileValidationStatusEnum>
}

/** Input type for DastSiteProfile authentication */
export type DastSiteProfileAuth = {
  __typename?: "DastSiteProfileAuth"
  /** Indicates whether authentication is enabled. */
  enabled?: Maybe<Scalars["Boolean"]>
  /** Redacted password to authenticate with on the target website. */
  password?: Maybe<Scalars["String"]>
  /** The name of password field at the sign-in HTML form. */
  passwordField?: Maybe<Scalars["String"]>
  /** The URL of the page containing the sign-in HTML form on the target website. */
  url?: Maybe<Scalars["String"]>
  /** The username to authenticate with on the target website. */
  username?: Maybe<Scalars["String"]>
  /** The name of username field at the sign-in HTML form. */
  usernameField?: Maybe<Scalars["String"]>
}

/** Input type for DastSiteProfile authentication */
export type DastSiteProfileAuthInput = {
  /** Indicates whether authentication is enabled. */
  enabled?: Maybe<Scalars["Boolean"]>
  /** The URL of the page containing the sign-in HTML form on the target website. */
  url?: Maybe<Scalars["String"]>
  /** The name of username field at the sign-in HTML form. */
  usernameField?: Maybe<Scalars["String"]>
  /** The name of password field at the sign-in HTML form. */
  passwordField?: Maybe<Scalars["String"]>
  /** The username to authenticate with on the target website. */
  username?: Maybe<Scalars["String"]>
  /** The password to authenticate with on the target website. */
  password?: Maybe<Scalars["String"]>
}

/** The connection type for DastSiteProfile. */
export type DastSiteProfileConnection = {
  __typename?: "DastSiteProfileConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<DastSiteProfileEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<DastSiteProfile>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** Autogenerated input type of DastSiteProfileCreate */
export type DastSiteProfileCreateInput = {
  /** The project the site profile belongs to. */
  fullPath: Scalars["ID"]
  /** The name of the site profile. */
  profileName: Scalars["String"]
  /** The URL of the target to be scanned. */
  targetUrl?: Maybe<Scalars["String"]>
  /** The type of target to be scanned. */
  targetType?: Maybe<DastTargetTypeEnum>
  /** The URLs to skip during an authenticated scan. Defaults to `[]`. */
  excludedUrls?: Maybe<Array<Scalars["String"]>>
  /** Comma-separated list of request header names and values to be added to every request made by DAST. */
  requestHeaders?: Maybe<Scalars["String"]>
  /** Parameters for authentication. */
  auth?: Maybe<DastSiteProfileAuthInput>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of DastSiteProfileCreate */
export type DastSiteProfileCreatePayload = {
  __typename?: "DastSiteProfileCreatePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** ID of the site profile. */
  id?: Maybe<Scalars["DastSiteProfileID"]>
}

/** Autogenerated input type of DastSiteProfileDelete */
export type DastSiteProfileDeleteInput = {
  /** The project the site profile belongs to. */
  fullPath: Scalars["ID"]
  /** ID of the site profile to be deleted. */
  id: Scalars["DastSiteProfileID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of DastSiteProfileDelete */
export type DastSiteProfileDeletePayload = {
  __typename?: "DastSiteProfileDeletePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

/** An edge in a connection. */
export type DastSiteProfileEdge = {
  __typename?: "DastSiteProfileEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<DastSiteProfile>
}

/** Check permissions for the current user on site profile */
export type DastSiteProfilePermissions = {
  __typename?: "DastSiteProfilePermissions"
  /** Indicates the user can perform `create_on_demand_dast_scan` on this resource */
  createOnDemandDastScan: Scalars["Boolean"]
}

/** Autogenerated input type of DastSiteProfileUpdate */
export type DastSiteProfileUpdateInput = {
  /** The project the site profile belongs to. */
  fullPath: Scalars["ID"]
  /** ID of the site profile to be updated. */
  id: Scalars["DastSiteProfileID"]
  /** The name of the site profile. */
  profileName: Scalars["String"]
  /** The URL of the target to be scanned. */
  targetUrl?: Maybe<Scalars["String"]>
  /** The type of target to be scanned. */
  targetType?: Maybe<DastTargetTypeEnum>
  /** The URLs to skip during an authenticated scan. */
  excludedUrls?: Maybe<Array<Scalars["String"]>>
  /** Comma-separated list of request header names and values to be added to every request made by DAST. */
  requestHeaders?: Maybe<Scalars["String"]>
  /** Parameters for authentication. */
  auth?: Maybe<DastSiteProfileAuthInput>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of DastSiteProfileUpdate */
export type DastSiteProfileUpdatePayload = {
  __typename?: "DastSiteProfileUpdatePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** ID of the site profile. */
  id?: Maybe<Scalars["DastSiteProfileID"]>
}

export enum DastSiteProfileValidationStatusEnum {
  /** No site validation exists. */
  None = "NONE",
  /** Site validation process has not started. */
  PendingValidation = "PENDING_VALIDATION",
  /** Site validation process is in progress. */
  InprogressValidation = "INPROGRESS_VALIDATION",
  /** Site validation process finished successfully. */
  PassedValidation = "PASSED_VALIDATION",
  /** Site validation process finished but failed. */
  FailedValidation = "FAILED_VALIDATION",
}

/** Autogenerated input type of DastSiteTokenCreate */
export type DastSiteTokenCreateInput = {
  /** The project the site token belongs to. */
  fullPath: Scalars["ID"]
  /** The URL of the target to be validated. */
  targetUrl?: Maybe<Scalars["String"]>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of DastSiteTokenCreate */
export type DastSiteTokenCreatePayload = {
  __typename?: "DastSiteTokenCreatePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** ID of the site token. */
  id?: Maybe<Scalars["DastSiteTokenID"]>
  /** The current validation status of the target. */
  status?: Maybe<DastSiteProfileValidationStatusEnum>
  /** Token string. */
  token?: Maybe<Scalars["String"]>
}

/** Represents a DAST Site Validation */
export type DastSiteValidation = {
  __typename?: "DastSiteValidation"
  /** Global ID of the site validation. */
  id: Scalars["DastSiteValidationID"]
  /** Normalized URL of the target to be validated. */
  normalizedTargetUrl?: Maybe<Scalars["String"]>
  /** Status of the site validation. */
  status: DastSiteProfileValidationStatusEnum
}

/** The connection type for DastSiteValidation. */
export type DastSiteValidationConnection = {
  __typename?: "DastSiteValidationConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<DastSiteValidationEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<DastSiteValidation>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** Autogenerated input type of DastSiteValidationCreate */
export type DastSiteValidationCreateInput = {
  /** The project the site profile belongs to. */
  fullPath: Scalars["ID"]
  /** ID of the site token. */
  dastSiteTokenId: Scalars["DastSiteTokenID"]
  /** The path to be requested during validation. */
  validationPath: Scalars["String"]
  /** The validation strategy to be used. */
  strategy?: Maybe<DastSiteValidationStrategyEnum>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of DastSiteValidationCreate */
export type DastSiteValidationCreatePayload = {
  __typename?: "DastSiteValidationCreatePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** ID of the site validation. */
  id?: Maybe<Scalars["DastSiteValidationID"]>
  /** The current validation status. */
  status?: Maybe<DastSiteProfileValidationStatusEnum>
}

/** An edge in a connection. */
export type DastSiteValidationEdge = {
  __typename?: "DastSiteValidationEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<DastSiteValidation>
}

/** Autogenerated input type of DastSiteValidationRevoke */
export type DastSiteValidationRevokeInput = {
  /** The project the site validation belongs to. */
  fullPath: Scalars["ID"]
  /** Normalized URL of the target to be revoked. */
  normalizedTargetUrl: Scalars["String"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of DastSiteValidationRevoke */
export type DastSiteValidationRevokePayload = {
  __typename?: "DastSiteValidationRevokePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

export enum DastSiteValidationStrategyEnum {
  /** Text file validation. */
  TextFile = "TEXT_FILE",
  /** Header validation. */
  Header = "HEADER",
}

export enum DastTargetTypeEnum {
  /** Website target. */
  Website = "WEBSITE",
  /** API target. */
  Api = "API",
}

/** Color of the data visualization palette */
export enum DataVisualizationColorEnum {
  /** Blue color */
  Blue = "BLUE",
  /** Orange color */
  Orange = "ORANGE",
  /** Aqua color */
  Aqua = "AQUA",
  /** Green color */
  Green = "GREEN",
  /** Magenta color */
  Magenta = "MAGENTA",
}

/** Weight of the data visualization palette */
export enum DataVisualizationWeightEnum {
  /** 50 weight */
  Weight_50 = "WEIGHT_50",
  /** 100 weight */
  Weight_100 = "WEIGHT_100",
  /** 200 weight */
  Weight_200 = "WEIGHT_200",
  /** 300 weight */
  Weight_300 = "WEIGHT_300",
  /** 400 weight */
  Weight_400 = "WEIGHT_400",
  /** 500 weight */
  Weight_500 = "WEIGHT_500",
  /** 600 weight */
  Weight_600 = "WEIGHT_600",
  /** 700 weight */
  Weight_700 = "WEIGHT_700",
  /** 800 weight */
  Weight_800 = "WEIGHT_800",
  /** 900 weight */
  Weight_900 = "WEIGHT_900",
  /** 950 weight */
  Weight_950 = "WEIGHT_950",
}

/** Autogenerated input type of DeleteAnnotation */
export type DeleteAnnotationInput = {
  /** Global ID of the annotation to delete. */
  id: Scalars["MetricsDashboardAnnotationID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of DeleteAnnotation */
export type DeleteAnnotationPayload = {
  __typename?: "DeleteAnnotationPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

/** The response from the AdminSidekiqQueuesDeleteJobs mutation */
export type DeleteJobsResponse = {
  __typename?: "DeleteJobsResponse"
  /** Whether or not the entire queue was processed in time; if not, retrying the same request is safe. */
  completed?: Maybe<Scalars["Boolean"]>
  /** The number of matching jobs deleted. */
  deletedJobs?: Maybe<Scalars["Int"]>
  /** The queue size after processing. */
  queueSize?: Maybe<Scalars["Int"]>
}

/** A single design */
export type Design = Noteable &
  DesignFields &
  CurrentUserTodos & {
    __typename?: "Design"
    /** To-do items for the current user. */
    currentUserTodos: TodoConnection
    /** The diff refs for this design. */
    diffRefs: DiffRefs
    /** All discussions on this noteable. */
    discussions: DiscussionConnection
    /** How this design was changed in the current version. */
    event: DesignVersionEvent
    /** The filename of the design. */
    filename: Scalars["String"]
    /** The full path to the design file. */
    fullPath: Scalars["String"]
    /** The ID of this design. */
    id: Scalars["ID"]
    /** The URL of the full-sized image. */
    image: Scalars["String"]
    /** The URL of the design resized to fit within the bounds of 432x230. This will be `null` if the image has not been generated */
    imageV432x230?: Maybe<Scalars["String"]>
    /** The issue the design belongs to. */
    issue: Issue
    /** All notes on this noteable. */
    notes: NoteConnection
    /** The total count of user-created notes for this design. */
    notesCount: Scalars["Int"]
    /** The project the design belongs to. */
    project: Project
    /** All versions related to this design ordered newest first. */
    versions: DesignVersionConnection
  }

/** A single design */
export type DesignCurrentUserTodosArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
  state?: Maybe<TodoStateEnum>
}

/** A single design */
export type DesignDiscussionsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** A single design */
export type DesignNotesArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** A single design */
export type DesignVersionsArgs = {
  earlierOrEqualToSha?: Maybe<Scalars["String"]>
  earlierOrEqualToId?: Maybe<Scalars["DesignManagementVersionID"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** A design pinned to a specific version. The image field reflects the design as of the associated version */
export type DesignAtVersion = DesignFields & {
  __typename?: "DesignAtVersion"
  /** The underlying design. */
  design: Design
  /** The diff refs for this design. */
  diffRefs: DiffRefs
  /** How this design was changed in the current version. */
  event: DesignVersionEvent
  /** The filename of the design. */
  filename: Scalars["String"]
  /** The full path to the design file. */
  fullPath: Scalars["String"]
  /** The ID of this design. */
  id: Scalars["ID"]
  /** The URL of the full-sized image. */
  image: Scalars["String"]
  /** The URL of the design resized to fit within the bounds of 432x230. This will be `null` if the image has not been generated */
  imageV432x230?: Maybe<Scalars["String"]>
  /** The issue the design belongs to. */
  issue: Issue
  /** The total count of user-created notes for this design. */
  notesCount: Scalars["Int"]
  /** The project the design belongs to. */
  project: Project
  /** The version this design-at-versions is pinned to. */
  version: DesignVersion
}

/** The connection type for DesignAtVersion. */
export type DesignAtVersionConnection = {
  __typename?: "DesignAtVersionConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<DesignAtVersionEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<DesignAtVersion>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type DesignAtVersionEdge = {
  __typename?: "DesignAtVersionEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<DesignAtVersion>
}

/** A collection of designs */
export type DesignCollection = {
  __typename?: "DesignCollection"
  /** Copy state of the design collection. */
  copyState?: Maybe<DesignCollectionCopyState>
  /** Find a specific design. */
  design?: Maybe<Design>
  /** Find a design as of a version. */
  designAtVersion?: Maybe<DesignAtVersion>
  /** All designs for the design collection. */
  designs: DesignConnection
  /** Issue associated with the design collection. */
  issue: Issue
  /** Project associated with the design collection. */
  project: Project
  /** A specific version. */
  version?: Maybe<DesignVersion>
  /** All versions related to all designs, ordered newest first. */
  versions: DesignVersionConnection
}

/** A collection of designs */
export type DesignCollectionDesignArgs = {
  id?: Maybe<Scalars["DesignManagementDesignID"]>
  filename?: Maybe<Scalars["String"]>
}

/** A collection of designs */
export type DesignCollectionDesignAtVersionArgs = {
  id: Scalars["DesignManagementDesignAtVersionID"]
}

/** A collection of designs */
export type DesignCollectionDesignsArgs = {
  ids?: Maybe<Array<Scalars["DesignManagementDesignID"]>>
  filenames?: Maybe<Array<Scalars["String"]>>
  atVersion?: Maybe<Scalars["DesignManagementVersionID"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** A collection of designs */
export type DesignCollectionVersionArgs = {
  sha?: Maybe<Scalars["String"]>
  id?: Maybe<Scalars["DesignManagementVersionID"]>
}

/** A collection of designs */
export type DesignCollectionVersionsArgs = {
  earlierOrEqualToSha?: Maybe<Scalars["String"]>
  earlierOrEqualToId?: Maybe<Scalars["DesignManagementVersionID"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Copy state of a DesignCollection */
export enum DesignCollectionCopyState {
  /** The DesignCollection has no copy in progress */
  Ready = "READY",
  /** The DesignCollection is being copied */
  InProgress = "IN_PROGRESS",
  /** The DesignCollection encountered an error during a copy */
  Error = "ERROR",
}

/** The connection type for Design. */
export type DesignConnection = {
  __typename?: "DesignConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<DesignEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<Design>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type DesignEdge = {
  __typename?: "DesignEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<Design>
}

export type DesignFields = {
  /** The diff refs for this design. */
  diffRefs: DiffRefs
  /** How this design was changed in the current version. */
  event: DesignVersionEvent
  /** The filename of the design. */
  filename: Scalars["String"]
  /** The full path to the design file. */
  fullPath: Scalars["String"]
  /** The ID of this design. */
  id: Scalars["ID"]
  /** The URL of the full-sized image. */
  image: Scalars["String"]
  /** The URL of the design resized to fit within the bounds of 432x230. This will be `null` if the image has not been generated */
  imageV432x230?: Maybe<Scalars["String"]>
  /** The issue the design belongs to. */
  issue: Issue
  /** The total count of user-created notes for this design. */
  notesCount: Scalars["Int"]
  /** The project the design belongs to. */
  project: Project
}

export type DesignManagement = {
  __typename?: "DesignManagement"
  /** Find a design as of a version. */
  designAtVersion?: Maybe<DesignAtVersion>
  /** Find a version. */
  version?: Maybe<DesignVersion>
}

export type DesignManagementDesignAtVersionArgs = {
  id: Scalars["DesignManagementDesignAtVersionID"]
}

export type DesignManagementVersionArgs = {
  id: Scalars["DesignManagementVersionID"]
}

/** Autogenerated input type of DesignManagementDelete */
export type DesignManagementDeleteInput = {
  /** The project where the issue is to upload designs for. */
  projectPath: Scalars["ID"]
  /** The IID of the issue to modify designs for. */
  iid: Scalars["ID"]
  /** The filenames of the designs to delete. */
  filenames: Array<Scalars["String"]>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of DesignManagementDelete */
export type DesignManagementDeletePayload = {
  __typename?: "DesignManagementDeletePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The new version in which the designs are deleted. */
  version?: Maybe<DesignVersion>
}

/** Autogenerated input type of DesignManagementMove */
export type DesignManagementMoveInput = {
  /** ID of the design to move. */
  id: Scalars["DesignManagementDesignID"]
  /** ID of the immediately preceding design. */
  previous?: Maybe<Scalars["DesignManagementDesignID"]>
  /** ID of the immediately following design. */
  next?: Maybe<Scalars["DesignManagementDesignID"]>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of DesignManagementMove */
export type DesignManagementMovePayload = {
  __typename?: "DesignManagementMovePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** The current state of the collection. */
  designCollection?: Maybe<DesignCollection>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

/** Autogenerated input type of DesignManagementUpload */
export type DesignManagementUploadInput = {
  /** The project where the issue is to upload designs for. */
  projectPath: Scalars["ID"]
  /** The IID of the issue to modify designs for. */
  iid: Scalars["ID"]
  /** The files to upload. */
  files: Array<Scalars["Upload"]>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of DesignManagementUpload */
export type DesignManagementUploadPayload = {
  __typename?: "DesignManagementUploadPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** The designs that were uploaded by the mutation. */
  designs: Array<Design>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** Any designs that were skipped from the upload due to there being no change to their content since their last version */
  skippedDesigns: Array<Design>
}

/** A specific version in which designs were added, modified or deleted */
export type DesignVersion = {
  __typename?: "DesignVersion"
  /** Author of the version. */
  author: UserCore
  /** Timestamp of when the version was created. */
  createdAt: Scalars["Time"]
  /** A particular design as of this version, provided it is visible at this version. */
  designAtVersion: DesignAtVersion
  /** All designs that were changed in the version. */
  designs: DesignConnection
  /** All designs that are visible at this version, as of this version. */
  designsAtVersion: DesignAtVersionConnection
  /** ID of the design version. */
  id: Scalars["ID"]
  /** SHA of the design version. */
  sha: Scalars["ID"]
}

/** A specific version in which designs were added, modified or deleted */
export type DesignVersionDesignAtVersionArgs = {
  id?: Maybe<Scalars["DesignManagementDesignAtVersionID"]>
  designId?: Maybe<Scalars["DesignManagementDesignID"]>
  filename?: Maybe<Scalars["String"]>
}

/** A specific version in which designs were added, modified or deleted */
export type DesignVersionDesignsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** A specific version in which designs were added, modified or deleted */
export type DesignVersionDesignsAtVersionArgs = {
  ids?: Maybe<Array<Scalars["DesignManagementDesignID"]>>
  filenames?: Maybe<Array<Scalars["String"]>>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** The connection type for DesignVersion. */
export type DesignVersionConnection = {
  __typename?: "DesignVersionConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<DesignVersionEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<DesignVersion>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type DesignVersionEdge = {
  __typename?: "DesignVersionEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<DesignVersion>
}

/** Mutation event of a design within a version */
export enum DesignVersionEvent {
  /** No change. */
  None = "NONE",
  /** A creation event */
  Creation = "CREATION",
  /** A modification event */
  Modification = "MODIFICATION",
  /** A deletion event */
  Deletion = "DELETION",
}

/** Autogenerated input type of DestroyBoard */
export type DestroyBoardInput = {
  /** The global ID of the board to destroy. */
  id: Scalars["BoardID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated input type of DestroyBoardList */
export type DestroyBoardListInput = {
  /** Global ID of the list to destroy. Only label lists are accepted. */
  listId: Scalars["ListID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of DestroyBoardList */
export type DestroyBoardListPayload = {
  __typename?: "DestroyBoardListPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The list after mutation. */
  list?: Maybe<BoardList>
}

/** Autogenerated return type of DestroyBoard */
export type DestroyBoardPayload = {
  __typename?: "DestroyBoardPayload"
  /** The board after mutation. */
  board?: Maybe<Board>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

/** Autogenerated input type of DestroyComplianceFramework */
export type DestroyComplianceFrameworkInput = {
  /** The global ID of the compliance framework to destroy. */
  id: Scalars["ComplianceManagementFrameworkID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of DestroyComplianceFramework */
export type DestroyComplianceFrameworkPayload = {
  __typename?: "DestroyComplianceFrameworkPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

/** Autogenerated input type of DestroyContainerRepository */
export type DestroyContainerRepositoryInput = {
  /** ID of the container repository. */
  id: Scalars["ContainerRepositoryID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of DestroyContainerRepository */
export type DestroyContainerRepositoryPayload = {
  __typename?: "DestroyContainerRepositoryPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** The container repository policy after scheduling the deletion. */
  containerRepository: ContainerRepository
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

/** Autogenerated input type of DestroyContainerRepositoryTags */
export type DestroyContainerRepositoryTagsInput = {
  /** ID of the container repository. */
  id: Scalars["ContainerRepositoryID"]
  /** Container repository tag(s) to delete. Total number can't be greater than 20 */
  tagNames: Array<Scalars["String"]>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of DestroyContainerRepositoryTags */
export type DestroyContainerRepositoryTagsPayload = {
  __typename?: "DestroyContainerRepositoryTagsPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Deleted container repository tags. */
  deletedTagNames: Array<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

/** Autogenerated input type of DestroyEpicBoard */
export type DestroyEpicBoardInput = {
  /** Global ID of the board to destroy. */
  id: Scalars["BoardsEpicBoardID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of DestroyEpicBoard */
export type DestroyEpicBoardPayload = {
  __typename?: "DestroyEpicBoardPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Epic board after mutation. */
  epicBoard?: Maybe<EpicBoard>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

/** Autogenerated input type of DestroyNote */
export type DestroyNoteInput = {
  /** The global ID of the note to destroy. */
  id: Scalars["NoteID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of DestroyNote */
export type DestroyNotePayload = {
  __typename?: "DestroyNotePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The note after mutation. */
  note?: Maybe<Note>
}

/** Autogenerated input type of DestroySnippet */
export type DestroySnippetInput = {
  /** The global ID of the snippet to destroy. */
  id: Scalars["SnippetID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of DestroySnippet */
export type DestroySnippetPayload = {
  __typename?: "DestroySnippetPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The snippet after mutation. */
  snippet?: Maybe<Snippet>
}

export type DetailedStatus = {
  __typename?: "DetailedStatus"
  /** Action information for the status. This includes method, button title, icon, path, and title. */
  action?: Maybe<StatusAction>
  /** Path of the details for the status. */
  detailsPath?: Maybe<Scalars["String"]>
  /** Favicon of the status. */
  favicon?: Maybe<Scalars["String"]>
  /** Group of the status. */
  group?: Maybe<Scalars["String"]>
  /** Indicates if the status has further details. */
  hasDetails?: Maybe<Scalars["Boolean"]>
  /** Icon of the status. */
  icon?: Maybe<Scalars["String"]>
  /** Label of the status. */
  label?: Maybe<Scalars["String"]>
  /** Text of the status. */
  text?: Maybe<Scalars["String"]>
  /** Tooltip associated with the status. */
  tooltip?: Maybe<Scalars["String"]>
}

/** Enabled namespace for DevopsAdoption */
export type DevopsAdoptionSegment = {
  __typename?: "DevopsAdoptionSegment"
  /** Namespace where data should be displayed. */
  displayNamespace?: Maybe<Namespace>
  /** ID of the enabled namespace. */
  id: Scalars["ID"]
  /** The latest adoption metrics for the enabled namespace. */
  latestSnapshot?: Maybe<DevopsAdoptionSnapshot>
  /** Namespace which should be calculated. */
  namespace?: Maybe<Namespace>
}

/** The connection type for DevopsAdoptionSegment. */
export type DevopsAdoptionSegmentConnection = {
  __typename?: "DevopsAdoptionSegmentConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<DevopsAdoptionSegmentEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<DevopsAdoptionSegment>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type DevopsAdoptionSegmentEdge = {
  __typename?: "DevopsAdoptionSegmentEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<DevopsAdoptionSegment>
}

/** Snapshot */
export type DevopsAdoptionSnapshot = {
  __typename?: "DevopsAdoptionSnapshot"
  /** Total number of projects with existing CODEOWNERS file. */
  codeOwnersUsedCount?: Maybe<Scalars["Int"]>
  /** At least one deployment succeeded. */
  deploySucceeded: Scalars["Boolean"]
  /** The end time for the snapshot where the data points were collected. */
  endTime: Scalars["Time"]
  /** At least one issue was opened. */
  issueOpened: Scalars["Boolean"]
  /** At least one merge request was approved. */
  mergeRequestApproved: Scalars["Boolean"]
  /** At least one merge request was opened. */
  mergeRequestOpened: Scalars["Boolean"]
  /** At least one pipeline succeeded. */
  pipelineSucceeded: Scalars["Boolean"]
  /** The time the snapshot was recorded. */
  recordedAt: Scalars["Time"]
  /** At least one runner was used. */
  runnerConfigured: Scalars["Boolean"]
  /** At least one security scan succeeded. */
  securityScanSucceeded: Scalars["Boolean"]
  /** The start time for the snapshot where the data points were collected. */
  startTime: Scalars["Time"]
  /** Total number of projects. */
  totalProjectsCount?: Maybe<Scalars["Int"]>
}

export type DiffImagePositionInput = {
  /** SHA of the HEAD at the time the comment was made. */
  headSha: Scalars["String"]
  /** Merge base of the branch the comment was made on. */
  baseSha?: Maybe<Scalars["String"]>
  /** SHA of the branch being compared against. */
  startSha: Scalars["String"]
  /** The paths of the file that was changed. Both of the properties of this input are optional, but at least one of them is required */
  paths: DiffPathsInput
  /** X position of the note. */
  x: Scalars["Int"]
  /** Y position of the note. */
  y: Scalars["Int"]
  /** Total width of the image. */
  width: Scalars["Int"]
  /** Total height of the image. */
  height: Scalars["Int"]
}

export type DiffPathsInput = {
  /** The path of the file on the start sha. */
  oldPath?: Maybe<Scalars["String"]>
  /** The path of the file on the head sha. */
  newPath?: Maybe<Scalars["String"]>
}

export type DiffPosition = {
  __typename?: "DiffPosition"
  /** Information about the branch, HEAD, and base at the time of commenting. */
  diffRefs: DiffRefs
  /** Path of the file that was changed. */
  filePath: Scalars["String"]
  /** Total height of the image. */
  height?: Maybe<Scalars["Int"]>
  /** Line on HEAD SHA that was changed. */
  newLine?: Maybe<Scalars["Int"]>
  /** Path of the file on the HEAD SHA. */
  newPath?: Maybe<Scalars["String"]>
  /** Line on start SHA that was changed. */
  oldLine?: Maybe<Scalars["Int"]>
  /** Path of the file on the start SHA. */
  oldPath?: Maybe<Scalars["String"]>
  /** Type of file the position refers to. */
  positionType: DiffPositionType
  /** Total width of the image. */
  width?: Maybe<Scalars["Int"]>
  /** X position of the note. */
  x?: Maybe<Scalars["Int"]>
  /** Y position of the note. */
  y?: Maybe<Scalars["Int"]>
}

export type DiffPositionInput = {
  /** SHA of the HEAD at the time the comment was made. */
  headSha: Scalars["String"]
  /** Merge base of the branch the comment was made on. */
  baseSha?: Maybe<Scalars["String"]>
  /** SHA of the branch being compared against. */
  startSha: Scalars["String"]
  /** The paths of the file that was changed. Both of the properties of this input are optional, but at least one of them is required */
  paths: DiffPathsInput
  /** Line on start SHA that was changed. */
  oldLine?: Maybe<Scalars["Int"]>
  /** Line on HEAD SHA that was changed. */
  newLine?: Maybe<Scalars["Int"]>
}

/** Type of file the position refers to */
export enum DiffPositionType {
  /** A text file. */
  Text = "text",
  /** An image. */
  Image = "image",
}

export type DiffRefs = {
  __typename?: "DiffRefs"
  /** Merge base of the branch the comment was made on. */
  baseSha?: Maybe<Scalars["String"]>
  /** SHA of the HEAD at the time the comment was made. */
  headSha: Scalars["String"]
  /** SHA of the branch being compared against. */
  startSha: Scalars["String"]
}

/** Changes to a single file */
export type DiffStats = {
  __typename?: "DiffStats"
  /** Number of lines added to this file. */
  additions: Scalars["Int"]
  /** Number of lines deleted from this file. */
  deletions: Scalars["Int"]
  /** File path, relative to repository root. */
  path: Scalars["String"]
}

/** Aggregated summary of changes */
export type DiffStatsSummary = {
  __typename?: "DiffStatsSummary"
  /** Number of lines added. */
  additions: Scalars["Int"]
  /** Number of lines changed. */
  changes: Scalars["Int"]
  /** Number of lines deleted. */
  deletions: Scalars["Int"]
  /** Number of files changed. */
  fileCount: Scalars["Int"]
}

/** Autogenerated input type of DisableDevopsAdoptionNamespace */
export type DisableDevopsAdoptionNamespaceInput = {
  /** One or many IDs of the enabled namespaces to disable. */
  id: Array<Scalars["AnalyticsDevopsAdoptionEnabledNamespaceID"]>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of DisableDevopsAdoptionNamespace */
export type DisableDevopsAdoptionNamespacePayload = {
  __typename?: "DisableDevopsAdoptionNamespacePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

export type Discussion = ResolvableInterface & {
  __typename?: "Discussion"
  /** Timestamp of the discussion's creation. */
  createdAt: Scalars["Time"]
  /** ID of this discussion. */
  id: Scalars["DiscussionID"]
  /** All notes in the discussion. */
  notes: NoteConnection
  /** ID used to reply to this discussion. */
  replyId: Scalars["DiscussionID"]
  /** Indicates if the object can be resolved. */
  resolvable: Scalars["Boolean"]
  /** Indicates if the object is resolved. */
  resolved: Scalars["Boolean"]
  /** Timestamp of when the object was resolved. */
  resolvedAt?: Maybe<Scalars["Time"]>
  /** User who resolved the object. */
  resolvedBy?: Maybe<UserCore>
}

export type DiscussionNotesArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** The connection type for Discussion. */
export type DiscussionConnection = {
  __typename?: "DiscussionConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<DiscussionEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<Discussion>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type DiscussionEdge = {
  __typename?: "DiscussionEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<Discussion>
}

/** Autogenerated input type of DiscussionToggleResolve */
export type DiscussionToggleResolveInput = {
  /** The global ID of the discussion. */
  id: Scalars["DiscussionID"]
  /** Will resolve the discussion when true, and unresolve the discussion when false. */
  resolve: Scalars["Boolean"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of DiscussionToggleResolve */
export type DiscussionToggleResolvePayload = {
  __typename?: "DiscussionToggleResolvePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** The discussion after mutation. */
  discussion?: Maybe<Discussion>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

/** Autogenerated input type of DismissVulnerability */
export type DismissVulnerabilityInput = {
  /** ID of the vulnerability to be dismissed. */
  id: Scalars["VulnerabilityID"]
  /** Comment why vulnerability should be dismissed. */
  comment?: Maybe<Scalars["String"]>
  /** Reason why vulnerability should be dismissed. */
  dismissalReason?: Maybe<VulnerabilityDismissalReason>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of DismissVulnerability */
export type DismissVulnerabilityPayload = {
  __typename?: "DismissVulnerabilityPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The vulnerability after dismissal. */
  vulnerability?: Maybe<Vulnerability>
}

/** Autogenerated input type of EnableDevopsAdoptionNamespace */
export type EnableDevopsAdoptionNamespaceInput = {
  /** Namespace ID. */
  namespaceId: Scalars["NamespaceID"]
  /** Display namespace ID. */
  displayNamespaceId?: Maybe<Scalars["NamespaceID"]>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of EnableDevopsAdoptionNamespace */
export type EnableDevopsAdoptionNamespacePayload = {
  __typename?: "EnableDevopsAdoptionNamespacePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Enabled namespace after mutation. */
  enabledNamespace?: Maybe<DevopsAdoptionSegment>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

export type Entry = {
  /** Flat path of the entry. */
  flatPath: Scalars["String"]
  /** ID of the entry. */
  id: Scalars["ID"]
  /** Name of the entry. */
  name: Scalars["String"]
  /** Path of the entry. */
  path: Scalars["String"]
  /** Last commit SHA for the entry. */
  sha: Scalars["String"]
  /** Type of tree entry. */
  type: EntryType
}

/** Type of a tree entry */
export enum EntryType {
  /** Directory tree type. */
  Tree = "tree",
  /** File tree type. */
  Blob = "blob",
  /** Commit tree type. */
  Commit = "commit",
}

/** Describes where code is deployed for a project */
export type Environment = {
  __typename?: "Environment"
  /** ID of the environment. */
  id: Scalars["ID"]
  /** The most severe open alert for the environment. If multiple alerts have equal severity, the most recent is returned. */
  latestOpenedMostSevereAlert?: Maybe<AlertManagementAlert>
  /** Metrics dashboard schema for the environment. */
  metricsDashboard?: Maybe<MetricsDashboard>
  /** Human-readable name of the environment. */
  name: Scalars["String"]
  /** The path to the environment. */
  path: Scalars["String"]
  /** State of the environment, for example: available/stopped. */
  state: Scalars["String"]
}

/** Describes where code is deployed for a project */
export type EnvironmentMetricsDashboardArgs = {
  path: Scalars["String"]
}

/** The connection type for Environment. */
export type EnvironmentConnection = {
  __typename?: "EnvironmentConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<EnvironmentEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<Environment>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type EnvironmentEdge = {
  __typename?: "EnvironmentEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<Environment>
}

/** Autogenerated input type of EnvironmentsCanaryIngressUpdate */
export type EnvironmentsCanaryIngressUpdateInput = {
  /** The global ID of the environment to update. */
  id: Scalars["EnvironmentID"]
  /** The weight of the Canary Ingress. */
  weight: Scalars["Int"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of EnvironmentsCanaryIngressUpdate */
export type EnvironmentsCanaryIngressUpdatePayload = {
  __typename?: "EnvironmentsCanaryIngressUpdatePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

/** Represents an epic */
export type Epic = Noteable &
  CurrentUserTodos &
  Eventable & {
    __typename?: "Epic"
    /** Author of the epic. */
    author: UserCore
    /** A list of award emojis associated with the epic. */
    awardEmoji?: Maybe<AwardEmojiConnection>
    /** Children (sub-epics) of the epic. */
    children?: Maybe<EpicConnection>
    /** Timestamp of when the epic was closed. */
    closedAt?: Maybe<Scalars["Time"]>
    /** Indicates if the epic is confidential. */
    confidential?: Maybe<Scalars["Boolean"]>
    /** Timestamp of when the epic was created. */
    createdAt?: Maybe<Scalars["Time"]>
    /** To-do items for the current user. */
    currentUserTodos: TodoConnection
    /** Number of open and closed descendant epics and issues. */
    descendantCounts?: Maybe<EpicDescendantCount>
    /** Total weight of open and closed issues in the epic and its descendants. */
    descendantWeightSum?: Maybe<EpicDescendantWeights>
    /** Description of the epic. */
    description?: Maybe<Scalars["String"]>
    /** The GitLab Flavored Markdown rendering of `description` */
    descriptionHtml?: Maybe<Scalars["String"]>
    /** All discussions on this noteable. */
    discussions: DiscussionConnection
    /** Number of downvotes the epic has received. */
    downvotes: Scalars["Int"]
    /** Due date of the epic. */
    dueDate?: Maybe<Scalars["Time"]>
    /** Fixed due date of the epic. */
    dueDateFixed?: Maybe<Scalars["Time"]>
    /** Inherited due date of the epic from milestones. */
    dueDateFromMilestones?: Maybe<Scalars["Time"]>
    /** Indicates if the due date has been manually set. */
    dueDateIsFixed?: Maybe<Scalars["Boolean"]>
    /** A list of events associated with the object. */
    events?: Maybe<EventConnection>
    /** Group to which the epic belongs. */
    group: Group
    /** Indicates if the epic has children. */
    hasChildren: Scalars["Boolean"]
    /** Indicates if the epic has direct issues. */
    hasIssues: Scalars["Boolean"]
    /** Indicates if the epic has a parent epic. */
    hasParent: Scalars["Boolean"]
    /** Current health status of the epic. */
    healthStatus?: Maybe<EpicHealthStatus>
    /** ID of the epic. */
    id: Scalars["ID"]
    /** Internal ID of the epic. */
    iid: Scalars["ID"]
    /** A list of issues associated with the epic. */
    issues?: Maybe<EpicIssueConnection>
    /** Labels assigned to the epic. */
    labels?: Maybe<LabelConnection>
    /** All notes on this noteable. */
    notes: NoteConnection
    /** Parent epic of the epic. */
    parent?: Maybe<Epic>
    /** List of participants for the epic. */
    participants?: Maybe<UserCoreConnection>
    /** Internal reference of the epic. Returned in shortened format by default. */
    reference: Scalars["String"]
    /** URI path of the epic-issue relationship. */
    relationPath?: Maybe<Scalars["String"]>
    /** The relative position of the epic in the epic tree. */
    relativePosition?: Maybe<Scalars["Int"]>
    /** Start date of the epic. */
    startDate?: Maybe<Scalars["Time"]>
    /** Fixed start date of the epic. */
    startDateFixed?: Maybe<Scalars["Time"]>
    /** Inherited start date of the epic from milestones. */
    startDateFromMilestones?: Maybe<Scalars["Time"]>
    /** Indicates if the start date has been manually set. */
    startDateIsFixed?: Maybe<Scalars["Boolean"]>
    /** State of the epic. */
    state: EpicState
    /** Indicates the currently logged in user is subscribed to the epic. */
    subscribed: Scalars["Boolean"]
    /** Title of the epic. */
    title?: Maybe<Scalars["String"]>
    /** The GitLab Flavored Markdown rendering of `title` */
    titleHtml?: Maybe<Scalars["String"]>
    /** Timestamp of when the epic was updated. */
    updatedAt?: Maybe<Scalars["Time"]>
    /** Number of upvotes the epic has received. */
    upvotes: Scalars["Int"]
    /** Number of user discussions in the epic. */
    userDiscussionsCount: Scalars["Int"]
    /** Number of user notes of the epic. */
    userNotesCount: Scalars["Int"]
    /** Permissions for the current user on the resource */
    userPermissions: EpicPermissions
    /** Web path of the epic. */
    webPath: Scalars["String"]
    /** Web URL of the epic. */
    webUrl: Scalars["String"]
  }

/** Represents an epic */
export type EpicAwardEmojiArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Represents an epic */
export type EpicChildrenArgs = {
  timeframe?: Maybe<Timeframe>
  iid?: Maybe<Scalars["ID"]>
  iids?: Maybe<Array<Scalars["ID"]>>
  state?: Maybe<EpicState>
  search?: Maybe<Scalars["String"]>
  sort?: Maybe<EpicSort>
  authorUsername?: Maybe<Scalars["String"]>
  labelName?: Maybe<Array<Scalars["String"]>>
  milestoneTitle?: Maybe<Scalars["String"]>
  iidStartsWith?: Maybe<Scalars["String"]>
  includeDescendantGroups?: Maybe<Scalars["Boolean"]>
  confidential?: Maybe<Scalars["Boolean"]>
  myReactionEmoji?: Maybe<Scalars["String"]>
  not?: Maybe<NegatedEpicFilterInput>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Represents an epic */
export type EpicCurrentUserTodosArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
  state?: Maybe<TodoStateEnum>
}

/** Represents an epic */
export type EpicDiscussionsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Represents an epic */
export type EpicEventsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Represents an epic */
export type EpicIssuesArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Represents an epic */
export type EpicLabelsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Represents an epic */
export type EpicNotesArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Represents an epic */
export type EpicParticipantsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Represents an epic */
export type EpicReferenceArgs = {
  full?: Maybe<Scalars["Boolean"]>
}

/** Autogenerated input type of EpicAddIssue */
export type EpicAddIssueInput = {
  /** The IID of the epic to mutate. */
  iid: Scalars["ID"]
  /** The group the epic to mutate belongs to. */
  groupPath: Scalars["ID"]
  /** The full path of the project the issue belongs to. */
  projectPath: Scalars["ID"]
  /** The IID of the issue to be added. */
  issueIid: Scalars["String"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of EpicAddIssue */
export type EpicAddIssuePayload = {
  __typename?: "EpicAddIssuePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** The epic after mutation. */
  epic?: Maybe<Epic>
  /** The epic-issue relation. */
  epicIssue?: Maybe<EpicIssue>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

/** Represents an epic board */
export type EpicBoard = {
  __typename?: "EpicBoard"
  /** Whether or not backlog list is hidden. */
  hideBacklogList?: Maybe<Scalars["Boolean"]>
  /** Whether or not closed list is hidden. */
  hideClosedList?: Maybe<Scalars["Boolean"]>
  /** Global ID of the epic board. */
  id: Scalars["BoardsEpicBoardID"]
  /** Labels of the board. */
  labels?: Maybe<LabelConnection>
  /** Epic board lists. */
  lists?: Maybe<EpicListConnection>
  /** Name of the epic board. */
  name?: Maybe<Scalars["String"]>
  /** Web path of the epic board. */
  webPath: Scalars["String"]
  /** Web URL of the epic board. */
  webUrl: Scalars["String"]
}

/** Represents an epic board */
export type EpicBoardLabelsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Represents an epic board */
export type EpicBoardListsArgs = {
  id?: Maybe<Scalars["BoardsEpicListID"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** The connection type for EpicBoard. */
export type EpicBoardConnection = {
  __typename?: "EpicBoardConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<EpicBoardEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<EpicBoard>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** Autogenerated input type of EpicBoardCreate */
export type EpicBoardCreateInput = {
  /** The board name. */
  name?: Maybe<Scalars["String"]>
  /** Whether or not backlog list is hidden. */
  hideBacklogList?: Maybe<Scalars["Boolean"]>
  /** Whether or not closed list is hidden. */
  hideClosedList?: Maybe<Scalars["Boolean"]>
  /** Labels of the issue. */
  labels?: Maybe<Array<Scalars["String"]>>
  /** IDs of labels to be added to the board. */
  labelIds?: Maybe<Array<Scalars["LabelID"]>>
  /** Full path of the group with which the resource is associated. */
  groupPath?: Maybe<Scalars["ID"]>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of EpicBoardCreate */
export type EpicBoardCreatePayload = {
  __typename?: "EpicBoardCreatePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** The created epic board. */
  epicBoard?: Maybe<EpicBoard>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

/** An edge in a connection. */
export type EpicBoardEdge = {
  __typename?: "EpicBoardEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<EpicBoard>
}

/** Autogenerated input type of EpicBoardListCreate */
export type EpicBoardListCreateInput = {
  /** Create the backlog list. */
  backlog?: Maybe<Scalars["Boolean"]>
  /** Global ID of an existing label. */
  labelId?: Maybe<Scalars["LabelID"]>
  /** Global ID of the issue board to mutate. */
  boardId: Scalars["BoardsEpicBoardID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of EpicBoardListCreate */
export type EpicBoardListCreatePayload = {
  __typename?: "EpicBoardListCreatePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** Epic list in the epic board. */
  list?: Maybe<EpicList>
}

/** Autogenerated input type of EpicBoardListDestroy */
export type EpicBoardListDestroyInput = {
  /** Global ID of the epic board list to destroy. */
  listId: Scalars["BoardsEpicListID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of EpicBoardListDestroy */
export type EpicBoardListDestroyPayload = {
  __typename?: "EpicBoardListDestroyPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The epic board list. `null` if the board was destroyed successfully. */
  list?: Maybe<EpicList>
}

/** Autogenerated input type of EpicBoardUpdate */
export type EpicBoardUpdateInput = {
  /** The board name. */
  name?: Maybe<Scalars["String"]>
  /** Whether or not backlog list is hidden. */
  hideBacklogList?: Maybe<Scalars["Boolean"]>
  /** Whether or not closed list is hidden. */
  hideClosedList?: Maybe<Scalars["Boolean"]>
  /** Labels of the issue. */
  labels?: Maybe<Array<Scalars["String"]>>
  /** IDs of labels to be added to the board. */
  labelIds?: Maybe<Array<Scalars["LabelID"]>>
  /** The epic board global ID. */
  id: Scalars["BoardsEpicBoardID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of EpicBoardUpdate */
export type EpicBoardUpdatePayload = {
  __typename?: "EpicBoardUpdatePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** The updated epic board. */
  epicBoard?: Maybe<EpicBoard>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

/** The connection type for Epic. */
export type EpicConnection = {
  __typename?: "EpicConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<EpicEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<Epic>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** Counts of descendent epics */
export type EpicDescendantCount = {
  __typename?: "EpicDescendantCount"
  /** Number of closed child epics. */
  closedEpics?: Maybe<Scalars["Int"]>
  /** Number of closed epic issues. */
  closedIssues?: Maybe<Scalars["Int"]>
  /** Number of opened child epics. */
  openedEpics?: Maybe<Scalars["Int"]>
  /** Number of opened epic issues. */
  openedIssues?: Maybe<Scalars["Int"]>
}

/** Total weight of open and closed descendant issues */
export type EpicDescendantWeights = {
  __typename?: "EpicDescendantWeights"
  /** Total weight of completed (closed) issues in this epic, including epic descendants. */
  closedIssues?: Maybe<Scalars["Int"]>
  /** Total weight of opened issues in this epic, including epic descendants. */
  openedIssues?: Maybe<Scalars["Int"]>
}

/** An edge in a connection. */
export type EpicEdge = {
  __typename?: "EpicEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<Epic>
}

export type EpicFilters = {
  /** Filter by label name. */
  labelName?: Maybe<Array<Maybe<Scalars["String"]>>>
  /** Filter by author username. */
  authorUsername?: Maybe<Scalars["String"]>
  /** Filter by reaction emoji applied by the current user. */
  myReactionEmoji?: Maybe<Scalars["String"]>
  /** Negated epic arguments. */
  not?: Maybe<NegatedEpicBoardIssueInput>
  /** Search query for epic title or description. */
  search?: Maybe<Scalars["String"]>
}

/** Health status of child issues */
export type EpicHealthStatus = {
  __typename?: "EpicHealthStatus"
  /** Number of issues at risk. */
  issuesAtRisk?: Maybe<Scalars["Int"]>
  /** Number of issues that need attention. */
  issuesNeedingAttention?: Maybe<Scalars["Int"]>
  /** Number of issues on track. */
  issuesOnTrack?: Maybe<Scalars["Int"]>
}

/** Relationship between an epic and an issue */
export type EpicIssue = Noteable &
  CurrentUserTodos & {
    __typename?: "EpicIssue"
    /** Alert associated to this issue. */
    alertManagementAlert?: Maybe<AlertManagementAlert>
    /** Assignees of the issue. */
    assignees?: Maybe<UserCoreConnection>
    /** User that created the issue. */
    author: UserCore
    /** Indicates the issue is blocked. */
    blocked: Scalars["Boolean"]
    /** Count of issues blocking this issue. */
    blockedByCount?: Maybe<Scalars["Int"]>
    /** Issues blocking this issue. */
    blockedByIssues?: Maybe<IssueConnection>
    /** Timestamp of when the issue was closed. */
    closedAt?: Maybe<Scalars["Time"]>
    /** Indicates the issue is confidential. */
    confidential: Scalars["Boolean"]
    /** User specific email address for the issue. */
    createNoteEmail?: Maybe<Scalars["String"]>
    /** Timestamp of when the issue was created. */
    createdAt: Scalars["Time"]
    /** To-do items for the current user. */
    currentUserTodos: TodoConnection
    /** Description of the issue. */
    description?: Maybe<Scalars["String"]>
    /** The GitLab Flavored Markdown rendering of `description` */
    descriptionHtml?: Maybe<Scalars["String"]>
    /** Collection of design images associated with this issue. */
    designCollection?: Maybe<DesignCollection>
    /** Indicates discussion is locked on the issue. */
    discussionLocked: Scalars["Boolean"]
    /** All discussions on this noteable. */
    discussions: DiscussionConnection
    /** Number of downvotes the issue has received. */
    downvotes: Scalars["Int"]
    /** Due date of the issue. */
    dueDate?: Maybe<Scalars["Time"]>
    /** Indicates if a project has email notifications disabled: `true` if email notifications are disabled. */
    emailsDisabled: Scalars["Boolean"]
    /** Epic to which this issue belongs. */
    epic?: Maybe<Epic>
    /** ID of the epic-issue relation. */
    epicIssueId: Scalars["ID"]
    /** Current health status. */
    healthStatus?: Maybe<HealthStatus>
    /** Human-readable time estimate of the issue. */
    humanTimeEstimate?: Maybe<Scalars["String"]>
    /** Human-readable total time reported as spent on the issue. */
    humanTotalTimeSpent?: Maybe<Scalars["String"]>
    /** Global ID of the epic-issue relation. */
    id?: Maybe<Scalars["ID"]>
    /** Internal ID of the issue. */
    iid: Scalars["ID"]
    /** Iteration of the issue. */
    iteration?: Maybe<Iteration>
    /** Labels of the issue. */
    labels?: Maybe<LabelConnection>
    /** Metric images associated to the issue. */
    metricImages?: Maybe<Array<MetricImage>>
    /** Milestone of the issue. */
    milestone?: Maybe<Milestone>
    /** Indicates if issue got moved from other project. */
    moved?: Maybe<Scalars["Boolean"]>
    /** Updated Issue after it got moved to another project. */
    movedTo?: Maybe<Issue>
    /** All notes on this noteable. */
    notes: NoteConnection
    /** List of participants in the issue. */
    participants?: Maybe<UserCoreConnection>
    /** Internal reference of the issue. Returned in shortened format by default. */
    reference: Scalars["String"]
    /** URI path of the epic-issue relation. */
    relationPath?: Maybe<Scalars["String"]>
    /** Relative position of the issue (used for positioning in epic tree and issue boards). */
    relativePosition?: Maybe<Scalars["Int"]>
    /** Severity level of the incident. */
    severity?: Maybe<IssuableSeverity>
    /** Timestamp of when the issue SLA expires. */
    slaDueAt?: Maybe<Scalars["Time"]>
    /** State of the issue. */
    state: IssueState
    /** Indicates whether an issue is published to the status page. */
    statusPagePublishedIncident?: Maybe<Scalars["Boolean"]>
    /** Indicates the currently logged in user is subscribed to the issue. */
    subscribed: Scalars["Boolean"]
    /** Task completion status of the issue. */
    taskCompletionStatus: TaskCompletionStatus
    /** Time estimate of the issue. */
    timeEstimate: Scalars["Int"]
    /** Timelogs on the issue. */
    timelogs: TimelogConnection
    /** Title of the issue. */
    title: Scalars["String"]
    /** The GitLab Flavored Markdown rendering of `title` */
    titleHtml?: Maybe<Scalars["String"]>
    /** Total time reported as spent on the issue. */
    totalTimeSpent: Scalars["Int"]
    /** Type of the issue. */
    type?: Maybe<IssueType>
    /** Timestamp of when the issue was last updated. */
    updatedAt: Scalars["Time"]
    /** User that last updated the issue. */
    updatedBy?: Maybe<UserCore>
    /** Number of upvotes the issue has received. */
    upvotes: Scalars["Int"]
    /** Number of user discussions in the issue. */
    userDiscussionsCount: Scalars["Int"]
    /** Number of user notes of the issue. */
    userNotesCount: Scalars["Int"]
    /** Permissions for the current user on the resource */
    userPermissions: IssuePermissions
    /** Web path of the issue. */
    webPath: Scalars["String"]
    /** Web URL of the issue. */
    webUrl: Scalars["String"]
    /** Weight of the issue. */
    weight?: Maybe<Scalars["Int"]>
  }

/** Relationship between an epic and an issue */
export type EpicIssueAssigneesArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Relationship between an epic and an issue */
export type EpicIssueBlockedByIssuesArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Relationship between an epic and an issue */
export type EpicIssueCurrentUserTodosArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
  state?: Maybe<TodoStateEnum>
}

/** Relationship between an epic and an issue */
export type EpicIssueDiscussionsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Relationship between an epic and an issue */
export type EpicIssueLabelsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Relationship between an epic and an issue */
export type EpicIssueNotesArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Relationship between an epic and an issue */
export type EpicIssueParticipantsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Relationship between an epic and an issue */
export type EpicIssueReferenceArgs = {
  full?: Maybe<Scalars["Boolean"]>
}

/** Relationship between an epic and an issue */
export type EpicIssueTimelogsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** The connection type for EpicIssue. */
export type EpicIssueConnection = {
  __typename?: "EpicIssueConnection"
  /** Total count of collection. */
  count: Scalars["Int"]
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<EpicIssueEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<EpicIssue>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
  /** Total weight of issues collection. */
  weight: Scalars["Int"]
}

/** An edge in a connection. */
export type EpicIssueEdge = {
  __typename?: "EpicIssueEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<EpicIssue>
}

/** Represents an epic board list */
export type EpicList = {
  __typename?: "EpicList"
  /** Indicates if this list is collapsed for this user. */
  collapsed?: Maybe<Scalars["Boolean"]>
  /** List epics. */
  epics?: Maybe<EpicConnection>
  /** Count of epics in the list. */
  epicsCount?: Maybe<Scalars["Int"]>
  /** Global ID of the board list. */
  id: Scalars["BoardsEpicListID"]
  /** Label of the list. */
  label?: Maybe<Label>
  /** Type of the list. */
  listType: Scalars["String"]
  /** Position of the list within the board. */
  position?: Maybe<Scalars["Int"]>
  /** Title of the list. */
  title: Scalars["String"]
}

/** Represents an epic board list */
export type EpicListEpicsArgs = {
  filters?: Maybe<EpicFilters>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** The connection type for EpicList. */
export type EpicListConnection = {
  __typename?: "EpicListConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<EpicListEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<EpicList>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type EpicListEdge = {
  __typename?: "EpicListEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<EpicList>
}

/** Autogenerated input type of EpicMoveList */
export type EpicMoveListInput = {
  /** Global ID of the board that the epic is in. */
  boardId: Scalars["BoardsEpicBoardID"]
  /** ID of the epic to mutate. */
  epicId: Scalars["EpicID"]
  /** ID of the board list that the epic will be moved from. */
  fromListId: Scalars["BoardsEpicListID"]
  /** ID of the board list that the epic will be moved to. */
  toListId: Scalars["BoardsEpicListID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of EpicMoveList */
export type EpicMoveListPayload = {
  __typename?: "EpicMoveListPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

/** Check permissions for the current user on an epic */
export type EpicPermissions = {
  __typename?: "EpicPermissions"
  /** Indicates the user can perform `admin_epic` on this resource */
  adminEpic: Scalars["Boolean"]
  /** Indicates the user can perform `award_emoji` on this resource */
  awardEmoji: Scalars["Boolean"]
  /** Indicates the user can perform `create_epic` on this resource */
  createEpic: Scalars["Boolean"]
  /** Indicates the user can perform `create_note` on this resource */
  createNote: Scalars["Boolean"]
  /** Indicates the user can perform `destroy_epic` on this resource */
  destroyEpic: Scalars["Boolean"]
  /** Indicates the user can perform `read_epic` on this resource */
  readEpic: Scalars["Boolean"]
  /** Indicates the user can perform `read_epic_iid` on this resource */
  readEpicIid: Scalars["Boolean"]
  /** Indicates the user can perform `update_epic` on this resource */
  updateEpic: Scalars["Boolean"]
}

/** Autogenerated input type of EpicSetSubscription */
export type EpicSetSubscriptionInput = {
  /** The IID of the epic to mutate. */
  iid: Scalars["ID"]
  /** The group the epic to mutate belongs to. */
  groupPath: Scalars["ID"]
  /** The desired state of the subscription. */
  subscribedState: Scalars["Boolean"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of EpicSetSubscription */
export type EpicSetSubscriptionPayload = {
  __typename?: "EpicSetSubscriptionPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** The epic after mutation. */
  epic?: Maybe<Epic>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

/** Roadmap sort values */
export enum EpicSort {
  /** Sort by start date in descending order. */
  StartDateDesc = "START_DATE_DESC",
  /** Sort by start date in ascending order. */
  StartDateAsc = "START_DATE_ASC",
  /** Sort by end date in descending order. */
  EndDateDesc = "END_DATE_DESC",
  /** Sort by end date in ascending order. */
  EndDateAsc = "END_DATE_ASC",
}

/** State of an epic */
export enum EpicState {
  All = "all",
  Opened = "opened",
  Closed = "closed",
}

/** State event of an epic */
export enum EpicStateEvent {
  /** Reopen the epic. */
  Reopen = "REOPEN",
  /** Close the epic. */
  Close = "CLOSE",
}

/** A node of an epic tree. */
export type EpicTreeNodeFieldsInputType = {
  /** The ID of the epic_issue or epic that is being moved. */
  id: Scalars["EpicTreeSortingID"]
  /** The ID of the epic_issue or issue that the actual epic or issue is switched with. */
  adjacentReferenceId?: Maybe<Scalars["EpicTreeSortingID"]>
  /** The type of the switch, after or before allowed. */
  relativePosition?: Maybe<MoveType>
  /** ID of the new parent epic. */
  newParentId?: Maybe<Scalars["EpicID"]>
}

/** Autogenerated input type of EpicTreeReorder */
export type EpicTreeReorderInput = {
  /** The ID of the base epic of the tree. */
  baseEpicId: Scalars["EpicID"]
  /** Parameters for updating the tree positions. */
  moved: EpicTreeNodeFieldsInputType
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of EpicTreeReorder */
export type EpicTreeReorderPayload = {
  __typename?: "EpicTreeReorderPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

/** Epic ID wildcard values */
export enum EpicWildcardId {
  /** No epic is assigned. */
  None = "NONE",
  /** Any epic is assigned. */
  Any = "ANY",
}

/** Autogenerated input type of EscalationPolicyCreate */
export type EscalationPolicyCreateInput = {
  /** The project to create the escalation policy for. */
  projectPath: Scalars["ID"]
  /** The name of the escalation policy. */
  name: Scalars["String"]
  /** The description of the escalation policy. */
  description?: Maybe<Scalars["String"]>
  /** The steps of the escalation policy. */
  rules: Array<EscalationRuleInput>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of EscalationPolicyCreate */
export type EscalationPolicyCreatePayload = {
  __typename?: "EscalationPolicyCreatePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The escalation policy. */
  escalationPolicy?: Maybe<EscalationPolicyType>
}

/** Autogenerated input type of EscalationPolicyDestroy */
export type EscalationPolicyDestroyInput = {
  /** The escalation policy internal ID to remove. */
  id: Scalars["IncidentManagementEscalationPolicyID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of EscalationPolicyDestroy */
export type EscalationPolicyDestroyPayload = {
  __typename?: "EscalationPolicyDestroyPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The escalation policy. */
  escalationPolicy?: Maybe<EscalationPolicyType>
}

/** Represents an escalation policy */
export type EscalationPolicyType = {
  __typename?: "EscalationPolicyType"
  /** The description of the escalation policy. */
  description?: Maybe<Scalars["String"]>
  /** ID of the escalation policy. */
  id?: Maybe<Scalars["IncidentManagementEscalationPolicyID"]>
  /** The name of the escalation policy. */
  name?: Maybe<Scalars["String"]>
  /** Steps of the escalation policy. */
  rules?: Maybe<Array<EscalationRuleType>>
}

/** The connection type for EscalationPolicyType. */
export type EscalationPolicyTypeConnection = {
  __typename?: "EscalationPolicyTypeConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<EscalationPolicyTypeEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<EscalationPolicyType>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type EscalationPolicyTypeEdge = {
  __typename?: "EscalationPolicyTypeEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<EscalationPolicyType>
}

/** Represents an escalation rule */
export type EscalationRuleInput = {
  /** The on-call schedule to notify. */
  oncallScheduleIid: Scalars["ID"]
  /** The time in seconds before the rule is activated. */
  elapsedTimeSeconds: Scalars["Int"]
  /** The status required to prevent the rule from activating. */
  status: EscalationRuleStatus
}

/** Escalation rule statuses */
export enum EscalationRuleStatus {
  /** . */
  Acknowledged = "ACKNOWLEDGED",
  /** . */
  Resolved = "RESOLVED",
}

/** Represents an escalation rule for an escalation policy */
export type EscalationRuleType = {
  __typename?: "EscalationRuleType"
  /** The time in seconds before the rule is activated. */
  elapsedTimeSeconds?: Maybe<Scalars["Int"]>
  /** ID of the escalation policy. */
  id?: Maybe<Scalars["IncidentManagementEscalationRuleID"]>
  /** The on-call schedule to notify. */
  oncallSchedule?: Maybe<IncidentManagementOncallSchedule>
  /** The status required to prevent the rule from activating. */
  status?: Maybe<EscalationRuleStatus>
}

/** Representing an event */
export type Event = {
  __typename?: "Event"
  /** Action of the event. */
  action: EventAction
  /** Author of this event. */
  author: UserCore
  /** When this event was created. */
  createdAt: Scalars["Time"]
  /** ID of the event. */
  id: Scalars["ID"]
  /** When this event was updated. */
  updatedAt: Scalars["Time"]
}

/** Event action */
export enum EventAction {
  /** Created action */
  Created = "CREATED",
  /** Updated action */
  Updated = "UPDATED",
  /** Closed action */
  Closed = "CLOSED",
  /** Reopened action */
  Reopened = "REOPENED",
  /** Pushed action */
  Pushed = "PUSHED",
  /** Commented action */
  Commented = "COMMENTED",
  /** Merged action */
  Merged = "MERGED",
  /** Joined action */
  Joined = "JOINED",
  /** Left action */
  Left = "LEFT",
  /** Destroyed action */
  Destroyed = "DESTROYED",
  /** Expired action */
  Expired = "EXPIRED",
  /** Approved action */
  Approved = "APPROVED",
  /** Archived action */
  Archived = "ARCHIVED",
}

/** The connection type for Event. */
export type EventConnection = {
  __typename?: "EventConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<EventEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<Event>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type EventEdge = {
  __typename?: "EventEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<Event>
}

export type Eventable = {
  /** A list of events associated with the object. */
  events?: Maybe<EventConnection>
}

export type EventableEventsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Autogenerated input type of ExportRequirements */
export type ExportRequirementsInput = {
  /** List requirements by sort order. */
  sort?: Maybe<Sort>
  /** Filter requirements by state. */
  state?: Maybe<RequirementState>
  /** Search query for requirement title. */
  search?: Maybe<Scalars["String"]>
  /** Filter requirements by author username. */
  authorUsername?: Maybe<Array<Scalars["String"]>>
  /** Full project path the requirements are associated with. */
  projectPath: Scalars["ID"]
  /** List of selected requirements fields to be exported. */
  selectedFields?: Maybe<Array<Scalars["String"]>>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of ExportRequirements */
export type ExportRequirementsPayload = {
  __typename?: "ExportRequirementsPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

/** Represents an external issue */
export type ExternalIssue = {
  __typename?: "ExternalIssue"
  /** Timestamp of when the issue was created. */
  createdAt?: Maybe<Scalars["Time"]>
  /** Type of external tracker. */
  externalTracker?: Maybe<Scalars["String"]>
  /** Relative reference of the issue in the external tracker. */
  relativeReference?: Maybe<Scalars["String"]>
  /** Status of the issue in the external tracker. */
  status?: Maybe<Scalars["String"]>
  /** Title of the issue in the external tracker. */
  title?: Maybe<Scalars["String"]>
  /** Timestamp of when the issue was updated. */
  updatedAt?: Maybe<Scalars["Time"]>
  /** URL to the issue in the external tracker. */
  webUrl?: Maybe<Scalars["String"]>
}

export type GeoNode = {
  __typename?: "GeoNode"
  /** The maximum concurrency of container repository sync for this secondary node. */
  containerRepositoriesMaxCapacity?: Maybe<Scalars["Int"]>
  /** Indicates whether this Geo node is enabled. */
  enabled?: Maybe<Scalars["Boolean"]>
  /** The maximum concurrency of LFS/attachment backfill for this secondary node. */
  filesMaxCapacity?: Maybe<Scalars["Int"]>
  /** Find group wiki repository registries on this Geo node. */
  groupWikiRepositoryRegistries?: Maybe<GroupWikiRepositoryRegistryConnection>
  /** ID of this GeoNode. */
  id: Scalars["ID"]
  /** The URL defined on the primary node that secondary nodes should use to contact it. */
  internalUrl?: Maybe<Scalars["String"]>
  /** Find LFS object registries on this Geo node. */
  lfsObjectRegistries?: Maybe<LfsObjectRegistryConnection>
  /** Find merge request diff registries on this Geo node. */
  mergeRequestDiffRegistries?: Maybe<MergeRequestDiffRegistryConnection>
  /** The interval (in days) in which the repository verification is valid. Once expired, it will be reverified. */
  minimumReverificationInterval?: Maybe<Scalars["Int"]>
  /** The unique identifier for this Geo node. */
  name?: Maybe<Scalars["String"]>
  /** Package file registries of the GeoNode. */
  packageFileRegistries?: Maybe<PackageFileRegistryConnection>
  /** Find pipeline artifact registries on this Geo node. */
  pipelineArtifactRegistries?: Maybe<PipelineArtifactRegistryConnection>
  /** Indicates whether this Geo node is the primary. */
  primary?: Maybe<Scalars["Boolean"]>
  /** The maximum concurrency of repository backfill for this secondary node. */
  reposMaxCapacity?: Maybe<Scalars["Int"]>
  /** The namespaces that should be synced, if `selective_sync_type` == `namespaces`. */
  selectiveSyncNamespaces?: Maybe<NamespaceConnection>
  /** The repository storages whose projects should be synced, if `selective_sync_type` == `shards`. */
  selectiveSyncShards?: Maybe<Array<Scalars["String"]>>
  /** Indicates if syncing is limited to only specific groups, or shards. */
  selectiveSyncType?: Maybe<Scalars["String"]>
  /** Find snippet repository registries on this Geo node. */
  snippetRepositoryRegistries?: Maybe<SnippetRepositoryRegistryConnection>
  /** Indicates if this secondary node will replicate blobs in Object Storage. */
  syncObjectStorage?: Maybe<Scalars["Boolean"]>
  /** Find terraform state version registries on this Geo node. */
  terraformStateVersionRegistries?: Maybe<TerraformStateVersionRegistryConnection>
  /** The user-facing URL for this Geo node. */
  url?: Maybe<Scalars["String"]>
  /** The maximum concurrency of repository verification for this secondary node. */
  verificationMaxCapacity?: Maybe<Scalars["Int"]>
}

export type GeoNodeGroupWikiRepositoryRegistriesArgs = {
  ids?: Maybe<Array<Scalars["ID"]>>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type GeoNodeLfsObjectRegistriesArgs = {
  ids?: Maybe<Array<Scalars["ID"]>>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type GeoNodeMergeRequestDiffRegistriesArgs = {
  ids?: Maybe<Array<Scalars["ID"]>>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type GeoNodePackageFileRegistriesArgs = {
  ids?: Maybe<Array<Scalars["ID"]>>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type GeoNodePipelineArtifactRegistriesArgs = {
  ids?: Maybe<Array<Scalars["ID"]>>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type GeoNodeSelectiveSyncNamespacesArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type GeoNodeSnippetRepositoryRegistriesArgs = {
  ids?: Maybe<Array<Scalars["ID"]>>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type GeoNodeTerraformStateVersionRegistriesArgs = {
  ids?: Maybe<Array<Scalars["ID"]>>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Autogenerated input type of GitlabSubscriptionActivate */
export type GitlabSubscriptionActivateInput = {
  /** Activation code received after purchasing a GitLab subscription. */
  activationCode: Scalars["String"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of GitlabSubscriptionActivate */
export type GitlabSubscriptionActivatePayload = {
  __typename?: "GitlabSubscriptionActivatePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The current license. */
  license?: Maybe<CurrentLicense>
}

export type GrafanaIntegration = {
  __typename?: "GrafanaIntegration"
  /** Timestamp of the issue's creation. */
  createdAt: Scalars["Time"]
  /** Indicates whether Grafana integration is enabled. */
  enabled: Scalars["Boolean"]
  /** URL for the Grafana host for the Grafana integration. */
  grafanaUrl: Scalars["String"]
  /** Internal ID of the Grafana integration. */
  id: Scalars["ID"]
  /** Timestamp of the issue's last activity. */
  updatedAt: Scalars["Time"]
}

export type Group = {
  __typename?: "Group"
  /** Size limit for repositories in the namespace in bytes. */
  actualRepositorySizeLimit?: Maybe<Scalars["Float"]>
  /** Additional storage purchased for the root namespace in bytes. */
  additionalPurchasedStorageSize?: Maybe<Scalars["Float"]>
  /** Indicates whether Auto DevOps is enabled for all projects within this group. */
  autoDevopsEnabled?: Maybe<Scalars["Boolean"]>
  /** Avatar URL of the group. */
  avatarUrl?: Maybe<Scalars["String"]>
  /** The number of billable users in the group. */
  billableMembersCount?: Maybe<Scalars["Int"]>
  /** A single board of the group. */
  board?: Maybe<Board>
  /** Boards of the group. */
  boards?: Maybe<BoardConnection>
  /** Represents the code coverage activity for this group. */
  codeCoverageActivities?: Maybe<CodeCoverageActivityConnection>
  /** Compliance frameworks available to projects in this namespace. */
  complianceFrameworks?: Maybe<ComplianceFrameworkConnection>
  /** Container repositories of the group. */
  containerRepositories?: Maybe<ContainerRepositoryConnection>
  /** Number of container repositories in the group. */
  containerRepositoriesCount: Scalars["Int"]
  /** Includes at least one project where the repository size exceeds the limit. */
  containsLockedProjects: Scalars["Boolean"]
  /** Description of the namespace. */
  description?: Maybe<Scalars["String"]>
  /** The GitLab Flavored Markdown rendering of `description` */
  descriptionHtml?: Maybe<Scalars["String"]>
  /** Indicates if a group has email notifications disabled. */
  emailsDisabled?: Maybe<Scalars["Boolean"]>
  /** Find a single epic. */
  epic?: Maybe<Epic>
  /** Find a single epic board. */
  epicBoard?: Maybe<EpicBoard>
  /** Find epic boards. */
  epicBoards?: Maybe<EpicBoardConnection>
  /** Find epics. */
  epics?: Maybe<EpicConnection>
  /** Indicates if Epics are enabled for namespace */
  epicsEnabled?: Maybe<Scalars["Boolean"]>
  /** Full name of the namespace. */
  fullName: Scalars["String"]
  /** Full path of the namespace. */
  fullPath: Scalars["ID"]
  /** A membership of a user within this group. */
  groupMembers?: Maybe<GroupMemberConnection>
  /** ID of the namespace. */
  id: Scalars["ID"]
  /** Status of the temporary storage increase. */
  isTemporaryStorageIncreaseEnabled: Scalars["Boolean"]
  /** Issues for projects in this group. */
  issues?: Maybe<IssueConnection>
  /** Find iteration cadences. */
  iterationCadences?: Maybe<IterationCadenceConnection>
  /** Find iterations. */
  iterations?: Maybe<IterationConnection>
  /** A label available on this group. */
  label?: Maybe<Label>
  /** Labels available on this group. */
  labels?: Maybe<LabelConnection>
  /** Indicates if Large File Storage (LFS) is enabled for namespace. */
  lfsEnabled?: Maybe<Scalars["Boolean"]>
  /** Indicates if a group is disabled from getting mentioned. */
  mentionsDisabled?: Maybe<Scalars["Boolean"]>
  /** Merge requests for projects in this group. */
  mergeRequests?: Maybe<MergeRequestConnection>
  /** Milestones of the group. */
  milestones?: Maybe<MilestoneConnection>
  /** Name of the namespace. */
  name: Scalars["String"]
  /** The package settings for the namespace. */
  packageSettings?: Maybe<PackageSettings>
  /** Packages of the group. */
  packages?: Maybe<PackageConnection>
  /** Parent group. */
  parent?: Maybe<Group>
  /** Path of the namespace. */
  path: Scalars["String"]
  /** The permission level required to create projects in the group. */
  projectCreationLevel?: Maybe<Scalars["String"]>
  /** Projects within this namespace. */
  projects: ProjectConnection
  /** Number of projects in the root namespace where the repository size exceeds the limit. */
  repositorySizeExcessProjectCount: Scalars["Int"]
  /** Indicates if users can request access to namespace. */
  requestAccessEnabled?: Maybe<Scalars["Boolean"]>
  /** Indicates if all users in this group are required to set up two-factor authentication. */
  requireTwoFactorAuthentication?: Maybe<Scalars["Boolean"]>
  /** Aggregated storage statistics of the namespace. Only available for root namespaces. */
  rootStorageStatistics?: Maybe<RootStorageStatistics>
  /** Indicates if sharing a project with another group within this group is prevented. */
  shareWithGroupLock?: Maybe<Scalars["Boolean"]>
  /** Group statistics. */
  stats?: Maybe<GroupStats>
  /** Total storage limit of the root namespace in bytes. */
  storageSizeLimit?: Maybe<Scalars["Float"]>
  /** The permission level required to create subgroups within the group. */
  subgroupCreationLevel?: Maybe<Scalars["String"]>
  /** Date until the temporary storage increase is active. */
  temporaryStorageIncreaseEndsOn?: Maybe<Scalars["Time"]>
  /** Time logged on issues and merge requests in the group and its subgroups. */
  timelogs: TimelogConnection
  /** Total repository size of all projects in the root namespace in bytes. */
  totalRepositorySize?: Maybe<Scalars["Float"]>
  /** Total excess repository size of all projects in the root namespace in bytes. */
  totalRepositorySizeExcess?: Maybe<Scalars["Float"]>
  /** Time before two-factor authentication is enforced. */
  twoFactorGracePeriod?: Maybe<Scalars["Int"]>
  /** Permissions for the current user on the resource */
  userPermissions: GroupPermissions
  /** Visibility of the namespace. */
  visibility?: Maybe<Scalars["String"]>
  /** Vulnerabilities reported on the projects in the group and its subgroups. */
  vulnerabilities?: Maybe<VulnerabilityConnection>
  /** Number of vulnerabilities per day for the projects in the group and its subgroups. */
  vulnerabilitiesCountByDay?: Maybe<VulnerabilitiesCountByDayConnection>
  /**
   * Number of vulnerabilities per severity level, per day, for the projects in the group and its subgroups. Deprecated in 13.3: Use `vulnerabilitiesCountByDay`.
   * @deprecated Use `vulnerabilitiesCountByDay`. Deprecated in 13.3.
   */
  vulnerabilitiesCountByDayAndSeverity?: Maybe<VulnerabilitiesCountByDayAndSeverityConnection>
  /** Represents vulnerable project counts for each grade. */
  vulnerabilityGrades: Array<VulnerableProjectsByGrade>
  /** Vulnerability scanners reported on the project vulnerabilities of the group and its subgroups. */
  vulnerabilityScanners?: Maybe<VulnerabilityScannerConnection>
  /** Counts for each vulnerability severity in the group and its subgroups. */
  vulnerabilitySeveritiesCount?: Maybe<VulnerabilitySeveritiesCount>
  /** Web URL of the group. */
  webUrl: Scalars["String"]
}

export type GroupBoardArgs = {
  id: Scalars["BoardID"]
}

export type GroupBoardsArgs = {
  id?: Maybe<Scalars["BoardID"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type GroupCodeCoverageActivitiesArgs = {
  startDate: Scalars["Date"]
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type GroupComplianceFrameworksArgs = {
  id?: Maybe<Scalars["ComplianceManagementFrameworkID"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type GroupContainerRepositoriesArgs = {
  name?: Maybe<Scalars["String"]>
  sort?: Maybe<ContainerRepositorySort>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type GroupEpicArgs = {
  timeframe?: Maybe<Timeframe>
  iid?: Maybe<Scalars["ID"]>
  iids?: Maybe<Array<Scalars["ID"]>>
  state?: Maybe<EpicState>
  search?: Maybe<Scalars["String"]>
  sort?: Maybe<EpicSort>
  authorUsername?: Maybe<Scalars["String"]>
  labelName?: Maybe<Array<Scalars["String"]>>
  milestoneTitle?: Maybe<Scalars["String"]>
  iidStartsWith?: Maybe<Scalars["String"]>
  includeDescendantGroups?: Maybe<Scalars["Boolean"]>
  confidential?: Maybe<Scalars["Boolean"]>
  myReactionEmoji?: Maybe<Scalars["String"]>
  not?: Maybe<NegatedEpicFilterInput>
}

export type GroupEpicBoardArgs = {
  id: Scalars["BoardsEpicBoardID"]
}

export type GroupEpicBoardsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type GroupEpicsArgs = {
  timeframe?: Maybe<Timeframe>
  iid?: Maybe<Scalars["ID"]>
  iids?: Maybe<Array<Scalars["ID"]>>
  state?: Maybe<EpicState>
  search?: Maybe<Scalars["String"]>
  sort?: Maybe<EpicSort>
  authorUsername?: Maybe<Scalars["String"]>
  labelName?: Maybe<Array<Scalars["String"]>>
  milestoneTitle?: Maybe<Scalars["String"]>
  iidStartsWith?: Maybe<Scalars["String"]>
  includeDescendantGroups?: Maybe<Scalars["Boolean"]>
  confidential?: Maybe<Scalars["Boolean"]>
  myReactionEmoji?: Maybe<Scalars["String"]>
  not?: Maybe<NegatedEpicFilterInput>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type GroupGroupMembersArgs = {
  search?: Maybe<Scalars["String"]>
  relations?: Maybe<Array<GroupMemberRelation>>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type GroupIssuesArgs = {
  iid?: Maybe<Scalars["String"]>
  iids?: Maybe<Array<Scalars["String"]>>
  labelName?: Maybe<Array<Maybe<Scalars["String"]>>>
  milestoneTitle?: Maybe<Array<Maybe<Scalars["String"]>>>
  authorUsername?: Maybe<Scalars["String"]>
  assigneeUsernames?: Maybe<Array<Scalars["String"]>>
  assigneeId?: Maybe<Scalars["String"]>
  createdBefore?: Maybe<Scalars["Time"]>
  createdAfter?: Maybe<Scalars["Time"]>
  updatedBefore?: Maybe<Scalars["Time"]>
  updatedAfter?: Maybe<Scalars["Time"]>
  closedBefore?: Maybe<Scalars["Time"]>
  closedAfter?: Maybe<Scalars["Time"]>
  search?: Maybe<Scalars["String"]>
  types?: Maybe<Array<IssueType>>
  not?: Maybe<NegatedIssueFilterInput>
  state?: Maybe<IssuableState>
  sort?: Maybe<IssueSort>
  iterationId?: Maybe<Array<Maybe<Scalars["ID"]>>>
  iterationWildcardId?: Maybe<IterationWildcardId>
  epicId?: Maybe<Scalars["String"]>
  weight?: Maybe<Scalars["String"]>
  includeSubgroups?: Maybe<Scalars["Boolean"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type GroupIterationCadencesArgs = {
  id?: Maybe<Scalars["IterationsCadenceID"]>
  title?: Maybe<Scalars["String"]>
  durationInWeeks?: Maybe<Scalars["Int"]>
  automatic?: Maybe<Scalars["Boolean"]>
  active?: Maybe<Scalars["Boolean"]>
  includeAncestorGroups?: Maybe<Scalars["Boolean"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type GroupIterationsArgs = {
  timeframe?: Maybe<Timeframe>
  state?: Maybe<IterationState>
  title?: Maybe<Scalars["String"]>
  id?: Maybe<Scalars["ID"]>
  iid?: Maybe<Scalars["ID"]>
  includeAncestors?: Maybe<Scalars["Boolean"]>
  iterationCadenceIds?: Maybe<Array<Scalars["IterationsCadenceID"]>>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type GroupLabelArgs = {
  title: Scalars["String"]
}

export type GroupLabelsArgs = {
  searchTerm?: Maybe<Scalars["String"]>
  includeAncestorGroups?: Maybe<Scalars["Boolean"]>
  includeDescendantGroups?: Maybe<Scalars["Boolean"]>
  onlyGroupLabels?: Maybe<Scalars["Boolean"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type GroupMergeRequestsArgs = {
  iids?: Maybe<Array<Scalars["String"]>>
  sourceBranches?: Maybe<Array<Scalars["String"]>>
  targetBranches?: Maybe<Array<Scalars["String"]>>
  state?: Maybe<MergeRequestState>
  labels?: Maybe<Array<Scalars["String"]>>
  mergedAfter?: Maybe<Scalars["Time"]>
  mergedBefore?: Maybe<Scalars["Time"]>
  milestoneTitle?: Maybe<Scalars["String"]>
  sort?: Maybe<MergeRequestSort>
  not?: Maybe<MergeRequestsResolverNegatedParams>
  includeSubgroups?: Maybe<Scalars["Boolean"]>
  assigneeUsername?: Maybe<Scalars["String"]>
  authorUsername?: Maybe<Scalars["String"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type GroupMilestonesArgs = {
  timeframe?: Maybe<Timeframe>
  ids?: Maybe<Array<Scalars["ID"]>>
  state?: Maybe<MilestoneStateEnum>
  title?: Maybe<Scalars["String"]>
  searchTitle?: Maybe<Scalars["String"]>
  containingDate?: Maybe<Scalars["Time"]>
  includeDescendants?: Maybe<Scalars["Boolean"]>
  includeAncestors?: Maybe<Scalars["Boolean"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type GroupPackagesArgs = {
  sort?: Maybe<PackageGroupSort>
  packageName?: Maybe<Scalars["String"]>
  packageType?: Maybe<PackageTypeEnum>
  status?: Maybe<PackageStatus>
  includeVersionless?: Maybe<Scalars["Boolean"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type GroupProjectsArgs = {
  includeSubgroups?: Maybe<Scalars["Boolean"]>
  search?: Maybe<Scalars["String"]>
  sort?: Maybe<NamespaceProjectSort>
  ids?: Maybe<Array<Scalars["ID"]>>
  hasCodeCoverage?: Maybe<Scalars["Boolean"]>
  hasVulnerabilities?: Maybe<Scalars["Boolean"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type GroupTimelogsArgs = {
  startDate?: Maybe<Scalars["Time"]>
  endDate?: Maybe<Scalars["Time"]>
  startTime?: Maybe<Scalars["Time"]>
  endTime?: Maybe<Scalars["Time"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type GroupVulnerabilitiesArgs = {
  projectId?: Maybe<Array<Scalars["ID"]>>
  reportType?: Maybe<Array<VulnerabilityReportType>>
  severity?: Maybe<Array<VulnerabilitySeverity>>
  state?: Maybe<Array<VulnerabilityState>>
  scanner?: Maybe<Array<Scalars["String"]>>
  scannerId?: Maybe<Array<Scalars["VulnerabilitiesScannerID"]>>
  sort?: Maybe<VulnerabilitySort>
  hasResolution?: Maybe<Scalars["Boolean"]>
  hasIssues?: Maybe<Scalars["Boolean"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type GroupVulnerabilitiesCountByDayArgs = {
  startDate: Scalars["ISO8601Date"]
  endDate: Scalars["ISO8601Date"]
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type GroupVulnerabilitiesCountByDayAndSeverityArgs = {
  startDate: Scalars["ISO8601Date"]
  endDate: Scalars["ISO8601Date"]
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type GroupVulnerabilityGradesArgs = {
  includeSubgroups?: Maybe<Scalars["Boolean"]>
}

export type GroupVulnerabilityScannersArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type GroupVulnerabilitySeveritiesCountArgs = {
  projectId?: Maybe<Array<Scalars["ID"]>>
  reportType?: Maybe<Array<VulnerabilityReportType>>
  severity?: Maybe<Array<VulnerabilitySeverity>>
  state?: Maybe<Array<VulnerabilityState>>
  scanner?: Maybe<Array<Scalars["String"]>>
}

/** Represents a Group Membership */
export type GroupMember = MemberInterface & {
  __typename?: "GroupMember"
  /** GitLab::Access level. */
  accessLevel?: Maybe<AccessLevel>
  /** Date and time the membership was created. */
  createdAt?: Maybe<Scalars["Time"]>
  /** User that authorized membership. */
  createdBy?: Maybe<UserCore>
  /** Date and time the membership expires. */
  expiresAt?: Maybe<Scalars["Time"]>
  /** Group that a User is a member of. */
  group?: Maybe<Group>
  /** ID of the member. */
  id: Scalars["ID"]
  /** Date and time the membership was last updated. */
  updatedAt?: Maybe<Scalars["Time"]>
  /** User that is associated with the member object. */
  user: UserCore
  /** Permissions for the current user on the resource */
  userPermissions: GroupPermissions
}

/** The connection type for GroupMember. */
export type GroupMemberConnection = {
  __typename?: "GroupMemberConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<GroupMemberEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<GroupMember>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type GroupMemberEdge = {
  __typename?: "GroupMemberEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<GroupMember>
}

/** Group member relation */
export enum GroupMemberRelation {
  /** Members in the group itself. */
  Direct = "DIRECT",
  /** Members in the group's ancestor groups. */
  Inherited = "INHERITED",
  /** Members in the group's subgroups. */
  Descendants = "DESCENDANTS",
}

export type GroupPermissions = {
  __typename?: "GroupPermissions"
  /** Indicates the user can perform `read_group` on this resource */
  readGroup: Scalars["Boolean"]
}

/** Contains release-related statistics about a group */
export type GroupReleaseStats = {
  __typename?: "GroupReleaseStats"
  /** Total number of releases in all descendant projects of the group. */
  releasesCount?: Maybe<Scalars["Int"]>
  /** Percentage of the group's descendant projects that have at least one release. */
  releasesPercentage?: Maybe<Scalars["Int"]>
}

/** Contains statistics about a group */
export type GroupStats = {
  __typename?: "GroupStats"
  /** Statistics related to releases within the group. */
  releaseStats?: Maybe<GroupReleaseStats>
}

/** Represents the Geo sync and verification state of a group wiki repository */
export type GroupWikiRepositoryRegistry = {
  __typename?: "GroupWikiRepositoryRegistry"
  /** Timestamp when the GroupWikiRepositoryRegistry was created */
  createdAt?: Maybe<Scalars["Time"]>
  /** ID of the Group Wiki Repository. */
  groupWikiRepositoryId: Scalars["ID"]
  /** ID of the GroupWikiRepositoryRegistry */
  id: Scalars["ID"]
  /** Error message during sync of the GroupWikiRepositoryRegistry */
  lastSyncFailure?: Maybe<Scalars["String"]>
  /** Timestamp of the most recent successful sync of the GroupWikiRepositoryRegistry */
  lastSyncedAt?: Maybe<Scalars["Time"]>
  /** Timestamp after which the GroupWikiRepositoryRegistry should be resynced */
  retryAt?: Maybe<Scalars["Time"]>
  /** Number of consecutive failed sync attempts of the GroupWikiRepositoryRegistry */
  retryCount?: Maybe<Scalars["Int"]>
  /** Sync state of the GroupWikiRepositoryRegistry */
  state?: Maybe<RegistryState>
}

/** The connection type for GroupWikiRepositoryRegistry. */
export type GroupWikiRepositoryRegistryConnection = {
  __typename?: "GroupWikiRepositoryRegistryConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<GroupWikiRepositoryRegistryEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<GroupWikiRepositoryRegistry>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type GroupWikiRepositoryRegistryEdge = {
  __typename?: "GroupWikiRepositoryRegistryEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<GroupWikiRepositoryRegistry>
}

/** Health status of an issue or epic */
export enum HealthStatus {
  OnTrack = "onTrack",
  NeedsAttention = "needsAttention",
  AtRisk = "atRisk",
}

/** Autogenerated input type of HttpIntegrationCreate */
export type HttpIntegrationCreateInput = {
  /** The project to create the integration in. */
  projectPath: Scalars["ID"]
  /** The name of the integration. */
  name: Scalars["String"]
  /** Whether the integration is receiving alerts. */
  active: Scalars["Boolean"]
  /** The example of an alert payload. */
  payloadExample?: Maybe<Scalars["JsonString"]>
  /** The custom mapping of GitLab alert attributes to fields from the payload_example. */
  payloadAttributeMappings?: Maybe<Array<AlertManagementPayloadAlertFieldInput>>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of HttpIntegrationCreate */
export type HttpIntegrationCreatePayload = {
  __typename?: "HttpIntegrationCreatePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The HTTP integration. */
  integration?: Maybe<AlertManagementHttpIntegration>
}

/** Autogenerated input type of HttpIntegrationDestroy */
export type HttpIntegrationDestroyInput = {
  /** The ID of the integration to remove. */
  id: Scalars["AlertManagementHttpIntegrationID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of HttpIntegrationDestroy */
export type HttpIntegrationDestroyPayload = {
  __typename?: "HttpIntegrationDestroyPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The HTTP integration. */
  integration?: Maybe<AlertManagementHttpIntegration>
}

/** Autogenerated input type of HttpIntegrationResetToken */
export type HttpIntegrationResetTokenInput = {
  /** The ID of the integration to mutate. */
  id: Scalars["AlertManagementHttpIntegrationID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of HttpIntegrationResetToken */
export type HttpIntegrationResetTokenPayload = {
  __typename?: "HttpIntegrationResetTokenPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The HTTP integration. */
  integration?: Maybe<AlertManagementHttpIntegration>
}

/** Autogenerated input type of HttpIntegrationUpdate */
export type HttpIntegrationUpdateInput = {
  /** The ID of the integration to mutate. */
  id: Scalars["AlertManagementHttpIntegrationID"]
  /** The name of the integration. */
  name?: Maybe<Scalars["String"]>
  /** Whether the integration is receiving alerts. */
  active?: Maybe<Scalars["Boolean"]>
  /** The example of an alert payload. */
  payloadExample?: Maybe<Scalars["JsonString"]>
  /** The custom mapping of GitLab alert attributes to fields from the payload_example. */
  payloadAttributeMappings?: Maybe<Array<AlertManagementPayloadAlertFieldInput>>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of HttpIntegrationUpdate */
export type HttpIntegrationUpdatePayload = {
  __typename?: "HttpIntegrationUpdatePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The HTTP integration. */
  integration?: Maybe<AlertManagementHttpIntegration>
}

/** Describes an incident management on-call rotation */
export type IncidentManagementOncallRotation = {
  __typename?: "IncidentManagementOncallRotation"
  /** Active period for the on-call rotation. */
  activePeriod?: Maybe<OncallRotationActivePeriodType>
  /** End date and time of the on-call rotation. */
  endsAt?: Maybe<Scalars["Time"]>
  /** ID of the on-call rotation. */
  id: Scalars["IncidentManagementOncallRotationID"]
  /** Length of the on-call schedule, in the units specified by lengthUnit. */
  length?: Maybe<Scalars["Int"]>
  /** Unit of the on-call rotation length. */
  lengthUnit?: Maybe<OncallRotationUnitEnum>
  /** Name of the on-call rotation. */
  name: Scalars["String"]
  /** Participants of the on-call rotation. */
  participants?: Maybe<OncallParticipantTypeConnection>
  /** Blocks of time for which a participant is on-call within a given time frame. Time frame cannot exceed one month. */
  shifts?: Maybe<IncidentManagementOncallShiftConnection>
  /** Start date of the on-call rotation. */
  startsAt?: Maybe<Scalars["Time"]>
}

/** Describes an incident management on-call rotation */
export type IncidentManagementOncallRotationParticipantsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Describes an incident management on-call rotation */
export type IncidentManagementOncallRotationShiftsArgs = {
  startTime: Scalars["Time"]
  endTime: Scalars["Time"]
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** The connection type for IncidentManagementOncallRotation. */
export type IncidentManagementOncallRotationConnection = {
  __typename?: "IncidentManagementOncallRotationConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<IncidentManagementOncallRotationEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<IncidentManagementOncallRotation>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type IncidentManagementOncallRotationEdge = {
  __typename?: "IncidentManagementOncallRotationEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<IncidentManagementOncallRotation>
}

/** Describes an incident management on-call schedule */
export type IncidentManagementOncallSchedule = {
  __typename?: "IncidentManagementOncallSchedule"
  /** Description of the on-call schedule. */
  description?: Maybe<Scalars["String"]>
  /** Internal ID of the on-call schedule. */
  iid: Scalars["ID"]
  /** Name of the on-call schedule. */
  name: Scalars["String"]
  /** On-call rotation for the on-call schedule. */
  rotation?: Maybe<IncidentManagementOncallRotation>
  /** On-call rotations for the on-call schedule. */
  rotations: IncidentManagementOncallRotationConnection
  /** Time zone of the on-call schedule. */
  timezone: Scalars["String"]
}

/** Describes an incident management on-call schedule */
export type IncidentManagementOncallScheduleRotationArgs = {
  id: Scalars["IncidentManagementOncallRotationID"]
}

/** Describes an incident management on-call schedule */
export type IncidentManagementOncallScheduleRotationsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** The connection type for IncidentManagementOncallSchedule. */
export type IncidentManagementOncallScheduleConnection = {
  __typename?: "IncidentManagementOncallScheduleConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<IncidentManagementOncallScheduleEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<IncidentManagementOncallSchedule>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type IncidentManagementOncallScheduleEdge = {
  __typename?: "IncidentManagementOncallScheduleEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<IncidentManagementOncallSchedule>
}

/** A block of time for which a participant is on-call. */
export type IncidentManagementOncallShift = {
  __typename?: "IncidentManagementOncallShift"
  /** End time of the on-call shift. */
  endsAt?: Maybe<Scalars["Time"]>
  /** Participant assigned to the on-call shift. */
  participant?: Maybe<OncallParticipantType>
  /** Start time of the on-call shift. */
  startsAt?: Maybe<Scalars["Time"]>
}

/** The connection type for IncidentManagementOncallShift. */
export type IncidentManagementOncallShiftConnection = {
  __typename?: "IncidentManagementOncallShiftConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<IncidentManagementOncallShiftEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<IncidentManagementOncallShift>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type IncidentManagementOncallShiftEdge = {
  __typename?: "IncidentManagementOncallShiftEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<IncidentManagementOncallShift>
}

export type InstanceSecurityDashboard = {
  __typename?: "InstanceSecurityDashboard"
  /** Projects selected in Instance Security Dashboard. */
  projects: ProjectConnection
  /** Represents vulnerable project counts for each grade. */
  vulnerabilityGrades: Array<VulnerableProjectsByGrade>
  /** Vulnerability scanners reported on the vulnerabilities from projects selected in Instance Security Dashboard. */
  vulnerabilityScanners?: Maybe<VulnerabilityScannerConnection>
  /** Counts for each vulnerability severity from projects selected in Instance Security Dashboard. */
  vulnerabilitySeveritiesCount?: Maybe<VulnerabilitySeveritiesCount>
}

export type InstanceSecurityDashboardProjectsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type InstanceSecurityDashboardVulnerabilityScannersArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type InstanceSecurityDashboardVulnerabilitySeveritiesCountArgs = {
  projectId?: Maybe<Array<Scalars["ID"]>>
  reportType?: Maybe<Array<VulnerabilityReportType>>
  severity?: Maybe<Array<VulnerabilitySeverity>>
  state?: Maybe<Array<VulnerabilityState>>
  scanner?: Maybe<Array<Scalars["String"]>>
}

/** Represents an issuable. */
export type Issuable = Epic | Issue | MergeRequest

/** Incident severity */
export enum IssuableSeverity {
  /** Unknown severity */
  Unknown = "UNKNOWN",
  /** Low severity */
  Low = "LOW",
  /** Medium severity */
  Medium = "MEDIUM",
  /** High severity */
  High = "HIGH",
  /** Critical severity */
  Critical = "CRITICAL",
}

/** State of a GitLab issue or merge request */
export enum IssuableState {
  /** In open state. */
  Opened = "opened",
  /** In closed state. */
  Closed = "closed",
  /** Discussion has been locked. */
  Locked = "locked",
  /** All available. */
  All = "all",
}

export type Issue = Noteable &
  CurrentUserTodos & {
    __typename?: "Issue"
    /** Alert associated to this issue. */
    alertManagementAlert?: Maybe<AlertManagementAlert>
    /** Assignees of the issue. */
    assignees?: Maybe<UserCoreConnection>
    /** User that created the issue. */
    author: UserCore
    /** Indicates the issue is blocked. */
    blocked: Scalars["Boolean"]
    /** Count of issues blocking this issue. */
    blockedByCount?: Maybe<Scalars["Int"]>
    /** Issues blocking this issue. */
    blockedByIssues?: Maybe<IssueConnection>
    /** Timestamp of when the issue was closed. */
    closedAt?: Maybe<Scalars["Time"]>
    /** Indicates the issue is confidential. */
    confidential: Scalars["Boolean"]
    /** User specific email address for the issue. */
    createNoteEmail?: Maybe<Scalars["String"]>
    /** Timestamp of when the issue was created. */
    createdAt: Scalars["Time"]
    /** To-do items for the current user. */
    currentUserTodos: TodoConnection
    /** Description of the issue. */
    description?: Maybe<Scalars["String"]>
    /** The GitLab Flavored Markdown rendering of `description` */
    descriptionHtml?: Maybe<Scalars["String"]>
    /** Collection of design images associated with this issue. */
    designCollection?: Maybe<DesignCollection>
    /** Indicates discussion is locked on the issue. */
    discussionLocked: Scalars["Boolean"]
    /** All discussions on this noteable. */
    discussions: DiscussionConnection
    /** Number of downvotes the issue has received. */
    downvotes: Scalars["Int"]
    /** Due date of the issue. */
    dueDate?: Maybe<Scalars["Time"]>
    /** Indicates if a project has email notifications disabled: `true` if email notifications are disabled. */
    emailsDisabled: Scalars["Boolean"]
    /** Epic to which this issue belongs. */
    epic?: Maybe<Epic>
    /** Current health status. */
    healthStatus?: Maybe<HealthStatus>
    /** Human-readable time estimate of the issue. */
    humanTimeEstimate?: Maybe<Scalars["String"]>
    /** Human-readable total time reported as spent on the issue. */
    humanTotalTimeSpent?: Maybe<Scalars["String"]>
    /** ID of the issue. */
    id: Scalars["ID"]
    /** Internal ID of the issue. */
    iid: Scalars["ID"]
    /** Iteration of the issue. */
    iteration?: Maybe<Iteration>
    /** Labels of the issue. */
    labels?: Maybe<LabelConnection>
    /** Metric images associated to the issue. */
    metricImages?: Maybe<Array<MetricImage>>
    /** Milestone of the issue. */
    milestone?: Maybe<Milestone>
    /** Indicates if issue got moved from other project. */
    moved?: Maybe<Scalars["Boolean"]>
    /** Updated Issue after it got moved to another project. */
    movedTo?: Maybe<Issue>
    /** All notes on this noteable. */
    notes: NoteConnection
    /** List of participants in the issue. */
    participants?: Maybe<UserCoreConnection>
    /** Internal reference of the issue. Returned in shortened format by default. */
    reference: Scalars["String"]
    /** Relative position of the issue (used for positioning in epic tree and issue boards). */
    relativePosition?: Maybe<Scalars["Int"]>
    /** Severity level of the incident. */
    severity?: Maybe<IssuableSeverity>
    /** Timestamp of when the issue SLA expires. */
    slaDueAt?: Maybe<Scalars["Time"]>
    /** State of the issue. */
    state: IssueState
    /** Indicates whether an issue is published to the status page. */
    statusPagePublishedIncident?: Maybe<Scalars["Boolean"]>
    /** Indicates the currently logged in user is subscribed to the issue. */
    subscribed: Scalars["Boolean"]
    /** Task completion status of the issue. */
    taskCompletionStatus: TaskCompletionStatus
    /** Time estimate of the issue. */
    timeEstimate: Scalars["Int"]
    /** Timelogs on the issue. */
    timelogs: TimelogConnection
    /** Title of the issue. */
    title: Scalars["String"]
    /** The GitLab Flavored Markdown rendering of `title` */
    titleHtml?: Maybe<Scalars["String"]>
    /** Total time reported as spent on the issue. */
    totalTimeSpent: Scalars["Int"]
    /** Type of the issue. */
    type?: Maybe<IssueType>
    /** Timestamp of when the issue was last updated. */
    updatedAt: Scalars["Time"]
    /** User that last updated the issue. */
    updatedBy?: Maybe<UserCore>
    /** Number of upvotes the issue has received. */
    upvotes: Scalars["Int"]
    /** Number of user discussions in the issue. */
    userDiscussionsCount: Scalars["Int"]
    /** Number of user notes of the issue. */
    userNotesCount: Scalars["Int"]
    /** Permissions for the current user on the resource */
    userPermissions: IssuePermissions
    /** Web path of the issue. */
    webPath: Scalars["String"]
    /** Web URL of the issue. */
    webUrl: Scalars["String"]
    /** Weight of the issue. */
    weight?: Maybe<Scalars["Int"]>
  }

export type IssueAssigneesArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type IssueBlockedByIssuesArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type IssueCurrentUserTodosArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
  state?: Maybe<TodoStateEnum>
}

export type IssueDiscussionsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type IssueLabelsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type IssueNotesArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type IssueParticipantsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type IssueReferenceArgs = {
  full?: Maybe<Scalars["Boolean"]>
}

export type IssueTimelogsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** The connection type for Issue. */
export type IssueConnection = {
  __typename?: "IssueConnection"
  /** Total count of collection. */
  count: Scalars["Int"]
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<IssueEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<Issue>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
  /** Total weight of issues collection. */
  weight: Scalars["Int"]
}

/** An edge in a connection. */
export type IssueEdge = {
  __typename?: "IssueEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<Issue>
}

/** Autogenerated input type of IssueMove */
export type IssueMoveInput = {
  /** The project the issue to mutate is in. */
  projectPath: Scalars["ID"]
  /** The IID of the issue to mutate. */
  iid: Scalars["String"]
  /** The project to move the issue to. */
  targetProjectPath: Scalars["ID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated input type of IssueMoveList */
export type IssueMoveListInput = {
  /** Project the issue to mutate is in. */
  projectPath: Scalars["ID"]
  /** IID of the issue to mutate. */
  iid: Scalars["String"]
  /** Global ID of the board that the issue is in. */
  boardId: Scalars["BoardID"]
  /** ID of the board list that the issue will be moved from. */
  fromListId?: Maybe<Scalars["ID"]>
  /** ID of the board list that the issue will be moved to. */
  toListId?: Maybe<Scalars["ID"]>
  /** ID of issue that should be placed before the current issue. */
  moveBeforeId?: Maybe<Scalars["ID"]>
  /** ID of issue that should be placed after the current issue. */
  moveAfterId?: Maybe<Scalars["ID"]>
  /** The ID of the parent epic. NULL when removing the association. */
  epicId?: Maybe<Scalars["EpicID"]>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of IssueMoveList */
export type IssueMoveListPayload = {
  __typename?: "IssueMoveListPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The issue after mutation. */
  issue?: Maybe<Issue>
}

/** Autogenerated return type of IssueMove */
export type IssueMovePayload = {
  __typename?: "IssueMovePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The issue after mutation. */
  issue?: Maybe<Issue>
}

/** Check permissions for the current user on a issue */
export type IssuePermissions = {
  __typename?: "IssuePermissions"
  /** Indicates the user can perform `admin_issue` on this resource */
  adminIssue: Scalars["Boolean"]
  /** Indicates the user can perform `create_design` on this resource */
  createDesign: Scalars["Boolean"]
  /** Indicates the user can perform `create_note` on this resource */
  createNote: Scalars["Boolean"]
  /** Indicates the user can perform `destroy_design` on this resource */
  destroyDesign: Scalars["Boolean"]
  /** Indicates the user can perform `read_design` on this resource */
  readDesign: Scalars["Boolean"]
  /** Indicates the user can perform `read_issue` on this resource */
  readIssue: Scalars["Boolean"]
  /** Indicates the user can perform `reopen_issue` on this resource */
  reopenIssue: Scalars["Boolean"]
  /** Indicates the user can perform `update_issue` on this resource */
  updateIssue: Scalars["Boolean"]
}

/** Autogenerated input type of IssueSetAssignees */
export type IssueSetAssigneesInput = {
  /** The project the issue to mutate is in. */
  projectPath: Scalars["ID"]
  /** The IID of the issue to mutate. */
  iid: Scalars["String"]
  /** The usernames to assign to the resource. Replaces existing assignees by default. */
  assigneeUsernames: Array<Scalars["String"]>
  /** The operation to perform. Defaults to REPLACE. */
  operationMode?: Maybe<MutationOperationMode>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of IssueSetAssignees */
export type IssueSetAssigneesPayload = {
  __typename?: "IssueSetAssigneesPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The issue after mutation. */
  issue?: Maybe<Issue>
}

/** Autogenerated input type of IssueSetConfidential */
export type IssueSetConfidentialInput = {
  /** The project the issue to mutate is in. */
  projectPath: Scalars["ID"]
  /** The IID of the issue to mutate. */
  iid: Scalars["String"]
  /** Whether or not to set the issue as a confidential. */
  confidential: Scalars["Boolean"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of IssueSetConfidential */
export type IssueSetConfidentialPayload = {
  __typename?: "IssueSetConfidentialPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The issue after mutation. */
  issue?: Maybe<Issue>
}

/** Autogenerated input type of IssueSetDueDate */
export type IssueSetDueDateInput = {
  /** The project the issue to mutate is in. */
  projectPath: Scalars["ID"]
  /** The IID of the issue to mutate. */
  iid: Scalars["String"]
  /** The desired due date for the issue, due date will be removed if absent or set to null */
  dueDate?: Maybe<Scalars["Time"]>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of IssueSetDueDate */
export type IssueSetDueDatePayload = {
  __typename?: "IssueSetDueDatePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The issue after mutation. */
  issue?: Maybe<Issue>
}

/** Autogenerated input type of IssueSetEpic */
export type IssueSetEpicInput = {
  /** The project the issue to mutate is in. */
  projectPath: Scalars["ID"]
  /** The IID of the issue to mutate. */
  iid: Scalars["String"]
  /** Global ID of the epic to be assigned to the issue, epic will be removed if absent or set to null */
  epicId?: Maybe<Scalars["EpicID"]>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of IssueSetEpic */
export type IssueSetEpicPayload = {
  __typename?: "IssueSetEpicPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The issue after mutation. */
  issue?: Maybe<Issue>
}

/** Autogenerated input type of IssueSetIteration */
export type IssueSetIterationInput = {
  /** The project the issue to mutate is in. */
  projectPath: Scalars["ID"]
  /** The IID of the issue to mutate. */
  iid: Scalars["String"]
  /** The iteration to assign to the issue. */
  iterationId?: Maybe<Scalars["IterationID"]>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of IssueSetIteration */
export type IssueSetIterationPayload = {
  __typename?: "IssueSetIterationPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The issue after mutation. */
  issue?: Maybe<Issue>
}

/** Autogenerated input type of IssueSetLocked */
export type IssueSetLockedInput = {
  /** The project the issue to mutate is in. */
  projectPath: Scalars["ID"]
  /** The IID of the issue to mutate. */
  iid: Scalars["String"]
  /** Whether or not to lock discussion on the issue. */
  locked: Scalars["Boolean"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of IssueSetLocked */
export type IssueSetLockedPayload = {
  __typename?: "IssueSetLockedPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The issue after mutation. */
  issue?: Maybe<Issue>
}

/** Autogenerated input type of IssueSetSeverity */
export type IssueSetSeverityInput = {
  /** The project the issue to mutate is in. */
  projectPath: Scalars["ID"]
  /** The IID of the issue to mutate. */
  iid: Scalars["String"]
  /** Set the incident severity level. */
  severity: IssuableSeverity
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of IssueSetSeverity */
export type IssueSetSeverityPayload = {
  __typename?: "IssueSetSeverityPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The issue after mutation. */
  issue?: Maybe<Issue>
}

/** Autogenerated input type of IssueSetSubscription */
export type IssueSetSubscriptionInput = {
  /** The desired state of the subscription. */
  subscribedState: Scalars["Boolean"]
  /** The project the issue to mutate is in. */
  projectPath: Scalars["ID"]
  /** The IID of the issue to mutate. */
  iid: Scalars["String"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of IssueSetSubscription */
export type IssueSetSubscriptionPayload = {
  __typename?: "IssueSetSubscriptionPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The issue after mutation. */
  issue?: Maybe<Issue>
}

/** Autogenerated input type of IssueSetWeight */
export type IssueSetWeightInput = {
  /** The project the issue to mutate is in. */
  projectPath: Scalars["ID"]
  /** The IID of the issue to mutate. */
  iid: Scalars["String"]
  /** The desired weight for the issue. */
  weight: Scalars["Int"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of IssueSetWeight */
export type IssueSetWeightPayload = {
  __typename?: "IssueSetWeightPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The issue after mutation. */
  issue?: Maybe<Issue>
}

/** Values for sorting issues */
export enum IssueSort {
  /** Updated at descending order. */
  UpdatedDesc = "UPDATED_DESC",
  /** Updated at ascending order. */
  UpdatedAsc = "UPDATED_ASC",
  /** Created at descending order. */
  CreatedDesc = "CREATED_DESC",
  /** Created at ascending order. */
  CreatedAsc = "CREATED_ASC",
  /** Priority by ascending order. */
  PriorityAsc = "PRIORITY_ASC",
  /** Priority by descending order. */
  PriorityDesc = "PRIORITY_DESC",
  /** Label priority by ascending order. */
  LabelPriorityAsc = "LABEL_PRIORITY_ASC",
  /** Label priority by descending order. */
  LabelPriorityDesc = "LABEL_PRIORITY_DESC",
  /** Milestone due date by ascending order. */
  MilestoneDueAsc = "MILESTONE_DUE_ASC",
  /** Milestone due date by descending order. */
  MilestoneDueDesc = "MILESTONE_DUE_DESC",
  /** Due date by ascending order. */
  DueDateAsc = "DUE_DATE_ASC",
  /** Due date by descending order. */
  DueDateDesc = "DUE_DATE_DESC",
  /** Relative position by ascending order. */
  RelativePositionAsc = "RELATIVE_POSITION_ASC",
  /** Severity from less critical to more critical. */
  SeverityAsc = "SEVERITY_ASC",
  /** Severity from more critical to less critical. */
  SeverityDesc = "SEVERITY_DESC",
  /** Weight by ascending order. */
  WeightAsc = "WEIGHT_ASC",
  /** Weight by descending order. */
  WeightDesc = "WEIGHT_DESC",
  /** Published issues shown last. */
  PublishedAsc = "PUBLISHED_ASC",
  /** Published issues shown first. */
  PublishedDesc = "PUBLISHED_DESC",
  /** Issues with earliest SLA due time shown first. */
  SlaDueAtAsc = "SLA_DUE_AT_ASC",
  /** Issues with latest SLA due time shown first. */
  SlaDueAtDesc = "SLA_DUE_AT_DESC",
}

/** State of a GitLab issue */
export enum IssueState {
  /** In open state. */
  Opened = "opened",
  /** In closed state. */
  Closed = "closed",
  /** Discussion has been locked. */
  Locked = "locked",
  /** All available. */
  All = "all",
}

/** Values for issue state events */
export enum IssueStateEvent {
  /** Reopens the issue. */
  Reopen = "REOPEN",
  /** Closes the issue. */
  Close = "CLOSE",
}

/** Represents total number of issues for the represented statuses */
export type IssueStatusCountsType = {
  __typename?: "IssueStatusCountsType"
  /** Number of issues with status ALL for the project */
  all?: Maybe<Scalars["Int"]>
  /** Number of issues with status CLOSED for the project */
  closed?: Maybe<Scalars["Int"]>
  /** Number of issues with status OPENED for the project */
  opened?: Maybe<Scalars["Int"]>
}

/** Issue type */
export enum IssueType {
  /** Issue issue type */
  Issue = "ISSUE",
  /** Incident issue type */
  Incident = "INCIDENT",
  /** Test Case issue type */
  TestCase = "TEST_CASE",
  /** Requirement issue type */
  Requirement = "REQUIREMENT",
}

/** Represents an iteration object */
export type Iteration = TimeboxReportInterface & {
  __typename?: "Iteration"
  /** Timestamp of iteration creation. */
  createdAt: Scalars["Time"]
  /** Description of the iteration. */
  description?: Maybe<Scalars["String"]>
  /** The GitLab Flavored Markdown rendering of `description` */
  descriptionHtml?: Maybe<Scalars["String"]>
  /** Timestamp of the iteration due date. */
  dueDate?: Maybe<Scalars["Time"]>
  /** ID of the iteration. */
  id: Scalars["ID"]
  /** Internal ID of the iteration. */
  iid: Scalars["ID"]
  /** Cadence of the iteration. */
  iterationCadence: IterationCadence
  /** Historically accurate report about the timebox. */
  report?: Maybe<TimeboxReport>
  /** Web path of the iteration, scoped to the query parent. Only valid for Project parents. Returns null in other contexts. */
  scopedPath?: Maybe<Scalars["String"]>
  /** Web URL of the iteration, scoped to the query parent. Only valid for Project parents. Returns null in other contexts. */
  scopedUrl?: Maybe<Scalars["String"]>
  /** Timestamp of the iteration start date. */
  startDate?: Maybe<Scalars["Time"]>
  /** State of the iteration. */
  state: IterationState
  /** Title of the iteration. */
  title: Scalars["String"]
  /** Timestamp of last iteration update. */
  updatedAt: Scalars["Time"]
  /** Web path of the iteration. */
  webPath: Scalars["String"]
  /** Web URL of the iteration. */
  webUrl: Scalars["String"]
}

/** Represents an iteration cadence */
export type IterationCadence = {
  __typename?: "IterationCadence"
  /** Whether the iteration cadence is active. */
  active?: Maybe<Scalars["Boolean"]>
  /** Whether the iteration cadence should automatically generate future iterations. */
  automatic?: Maybe<Scalars["Boolean"]>
  /** Description of the iteration cadence. Maximum length is 5000 characters. */
  description?: Maybe<Scalars["String"]>
  /** Duration in weeks of the iterations within this cadence. */
  durationInWeeks?: Maybe<Scalars["Int"]>
  /** Global ID of the iteration cadence. */
  id: Scalars["IterationsCadenceID"]
  /** Future iterations to be created when iteration cadence is set to automatic. */
  iterationsInAdvance?: Maybe<Scalars["Int"]>
  /** Whether the iteration cadence should roll over issues to the next iteration or not. */
  rollOver: Scalars["Boolean"]
  /** Timestamp of the iteration cadence start date. */
  startDate?: Maybe<Scalars["Time"]>
  /** Title of the iteration cadence. */
  title: Scalars["String"]
}

/** The connection type for IterationCadence. */
export type IterationCadenceConnection = {
  __typename?: "IterationCadenceConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<IterationCadenceEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<IterationCadence>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** Autogenerated input type of IterationCadenceCreate */
export type IterationCadenceCreateInput = {
  /** The group where the iteration cadence is created. */
  groupPath: Scalars["ID"]
  /** Title of the iteration cadence. */
  title?: Maybe<Scalars["String"]>
  /** Duration in weeks of the iterations within this cadence. */
  durationInWeeks?: Maybe<Scalars["Int"]>
  /** Future iterations to be created when iteration cadence is set to automatic. */
  iterationsInAdvance?: Maybe<Scalars["Int"]>
  /** Timestamp of the iteration cadence start date. */
  startDate?: Maybe<Scalars["Time"]>
  /** Whether the iteration cadence should automatically generate future iterations. */
  automatic: Scalars["Boolean"]
  /** Whether the iteration cadence is active. */
  active: Scalars["Boolean"]
  /** Whether the iteration cadence should roll over issues to the next iteration or not. */
  rollOver?: Maybe<Scalars["Boolean"]>
  /** Description of the iteration cadence. Maximum length is 5000 characters. */
  description?: Maybe<Scalars["String"]>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of IterationCadenceCreate */
export type IterationCadenceCreatePayload = {
  __typename?: "IterationCadenceCreatePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The created iteration cadence. */
  iterationCadence?: Maybe<IterationCadence>
}

/** Autogenerated input type of IterationCadenceDestroy */
export type IterationCadenceDestroyInput = {
  /** Global ID of the iteration cadence. */
  id: Scalars["IterationsCadenceID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of IterationCadenceDestroy */
export type IterationCadenceDestroyPayload = {
  __typename?: "IterationCadenceDestroyPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** Group the iteration cadence belongs to. */
  group: Group
}

/** An edge in a connection. */
export type IterationCadenceEdge = {
  __typename?: "IterationCadenceEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<IterationCadence>
}

/** Autogenerated input type of IterationCadenceUpdate */
export type IterationCadenceUpdateInput = {
  /** Global ID of the iteration cadence. */
  id: Scalars["IterationsCadenceID"]
  /** Title of the iteration cadence. */
  title?: Maybe<Scalars["String"]>
  /** Duration in weeks of the iterations within this cadence. */
  durationInWeeks?: Maybe<Scalars["Int"]>
  /** Future iterations to be created when iteration cadence is set to automatic. */
  iterationsInAdvance?: Maybe<Scalars["Int"]>
  /** Timestamp of the iteration cadence start date. */
  startDate?: Maybe<Scalars["Time"]>
  /** Whether the iteration cadence should automatically generate future iterations. */
  automatic?: Maybe<Scalars["Boolean"]>
  /** Whether the iteration cadence is active. */
  active?: Maybe<Scalars["Boolean"]>
  /** Whether the iteration cadence should roll over issues to the next iteration or not. */
  rollOver?: Maybe<Scalars["Boolean"]>
  /** Description of the iteration cadence. Maximum length is 5000 characters. */
  description?: Maybe<Scalars["String"]>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of IterationCadenceUpdate */
export type IterationCadenceUpdatePayload = {
  __typename?: "IterationCadenceUpdatePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The updated iteration cadence. */
  iterationCadence?: Maybe<IterationCadence>
}

/** The connection type for Iteration. */
export type IterationConnection = {
  __typename?: "IterationConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<IterationEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<Iteration>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** Autogenerated input type of IterationDelete */
export type IterationDeleteInput = {
  /** ID of the iteration. */
  id: Scalars["IterationID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of IterationDelete */
export type IterationDeletePayload = {
  __typename?: "IterationDeletePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** Group the iteration belongs to. */
  group: Group
}

/** An edge in a connection. */
export type IterationEdge = {
  __typename?: "IterationEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<Iteration>
}

/** State of a GitLab iteration */
export enum IterationState {
  Upcoming = "upcoming",
  Started = "started",
  Opened = "opened",
  Closed = "closed",
  All = "all",
}

/** Iteration ID wildcard values */
export enum IterationWildcardId {
  /** No iteration is assigned. */
  None = "NONE",
  /** An iteration is assigned. */
  Any = "ANY",
  /** Current iteration. */
  Current = "CURRENT",
}

export type JiraImport = {
  __typename?: "JiraImport"
  /** Timestamp of when the Jira import was created. */
  createdAt?: Maybe<Scalars["Time"]>
  /** Count of issues that failed to import. */
  failedToImportCount: Scalars["Int"]
  /** Count of issues that were successfully imported. */
  importedIssuesCount: Scalars["Int"]
  /** Project key for the imported Jira project. */
  jiraProjectKey: Scalars["String"]
  /** Timestamp of when the Jira import was scheduled. */
  scheduledAt?: Maybe<Scalars["Time"]>
  /** User that started the Jira import. */
  scheduledBy?: Maybe<UserCore>
  /** Total count of issues that were attempted to import. */
  totalIssueCount: Scalars["Int"]
}

/** The connection type for JiraImport. */
export type JiraImportConnection = {
  __typename?: "JiraImportConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<JiraImportEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<JiraImport>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type JiraImportEdge = {
  __typename?: "JiraImportEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<JiraImport>
}

/** Autogenerated input type of JiraImportStart */
export type JiraImportStartInput = {
  /** The project to import the Jira project into. */
  projectPath: Scalars["ID"]
  /** Project key of the importer Jira project. */
  jiraProjectKey: Scalars["String"]
  /** Project name of the importer Jira project. */
  jiraProjectName?: Maybe<Scalars["String"]>
  /** The mapping of Jira to GitLab users. */
  usersMapping?: Maybe<Array<JiraUsersMappingInputType>>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of JiraImportStart */
export type JiraImportStartPayload = {
  __typename?: "JiraImportStartPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The Jira import data after mutation. */
  jiraImport?: Maybe<JiraImport>
}

/** Autogenerated input type of JiraImportUsers */
export type JiraImportUsersInput = {
  /** The project to import the Jira users into. */
  projectPath: Scalars["ID"]
  /** The index of the record the import should started at, default 0 (50 records returned). */
  startAt?: Maybe<Scalars["Int"]>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of JiraImportUsers */
export type JiraImportUsersPayload = {
  __typename?: "JiraImportUsersPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** Users returned from Jira, matched by email and name if possible. */
  jiraUsers?: Maybe<Array<JiraUser>>
}

export type JiraProject = {
  __typename?: "JiraProject"
  /** Key of the Jira project. */
  key: Scalars["String"]
  /** Name of the Jira project. */
  name?: Maybe<Scalars["String"]>
  /** ID of the Jira project. */
  projectId: Scalars["Int"]
}

/** The connection type for JiraProject. */
export type JiraProjectConnection = {
  __typename?: "JiraProjectConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<JiraProjectEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<JiraProject>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type JiraProjectEdge = {
  __typename?: "JiraProjectEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<JiraProject>
}

export type JiraService = Service & {
  __typename?: "JiraService"
  /** Indicates if the service is active. */
  active?: Maybe<Scalars["Boolean"]>
  /** List of all Jira projects fetched through Jira REST API. */
  projects?: Maybe<JiraProjectConnection>
  /** Class name of the service. */
  type?: Maybe<Scalars["String"]>
}

export type JiraServiceProjectsArgs = {
  name?: Maybe<Scalars["String"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type JiraUser = {
  __typename?: "JiraUser"
  /** ID of the matched GitLab user. */
  gitlabId?: Maybe<Scalars["Int"]>
  /** Name of the matched GitLab user. */
  gitlabName?: Maybe<Scalars["String"]>
  /** Username of the matched GitLab user. */
  gitlabUsername?: Maybe<Scalars["String"]>
  /** Account ID of the Jira user. */
  jiraAccountId: Scalars["String"]
  /** Display name of the Jira user. */
  jiraDisplayName: Scalars["String"]
  /** Email of the Jira user, returned only for users with public emails. */
  jiraEmail?: Maybe<Scalars["String"]>
}

export type JiraUsersMappingInputType = {
  /** Jira account ID of the user. */
  jiraAccountId: Scalars["String"]
  /** ID of the GitLab user. */
  gitlabId?: Maybe<Scalars["Int"]>
}

export enum JobArtifactFileType {
  /** ARCHIVE job artifact file type. */
  Archive = "ARCHIVE",
  /** METADATA job artifact file type. */
  Metadata = "METADATA",
  /** TRACE job artifact file type. */
  Trace = "TRACE",
  /** JUNIT job artifact file type. */
  Junit = "JUNIT",
  /** METRICS job artifact file type. */
  Metrics = "METRICS",
  /** METRICS REFEREE job artifact file type. */
  MetricsReferee = "METRICS_REFEREE",
  /** NETWORK REFEREE job artifact file type. */
  NetworkReferee = "NETWORK_REFEREE",
  /** DOTENV job artifact file type. */
  Dotenv = "DOTENV",
  /** COBERTURA job artifact file type. */
  Cobertura = "COBERTURA",
  /** CLUSTER APPLICATIONS job artifact file type. */
  ClusterApplications = "CLUSTER_APPLICATIONS",
  /** LSIF job artifact file type. */
  Lsif = "LSIF",
  /** SAST job artifact file type. */
  Sast = "SAST",
  /** SECRET DETECTION job artifact file type. */
  SecretDetection = "SECRET_DETECTION",
  /** DEPENDENCY SCANNING job artifact file type. */
  DependencyScanning = "DEPENDENCY_SCANNING",
  /** CONTAINER SCANNING job artifact file type. */
  ContainerScanning = "CONTAINER_SCANNING",
  /** DAST job artifact file type. */
  Dast = "DAST",
  /** LICENSE MANAGEMENT job artifact file type. */
  LicenseManagement = "LICENSE_MANAGEMENT",
  /** LICENSE SCANNING job artifact file type. */
  LicenseScanning = "LICENSE_SCANNING",
  /** ACCESSIBILITY job artifact file type. */
  Accessibility = "ACCESSIBILITY",
  /** CODE QUALITY job artifact file type. */
  Codequality = "CODEQUALITY",
  /** PERFORMANCE job artifact file type. */
  Performance = "PERFORMANCE",
  /** BROWSER PERFORMANCE job artifact file type. */
  BrowserPerformance = "BROWSER_PERFORMANCE",
  /** LOAD PERFORMANCE job artifact file type. */
  LoadPerformance = "LOAD_PERFORMANCE",
  /** TERRAFORM job artifact file type. */
  Terraform = "TERRAFORM",
  /** REQUIREMENTS job artifact file type. */
  Requirements = "REQUIREMENTS",
  /** COVERAGE FUZZING job artifact file type. */
  CoverageFuzzing = "COVERAGE_FUZZING",
  /** API FUZZING job artifact file type. */
  ApiFuzzing = "API_FUZZING",
}

export type JobPermissions = {
  __typename?: "JobPermissions"
  /** Indicates the user can perform `read_build` on this resource */
  readBuild: Scalars["Boolean"]
  /** Indicates the user can perform `read_job_artifacts` on this resource */
  readJobArtifacts: Scalars["Boolean"]
  /** Indicates the user can perform `update_build` on this resource */
  updateBuild: Scalars["Boolean"]
}

/** Autogenerated input type of JobPlay */
export type JobPlayInput = {
  /** The ID of the job to mutate. */
  id: Scalars["CiBuildID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of JobPlay */
export type JobPlayPayload = {
  __typename?: "JobPlayPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The job after the mutation. */
  job?: Maybe<CiJob>
}

/** Autogenerated input type of JobRetry */
export type JobRetryInput = {
  /** The ID of the job to mutate. */
  id: Scalars["CiBuildID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of JobRetry */
export type JobRetryPayload = {
  __typename?: "JobRetryPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The job after the mutation. */
  job?: Maybe<CiJob>
}

export type Kas = {
  __typename?: "Kas"
  /** Indicates whether the Kubernetes Agent Server is enabled. */
  enabled: Scalars["Boolean"]
  /** The URL used by the Agents to communicate with KAS. */
  externalUrl?: Maybe<Scalars["String"]>
  /** KAS version. */
  version?: Maybe<Scalars["String"]>
}

export type Label = {
  __typename?: "Label"
  /** Background color of the label. */
  color: Scalars["String"]
  /** When this label was created. */
  createdAt: Scalars["Time"]
  /** Description of the label (Markdown rendered as HTML for caching). */
  description?: Maybe<Scalars["String"]>
  /** The GitLab Flavored Markdown rendering of `description` */
  descriptionHtml?: Maybe<Scalars["String"]>
  /** Label ID. */
  id: Scalars["ID"]
  /** Text color of the label. */
  textColor: Scalars["String"]
  /** Content of the label. */
  title: Scalars["String"]
  /** When this label was last updated. */
  updatedAt: Scalars["Time"]
}

/** The connection type for Label. */
export type LabelConnection = {
  __typename?: "LabelConnection"
  /** Total count of collection. */
  count: Scalars["Int"]
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<LabelEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<Label>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** Autogenerated input type of LabelCreate */
export type LabelCreateInput = {
  /** Full path of the project with which the resource is associated. */
  projectPath?: Maybe<Scalars["ID"]>
  /** Full path of the group with which the resource is associated. */
  groupPath?: Maybe<Scalars["ID"]>
  /** Title of the label. */
  title: Scalars["String"]
  /** Description of the label. */
  description?: Maybe<Scalars["String"]>
  /**
   * The color of the label given in 6-digit hex notation with leading '#' sign
   * (for example, `#FFAABB`) or one of the CSS color names.
   */
  color?: Maybe<Scalars["String"]>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of LabelCreate */
export type LabelCreatePayload = {
  __typename?: "LabelCreatePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The label after mutation. */
  label?: Maybe<Label>
}

/** An edge in a connection. */
export type LabelEdge = {
  __typename?: "LabelEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<Label>
}

/** Represents the Geo sync and verification state of an LFS object */
export type LfsObjectRegistry = {
  __typename?: "LfsObjectRegistry"
  /** Timestamp when the LfsObjectRegistry was created */
  createdAt?: Maybe<Scalars["Time"]>
  /** ID of the LfsObjectRegistry */
  id: Scalars["ID"]
  /** Error message during sync of the LfsObjectRegistry */
  lastSyncFailure?: Maybe<Scalars["String"]>
  /** Timestamp of the most recent successful sync of the LfsObjectRegistry */
  lastSyncedAt?: Maybe<Scalars["Time"]>
  /** ID of the LFS object. */
  lfsObjectId: Scalars["ID"]
  /** Timestamp after which the LfsObjectRegistry should be resynced */
  retryAt?: Maybe<Scalars["Time"]>
  /** Number of consecutive failed sync attempts of the LfsObjectRegistry */
  retryCount?: Maybe<Scalars["Int"]>
  /** Sync state of the LfsObjectRegistry */
  state?: Maybe<RegistryState>
}

/** The connection type for LfsObjectRegistry. */
export type LfsObjectRegistryConnection = {
  __typename?: "LfsObjectRegistryConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<LfsObjectRegistryEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<LfsObjectRegistry>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type LfsObjectRegistryEdge = {
  __typename?: "LfsObjectRegistryEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<LfsObjectRegistry>
}

/** Represents an entry from the Cloud License history */
export type LicenseHistoryEntry = {
  __typename?: "LicenseHistoryEntry"
  /** Date when the license was activated. */
  activatedAt?: Maybe<Scalars["Date"]>
  /** Date, including grace period, when licensed features will be blocked. */
  blockChangesAt?: Maybe<Scalars["Date"]>
  /** Company of the licensee. */
  company?: Maybe<Scalars["String"]>
  /** Email of the licensee. */
  email?: Maybe<Scalars["String"]>
  /** Date when the license expires. */
  expiresAt?: Maybe<Scalars["Date"]>
  /** ID of the license. */
  id: Scalars["ID"]
  /** Name of the licensee. */
  name?: Maybe<Scalars["String"]>
  /** Name of the subscription plan. */
  plan: Scalars["String"]
  /** Date when the license started. */
  startsAt?: Maybe<Scalars["Date"]>
  /** Type of the license. */
  type: Scalars["String"]
  /** Number of paid users in the license. */
  usersInLicenseCount?: Maybe<Scalars["Int"]>
}

/** The connection type for LicenseHistoryEntry. */
export type LicenseHistoryEntryConnection = {
  __typename?: "LicenseHistoryEntryConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<LicenseHistoryEntryEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<LicenseHistoryEntry>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type LicenseHistoryEntryEdge = {
  __typename?: "LicenseHistoryEntryEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<LicenseHistoryEntry>
}

/** List limit metric setting */
export enum ListLimitMetric {
  AllMetrics = "all_metrics",
  IssueCount = "issue_count",
  IssueWeights = "issue_weights",
}

/** Autogenerated input type of MarkAsSpamSnippet */
export type MarkAsSpamSnippetInput = {
  /** The global ID of the snippet to update. */
  id: Scalars["SnippetID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of MarkAsSpamSnippet */
export type MarkAsSpamSnippetPayload = {
  __typename?: "MarkAsSpamSnippetPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The snippet after mutation. */
  snippet?: Maybe<Snippet>
}

/** Maven metadata */
export type MavenMetadata = {
  __typename?: "MavenMetadata"
  /** App group of the Maven package. */
  appGroup: Scalars["String"]
  /** App name of the Maven package. */
  appName: Scalars["String"]
  /** App version of the Maven package. */
  appVersion?: Maybe<Scalars["String"]>
  /** Date of creation. */
  createdAt: Scalars["Time"]
  /** ID of the metadatum. */
  id: Scalars["PackagesMavenMetadatumID"]
  /** Path of the Maven package. */
  path: Scalars["String"]
  /** Date of most recent update. */
  updatedAt: Scalars["Time"]
}

/** Possible identifier types for a measurement */
export enum MeasurementIdentifier {
  /** Project count. */
  Projects = "PROJECTS",
  /** User count. */
  Users = "USERS",
  /** Issue count. */
  Issues = "ISSUES",
  /** Merge request count. */
  MergeRequests = "MERGE_REQUESTS",
  /** Group count. */
  Groups = "GROUPS",
  /** Pipeline count. */
  Pipelines = "PIPELINES",
  /** Pipeline count with success status. */
  PipelinesSucceeded = "PIPELINES_SUCCEEDED",
  /** Pipeline count with failed status. */
  PipelinesFailed = "PIPELINES_FAILED",
  /** Pipeline count with canceled status. */
  PipelinesCanceled = "PIPELINES_CANCELED",
  /** Pipeline count with skipped status. */
  PipelinesSkipped = "PIPELINES_SKIPPED",
}

export type MemberInterface = {
  /** GitLab::Access level. */
  accessLevel?: Maybe<AccessLevel>
  /** Date and time the membership was created. */
  createdAt?: Maybe<Scalars["Time"]>
  /** User that authorized membership. */
  createdBy?: Maybe<UserCore>
  /** Date and time the membership expires. */
  expiresAt?: Maybe<Scalars["Time"]>
  /** ID of the member. */
  id: Scalars["ID"]
  /** Date and time the membership was last updated. */
  updatedAt?: Maybe<Scalars["Time"]>
  /** User that is associated with the member object. */
  user: UserCore
}

/** The connection type for MemberInterface. */
export type MemberInterfaceConnection = {
  __typename?: "MemberInterfaceConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<MemberInterfaceEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<MemberInterface>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type MemberInterfaceEdge = {
  __typename?: "MemberInterfaceEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<MemberInterface>
}

export type MergeRequest = Noteable &
  CurrentUserTodos & {
    __typename?: "MergeRequest"
    /** Indicates if members of the target project can push to the fork. */
    allowCollaboration?: Maybe<Scalars["Boolean"]>
    /** Number of approvals left. */
    approvalsLeft?: Maybe<Scalars["Int"]>
    /** Number of approvals required. */
    approvalsRequired?: Maybe<Scalars["Int"]>
    /** Indicates if the merge request has all the required approvals. Returns true if no required approvals are configured. */
    approved: Scalars["Boolean"]
    /** Users who approved the merge request. */
    approvedBy?: Maybe<UserCoreConnection>
    /** Assignees of the merge request. */
    assignees?: Maybe<MergeRequestAssigneeConnection>
    /** User who created this merge request. */
    author?: Maybe<UserCore>
    /** Indicates if auto merge is enabled for the merge request. */
    autoMergeEnabled: Scalars["Boolean"]
    /** Selected auto merge strategy. */
    autoMergeStrategy?: Maybe<Scalars["String"]>
    /** Array of available auto merge strategies. */
    availableAutoMergeStrategies?: Maybe<Array<Scalars["String"]>>
    /** Number of commits in the merge request. */
    commitCount?: Maybe<Scalars["Int"]>
    /** Merge request commits excluding merge commits. */
    commitsWithoutMergeCommits?: Maybe<CommitConnection>
    /** Indicates if the merge request has conflicts. */
    conflicts: Scalars["Boolean"]
    /** Timestamp of when the merge request was created. */
    createdAt: Scalars["Time"]
    /** To-do items for the current user. */
    currentUserTodos: TodoConnection
    /** Default merge commit message of the merge request. */
    defaultMergeCommitMessage?: Maybe<Scalars["String"]>
    /** Default merge commit message of the merge request with description. */
    defaultMergeCommitMessageWithDescription?: Maybe<Scalars["String"]>
    /** Default squash commit message of the merge request. */
    defaultSquashCommitMessage?: Maybe<Scalars["String"]>
    /** Description of the merge request (Markdown rendered as HTML for caching). */
    description?: Maybe<Scalars["String"]>
    /** The GitLab Flavored Markdown rendering of `description` */
    descriptionHtml?: Maybe<Scalars["String"]>
    /** Diff head SHA of the merge request. */
    diffHeadSha?: Maybe<Scalars["String"]>
    /** References of the base SHA, the head SHA, and the start SHA for this merge request. */
    diffRefs?: Maybe<DiffRefs>
    /** Details about which files were changed in this merge request. */
    diffStats?: Maybe<Array<DiffStats>>
    /** Summary of which files were changed in this merge request. */
    diffStatsSummary?: Maybe<DiffStatsSummary>
    /** Indicates if comments on the merge request are locked to members only. */
    discussionLocked: Scalars["Boolean"]
    /** All discussions on this noteable. */
    discussions: DiscussionConnection
    /** Indicates if the source branch is behind the target branch. */
    divergedFromTargetBranch: Scalars["Boolean"]
    /** Number of downvotes for the merge request. */
    downvotes: Scalars["Int"]
    /** Indicates if the merge request is a draft. */
    draft: Scalars["Boolean"]
    /** Indicates if the project settings will lead to source branch deletion after merge. */
    forceRemoveSourceBranch?: Maybe<Scalars["Boolean"]>
    /** Indicates if the merge request has CI. */
    hasCi: Scalars["Boolean"]
    /** Indicates if the source branch has any security reports. */
    hasSecurityReports: Scalars["Boolean"]
    /** The pipeline running on the branch HEAD of the merge request. */
    headPipeline?: Maybe<Pipeline>
    /** ID of the merge request. */
    id: Scalars["ID"]
    /** Internal ID of the merge request. */
    iid: Scalars["String"]
    /** Commit SHA of the merge request if merge is in progress. */
    inProgressMergeCommitSha?: Maybe<Scalars["String"]>
    /** Labels of the merge request. */
    labels?: Maybe<LabelConnection>
    /** SHA of the merge request commit (set once merged). */
    mergeCommitSha?: Maybe<Scalars["String"]>
    /** Error message due to a merge error. */
    mergeError?: Maybe<Scalars["String"]>
    /** Indicates if a merge is currently occurring. */
    mergeOngoing: Scalars["Boolean"]
    /**
     * Status of the merge request. Deprecated in 14.0: This was renamed.
     * @deprecated This was renamed. Please use `MergeRequest.mergeStatusEnum`. Deprecated in 14.0.
     */
    mergeStatus?: Maybe<Scalars["String"]>
    /** Merge status of the merge request. */
    mergeStatusEnum?: Maybe<MergeStatus>
    /** Number of merge requests in the merge train. */
    mergeTrainsCount?: Maybe<Scalars["Int"]>
    /** User who merged this merge request. */
    mergeUser?: Maybe<UserCore>
    /** Indicates if the merge has been set to be merged when its pipeline succeeds (MWPS). */
    mergeWhenPipelineSucceeds?: Maybe<Scalars["Boolean"]>
    /** Indicates if the merge request is mergeable. */
    mergeable: Scalars["Boolean"]
    /** Indicates if all discussions in the merge request have been resolved, allowing the merge request to be merged. */
    mergeableDiscussionsState?: Maybe<Scalars["Boolean"]>
    /** Timestamp of when the merge request was merged, null if not merged. */
    mergedAt?: Maybe<Scalars["Time"]>
    /** The milestone of the merge request. */
    milestone?: Maybe<Milestone>
    /** All notes on this noteable. */
    notes: NoteConnection
    /** Participants in the merge request. This includes the author, assignees, reviewers, and users mentioned in notes. */
    participants?: Maybe<UserCoreConnection>
    /** Pipelines for the merge request. Note: for performance reasons, no more than the most recent 500 pipelines will be returned. */
    pipelines?: Maybe<PipelineConnection>
    /** Alias for target_project. */
    project: Project
    /** ID of the merge request project. */
    projectId: Scalars["Int"]
    /** Rebase commit SHA of the merge request. */
    rebaseCommitSha?: Maybe<Scalars["String"]>
    /** Indicates if there is a rebase currently in progress for the merge request. */
    rebaseInProgress: Scalars["Boolean"]
    /** Internal reference of the merge request. Returned in shortened format by default. */
    reference: Scalars["String"]
    /** Users from whom a review has been requested. */
    reviewers?: Maybe<MergeRequestReviewerConnection>
    /** Indicates if the merge request is created by @GitLab-Security-Bot. */
    securityAutoFix?: Maybe<Scalars["Boolean"]>
    /** Indicates if the target branch security reports are out of date. */
    securityReportsUpToDateOnTargetBranch: Scalars["Boolean"]
    /** Indicates if the merge request will be rebased. */
    shouldBeRebased: Scalars["Boolean"]
    /** Indicates if the source branch of the merge request will be deleted after merge. */
    shouldRemoveSourceBranch?: Maybe<Scalars["Boolean"]>
    /** Source branch of the merge request. */
    sourceBranch: Scalars["String"]
    /** Indicates if the source branch of the merge request exists. */
    sourceBranchExists: Scalars["Boolean"]
    /** Indicates if the source branch is protected. */
    sourceBranchProtected: Scalars["Boolean"]
    /** Source project of the merge request. */
    sourceProject?: Maybe<Project>
    /** ID of the merge request source project. */
    sourceProjectId?: Maybe<Scalars["Int"]>
    /** Indicates if squash on merge is enabled. */
    squash: Scalars["Boolean"]
    /** Indicates if squash on merge is enabled. */
    squashOnMerge: Scalars["Boolean"]
    /** State of the merge request. */
    state: MergeRequestState
    /** Indicates if the currently logged in user is subscribed to this merge request. */
    subscribed: Scalars["Boolean"]
    /** Target branch of the merge request. */
    targetBranch: Scalars["String"]
    /** Indicates if the target branch of the merge request exists. */
    targetBranchExists: Scalars["Boolean"]
    /** Target project of the merge request. */
    targetProject: Project
    /** ID of the merge request target project. */
    targetProjectId: Scalars["Int"]
    /** Completion status of tasks */
    taskCompletionStatus: TaskCompletionStatus
    /** Time estimate of the merge request. */
    timeEstimate: Scalars["Int"]
    /** Timelogs on the merge request. */
    timelogs: TimelogConnection
    /** Title of the merge request. */
    title: Scalars["String"]
    /** The GitLab Flavored Markdown rendering of `title` */
    titleHtml?: Maybe<Scalars["String"]>
    /** Total time reported as spent on the merge request. */
    totalTimeSpent: Scalars["Int"]
    /** Timestamp of when the merge request was last updated. */
    updatedAt: Scalars["Time"]
    /** Number of upvotes for the merge request. */
    upvotes: Scalars["Int"]
    /** Number of user discussions in the merge request. */
    userDiscussionsCount?: Maybe<Scalars["Int"]>
    /** User notes count of the merge request. */
    userNotesCount?: Maybe<Scalars["Int"]>
    /** Permissions for the current user on the resource */
    userPermissions: MergeRequestPermissions
    /** Web URL of the merge request. */
    webUrl?: Maybe<Scalars["String"]>
    /**
     * Indicates if the merge request is a draft. Deprecated in 13.12: Use `draft`.
     * @deprecated Use `draft`. Deprecated in 13.12.
     */
    workInProgress: Scalars["Boolean"]
  }

export type MergeRequestApprovedByArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type MergeRequestAssigneesArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type MergeRequestCommitsWithoutMergeCommitsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type MergeRequestCurrentUserTodosArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
  state?: Maybe<TodoStateEnum>
}

export type MergeRequestDiffStatsArgs = {
  path?: Maybe<Scalars["String"]>
}

export type MergeRequestDiscussionsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type MergeRequestLabelsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type MergeRequestNotesArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type MergeRequestParticipantsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type MergeRequestPipelinesArgs = {
  status?: Maybe<PipelineStatusEnum>
  ref?: Maybe<Scalars["String"]>
  sha?: Maybe<Scalars["String"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type MergeRequestReferenceArgs = {
  full?: Maybe<Scalars["Boolean"]>
}

export type MergeRequestReviewersArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type MergeRequestTimelogsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Autogenerated input type of MergeRequestAccept */
export type MergeRequestAcceptInput = {
  /** The project the merge request to mutate is in. */
  projectPath: Scalars["ID"]
  /** The IID of the merge request to mutate. */
  iid: Scalars["String"]
  /** How to merge this merge request. */
  strategy?: Maybe<MergeStrategyEnum>
  /** Custom merge commit message. */
  commitMessage?: Maybe<Scalars["String"]>
  /** Custom squash commit message (if squash is true). */
  squashCommitMessage?: Maybe<Scalars["String"]>
  /** The HEAD SHA at the time when this merge was requested. */
  sha: Scalars["String"]
  /** Should the source branch be removed. */
  shouldRemoveSourceBranch?: Maybe<Scalars["Boolean"]>
  /** Squash commits on the source branch before merge. */
  squash?: Maybe<Scalars["Boolean"]>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of MergeRequestAccept */
export type MergeRequestAcceptPayload = {
  __typename?: "MergeRequestAcceptPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The merge request after mutation. */
  mergeRequest?: Maybe<MergeRequest>
}

/** A user assigned to a merge request. */
export type MergeRequestAssignee = User & {
  __typename?: "MergeRequestAssignee"
  /** Merge requests assigned to the user. */
  assignedMergeRequests?: Maybe<MergeRequestConnection>
  /** Merge requests authored by the user. */
  authoredMergeRequests?: Maybe<MergeRequestConnection>
  /** URL of the user's avatar. */
  avatarUrl?: Maybe<Scalars["String"]>
  /** Indicates if the user is a bot. */
  bot: Scalars["Boolean"]
  /** User callouts that belong to the user. */
  callouts?: Maybe<UserCalloutConnection>
  /**
   * User email. Deprecated in 13.7: This was renamed.
   * @deprecated This was renamed. Please use `User.publicEmail`. Deprecated in 13.7.
   */
  email?: Maybe<Scalars["String"]>
  /** Group count for the user. */
  groupCount?: Maybe<Scalars["Int"]>
  /** Group memberships of the user. */
  groupMemberships?: Maybe<GroupMemberConnection>
  /** ID of the user. */
  id: Scalars["ID"]
  /** The location of the user. */
  location?: Maybe<Scalars["String"]>
  /** Details of this user's interactions with the merge request. */
  mergeRequestInteraction?: Maybe<UserMergeRequestInteraction>
  /** Human-readable name of the user. */
  name: Scalars["String"]
  /** Project memberships of the user. */
  projectMemberships?: Maybe<ProjectMemberConnection>
  /** User's public email. */
  publicEmail?: Maybe<Scalars["String"]>
  /** Merge requests assigned to the user for review. */
  reviewRequestedMergeRequests?: Maybe<MergeRequestConnection>
  /** Snippets authored by the user. */
  snippets?: Maybe<SnippetConnection>
  /** Projects starred by the user. */
  starredProjects?: Maybe<ProjectConnection>
  /** State of the user. */
  state: UserState
  /** User status. */
  status?: Maybe<UserStatus>
  /** To-do items of the user. */
  todos?: Maybe<TodoConnection>
  /** Permissions for the current user on the resource. */
  userPermissions: UserPermissions
  /** Username of the user. Unique within this instance of GitLab. */
  username: Scalars["String"]
  /** Web path of the user. */
  webPath: Scalars["String"]
  /** Web URL of the user. */
  webUrl: Scalars["String"]
}

/** A user assigned to a merge request. */
export type MergeRequestAssigneeAssignedMergeRequestsArgs = {
  iids?: Maybe<Array<Scalars["String"]>>
  sourceBranches?: Maybe<Array<Scalars["String"]>>
  targetBranches?: Maybe<Array<Scalars["String"]>>
  state?: Maybe<MergeRequestState>
  labels?: Maybe<Array<Scalars["String"]>>
  mergedAfter?: Maybe<Scalars["Time"]>
  mergedBefore?: Maybe<Scalars["Time"]>
  milestoneTitle?: Maybe<Scalars["String"]>
  sort?: Maybe<MergeRequestSort>
  not?: Maybe<MergeRequestsResolverNegatedParams>
  projectPath?: Maybe<Scalars["String"]>
  projectId?: Maybe<Scalars["ProjectID"]>
  authorUsername?: Maybe<Scalars["String"]>
  reviewerUsername?: Maybe<Scalars["String"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** A user assigned to a merge request. */
export type MergeRequestAssigneeAuthoredMergeRequestsArgs = {
  iids?: Maybe<Array<Scalars["String"]>>
  sourceBranches?: Maybe<Array<Scalars["String"]>>
  targetBranches?: Maybe<Array<Scalars["String"]>>
  state?: Maybe<MergeRequestState>
  labels?: Maybe<Array<Scalars["String"]>>
  mergedAfter?: Maybe<Scalars["Time"]>
  mergedBefore?: Maybe<Scalars["Time"]>
  milestoneTitle?: Maybe<Scalars["String"]>
  sort?: Maybe<MergeRequestSort>
  not?: Maybe<MergeRequestsResolverNegatedParams>
  projectPath?: Maybe<Scalars["String"]>
  projectId?: Maybe<Scalars["ProjectID"]>
  assigneeUsername?: Maybe<Scalars["String"]>
  reviewerUsername?: Maybe<Scalars["String"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** A user assigned to a merge request. */
export type MergeRequestAssigneeCalloutsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** A user assigned to a merge request. */
export type MergeRequestAssigneeGroupMembershipsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** A user assigned to a merge request. */
export type MergeRequestAssigneeProjectMembershipsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** A user assigned to a merge request. */
export type MergeRequestAssigneeReviewRequestedMergeRequestsArgs = {
  iids?: Maybe<Array<Scalars["String"]>>
  sourceBranches?: Maybe<Array<Scalars["String"]>>
  targetBranches?: Maybe<Array<Scalars["String"]>>
  state?: Maybe<MergeRequestState>
  labels?: Maybe<Array<Scalars["String"]>>
  mergedAfter?: Maybe<Scalars["Time"]>
  mergedBefore?: Maybe<Scalars["Time"]>
  milestoneTitle?: Maybe<Scalars["String"]>
  sort?: Maybe<MergeRequestSort>
  not?: Maybe<MergeRequestsResolverNegatedParams>
  projectPath?: Maybe<Scalars["String"]>
  projectId?: Maybe<Scalars["ProjectID"]>
  authorUsername?: Maybe<Scalars["String"]>
  assigneeUsername?: Maybe<Scalars["String"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** A user assigned to a merge request. */
export type MergeRequestAssigneeSnippetsArgs = {
  ids?: Maybe<Array<Scalars["SnippetID"]>>
  visibility?: Maybe<VisibilityScopesEnum>
  type?: Maybe<TypeEnum>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** A user assigned to a merge request. */
export type MergeRequestAssigneeStarredProjectsArgs = {
  search?: Maybe<Scalars["String"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** A user assigned to a merge request. */
export type MergeRequestAssigneeTodosArgs = {
  action?: Maybe<Array<TodoActionEnum>>
  authorId?: Maybe<Array<Scalars["ID"]>>
  projectId?: Maybe<Array<Scalars["ID"]>>
  groupId?: Maybe<Array<Scalars["ID"]>>
  state?: Maybe<Array<TodoStateEnum>>
  type?: Maybe<Array<TodoTargetEnum>>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** The connection type for MergeRequestAssignee. */
export type MergeRequestAssigneeConnection = {
  __typename?: "MergeRequestAssigneeConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<MergeRequestAssigneeEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<MergeRequestAssignee>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type MergeRequestAssigneeEdge = {
  __typename?: "MergeRequestAssigneeEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<MergeRequestAssignee>
}

/** The connection type for MergeRequest. */
export type MergeRequestConnection = {
  __typename?: "MergeRequestConnection"
  /** Total count of collection. */
  count: Scalars["Int"]
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<MergeRequestEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<MergeRequest>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
  /** Total sum of time to merge, in seconds, for the collection of merge requests. */
  totalTimeToMerge?: Maybe<Scalars["Float"]>
}

/** Autogenerated input type of MergeRequestCreate */
export type MergeRequestCreateInput = {
  /** Project full path the merge request is associated with. */
  projectPath: Scalars["ID"]
  /** Title of the merge request. */
  title: Scalars["String"]
  /** Source branch of the merge request. */
  sourceBranch: Scalars["String"]
  /** Target branch of the merge request. */
  targetBranch: Scalars["String"]
  /** Description of the merge request (Markdown rendered as HTML for caching). */
  description?: Maybe<Scalars["String"]>
  /** Labels of the merge request. */
  labels?: Maybe<Array<Scalars["String"]>>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of MergeRequestCreate */
export type MergeRequestCreatePayload = {
  __typename?: "MergeRequestCreatePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The merge request after mutation. */
  mergeRequest?: Maybe<MergeRequest>
}

/** Represents the Geo sync and verification state of a Merge Request diff */
export type MergeRequestDiffRegistry = {
  __typename?: "MergeRequestDiffRegistry"
  /** Timestamp when the MergeRequestDiffRegistry was created */
  createdAt?: Maybe<Scalars["Time"]>
  /** ID of the MergeRequestDiffRegistry */
  id: Scalars["ID"]
  /** Error message during sync of the MergeRequestDiffRegistry */
  lastSyncFailure?: Maybe<Scalars["String"]>
  /** Timestamp of the most recent successful sync of the MergeRequestDiffRegistry */
  lastSyncedAt?: Maybe<Scalars["Time"]>
  /** ID of the Merge Request diff. */
  mergeRequestDiffId: Scalars["ID"]
  /** Timestamp after which the MergeRequestDiffRegistry should be resynced */
  retryAt?: Maybe<Scalars["Time"]>
  /** Number of consecutive failed sync attempts of the MergeRequestDiffRegistry */
  retryCount?: Maybe<Scalars["Int"]>
  /** Sync state of the MergeRequestDiffRegistry */
  state?: Maybe<RegistryState>
}

/** The connection type for MergeRequestDiffRegistry. */
export type MergeRequestDiffRegistryConnection = {
  __typename?: "MergeRequestDiffRegistryConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<MergeRequestDiffRegistryEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<MergeRequestDiffRegistry>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type MergeRequestDiffRegistryEdge = {
  __typename?: "MergeRequestDiffRegistryEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<MergeRequestDiffRegistry>
}

/** An edge in a connection. */
export type MergeRequestEdge = {
  __typename?: "MergeRequestEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<MergeRequest>
}

/** New state to apply to a merge request. */
export enum MergeRequestNewState {
  /** Open the merge request if it is closed. */
  Open = "OPEN",
  /** Close the merge request if it is open. */
  Closed = "CLOSED",
}

/** Check permissions for the current user on a merge request */
export type MergeRequestPermissions = {
  __typename?: "MergeRequestPermissions"
  /** Indicates the user can perform `admin_merge_request` on this resource */
  adminMergeRequest: Scalars["Boolean"]
  /** Indicates the user can perform `can_merge` on this resource */
  canMerge: Scalars["Boolean"]
  /** Indicates the user can perform `cherry_pick_on_current_merge_request` on this resource */
  cherryPickOnCurrentMergeRequest: Scalars["Boolean"]
  /** Indicates the user can perform `create_note` on this resource */
  createNote: Scalars["Boolean"]
  /** Indicates the user can perform `push_to_source_branch` on this resource */
  pushToSourceBranch: Scalars["Boolean"]
  /** Indicates the user can perform `read_merge_request` on this resource */
  readMergeRequest: Scalars["Boolean"]
  /** Indicates the user can perform `remove_source_branch` on this resource */
  removeSourceBranch: Scalars["Boolean"]
  /** Indicates the user can perform `revert_on_current_merge_request` on this resource */
  revertOnCurrentMergeRequest: Scalars["Boolean"]
  /** Indicates the user can perform `update_merge_request` on this resource */
  updateMergeRequest: Scalars["Boolean"]
}

/** State of a review of a GitLab merge request. */
export enum MergeRequestReviewState {
  /** The merge request is unreviewed. */
  Unreviewed = "UNREVIEWED",
  /** The merge request is reviewed. */
  Reviewed = "REVIEWED",
}

/** A user assigned to a merge request as a reviewer. */
export type MergeRequestReviewer = User & {
  __typename?: "MergeRequestReviewer"
  /** Merge requests assigned to the user. */
  assignedMergeRequests?: Maybe<MergeRequestConnection>
  /** Merge requests authored by the user. */
  authoredMergeRequests?: Maybe<MergeRequestConnection>
  /** URL of the user's avatar. */
  avatarUrl?: Maybe<Scalars["String"]>
  /** Indicates if the user is a bot. */
  bot: Scalars["Boolean"]
  /** User callouts that belong to the user. */
  callouts?: Maybe<UserCalloutConnection>
  /**
   * User email. Deprecated in 13.7: This was renamed.
   * @deprecated This was renamed. Please use `User.publicEmail`. Deprecated in 13.7.
   */
  email?: Maybe<Scalars["String"]>
  /** Group count for the user. */
  groupCount?: Maybe<Scalars["Int"]>
  /** Group memberships of the user. */
  groupMemberships?: Maybe<GroupMemberConnection>
  /** ID of the user. */
  id: Scalars["ID"]
  /** The location of the user. */
  location?: Maybe<Scalars["String"]>
  /** Details of this user's interactions with the merge request. */
  mergeRequestInteraction?: Maybe<UserMergeRequestInteraction>
  /** Human-readable name of the user. */
  name: Scalars["String"]
  /** Project memberships of the user. */
  projectMemberships?: Maybe<ProjectMemberConnection>
  /** User's public email. */
  publicEmail?: Maybe<Scalars["String"]>
  /** Merge requests assigned to the user for review. */
  reviewRequestedMergeRequests?: Maybe<MergeRequestConnection>
  /** Snippets authored by the user. */
  snippets?: Maybe<SnippetConnection>
  /** Projects starred by the user. */
  starredProjects?: Maybe<ProjectConnection>
  /** State of the user. */
  state: UserState
  /** User status. */
  status?: Maybe<UserStatus>
  /** To-do items of the user. */
  todos?: Maybe<TodoConnection>
  /** Permissions for the current user on the resource. */
  userPermissions: UserPermissions
  /** Username of the user. Unique within this instance of GitLab. */
  username: Scalars["String"]
  /** Web path of the user. */
  webPath: Scalars["String"]
  /** Web URL of the user. */
  webUrl: Scalars["String"]
}

/** A user assigned to a merge request as a reviewer. */
export type MergeRequestReviewerAssignedMergeRequestsArgs = {
  iids?: Maybe<Array<Scalars["String"]>>
  sourceBranches?: Maybe<Array<Scalars["String"]>>
  targetBranches?: Maybe<Array<Scalars["String"]>>
  state?: Maybe<MergeRequestState>
  labels?: Maybe<Array<Scalars["String"]>>
  mergedAfter?: Maybe<Scalars["Time"]>
  mergedBefore?: Maybe<Scalars["Time"]>
  milestoneTitle?: Maybe<Scalars["String"]>
  sort?: Maybe<MergeRequestSort>
  not?: Maybe<MergeRequestsResolverNegatedParams>
  projectPath?: Maybe<Scalars["String"]>
  projectId?: Maybe<Scalars["ProjectID"]>
  authorUsername?: Maybe<Scalars["String"]>
  reviewerUsername?: Maybe<Scalars["String"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** A user assigned to a merge request as a reviewer. */
export type MergeRequestReviewerAuthoredMergeRequestsArgs = {
  iids?: Maybe<Array<Scalars["String"]>>
  sourceBranches?: Maybe<Array<Scalars["String"]>>
  targetBranches?: Maybe<Array<Scalars["String"]>>
  state?: Maybe<MergeRequestState>
  labels?: Maybe<Array<Scalars["String"]>>
  mergedAfter?: Maybe<Scalars["Time"]>
  mergedBefore?: Maybe<Scalars["Time"]>
  milestoneTitle?: Maybe<Scalars["String"]>
  sort?: Maybe<MergeRequestSort>
  not?: Maybe<MergeRequestsResolverNegatedParams>
  projectPath?: Maybe<Scalars["String"]>
  projectId?: Maybe<Scalars["ProjectID"]>
  assigneeUsername?: Maybe<Scalars["String"]>
  reviewerUsername?: Maybe<Scalars["String"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** A user assigned to a merge request as a reviewer. */
export type MergeRequestReviewerCalloutsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** A user assigned to a merge request as a reviewer. */
export type MergeRequestReviewerGroupMembershipsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** A user assigned to a merge request as a reviewer. */
export type MergeRequestReviewerProjectMembershipsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** A user assigned to a merge request as a reviewer. */
export type MergeRequestReviewerReviewRequestedMergeRequestsArgs = {
  iids?: Maybe<Array<Scalars["String"]>>
  sourceBranches?: Maybe<Array<Scalars["String"]>>
  targetBranches?: Maybe<Array<Scalars["String"]>>
  state?: Maybe<MergeRequestState>
  labels?: Maybe<Array<Scalars["String"]>>
  mergedAfter?: Maybe<Scalars["Time"]>
  mergedBefore?: Maybe<Scalars["Time"]>
  milestoneTitle?: Maybe<Scalars["String"]>
  sort?: Maybe<MergeRequestSort>
  not?: Maybe<MergeRequestsResolverNegatedParams>
  projectPath?: Maybe<Scalars["String"]>
  projectId?: Maybe<Scalars["ProjectID"]>
  authorUsername?: Maybe<Scalars["String"]>
  assigneeUsername?: Maybe<Scalars["String"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** A user assigned to a merge request as a reviewer. */
export type MergeRequestReviewerSnippetsArgs = {
  ids?: Maybe<Array<Scalars["SnippetID"]>>
  visibility?: Maybe<VisibilityScopesEnum>
  type?: Maybe<TypeEnum>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** A user assigned to a merge request as a reviewer. */
export type MergeRequestReviewerStarredProjectsArgs = {
  search?: Maybe<Scalars["String"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** A user assigned to a merge request as a reviewer. */
export type MergeRequestReviewerTodosArgs = {
  action?: Maybe<Array<TodoActionEnum>>
  authorId?: Maybe<Array<Scalars["ID"]>>
  projectId?: Maybe<Array<Scalars["ID"]>>
  groupId?: Maybe<Array<Scalars["ID"]>>
  state?: Maybe<Array<TodoStateEnum>>
  type?: Maybe<Array<TodoTargetEnum>>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** The connection type for MergeRequestReviewer. */
export type MergeRequestReviewerConnection = {
  __typename?: "MergeRequestReviewerConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<MergeRequestReviewerEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<MergeRequestReviewer>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type MergeRequestReviewerEdge = {
  __typename?: "MergeRequestReviewerEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<MergeRequestReviewer>
}

/** Autogenerated input type of MergeRequestReviewerRereview */
export type MergeRequestReviewerRereviewInput = {
  /** The project the merge request to mutate is in. */
  projectPath: Scalars["ID"]
  /** The IID of the merge request to mutate. */
  iid: Scalars["String"]
  /** The user ID for the user that has been requested for a new review. */
  userId: Scalars["UserID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of MergeRequestReviewerRereview */
export type MergeRequestReviewerRereviewPayload = {
  __typename?: "MergeRequestReviewerRereviewPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The merge request after mutation. */
  mergeRequest?: Maybe<MergeRequest>
}

/** Autogenerated input type of MergeRequestSetAssignees */
export type MergeRequestSetAssigneesInput = {
  /** The project the merge request to mutate is in. */
  projectPath: Scalars["ID"]
  /** The IID of the merge request to mutate. */
  iid: Scalars["String"]
  /** The usernames to assign to the resource. Replaces existing assignees by default. */
  assigneeUsernames: Array<Scalars["String"]>
  /** The operation to perform. Defaults to REPLACE. */
  operationMode?: Maybe<MutationOperationMode>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of MergeRequestSetAssignees */
export type MergeRequestSetAssigneesPayload = {
  __typename?: "MergeRequestSetAssigneesPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The merge request after mutation. */
  mergeRequest?: Maybe<MergeRequest>
}

/** Autogenerated input type of MergeRequestSetDraft */
export type MergeRequestSetDraftInput = {
  /** The project the merge request to mutate is in. */
  projectPath: Scalars["ID"]
  /** The IID of the merge request to mutate. */
  iid: Scalars["String"]
  /** Whether or not to set the merge request as a draft. */
  draft: Scalars["Boolean"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of MergeRequestSetDraft */
export type MergeRequestSetDraftPayload = {
  __typename?: "MergeRequestSetDraftPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The merge request after mutation. */
  mergeRequest?: Maybe<MergeRequest>
}

/** Autogenerated input type of MergeRequestSetLabels */
export type MergeRequestSetLabelsInput = {
  /** The project the merge request to mutate is in. */
  projectPath: Scalars["ID"]
  /** The IID of the merge request to mutate. */
  iid: Scalars["String"]
  /** The Label IDs to set. Replaces existing labels by default. */
  labelIds: Array<Scalars["LabelID"]>
  /** Changes the operation mode. Defaults to REPLACE. */
  operationMode?: Maybe<MutationOperationMode>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of MergeRequestSetLabels */
export type MergeRequestSetLabelsPayload = {
  __typename?: "MergeRequestSetLabelsPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The merge request after mutation. */
  mergeRequest?: Maybe<MergeRequest>
}

/** Autogenerated input type of MergeRequestSetLocked */
export type MergeRequestSetLockedInput = {
  /** The project the merge request to mutate is in. */
  projectPath: Scalars["ID"]
  /** The IID of the merge request to mutate. */
  iid: Scalars["String"]
  /** Whether or not to lock the merge request. */
  locked: Scalars["Boolean"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of MergeRequestSetLocked */
export type MergeRequestSetLockedPayload = {
  __typename?: "MergeRequestSetLockedPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The merge request after mutation. */
  mergeRequest?: Maybe<MergeRequest>
}

/** Autogenerated input type of MergeRequestSetMilestone */
export type MergeRequestSetMilestoneInput = {
  /** The project the merge request to mutate is in. */
  projectPath: Scalars["ID"]
  /** The IID of the merge request to mutate. */
  iid: Scalars["String"]
  /** The milestone to assign to the merge request. */
  milestoneId?: Maybe<Scalars["MilestoneID"]>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of MergeRequestSetMilestone */
export type MergeRequestSetMilestonePayload = {
  __typename?: "MergeRequestSetMilestonePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The merge request after mutation. */
  mergeRequest?: Maybe<MergeRequest>
}

/** Autogenerated input type of MergeRequestSetSubscription */
export type MergeRequestSetSubscriptionInput = {
  /** The desired state of the subscription. */
  subscribedState: Scalars["Boolean"]
  /** The project the merge request to mutate is in. */
  projectPath: Scalars["ID"]
  /** The IID of the merge request to mutate. */
  iid: Scalars["String"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of MergeRequestSetSubscription */
export type MergeRequestSetSubscriptionPayload = {
  __typename?: "MergeRequestSetSubscriptionPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The merge request after mutation. */
  mergeRequest?: Maybe<MergeRequest>
}

/** Autogenerated input type of MergeRequestSetWip */
export type MergeRequestSetWipInput = {
  /** The project the merge request to mutate is in. */
  projectPath: Scalars["ID"]
  /** The IID of the merge request to mutate. */
  iid: Scalars["String"]
  /** Whether or not to set the merge request as a draft. */
  wip: Scalars["Boolean"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of MergeRequestSetWip */
export type MergeRequestSetWipPayload = {
  __typename?: "MergeRequestSetWipPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The merge request after mutation. */
  mergeRequest?: Maybe<MergeRequest>
}

/** Values for sorting merge requests */
export enum MergeRequestSort {
  /** Updated at descending order. */
  UpdatedDesc = "UPDATED_DESC",
  /** Updated at ascending order. */
  UpdatedAsc = "UPDATED_ASC",
  /** Created at descending order. */
  CreatedDesc = "CREATED_DESC",
  /** Created at ascending order. */
  CreatedAsc = "CREATED_ASC",
  /** Priority by ascending order. */
  PriorityAsc = "PRIORITY_ASC",
  /** Priority by descending order. */
  PriorityDesc = "PRIORITY_DESC",
  /** Label priority by ascending order. */
  LabelPriorityAsc = "LABEL_PRIORITY_ASC",
  /** Label priority by descending order. */
  LabelPriorityDesc = "LABEL_PRIORITY_DESC",
  /** Milestone due date by ascending order. */
  MilestoneDueAsc = "MILESTONE_DUE_ASC",
  /** Milestone due date by descending order. */
  MilestoneDueDesc = "MILESTONE_DUE_DESC",
  /** Merge time by ascending order. */
  MergedAtAsc = "MERGED_AT_ASC",
  /** Merge time by descending order. */
  MergedAtDesc = "MERGED_AT_DESC",
}

/** State of a GitLab merge request */
export enum MergeRequestState {
  /** In open state. */
  Opened = "opened",
  /** In closed state. */
  Closed = "closed",
  /** Discussion has been locked. */
  Locked = "locked",
  /** All available. */
  All = "all",
  /** Merge request has been merged. */
  Merged = "merged",
}

/** Autogenerated input type of MergeRequestUpdate */
export type MergeRequestUpdateInput = {
  /** The project the merge request to mutate is in. */
  projectPath: Scalars["ID"]
  /** The IID of the merge request to mutate. */
  iid: Scalars["String"]
  /** Title of the merge request. */
  title?: Maybe<Scalars["String"]>
  /** Target branch of the merge request. */
  targetBranch?: Maybe<Scalars["String"]>
  /** Description of the merge request (Markdown rendered as HTML for caching). */
  description?: Maybe<Scalars["String"]>
  /** The action to perform to change the state. */
  state?: Maybe<MergeRequestNewState>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of MergeRequestUpdate */
export type MergeRequestUpdatePayload = {
  __typename?: "MergeRequestUpdatePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The merge request after mutation. */
  mergeRequest?: Maybe<MergeRequest>
}

export type MergeRequestsResolverNegatedParams = {
  /** Array of label names. All resolved merge requests will not have these labels. */
  labels?: Maybe<Array<Scalars["String"]>>
  /** Title of the milestone. */
  milestoneTitle?: Maybe<Scalars["String"]>
}

/** Representation of whether a GitLab merge request can be merged. */
export enum MergeStatus {
  /** Merge status has not been checked. */
  Unchecked = "UNCHECKED",
  /** Currently checking for mergeability. */
  Checking = "CHECKING",
  /** There are no conflicts between the source and target branches. */
  CanBeMerged = "CAN_BE_MERGED",
  /** There are conflicts between the source and target branches. */
  CannotBeMerged = "CANNOT_BE_MERGED",
  /** Currently unchecked. The previous state was `CANNOT_BE_MERGED`. */
  CannotBeMergedRecheck = "CANNOT_BE_MERGED_RECHECK",
}

export enum MergeStrategyEnum {
  /** Use the merge_train merge strategy. */
  MergeTrain = "MERGE_TRAIN",
  /** Use the add_to_merge_train_when_pipeline_succeeds merge strategy. */
  AddToMergeTrainWhenPipelineSucceeds = "ADD_TO_MERGE_TRAIN_WHEN_PIPELINE_SUCCEEDS",
  /** Use the merge_when_pipeline_succeeds merge strategy. */
  MergeWhenPipelineSucceeds = "MERGE_WHEN_PIPELINE_SUCCEEDS",
}

export type Metadata = {
  __typename?: "Metadata"
  /** Metadata about KAS. */
  kas: Kas
  /** Revision. */
  revision: Scalars["String"]
  /** Version. */
  version: Scalars["String"]
}

/** Represents a metric image upload */
export type MetricImage = {
  __typename?: "MetricImage"
  /** File name of the metric image. */
  fileName?: Maybe<Scalars["String"]>
  /** File path of the metric image. */
  filePath?: Maybe<Scalars["String"]>
  /** ID of the metric upload. */
  id: Scalars["ID"]
  /** Internal ID of the metric upload. */
  iid: Scalars["ID"]
  /** URL of the metric source. */
  url: Scalars["String"]
}

export type MetricsDashboard = {
  __typename?: "MetricsDashboard"
  /** Annotations added to the dashboard. */
  annotations?: Maybe<MetricsDashboardAnnotationConnection>
  /** Path to a file with the dashboard definition. */
  path?: Maybe<Scalars["String"]>
  /** Dashboard schema validation warnings. */
  schemaValidationWarnings?: Maybe<Array<Scalars["String"]>>
}

export type MetricsDashboardAnnotationsArgs = {
  from: Scalars["Time"]
  to?: Maybe<Scalars["Time"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type MetricsDashboardAnnotation = {
  __typename?: "MetricsDashboardAnnotation"
  /** Description of the annotation. */
  description?: Maybe<Scalars["String"]>
  /** Timestamp marking end of annotated time span. */
  endingAt?: Maybe<Scalars["Time"]>
  /** ID of the annotation. */
  id: Scalars["ID"]
  /** ID of a dashboard panel to which the annotation should be scoped. */
  panelId?: Maybe<Scalars["String"]>
  /** Timestamp marking start of annotated time span. */
  startingAt?: Maybe<Scalars["Time"]>
}

/** The connection type for MetricsDashboardAnnotation. */
export type MetricsDashboardAnnotationConnection = {
  __typename?: "MetricsDashboardAnnotationConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<MetricsDashboardAnnotationEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<MetricsDashboardAnnotation>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type MetricsDashboardAnnotationEdge = {
  __typename?: "MetricsDashboardAnnotationEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<MetricsDashboardAnnotation>
}

/** Represents a milestone */
export type Milestone = TimeboxReportInterface & {
  __typename?: "Milestone"
  /** Timestamp of milestone creation. */
  createdAt: Scalars["Time"]
  /** Description of the milestone. */
  description?: Maybe<Scalars["String"]>
  /** Timestamp of the milestone due date. */
  dueDate?: Maybe<Scalars["Time"]>
  /** Indicates if milestone is at group level. */
  groupMilestone: Scalars["Boolean"]
  /** ID of the milestone. */
  id: Scalars["ID"]
  /** Internal ID of the milestone. */
  iid: Scalars["ID"]
  /** Indicates if milestone is at project level. */
  projectMilestone: Scalars["Boolean"]
  /** Historically accurate report about the timebox. */
  report?: Maybe<TimeboxReport>
  /** Timestamp of the milestone start date. */
  startDate?: Maybe<Scalars["Time"]>
  /** State of the milestone. */
  state: MilestoneStateEnum
  /** Milestone statistics. */
  stats?: Maybe<MilestoneStats>
  /** Indicates if milestone is at subgroup level. */
  subgroupMilestone: Scalars["Boolean"]
  /** Title of the milestone. */
  title: Scalars["String"]
  /** Timestamp of last milestone update. */
  updatedAt: Scalars["Time"]
  /** Web path of the milestone. */
  webPath: Scalars["String"]
}

/** The connection type for Milestone. */
export type MilestoneConnection = {
  __typename?: "MilestoneConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<MilestoneEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<Milestone>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type MilestoneEdge = {
  __typename?: "MilestoneEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<Milestone>
}

/** Current state of milestone */
export enum MilestoneStateEnum {
  /** Milestone is currently active. */
  Active = "active",
  /** Milestone is closed. */
  Closed = "closed",
}

/** Contains statistics about a milestone */
export type MilestoneStats = {
  __typename?: "MilestoneStats"
  /** Number of closed issues associated with the milestone. */
  closedIssuesCount?: Maybe<Scalars["Int"]>
  /** Total number of issues associated with the milestone. */
  totalIssuesCount?: Maybe<Scalars["Int"]>
}

/** The position to which the adjacent object should be moved */
export enum MoveType {
  /** The adjacent object will be moved before the object that is being moved. */
  Before = "before",
  /** The adjacent object will be moved after the object that is being moved. */
  After = "after",
}

export type Mutation = {
  __typename?: "Mutation"
  /** @deprecated Use awardEmojiAdd. Deprecated in 13.2. */
  addAwardEmoji?: Maybe<AddAwardEmojiPayload>
  addProjectToSecurityDashboard?: Maybe<AddProjectToSecurityDashboardPayload>
  adminSidekiqQueuesDeleteJobs?: Maybe<AdminSidekiqQueuesDeleteJobsPayload>
  alertSetAssignees?: Maybe<AlertSetAssigneesPayload>
  alertTodoCreate?: Maybe<AlertTodoCreatePayload>
  apiFuzzingCiConfigurationCreate?: Maybe<ApiFuzzingCiConfigurationCreatePayload>
  awardEmojiAdd?: Maybe<AwardEmojiAddPayload>
  awardEmojiRemove?: Maybe<AwardEmojiRemovePayload>
  awardEmojiToggle?: Maybe<AwardEmojiTogglePayload>
  boardListCreate?: Maybe<BoardListCreatePayload>
  boardListUpdateLimitMetrics?: Maybe<BoardListUpdateLimitMetricsPayload>
  /** **BETA** This endpoint is subject to change without notice. */
  bulkEnableDevopsAdoptionNamespaces?: Maybe<BulkEnableDevopsAdoptionNamespacesPayload>
  ciCdSettingsUpdate?: Maybe<CiCdSettingsUpdatePayload>
  clusterAgentDelete?: Maybe<ClusterAgentDeletePayload>
  clusterAgentTokenCreate?: Maybe<ClusterAgentTokenCreatePayload>
  clusterAgentTokenDelete?: Maybe<ClusterAgentTokenDeletePayload>
  commitCreate?: Maybe<CommitCreatePayload>
  /**
   * Configure SAST for a project by enabling SAST in a new or modified
   * `.gitlab-ci.yml` file in a new branch. The new branch and a URL to
   * create a Merge Request are a part of the response.
   */
  configureSast?: Maybe<ConfigureSastPayload>
  /**
   * Configure Secret Detection for a project by enabling Secret Detection
   * in a new or modified `.gitlab-ci.yml` file in a new branch. The new
   * branch and a URL to create a Merge Request are a part of the
   * response.
   */
  configureSecretDetection?: Maybe<ConfigureSecretDetectionPayload>
  createAlertIssue?: Maybe<CreateAlertIssuePayload>
  createAnnotation?: Maybe<CreateAnnotationPayload>
  createBoard?: Maybe<CreateBoardPayload>
  createBranch?: Maybe<CreateBranchPayload>
  createClusterAgent?: Maybe<CreateClusterAgentPayload>
  createComplianceFramework?: Maybe<CreateComplianceFrameworkPayload>
  createDiffNote?: Maybe<CreateDiffNotePayload>
  createEpic?: Maybe<CreateEpicPayload>
  createImageDiffNote?: Maybe<CreateImageDiffNotePayload>
  createIssue?: Maybe<CreateIssuePayload>
  createIteration?: Maybe<CreateIterationPayload>
  createNote?: Maybe<CreateNotePayload>
  createRequirement?: Maybe<CreateRequirementPayload>
  createSnippet?: Maybe<CreateSnippetPayload>
  createTestCase?: Maybe<CreateTestCasePayload>
  dastOnDemandScanCreate?: Maybe<DastOnDemandScanCreatePayload>
  dastProfileCreate?: Maybe<DastProfileCreatePayload>
  dastProfileDelete?: Maybe<DastProfileDeletePayload>
  dastProfileRun?: Maybe<DastProfileRunPayload>
  dastProfileUpdate?: Maybe<DastProfileUpdatePayload>
  dastScannerProfileCreate?: Maybe<DastScannerProfileCreatePayload>
  dastScannerProfileDelete?: Maybe<DastScannerProfileDeletePayload>
  dastScannerProfileUpdate?: Maybe<DastScannerProfileUpdatePayload>
  dastSiteProfileCreate?: Maybe<DastSiteProfileCreatePayload>
  dastSiteProfileDelete?: Maybe<DastSiteProfileDeletePayload>
  dastSiteProfileUpdate?: Maybe<DastSiteProfileUpdatePayload>
  dastSiteTokenCreate?: Maybe<DastSiteTokenCreatePayload>
  dastSiteValidationCreate?: Maybe<DastSiteValidationCreatePayload>
  dastSiteValidationRevoke?: Maybe<DastSiteValidationRevokePayload>
  deleteAnnotation?: Maybe<DeleteAnnotationPayload>
  designManagementDelete?: Maybe<DesignManagementDeletePayload>
  designManagementMove?: Maybe<DesignManagementMovePayload>
  designManagementUpload?: Maybe<DesignManagementUploadPayload>
  destroyBoard?: Maybe<DestroyBoardPayload>
  destroyBoardList?: Maybe<DestroyBoardListPayload>
  destroyComplianceFramework?: Maybe<DestroyComplianceFrameworkPayload>
  destroyContainerRepository?: Maybe<DestroyContainerRepositoryPayload>
  destroyContainerRepositoryTags?: Maybe<DestroyContainerRepositoryTagsPayload>
  destroyEpicBoard?: Maybe<DestroyEpicBoardPayload>
  destroyNote?: Maybe<DestroyNotePayload>
  destroySnippet?: Maybe<DestroySnippetPayload>
  /** **BETA** This endpoint is subject to change without notice. */
  disableDevopsAdoptionNamespace?: Maybe<DisableDevopsAdoptionNamespacePayload>
  /** Toggles the resolved state of a discussion */
  discussionToggleResolve?: Maybe<DiscussionToggleResolvePayload>
  /** @deprecated Use vulnerabilityDismiss. Deprecated in 13.5. */
  dismissVulnerability?: Maybe<DismissVulnerabilityPayload>
  /** **BETA** This endpoint is subject to change without notice. */
  enableDevopsAdoptionNamespace?: Maybe<EnableDevopsAdoptionNamespacePayload>
  environmentsCanaryIngressUpdate?: Maybe<EnvironmentsCanaryIngressUpdatePayload>
  epicAddIssue?: Maybe<EpicAddIssuePayload>
  epicBoardCreate?: Maybe<EpicBoardCreatePayload>
  epicBoardListCreate?: Maybe<EpicBoardListCreatePayload>
  /** Destroys an epic board list. */
  epicBoardListDestroy?: Maybe<EpicBoardListDestroyPayload>
  epicBoardUpdate?: Maybe<EpicBoardUpdatePayload>
  epicMoveList?: Maybe<EpicMoveListPayload>
  epicSetSubscription?: Maybe<EpicSetSubscriptionPayload>
  epicTreeReorder?: Maybe<EpicTreeReorderPayload>
  escalationPolicyCreate?: Maybe<EscalationPolicyCreatePayload>
  escalationPolicyDestroy?: Maybe<EscalationPolicyDestroyPayload>
  exportRequirements?: Maybe<ExportRequirementsPayload>
  gitlabSubscriptionActivate?: Maybe<GitlabSubscriptionActivatePayload>
  httpIntegrationCreate?: Maybe<HttpIntegrationCreatePayload>
  httpIntegrationDestroy?: Maybe<HttpIntegrationDestroyPayload>
  httpIntegrationResetToken?: Maybe<HttpIntegrationResetTokenPayload>
  httpIntegrationUpdate?: Maybe<HttpIntegrationUpdatePayload>
  issueMove?: Maybe<IssueMovePayload>
  issueMoveList?: Maybe<IssueMoveListPayload>
  issueSetAssignees?: Maybe<IssueSetAssigneesPayload>
  issueSetConfidential?: Maybe<IssueSetConfidentialPayload>
  issueSetDueDate?: Maybe<IssueSetDueDatePayload>
  issueSetEpic?: Maybe<IssueSetEpicPayload>
  issueSetIteration?: Maybe<IssueSetIterationPayload>
  issueSetLocked?: Maybe<IssueSetLockedPayload>
  issueSetSeverity?: Maybe<IssueSetSeverityPayload>
  issueSetSubscription?: Maybe<IssueSetSubscriptionPayload>
  issueSetWeight?: Maybe<IssueSetWeightPayload>
  iterationCadenceCreate?: Maybe<IterationCadenceCreatePayload>
  iterationCadenceDestroy?: Maybe<IterationCadenceDestroyPayload>
  iterationCadenceUpdate?: Maybe<IterationCadenceUpdatePayload>
  iterationDelete?: Maybe<IterationDeletePayload>
  jiraImportStart?: Maybe<JiraImportStartPayload>
  jiraImportUsers?: Maybe<JiraImportUsersPayload>
  jobPlay?: Maybe<JobPlayPayload>
  jobRetry?: Maybe<JobRetryPayload>
  labelCreate?: Maybe<LabelCreatePayload>
  markAsSpamSnippet?: Maybe<MarkAsSpamSnippetPayload>
  /**
   * Accepts a merge request.
   * When accepted, the source branch will be merged into the target branch, either
   * immediately if possible, or using one of the automatic merge strategies.
   */
  mergeRequestAccept?: Maybe<MergeRequestAcceptPayload>
  mergeRequestCreate?: Maybe<MergeRequestCreatePayload>
  mergeRequestReviewerRereview?: Maybe<MergeRequestReviewerRereviewPayload>
  mergeRequestSetAssignees?: Maybe<MergeRequestSetAssigneesPayload>
  mergeRequestSetDraft?: Maybe<MergeRequestSetDraftPayload>
  mergeRequestSetLabels?: Maybe<MergeRequestSetLabelsPayload>
  mergeRequestSetLocked?: Maybe<MergeRequestSetLockedPayload>
  mergeRequestSetMilestone?: Maybe<MergeRequestSetMilestonePayload>
  mergeRequestSetSubscription?: Maybe<MergeRequestSetSubscriptionPayload>
  /** @deprecated Use mergeRequestSetDraft. Deprecated in 13.12. */
  mergeRequestSetWip?: Maybe<MergeRequestSetWipPayload>
  /** Update attributes of a merge request */
  mergeRequestUpdate?: Maybe<MergeRequestUpdatePayload>
  namespaceIncreaseStorageTemporarily?: Maybe<NamespaceIncreaseStorageTemporarilyPayload>
  oncallRotationCreate?: Maybe<OncallRotationCreatePayload>
  oncallRotationDestroy?: Maybe<OncallRotationDestroyPayload>
  oncallRotationUpdate?: Maybe<OncallRotationUpdatePayload>
  oncallScheduleCreate?: Maybe<OncallScheduleCreatePayload>
  oncallScheduleDestroy?: Maybe<OncallScheduleDestroyPayload>
  oncallScheduleUpdate?: Maybe<OncallScheduleUpdatePayload>
  pipelineCancel?: Maybe<PipelineCancelPayload>
  pipelineDestroy?: Maybe<PipelineDestroyPayload>
  pipelineRetry?: Maybe<PipelineRetryPayload>
  prometheusIntegrationCreate?: Maybe<PrometheusIntegrationCreatePayload>
  prometheusIntegrationResetToken?: Maybe<PrometheusIntegrationResetTokenPayload>
  prometheusIntegrationUpdate?: Maybe<PrometheusIntegrationUpdatePayload>
  promoteToEpic?: Maybe<PromoteToEpicPayload>
  releaseAssetLinkCreate?: Maybe<ReleaseAssetLinkCreatePayload>
  releaseAssetLinkDelete?: Maybe<ReleaseAssetLinkDeletePayload>
  releaseAssetLinkUpdate?: Maybe<ReleaseAssetLinkUpdatePayload>
  releaseCreate?: Maybe<ReleaseCreatePayload>
  releaseDelete?: Maybe<ReleaseDeletePayload>
  releaseUpdate?: Maybe<ReleaseUpdatePayload>
  /** @deprecated Use awardEmojiRemove. Deprecated in 13.2. */
  removeAwardEmoji?: Maybe<RemoveAwardEmojiPayload>
  removeProjectFromSecurityDashboard?: Maybe<RemoveProjectFromSecurityDashboardPayload>
  /** Repositions a DiffNote on an image (a `Note` where the `position.positionType` is `"image"`) */
  repositionImageDiffNote?: Maybe<RepositionImageDiffNotePayload>
  /** @deprecated Use vulnerabilityRevertToDetected. Deprecated in 13.5. */
  revertVulnerabilityToDetected?: Maybe<RevertVulnerabilityToDetectedPayload>
  /** @deprecated Use DastOnDemandScanCreate. Deprecated in 13.4. */
  runDastScan?: Maybe<RunDastScanPayload>
  terraformStateDelete?: Maybe<TerraformStateDeletePayload>
  terraformStateLock?: Maybe<TerraformStateLockPayload>
  terraformStateUnlock?: Maybe<TerraformStateUnlockPayload>
  todoCreate?: Maybe<TodoCreatePayload>
  todoMarkDone?: Maybe<TodoMarkDonePayload>
  todoRestore?: Maybe<TodoRestorePayload>
  todoRestoreMany?: Maybe<TodoRestoreManyPayload>
  todosMarkAllDone?: Maybe<TodosMarkAllDonePayload>
  /** @deprecated Use awardEmojiToggle. Deprecated in 13.2. */
  toggleAwardEmoji?: Maybe<ToggleAwardEmojiPayload>
  updateAlertStatus?: Maybe<UpdateAlertStatusPayload>
  updateBoard?: Maybe<UpdateBoardPayload>
  updateBoardEpicUserPreferences?: Maybe<UpdateBoardEpicUserPreferencesPayload>
  updateBoardList?: Maybe<UpdateBoardListPayload>
  updateComplianceFramework?: Maybe<UpdateComplianceFrameworkPayload>
  updateContainerExpirationPolicy?: Maybe<UpdateContainerExpirationPolicyPayload>
  updateEpic?: Maybe<UpdateEpicPayload>
  updateEpicBoardList?: Maybe<UpdateEpicBoardListPayload>
  /**
   * Updates a DiffNote on an image (a `Note` where the `position.positionType` is `"image"`).
   * If the body of the Note contains only quick actions,
   * the Note will be destroyed during the update, and no Note will be
   * returned.
   */
  updateImageDiffNote?: Maybe<UpdateImageDiffNotePayload>
  updateIssue?: Maybe<UpdateIssuePayload>
  updateIteration?: Maybe<UpdateIterationPayload>
  updateNamespacePackageSettings?: Maybe<UpdateNamespacePackageSettingsPayload>
  /**
   * Updates a Note.
   * If the body of the Note contains only quick actions,
   * the Note will be destroyed during the update, and no Note will be
   * returned.
   */
  updateNote?: Maybe<UpdateNotePayload>
  updateRequirement?: Maybe<UpdateRequirementPayload>
  updateSnippet?: Maybe<UpdateSnippetPayload>
  userCalloutCreate?: Maybe<UserCalloutCreatePayload>
  vulnerabilityConfirm?: Maybe<VulnerabilityConfirmPayload>
  vulnerabilityDismiss?: Maybe<VulnerabilityDismissPayload>
  vulnerabilityExternalIssueLinkCreate?: Maybe<VulnerabilityExternalIssueLinkCreatePayload>
  vulnerabilityExternalIssueLinkDestroy?: Maybe<VulnerabilityExternalIssueLinkDestroyPayload>
  vulnerabilityResolve?: Maybe<VulnerabilityResolvePayload>
  vulnerabilityRevertToDetected?: Maybe<VulnerabilityRevertToDetectedPayload>
}

export type MutationAddAwardEmojiArgs = {
  input: AddAwardEmojiInput
}

export type MutationAddProjectToSecurityDashboardArgs = {
  input: AddProjectToSecurityDashboardInput
}

export type MutationAdminSidekiqQueuesDeleteJobsArgs = {
  input: AdminSidekiqQueuesDeleteJobsInput
}

export type MutationAlertSetAssigneesArgs = {
  input: AlertSetAssigneesInput
}

export type MutationAlertTodoCreateArgs = {
  input: AlertTodoCreateInput
}

export type MutationApiFuzzingCiConfigurationCreateArgs = {
  input: ApiFuzzingCiConfigurationCreateInput
}

export type MutationAwardEmojiAddArgs = {
  input: AwardEmojiAddInput
}

export type MutationAwardEmojiRemoveArgs = {
  input: AwardEmojiRemoveInput
}

export type MutationAwardEmojiToggleArgs = {
  input: AwardEmojiToggleInput
}

export type MutationBoardListCreateArgs = {
  input: BoardListCreateInput
}

export type MutationBoardListUpdateLimitMetricsArgs = {
  input: BoardListUpdateLimitMetricsInput
}

export type MutationBulkEnableDevopsAdoptionNamespacesArgs = {
  input: BulkEnableDevopsAdoptionNamespacesInput
}

export type MutationCiCdSettingsUpdateArgs = {
  input: CiCdSettingsUpdateInput
}

export type MutationClusterAgentDeleteArgs = {
  input: ClusterAgentDeleteInput
}

export type MutationClusterAgentTokenCreateArgs = {
  input: ClusterAgentTokenCreateInput
}

export type MutationClusterAgentTokenDeleteArgs = {
  input: ClusterAgentTokenDeleteInput
}

export type MutationCommitCreateArgs = {
  input: CommitCreateInput
}

export type MutationConfigureSastArgs = {
  input: ConfigureSastInput
}

export type MutationConfigureSecretDetectionArgs = {
  input: ConfigureSecretDetectionInput
}

export type MutationCreateAlertIssueArgs = {
  input: CreateAlertIssueInput
}

export type MutationCreateAnnotationArgs = {
  input: CreateAnnotationInput
}

export type MutationCreateBoardArgs = {
  input: CreateBoardInput
}

export type MutationCreateBranchArgs = {
  input: CreateBranchInput
}

export type MutationCreateClusterAgentArgs = {
  input: CreateClusterAgentInput
}

export type MutationCreateComplianceFrameworkArgs = {
  input: CreateComplianceFrameworkInput
}

export type MutationCreateDiffNoteArgs = {
  input: CreateDiffNoteInput
}

export type MutationCreateEpicArgs = {
  input: CreateEpicInput
}

export type MutationCreateImageDiffNoteArgs = {
  input: CreateImageDiffNoteInput
}

export type MutationCreateIssueArgs = {
  input: CreateIssueInput
}

export type MutationCreateIterationArgs = {
  input: CreateIterationInput
}

export type MutationCreateNoteArgs = {
  input: CreateNoteInput
}

export type MutationCreateRequirementArgs = {
  input: CreateRequirementInput
}

export type MutationCreateSnippetArgs = {
  input: CreateSnippetInput
}

export type MutationCreateTestCaseArgs = {
  input: CreateTestCaseInput
}

export type MutationDastOnDemandScanCreateArgs = {
  input: DastOnDemandScanCreateInput
}

export type MutationDastProfileCreateArgs = {
  input: DastProfileCreateInput
}

export type MutationDastProfileDeleteArgs = {
  input: DastProfileDeleteInput
}

export type MutationDastProfileRunArgs = {
  input: DastProfileRunInput
}

export type MutationDastProfileUpdateArgs = {
  input: DastProfileUpdateInput
}

export type MutationDastScannerProfileCreateArgs = {
  input: DastScannerProfileCreateInput
}

export type MutationDastScannerProfileDeleteArgs = {
  input: DastScannerProfileDeleteInput
}

export type MutationDastScannerProfileUpdateArgs = {
  input: DastScannerProfileUpdateInput
}

export type MutationDastSiteProfileCreateArgs = {
  input: DastSiteProfileCreateInput
}

export type MutationDastSiteProfileDeleteArgs = {
  input: DastSiteProfileDeleteInput
}

export type MutationDastSiteProfileUpdateArgs = {
  input: DastSiteProfileUpdateInput
}

export type MutationDastSiteTokenCreateArgs = {
  input: DastSiteTokenCreateInput
}

export type MutationDastSiteValidationCreateArgs = {
  input: DastSiteValidationCreateInput
}

export type MutationDastSiteValidationRevokeArgs = {
  input: DastSiteValidationRevokeInput
}

export type MutationDeleteAnnotationArgs = {
  input: DeleteAnnotationInput
}

export type MutationDesignManagementDeleteArgs = {
  input: DesignManagementDeleteInput
}

export type MutationDesignManagementMoveArgs = {
  input: DesignManagementMoveInput
}

export type MutationDesignManagementUploadArgs = {
  input: DesignManagementUploadInput
}

export type MutationDestroyBoardArgs = {
  input: DestroyBoardInput
}

export type MutationDestroyBoardListArgs = {
  input: DestroyBoardListInput
}

export type MutationDestroyComplianceFrameworkArgs = {
  input: DestroyComplianceFrameworkInput
}

export type MutationDestroyContainerRepositoryArgs = {
  input: DestroyContainerRepositoryInput
}

export type MutationDestroyContainerRepositoryTagsArgs = {
  input: DestroyContainerRepositoryTagsInput
}

export type MutationDestroyEpicBoardArgs = {
  input: DestroyEpicBoardInput
}

export type MutationDestroyNoteArgs = {
  input: DestroyNoteInput
}

export type MutationDestroySnippetArgs = {
  input: DestroySnippetInput
}

export type MutationDisableDevopsAdoptionNamespaceArgs = {
  input: DisableDevopsAdoptionNamespaceInput
}

export type MutationDiscussionToggleResolveArgs = {
  input: DiscussionToggleResolveInput
}

export type MutationDismissVulnerabilityArgs = {
  input: DismissVulnerabilityInput
}

export type MutationEnableDevopsAdoptionNamespaceArgs = {
  input: EnableDevopsAdoptionNamespaceInput
}

export type MutationEnvironmentsCanaryIngressUpdateArgs = {
  input: EnvironmentsCanaryIngressUpdateInput
}

export type MutationEpicAddIssueArgs = {
  input: EpicAddIssueInput
}

export type MutationEpicBoardCreateArgs = {
  input: EpicBoardCreateInput
}

export type MutationEpicBoardListCreateArgs = {
  input: EpicBoardListCreateInput
}

export type MutationEpicBoardListDestroyArgs = {
  input: EpicBoardListDestroyInput
}

export type MutationEpicBoardUpdateArgs = {
  input: EpicBoardUpdateInput
}

export type MutationEpicMoveListArgs = {
  input: EpicMoveListInput
}

export type MutationEpicSetSubscriptionArgs = {
  input: EpicSetSubscriptionInput
}

export type MutationEpicTreeReorderArgs = {
  input: EpicTreeReorderInput
}

export type MutationEscalationPolicyCreateArgs = {
  input: EscalationPolicyCreateInput
}

export type MutationEscalationPolicyDestroyArgs = {
  input: EscalationPolicyDestroyInput
}

export type MutationExportRequirementsArgs = {
  input: ExportRequirementsInput
}

export type MutationGitlabSubscriptionActivateArgs = {
  input: GitlabSubscriptionActivateInput
}

export type MutationHttpIntegrationCreateArgs = {
  input: HttpIntegrationCreateInput
}

export type MutationHttpIntegrationDestroyArgs = {
  input: HttpIntegrationDestroyInput
}

export type MutationHttpIntegrationResetTokenArgs = {
  input: HttpIntegrationResetTokenInput
}

export type MutationHttpIntegrationUpdateArgs = {
  input: HttpIntegrationUpdateInput
}

export type MutationIssueMoveArgs = {
  input: IssueMoveInput
}

export type MutationIssueMoveListArgs = {
  input: IssueMoveListInput
}

export type MutationIssueSetAssigneesArgs = {
  input: IssueSetAssigneesInput
}

export type MutationIssueSetConfidentialArgs = {
  input: IssueSetConfidentialInput
}

export type MutationIssueSetDueDateArgs = {
  input: IssueSetDueDateInput
}

export type MutationIssueSetEpicArgs = {
  input: IssueSetEpicInput
}

export type MutationIssueSetIterationArgs = {
  input: IssueSetIterationInput
}

export type MutationIssueSetLockedArgs = {
  input: IssueSetLockedInput
}

export type MutationIssueSetSeverityArgs = {
  input: IssueSetSeverityInput
}

export type MutationIssueSetSubscriptionArgs = {
  input: IssueSetSubscriptionInput
}

export type MutationIssueSetWeightArgs = {
  input: IssueSetWeightInput
}

export type MutationIterationCadenceCreateArgs = {
  input: IterationCadenceCreateInput
}

export type MutationIterationCadenceDestroyArgs = {
  input: IterationCadenceDestroyInput
}

export type MutationIterationCadenceUpdateArgs = {
  input: IterationCadenceUpdateInput
}

export type MutationIterationDeleteArgs = {
  input: IterationDeleteInput
}

export type MutationJiraImportStartArgs = {
  input: JiraImportStartInput
}

export type MutationJiraImportUsersArgs = {
  input: JiraImportUsersInput
}

export type MutationJobPlayArgs = {
  input: JobPlayInput
}

export type MutationJobRetryArgs = {
  input: JobRetryInput
}

export type MutationLabelCreateArgs = {
  input: LabelCreateInput
}

export type MutationMarkAsSpamSnippetArgs = {
  input: MarkAsSpamSnippetInput
}

export type MutationMergeRequestAcceptArgs = {
  input: MergeRequestAcceptInput
}

export type MutationMergeRequestCreateArgs = {
  input: MergeRequestCreateInput
}

export type MutationMergeRequestReviewerRereviewArgs = {
  input: MergeRequestReviewerRereviewInput
}

export type MutationMergeRequestSetAssigneesArgs = {
  input: MergeRequestSetAssigneesInput
}

export type MutationMergeRequestSetDraftArgs = {
  input: MergeRequestSetDraftInput
}

export type MutationMergeRequestSetLabelsArgs = {
  input: MergeRequestSetLabelsInput
}

export type MutationMergeRequestSetLockedArgs = {
  input: MergeRequestSetLockedInput
}

export type MutationMergeRequestSetMilestoneArgs = {
  input: MergeRequestSetMilestoneInput
}

export type MutationMergeRequestSetSubscriptionArgs = {
  input: MergeRequestSetSubscriptionInput
}

export type MutationMergeRequestSetWipArgs = {
  input: MergeRequestSetWipInput
}

export type MutationMergeRequestUpdateArgs = {
  input: MergeRequestUpdateInput
}

export type MutationNamespaceIncreaseStorageTemporarilyArgs = {
  input: NamespaceIncreaseStorageTemporarilyInput
}

export type MutationOncallRotationCreateArgs = {
  input: OncallRotationCreateInput
}

export type MutationOncallRotationDestroyArgs = {
  input: OncallRotationDestroyInput
}

export type MutationOncallRotationUpdateArgs = {
  input: OncallRotationUpdateInput
}

export type MutationOncallScheduleCreateArgs = {
  input: OncallScheduleCreateInput
}

export type MutationOncallScheduleDestroyArgs = {
  input: OncallScheduleDestroyInput
}

export type MutationOncallScheduleUpdateArgs = {
  input: OncallScheduleUpdateInput
}

export type MutationPipelineCancelArgs = {
  input: PipelineCancelInput
}

export type MutationPipelineDestroyArgs = {
  input: PipelineDestroyInput
}

export type MutationPipelineRetryArgs = {
  input: PipelineRetryInput
}

export type MutationPrometheusIntegrationCreateArgs = {
  input: PrometheusIntegrationCreateInput
}

export type MutationPrometheusIntegrationResetTokenArgs = {
  input: PrometheusIntegrationResetTokenInput
}

export type MutationPrometheusIntegrationUpdateArgs = {
  input: PrometheusIntegrationUpdateInput
}

export type MutationPromoteToEpicArgs = {
  input: PromoteToEpicInput
}

export type MutationReleaseAssetLinkCreateArgs = {
  input: ReleaseAssetLinkCreateInput
}

export type MutationReleaseAssetLinkDeleteArgs = {
  input: ReleaseAssetLinkDeleteInput
}

export type MutationReleaseAssetLinkUpdateArgs = {
  input: ReleaseAssetLinkUpdateInput
}

export type MutationReleaseCreateArgs = {
  input: ReleaseCreateInput
}

export type MutationReleaseDeleteArgs = {
  input: ReleaseDeleteInput
}

export type MutationReleaseUpdateArgs = {
  input: ReleaseUpdateInput
}

export type MutationRemoveAwardEmojiArgs = {
  input: RemoveAwardEmojiInput
}

export type MutationRemoveProjectFromSecurityDashboardArgs = {
  input: RemoveProjectFromSecurityDashboardInput
}

export type MutationRepositionImageDiffNoteArgs = {
  input: RepositionImageDiffNoteInput
}

export type MutationRevertVulnerabilityToDetectedArgs = {
  input: RevertVulnerabilityToDetectedInput
}

export type MutationRunDastScanArgs = {
  input: RunDastScanInput
}

export type MutationTerraformStateDeleteArgs = {
  input: TerraformStateDeleteInput
}

export type MutationTerraformStateLockArgs = {
  input: TerraformStateLockInput
}

export type MutationTerraformStateUnlockArgs = {
  input: TerraformStateUnlockInput
}

export type MutationTodoCreateArgs = {
  input: TodoCreateInput
}

export type MutationTodoMarkDoneArgs = {
  input: TodoMarkDoneInput
}

export type MutationTodoRestoreArgs = {
  input: TodoRestoreInput
}

export type MutationTodoRestoreManyArgs = {
  input: TodoRestoreManyInput
}

export type MutationTodosMarkAllDoneArgs = {
  input: TodosMarkAllDoneInput
}

export type MutationToggleAwardEmojiArgs = {
  input: ToggleAwardEmojiInput
}

export type MutationUpdateAlertStatusArgs = {
  input: UpdateAlertStatusInput
}

export type MutationUpdateBoardArgs = {
  input: UpdateBoardInput
}

export type MutationUpdateBoardEpicUserPreferencesArgs = {
  input: UpdateBoardEpicUserPreferencesInput
}

export type MutationUpdateBoardListArgs = {
  input: UpdateBoardListInput
}

export type MutationUpdateComplianceFrameworkArgs = {
  input: UpdateComplianceFrameworkInput
}

export type MutationUpdateContainerExpirationPolicyArgs = {
  input: UpdateContainerExpirationPolicyInput
}

export type MutationUpdateEpicArgs = {
  input: UpdateEpicInput
}

export type MutationUpdateEpicBoardListArgs = {
  input: UpdateEpicBoardListInput
}

export type MutationUpdateImageDiffNoteArgs = {
  input: UpdateImageDiffNoteInput
}

export type MutationUpdateIssueArgs = {
  input: UpdateIssueInput
}

export type MutationUpdateIterationArgs = {
  input: UpdateIterationInput
}

export type MutationUpdateNamespacePackageSettingsArgs = {
  input: UpdateNamespacePackageSettingsInput
}

export type MutationUpdateNoteArgs = {
  input: UpdateNoteInput
}

export type MutationUpdateRequirementArgs = {
  input: UpdateRequirementInput
}

export type MutationUpdateSnippetArgs = {
  input: UpdateSnippetInput
}

export type MutationUserCalloutCreateArgs = {
  input: UserCalloutCreateInput
}

export type MutationVulnerabilityConfirmArgs = {
  input: VulnerabilityConfirmInput
}

export type MutationVulnerabilityDismissArgs = {
  input: VulnerabilityDismissInput
}

export type MutationVulnerabilityExternalIssueLinkCreateArgs = {
  input: VulnerabilityExternalIssueLinkCreateInput
}

export type MutationVulnerabilityExternalIssueLinkDestroyArgs = {
  input: VulnerabilityExternalIssueLinkDestroyInput
}

export type MutationVulnerabilityResolveArgs = {
  input: VulnerabilityResolveInput
}

export type MutationVulnerabilityRevertToDetectedArgs = {
  input: VulnerabilityRevertToDetectedInput
}

/** Different toggles for changing mutator behavior */
export enum MutationOperationMode {
  /** Performs a replace operation. */
  Replace = "REPLACE",
  /** Performs an append operation. */
  Append = "APPEND",
  /** Performs a removal operation. */
  Remove = "REMOVE",
}

export type Namespace = {
  __typename?: "Namespace"
  /** Size limit for repositories in the namespace in bytes. */
  actualRepositorySizeLimit?: Maybe<Scalars["Float"]>
  /** Additional storage purchased for the root namespace in bytes. */
  additionalPurchasedStorageSize?: Maybe<Scalars["Float"]>
  /** Compliance frameworks available to projects in this namespace. */
  complianceFrameworks?: Maybe<ComplianceFrameworkConnection>
  /** Includes at least one project where the repository size exceeds the limit. */
  containsLockedProjects: Scalars["Boolean"]
  /** Description of the namespace. */
  description?: Maybe<Scalars["String"]>
  /** The GitLab Flavored Markdown rendering of `description` */
  descriptionHtml?: Maybe<Scalars["String"]>
  /** Full name of the namespace. */
  fullName: Scalars["String"]
  /** Full path of the namespace. */
  fullPath: Scalars["ID"]
  /** ID of the namespace. */
  id: Scalars["ID"]
  /** Status of the temporary storage increase. */
  isTemporaryStorageIncreaseEnabled: Scalars["Boolean"]
  /** Indicates if Large File Storage (LFS) is enabled for namespace. */
  lfsEnabled?: Maybe<Scalars["Boolean"]>
  /** Name of the namespace. */
  name: Scalars["String"]
  /** The package settings for the namespace. */
  packageSettings?: Maybe<PackageSettings>
  /** Path of the namespace. */
  path: Scalars["String"]
  /** Projects within this namespace. */
  projects: ProjectConnection
  /** Number of projects in the root namespace where the repository size exceeds the limit. */
  repositorySizeExcessProjectCount: Scalars["Int"]
  /** Indicates if users can request access to namespace. */
  requestAccessEnabled?: Maybe<Scalars["Boolean"]>
  /** Aggregated storage statistics of the namespace. Only available for root namespaces. */
  rootStorageStatistics?: Maybe<RootStorageStatistics>
  /** Total storage limit of the root namespace in bytes. */
  storageSizeLimit?: Maybe<Scalars["Float"]>
  /** Date until the temporary storage increase is active. */
  temporaryStorageIncreaseEndsOn?: Maybe<Scalars["Time"]>
  /** Total repository size of all projects in the root namespace in bytes. */
  totalRepositorySize?: Maybe<Scalars["Float"]>
  /** Total excess repository size of all projects in the root namespace in bytes. */
  totalRepositorySizeExcess?: Maybe<Scalars["Float"]>
  /** Visibility of the namespace. */
  visibility?: Maybe<Scalars["String"]>
}

export type NamespaceComplianceFrameworksArgs = {
  id?: Maybe<Scalars["ComplianceManagementFrameworkID"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type NamespaceProjectsArgs = {
  includeSubgroups?: Maybe<Scalars["Boolean"]>
  search?: Maybe<Scalars["String"]>
  sort?: Maybe<NamespaceProjectSort>
  ids?: Maybe<Array<Scalars["ID"]>>
  hasCodeCoverage?: Maybe<Scalars["Boolean"]>
  hasVulnerabilities?: Maybe<Scalars["Boolean"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** The connection type for Namespace. */
export type NamespaceConnection = {
  __typename?: "NamespaceConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<NamespaceEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<Namespace>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type NamespaceEdge = {
  __typename?: "NamespaceEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<Namespace>
}

/** Autogenerated input type of NamespaceIncreaseStorageTemporarily */
export type NamespaceIncreaseStorageTemporarilyInput = {
  /** The global ID of the namespace to mutate. */
  id: Scalars["NamespaceID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of NamespaceIncreaseStorageTemporarily */
export type NamespaceIncreaseStorageTemporarilyPayload = {
  __typename?: "NamespaceIncreaseStorageTemporarilyPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The namespace after mutation. */
  namespace?: Maybe<Namespace>
}

/** Values for sorting projects */
export enum NamespaceProjectSort {
  /** Most similar to the search query. */
  Similarity = "SIMILARITY",
  /** Sort by storage size. */
  Storage = "STORAGE",
}

export type NegatedBoardIssueInput = {
  /** Filter by label name. */
  labelName?: Maybe<Array<Maybe<Scalars["String"]>>>
  /** Filter by author username. */
  authorUsername?: Maybe<Scalars["String"]>
  /** Filter by reaction emoji applied by the current user. */
  myReactionEmoji?: Maybe<Scalars["String"]>
  /** List of IIDs of issues. For example ["1", "2"]. */
  iids?: Maybe<Array<Scalars["String"]>>
  /** Filter by milestone title. */
  milestoneTitle?: Maybe<Scalars["String"]>
  /** Filter by assignee username. */
  assigneeUsername?: Maybe<Array<Maybe<Scalars["String"]>>>
  /** Filter by release tag. */
  releaseTag?: Maybe<Scalars["String"]>
  /** Filter by epic ID. Incompatible with epicWildcardId. */
  epicId?: Maybe<Scalars["EpicID"]>
  /** Filter by iteration title. */
  iterationTitle?: Maybe<Scalars["String"]>
  /** Filter by weight. */
  weight?: Maybe<Scalars["String"]>
  /** Filter by a list of iteration IDs. Incompatible with iterationWildcardId. */
  iterationId?: Maybe<Array<Scalars["IterationID"]>>
  /** Filter by iteration ID wildcard. */
  iterationWildcardId?: Maybe<NegatedIterationWildcardId>
}

export type NegatedEpicBoardIssueInput = {
  /** Filter by label name. */
  labelName?: Maybe<Array<Maybe<Scalars["String"]>>>
  /** Filter by author username. */
  authorUsername?: Maybe<Scalars["String"]>
  /** Filter by reaction emoji applied by the current user. */
  myReactionEmoji?: Maybe<Scalars["String"]>
}

export type NegatedEpicFilterInput = {
  /** Filter by label name. */
  labelName?: Maybe<Array<Maybe<Scalars["String"]>>>
  /** Filter by author username. */
  authorUsername?: Maybe<Scalars["String"]>
  /** Filter by reaction emoji applied by the current user. */
  myReactionEmoji?: Maybe<Scalars["String"]>
}

export type NegatedIssueFilterInput = {
  /** List of IIDs of issues to exclude. For example, [1, 2]. */
  iids?: Maybe<Array<Scalars["String"]>>
  /** Labels not applied to this issue. */
  labelName?: Maybe<Array<Scalars["String"]>>
  /** Milestone not applied to this issue. */
  milestoneTitle?: Maybe<Array<Scalars["String"]>>
  /** Usernames of users not assigned to the issue. */
  assigneeUsernames?: Maybe<Array<Scalars["String"]>>
  /** ID of a user not assigned to the issues. */
  assigneeId?: Maybe<Scalars["String"]>
  /** ID of an epic not associated with the issues. */
  epicId?: Maybe<Scalars["String"]>
  /** Weight not applied to the issue. */
  weight?: Maybe<Scalars["String"]>
  /** List of iteration Global IDs not applied to the issue. */
  iterationId?: Maybe<Array<Scalars["ID"]>>
  /** Filter by negated iteration ID wildcard. */
  iterationWildcardId?: Maybe<IterationWildcardId>
}

/** Negated Iteration ID wildcard values */
export enum NegatedIterationWildcardId {
  /** Current iteration. */
  Current = "CURRENT",
}

export type Note = ResolvableInterface & {
  __typename?: "Note"
  /** User who wrote this note. */
  author: UserCore
  /** Content of the note. */
  body: Scalars["String"]
  /** The GitLab Flavored Markdown rendering of `note` */
  bodyHtml?: Maybe<Scalars["String"]>
  /** Indicates if this note is confidential. */
  confidential?: Maybe<Scalars["Boolean"]>
  /** Timestamp of the note creation. */
  createdAt: Scalars["Time"]
  /** The discussion this note is a part of. */
  discussion?: Maybe<Discussion>
  /** ID of the note. */
  id: Scalars["NoteID"]
  /** The position of this note on a diff. */
  position?: Maybe<DiffPosition>
  /** Project associated with the note. */
  project?: Maybe<Project>
  /** Indicates if the object can be resolved. */
  resolvable: Scalars["Boolean"]
  /** Indicates if the object is resolved. */
  resolved: Scalars["Boolean"]
  /** Timestamp of when the object was resolved. */
  resolvedAt?: Maybe<Scalars["Time"]>
  /** User who resolved the object. */
  resolvedBy?: Maybe<UserCore>
  /** Indicates whether this note was created by the system or by a user. */
  system: Scalars["Boolean"]
  /** Name of the icon corresponding to a system note. */
  systemNoteIconName?: Maybe<Scalars["String"]>
  /** Timestamp of the note's last activity. */
  updatedAt: Scalars["Time"]
  /** URL to view this Note in the Web UI. */
  url?: Maybe<Scalars["String"]>
  /** Permissions for the current user on the resource */
  userPermissions: NotePermissions
}

/** The connection type for Note. */
export type NoteConnection = {
  __typename?: "NoteConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<NoteEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<Note>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type NoteEdge = {
  __typename?: "NoteEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<Note>
}

export type NotePermissions = {
  __typename?: "NotePermissions"
  /** Indicates the user can perform `admin_note` on this resource */
  adminNote: Scalars["Boolean"]
  /** Indicates the user can perform `award_emoji` on this resource */
  awardEmoji: Scalars["Boolean"]
  /** Indicates the user can perform `create_note` on this resource */
  createNote: Scalars["Boolean"]
  /** Indicates the user can perform `read_note` on this resource */
  readNote: Scalars["Boolean"]
  /** Indicates the user can perform `reposition_note` on this resource */
  repositionNote: Scalars["Boolean"]
  /** Indicates the user can perform `resolve_note` on this resource */
  resolveNote: Scalars["Boolean"]
}

export type Noteable = {
  /** All discussions on this noteable. */
  discussions: DiscussionConnection
  /** All notes on this noteable. */
  notes: NoteConnection
}

export type NoteableDiscussionsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type NoteableNotesArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Nuget metadata */
export type NugetMetadata = {
  __typename?: "NugetMetadata"
  /** Icon URL of the Nuget package. */
  iconUrl: Scalars["String"]
  /** ID of the metadatum. */
  id: Scalars["PackagesNugetMetadatumID"]
  /** License URL of the Nuget package. */
  licenseUrl: Scalars["String"]
  /** Project URL of the Nuget package. */
  projectUrl: Scalars["String"]
}

/** The rotation participant and color palette */
export type OncallParticipantType = {
  __typename?: "OncallParticipantType"
  /** The color palette to assign to the on-call user. For example "blue". */
  colorPalette?: Maybe<Scalars["String"]>
  /** The color weight to assign to for the on-call user, for example "500". Max 4 chars. For easy identification of the user. */
  colorWeight?: Maybe<Scalars["String"]>
  /** ID of the on-call participant. */
  id: Scalars["IncidentManagementOncallParticipantID"]
  /** The user who is participating. */
  user: UserCore
}

/** The connection type for OncallParticipantType. */
export type OncallParticipantTypeConnection = {
  __typename?: "OncallParticipantTypeConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<OncallParticipantTypeEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<OncallParticipantType>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type OncallParticipantTypeEdge = {
  __typename?: "OncallParticipantTypeEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<OncallParticipantType>
}

/** Active period time range for on-call rotation */
export type OncallRotationActivePeriodInputType = {
  /** The start of the rotation active period in 24 hour format, for example "18:30". */
  startTime: Scalars["String"]
  /** The end of the rotation active period in 24 hour format, for example "18:30". */
  endTime: Scalars["String"]
}

/** Active period time range for on-call rotation */
export type OncallRotationActivePeriodType = {
  __typename?: "OncallRotationActivePeriodType"
  /** The end of the rotation active period. */
  endTime?: Maybe<Scalars["String"]>
  /** The start of the rotation active period. */
  startTime?: Maybe<Scalars["String"]>
}

/** Autogenerated input type of OncallRotationCreate */
export type OncallRotationCreateInput = {
  /** The project to create the on-call schedule in. */
  projectPath: Scalars["ID"]
  /** The IID of the on-call schedule to create the on-call rotation in. */
  scheduleIid: Scalars["String"]
  /** The name of the on-call rotation. */
  name: Scalars["String"]
  /** The start date and time of the on-call rotation, in the timezone of the on-call schedule. */
  startsAt: OncallRotationDateInputType
  /** The end date and time of the on-call rotation, in the timezone of the on-call schedule. */
  endsAt?: Maybe<OncallRotationDateInputType>
  /** The rotation length of the on-call rotation. */
  rotationLength: OncallRotationLengthInputType
  /** The active period of time that the on-call rotation should take place. */
  activePeriod?: Maybe<OncallRotationActivePeriodInputType>
  /** The usernames of users participating in the on-call rotation. A maximum limit of 100 participants applies. */
  participants: Array<OncallUserInputType>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of OncallRotationCreate */
export type OncallRotationCreatePayload = {
  __typename?: "OncallRotationCreatePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The on-call rotation. */
  oncallRotation?: Maybe<IncidentManagementOncallRotation>
}

/** Date input type for on-call rotation */
export type OncallRotationDateInputType = {
  /** The date component of the date in YYYY-MM-DD format. */
  date: Scalars["String"]
  /** The time component of the date in 24hr HH:MM format. */
  time: Scalars["String"]
}

/** Autogenerated input type of OncallRotationDestroy */
export type OncallRotationDestroyInput = {
  /** The project to remove the on-call schedule from. */
  projectPath: Scalars["ID"]
  /** The IID of the on-call schedule to the on-call rotation belongs to. */
  scheduleIid: Scalars["String"]
  /** The ID of the on-call rotation to remove. */
  id: Scalars["IncidentManagementOncallRotationID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of OncallRotationDestroy */
export type OncallRotationDestroyPayload = {
  __typename?: "OncallRotationDestroyPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The on-call rotation. */
  oncallRotation?: Maybe<IncidentManagementOncallRotation>
}

/** The rotation length of the on-call rotation */
export type OncallRotationLengthInputType = {
  /** The rotation length of the on-call rotation. */
  length: Scalars["Int"]
  /** The unit of the rotation length of the on-call rotation. */
  unit: OncallRotationUnitEnum
}

/** Rotation length unit of an on-call rotation */
export enum OncallRotationUnitEnum {
  /** Hours */
  Hours = "HOURS",
  /** Days */
  Days = "DAYS",
  /** Weeks */
  Weeks = "WEEKS",
}

/** Autogenerated input type of OncallRotationUpdate */
export type OncallRotationUpdateInput = {
  /** The ID of the on-call schedule to create the on-call rotation in. */
  id: Scalars["IncidentManagementOncallRotationID"]
  /** The name of the on-call rotation. */
  name?: Maybe<Scalars["String"]>
  /** The start date and time of the on-call rotation, in the timezone of the on-call schedule. */
  startsAt?: Maybe<OncallRotationDateInputType>
  /** The end date and time of the on-call rotation, in the timezone of the on-call schedule. */
  endsAt?: Maybe<OncallRotationDateInputType>
  /** The rotation length of the on-call rotation. */
  rotationLength?: Maybe<OncallRotationLengthInputType>
  /** The active period of time that the on-call rotation should take place. */
  activePeriod?: Maybe<OncallRotationActivePeriodInputType>
  /** The usernames of users participating in the on-call rotation. A maximum limit of 100 participants applies. */
  participants?: Maybe<Array<OncallUserInputType>>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of OncallRotationUpdate */
export type OncallRotationUpdatePayload = {
  __typename?: "OncallRotationUpdatePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The on-call rotation. */
  oncallRotation?: Maybe<IncidentManagementOncallRotation>
}

/** Autogenerated input type of OncallScheduleCreate */
export type OncallScheduleCreateInput = {
  /** The project to create the on-call schedule in. */
  projectPath: Scalars["ID"]
  /** The name of the on-call schedule. */
  name: Scalars["String"]
  /** The description of the on-call schedule. */
  description?: Maybe<Scalars["String"]>
  /** The timezone of the on-call schedule. */
  timezone: Scalars["String"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of OncallScheduleCreate */
export type OncallScheduleCreatePayload = {
  __typename?: "OncallScheduleCreatePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The on-call schedule. */
  oncallSchedule?: Maybe<IncidentManagementOncallSchedule>
}

/** Autogenerated input type of OncallScheduleDestroy */
export type OncallScheduleDestroyInput = {
  /** The project to remove the on-call schedule from. */
  projectPath: Scalars["ID"]
  /** The on-call schedule internal ID to remove. */
  iid: Scalars["String"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of OncallScheduleDestroy */
export type OncallScheduleDestroyPayload = {
  __typename?: "OncallScheduleDestroyPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The on-call schedule. */
  oncallSchedule?: Maybe<IncidentManagementOncallSchedule>
}

/** Autogenerated input type of OncallScheduleUpdate */
export type OncallScheduleUpdateInput = {
  /** The project to update the on-call schedule in. */
  projectPath: Scalars["ID"]
  /** The on-call schedule internal ID to update. */
  iid: Scalars["String"]
  /** The name of the on-call schedule. */
  name?: Maybe<Scalars["String"]>
  /** The description of the on-call schedule. */
  description?: Maybe<Scalars["String"]>
  /** The timezone of the on-call schedule. */
  timezone?: Maybe<Scalars["String"]>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of OncallScheduleUpdate */
export type OncallScheduleUpdatePayload = {
  __typename?: "OncallScheduleUpdatePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The on-call schedule. */
  oncallSchedule?: Maybe<IncidentManagementOncallSchedule>
}

/** The rotation user and color palette */
export type OncallUserInputType = {
  /** The username of the user to participate in the on-call rotation, such as `user_one`. */
  username: Scalars["String"]
  /** A value of DataVisualizationColorEnum. The color from the palette to assign to the on-call user. */
  colorPalette?: Maybe<DataVisualizationColorEnum>
  /** A value of DataVisualizationWeightEnum. The color weight to assign to for the on-call user. Note: To view on-call schedules in GitLab, do not provide a value below 500. A value between 500 and 950 ensures sufficient contrast. */
  colorWeight?: Maybe<DataVisualizationWeightEnum>
}

/** Represents a package in the Package Registry. Note that this type is in beta and susceptible to changes */
export type Package = {
  __typename?: "Package"
  /** Date of creation. */
  createdAt: Scalars["Time"]
  /** ID of the package. */
  id: Scalars["PackagesPackageID"]
  /** Package metadata. */
  metadata?: Maybe<PackageMetadata>
  /** Name of the package. */
  name: Scalars["String"]
  /** Package type. */
  packageType: PackageTypeEnum
  /** Pipelines that built the package. */
  pipelines?: Maybe<PipelineConnection>
  /** Project where the package is stored. */
  project: Project
  /** Package status. */
  status: PackageStatus
  /** Package tags. */
  tags?: Maybe<PackageTagConnection>
  /** Date of most recent update. */
  updatedAt: Scalars["Time"]
  /** Version string. */
  version?: Maybe<Scalars["String"]>
  /**
   * The other versions of the package. Deprecated in 13.11: This field is now only returned in the PackageDetailsType.
   * @deprecated This field is now only returned in the PackageDetailsType. Deprecated in 13.11.
   */
  versions?: Maybe<PackageConnection>
}

/** Represents a package in the Package Registry. Note that this type is in beta and susceptible to changes */
export type PackagePipelinesArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Represents a package in the Package Registry. Note that this type is in beta and susceptible to changes */
export type PackageTagsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Represents a package in the Package Registry. Note that this type is in beta and susceptible to changes */
export type PackageVersionsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Represents a composer JSON file */
export type PackageComposerJsonType = {
  __typename?: "PackageComposerJsonType"
  /** The license set in the Composer JSON file. */
  license?: Maybe<Scalars["String"]>
  /** The name set in the Composer JSON file. */
  name?: Maybe<Scalars["String"]>
  /** The type set in the Composer JSON file. */
  type?: Maybe<Scalars["String"]>
  /** The version set in the Composer JSON file. */
  version?: Maybe<Scalars["String"]>
}

/** The connection type for Package. */
export type PackageConnection = {
  __typename?: "PackageConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<PackageEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<Package>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** Represents a package details in the Package Registry. Note that this type is in beta and susceptible to changes */
export type PackageDetailsType = {
  __typename?: "PackageDetailsType"
  /** Date of creation. */
  createdAt: Scalars["Time"]
  /** ID of the package. */
  id: Scalars["PackagesPackageID"]
  /** Package metadata. */
  metadata?: Maybe<PackageMetadata>
  /** Name of the package. */
  name: Scalars["String"]
  /** Package files. */
  packageFiles?: Maybe<PackageFileConnection>
  /** Package type. */
  packageType: PackageTypeEnum
  /** Pipelines that built the package. */
  pipelines?: Maybe<PipelineConnection>
  /** Project where the package is stored. */
  project: Project
  /** Package status. */
  status: PackageStatus
  /** Package tags. */
  tags?: Maybe<PackageTagConnection>
  /** Date of most recent update. */
  updatedAt: Scalars["Time"]
  /** Version string. */
  version?: Maybe<Scalars["String"]>
  /** The other versions of the package. */
  versions?: Maybe<PackageConnection>
}

/** Represents a package details in the Package Registry. Note that this type is in beta and susceptible to changes */
export type PackageDetailsTypePackageFilesArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Represents a package details in the Package Registry. Note that this type is in beta and susceptible to changes */
export type PackageDetailsTypePipelinesArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Represents a package details in the Package Registry. Note that this type is in beta and susceptible to changes */
export type PackageDetailsTypeTagsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Represents a package details in the Package Registry. Note that this type is in beta and susceptible to changes */
export type PackageDetailsTypeVersionsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** An edge in a connection. */
export type PackageEdge = {
  __typename?: "PackageEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<Package>
}

/** Represents a package file */
export type PackageFile = {
  __typename?: "PackageFile"
  /** The created date. */
  createdAt: Scalars["Time"]
  /** Download path of the package file. */
  downloadPath: Scalars["String"]
  /** Md5 of the package file. */
  fileMd5?: Maybe<Scalars["String"]>
  /** File metadata. */
  fileMetadata?: Maybe<PackageFileMetadata>
  /** Name of the package file. */
  fileName: Scalars["String"]
  /** Sha1 of the package file. */
  fileSha1?: Maybe<Scalars["String"]>
  /** Sha256 of the package file. */
  fileSha256?: Maybe<Scalars["String"]>
  /** ID of the file. */
  id: Scalars["PackagesPackageFileID"]
  /** Size of the package file. */
  size: Scalars["String"]
  /** The updated date. */
  updatedAt: Scalars["Time"]
}

/** The connection type for PackageFile. */
export type PackageFileConnection = {
  __typename?: "PackageFileConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<PackageFileEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<PackageFile>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type PackageFileEdge = {
  __typename?: "PackageFileEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<PackageFile>
}

/** Represents metadata associated with a Package file */
export type PackageFileMetadata = {
  /** Date of creation. */
  createdAt: Scalars["Time"]
  /** Date of most recent update. */
  updatedAt: Scalars["Time"]
}

/** Represents the Geo sync and verification state of a package file */
export type PackageFileRegistry = {
  __typename?: "PackageFileRegistry"
  /** Timestamp when the PackageFileRegistry was created */
  createdAt?: Maybe<Scalars["Time"]>
  /** ID of the PackageFileRegistry */
  id: Scalars["ID"]
  /** Error message during sync of the PackageFileRegistry */
  lastSyncFailure?: Maybe<Scalars["String"]>
  /** Timestamp of the most recent successful sync of the PackageFileRegistry */
  lastSyncedAt?: Maybe<Scalars["Time"]>
  /** ID of the PackageFile. */
  packageFileId: Scalars["ID"]
  /** Timestamp after which the PackageFileRegistry should be resynced */
  retryAt?: Maybe<Scalars["Time"]>
  /** Number of consecutive failed sync attempts of the PackageFileRegistry */
  retryCount?: Maybe<Scalars["Int"]>
  /** Sync state of the PackageFileRegistry */
  state?: Maybe<RegistryState>
}

/** The connection type for PackageFileRegistry. */
export type PackageFileRegistryConnection = {
  __typename?: "PackageFileRegistryConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<PackageFileRegistryEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<PackageFileRegistry>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type PackageFileRegistryEdge = {
  __typename?: "PackageFileRegistryEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<PackageFileRegistry>
}

/** Values for sorting group packages */
export enum PackageGroupSort {
  /** Ordered by created_at in descending order. */
  CreatedDesc = "CREATED_DESC",
  /** Ordered by created_at in ascending order. */
  CreatedAsc = "CREATED_ASC",
  /** Ordered by name in descending order. */
  NameDesc = "NAME_DESC",
  /** Ordered by name in ascending order. */
  NameAsc = "NAME_ASC",
  /** Ordered by version in descending order. */
  VersionDesc = "VERSION_DESC",
  /** Ordered by version in ascending order. */
  VersionAsc = "VERSION_ASC",
  /** Ordered by type in descending order. */
  TypeDesc = "TYPE_DESC",
  /** Ordered by type in ascending order. */
  TypeAsc = "TYPE_ASC",
}

/** Represents metadata associated with a Package */
export type PackageMetadata =
  | ComposerMetadata
  | ConanMetadata
  | MavenMetadata
  | NugetMetadata
  | PypiMetadata

/** Namespace-level Package Registry settings */
export type PackageSettings = {
  __typename?: "PackageSettings"
  /** When generic_duplicates_allowed is false, you can publish duplicate packages with names that match this regex. Otherwise, this setting has no effect. */
  genericDuplicateExceptionRegex?: Maybe<Scalars["UntrustedRegexp"]>
  /** Indicates whether duplicate generic packages are allowed for this namespace. */
  genericDuplicatesAllowed: Scalars["Boolean"]
  /** When maven_duplicates_allowed is false, you can publish duplicate packages with names that match this regex. Otherwise, this setting has no effect. */
  mavenDuplicateExceptionRegex?: Maybe<Scalars["UntrustedRegexp"]>
  /** Indicates whether duplicate Maven packages are allowed for this namespace. */
  mavenDuplicatesAllowed: Scalars["Boolean"]
}

/** Values for sorting package */
export enum PackageSort {
  /** Ordered by created_at in descending order. */
  CreatedDesc = "CREATED_DESC",
  /** Ordered by created_at in ascending order. */
  CreatedAsc = "CREATED_ASC",
  /** Ordered by name in descending order. */
  NameDesc = "NAME_DESC",
  /** Ordered by name in ascending order. */
  NameAsc = "NAME_ASC",
  /** Ordered by version in descending order. */
  VersionDesc = "VERSION_DESC",
  /** Ordered by version in ascending order. */
  VersionAsc = "VERSION_ASC",
  /** Ordered by type in descending order. */
  TypeDesc = "TYPE_DESC",
  /** Ordered by type in ascending order. */
  TypeAsc = "TYPE_ASC",
}

export enum PackageStatus {
  /** Packages with a default status */
  Default = "DEFAULT",
  /** Packages with a hidden status */
  Hidden = "HIDDEN",
  /** Packages with a processing status */
  Processing = "PROCESSING",
  /** Packages with a error status */
  Error = "ERROR",
}

/** Represents a package tag */
export type PackageTag = {
  __typename?: "PackageTag"
  /** The created date. */
  createdAt: Scalars["Time"]
  /** The ID of the tag. */
  id: Scalars["ID"]
  /** The name of the tag. */
  name: Scalars["String"]
  /** The updated date. */
  updatedAt: Scalars["Time"]
}

/** The connection type for PackageTag. */
export type PackageTagConnection = {
  __typename?: "PackageTagConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<PackageTagEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<PackageTag>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type PackageTagEdge = {
  __typename?: "PackageTagEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<PackageTag>
}

export enum PackageTypeEnum {
  /** Packages from the Maven package manager */
  Maven = "MAVEN",
  /** Packages from the npm package manager */
  Npm = "NPM",
  /** Packages from the Conan package manager */
  Conan = "CONAN",
  /** Packages from the Nuget package manager */
  Nuget = "NUGET",
  /** Packages from the PyPI package manager */
  Pypi = "PYPI",
  /** Packages from the Composer package manager */
  Composer = "COMPOSER",
  /** Packages from the Generic package manager */
  Generic = "GENERIC",
  /** Packages from the Golang package manager */
  Golang = "GOLANG",
  /** Packages from the Debian package manager */
  Debian = "DEBIAN",
  /** Packages from the Rubygems package manager */
  Rubygems = "RUBYGEMS",
  /** Packages from the Helm package manager */
  Helm = "HELM",
  /** Packages from the Terraform Module package manager */
  TerraformModule = "TERRAFORM_MODULE",
}

/** Information about pagination in a connection. */
export type PageInfo = {
  __typename?: "PageInfo"
  /** When paginating forwards, the cursor to continue. */
  endCursor?: Maybe<Scalars["String"]>
  /** When paginating forwards, are there more items? */
  hasNextPage: Scalars["Boolean"]
  /** When paginating backwards, are there more items? */
  hasPreviousPage: Scalars["Boolean"]
  /** When paginating backwards, the cursor to continue. */
  startCursor?: Maybe<Scalars["String"]>
}

/** Represents a file or directory in the project repository that has been locked. */
export type PathLock = {
  __typename?: "PathLock"
  /** ID of the path lock. */
  id: Scalars["PathLockID"]
  /** The locked path. */
  path?: Maybe<Scalars["String"]>
  /** The user that has locked this path. */
  user?: Maybe<UserCore>
}

/** The connection type for PathLock. */
export type PathLockConnection = {
  __typename?: "PathLockConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<PathLockEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<PathLock>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type PathLockEdge = {
  __typename?: "PathLockEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<PathLock>
}

export type Pipeline = {
  __typename?: "Pipeline"
  /** Indicates if the pipeline is active. */
  active: Scalars["Boolean"]
  /** Base SHA of the source branch. */
  beforeSha?: Maybe<Scalars["String"]>
  /** Specifies if a pipeline can be canceled. */
  cancelable: Scalars["Boolean"]
  /** Code Quality degradations reported on the pipeline. */
  codeQualityReports?: Maybe<CodeQualityDegradationConnection>
  /** Path to the commit that triggered the pipeline. */
  commitPath?: Maybe<Scalars["String"]>
  /** Timestamp of the pipeline's commit. */
  committedAt?: Maybe<Scalars["Time"]>
  /** Indicates if a pipeline is complete. */
  complete: Scalars["Boolean"]
  /** Configuration source of the pipeline (UNKNOWN_SOURCE, REPOSITORY_SOURCE, AUTO_DEVOPS_SOURCE, WEBIDE_SOURCE, REMOTE_SOURCE, EXTERNAL_PROJECT_SOURCE, BRIDGE_SOURCE, PARAMETER_SOURCE, COMPLIANCE_SOURCE) */
  configSource?: Maybe<PipelineConfigSourceEnum>
  /** Coverage percentage. */
  coverage?: Maybe<Scalars["Float"]>
  /** Timestamp of the pipeline's creation. */
  createdAt: Scalars["Time"]
  /** Detailed status of the pipeline. */
  detailedStatus: DetailedStatus
  /** Pipelines this pipeline will trigger. */
  downstream?: Maybe<PipelineConnection>
  /** Duration of the pipeline in seconds. */
  duration?: Maybe<Scalars["Int"]>
  /** Timestamp of the pipeline's completion. */
  finishedAt?: Maybe<Scalars["Time"]>
  /** ID of the pipeline. */
  id: Scalars["ID"]
  /** Internal ID of the pipeline. */
  iid: Scalars["String"]
  /** A specific job in this pipeline, either by name or ID. */
  job?: Maybe<CiJob>
  /** Jobs belonging to the pipeline. */
  jobs?: Maybe<CiJobConnection>
  /** Relative path to the pipeline's page. */
  path?: Maybe<Scalars["String"]>
  /** Project the pipeline belongs to. */
  project?: Maybe<Project>
  /** How long the pipeline was queued before starting. */
  queuedDuration?: Maybe<Scalars["Duration"]>
  /** Specifies if a pipeline can be retried. */
  retryable: Scalars["Boolean"]
  /** Vulnerability findings reported on the pipeline. */
  securityReportFindings?: Maybe<PipelineSecurityReportFindingConnection>
  /** Vulnerability and scanned resource counts for each security scanner of the pipeline. */
  securityReportSummary?: Maybe<SecurityReportSummary>
  /** SHA of the pipeline's commit. */
  sha: Scalars["String"]
  /** Job where pipeline was triggered from. */
  sourceJob?: Maybe<CiJob>
  /** Stages of the pipeline. */
  stages?: Maybe<CiStageConnection>
  /** Timestamp when the pipeline was started. */
  startedAt?: Maybe<Scalars["Time"]>
  /** Status of the pipeline (CREATED, WAITING_FOR_RESOURCE, PREPARING, PENDING, RUNNING, FAILED, SUCCESS, CANCELED, SKIPPED, MANUAL, SCHEDULED) */
  status: PipelineStatusEnum
  /** Summary of the test report generated by the pipeline. */
  testReportSummary: TestReportSummary
  /** A specific test suite in a pipeline test report. */
  testSuite?: Maybe<TestSuite>
  /** Timestamp of the pipeline's last activity. */
  updatedAt: Scalars["Time"]
  /** Pipeline that triggered the pipeline. */
  upstream?: Maybe<Pipeline>
  /** Pipeline user. */
  user?: Maybe<UserCore>
  /** Permissions for the current user on the resource */
  userPermissions: PipelinePermissions
  /** Indicates if the pipeline has jobs with `needs` dependencies. */
  usesNeeds?: Maybe<Scalars["Boolean"]>
  /** Indicates if a pipeline has warnings. */
  warnings: Scalars["Boolean"]
}

export type PipelineCodeQualityReportsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type PipelineDownstreamArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type PipelineJobArgs = {
  id?: Maybe<Scalars["JobID"]>
  name?: Maybe<Scalars["String"]>
}

export type PipelineJobsArgs = {
  securityReportTypes?: Maybe<Array<SecurityReportTypeEnum>>
  statuses?: Maybe<Array<CiJobStatus>>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type PipelineSecurityReportFindingsArgs = {
  reportType?: Maybe<Array<Scalars["String"]>>
  severity?: Maybe<Array<Scalars["String"]>>
  scanner?: Maybe<Array<Scalars["String"]>>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type PipelineStagesArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type PipelineTestSuiteArgs = {
  buildIds: Array<Scalars["ID"]>
}

export type PipelineAnalytics = {
  __typename?: "PipelineAnalytics"
  /** Labels for the monthly pipeline count. */
  monthPipelinesLabels?: Maybe<Array<Scalars["String"]>>
  /** Total monthly successful pipeline count. */
  monthPipelinesSuccessful?: Maybe<Array<Scalars["Int"]>>
  /** Total monthly pipeline count. */
  monthPipelinesTotals?: Maybe<Array<Scalars["Int"]>>
  /** Pipeline times labels. */
  pipelineTimesLabels?: Maybe<Array<Scalars["String"]>>
  /** Pipeline times. */
  pipelineTimesValues?: Maybe<Array<Scalars["Int"]>>
  /** Labels for the weekly pipeline count. */
  weekPipelinesLabels?: Maybe<Array<Scalars["String"]>>
  /** Total weekly successful pipeline count. */
  weekPipelinesSuccessful?: Maybe<Array<Scalars["Int"]>>
  /** Total weekly pipeline count. */
  weekPipelinesTotals?: Maybe<Array<Scalars["Int"]>>
  /** Labels for the yearly pipeline count. */
  yearPipelinesLabels?: Maybe<Array<Scalars["String"]>>
  /** Total yearly successful pipeline count. */
  yearPipelinesSuccessful?: Maybe<Array<Scalars["Int"]>>
  /** Total yearly pipeline count. */
  yearPipelinesTotals?: Maybe<Array<Scalars["Int"]>>
}

/** Represents the Geo sync and verification state of a pipeline artifact */
export type PipelineArtifactRegistry = {
  __typename?: "PipelineArtifactRegistry"
  /** Timestamp when the PipelineArtifactRegistry was created */
  createdAt?: Maybe<Scalars["Time"]>
  /** ID of the PipelineArtifactRegistry */
  id: Scalars["ID"]
  /** Error message during sync of the PipelineArtifactRegistry */
  lastSyncFailure?: Maybe<Scalars["String"]>
  /** Timestamp of the most recent successful sync of the PipelineArtifactRegistry */
  lastSyncedAt?: Maybe<Scalars["Time"]>
  /** ID of the pipeline artifact. */
  pipelineArtifactId: Scalars["ID"]
  /** Timestamp after which the PipelineArtifactRegistry should be resynced */
  retryAt?: Maybe<Scalars["Time"]>
  /** Number of consecutive failed sync attempts of the PipelineArtifactRegistry */
  retryCount?: Maybe<Scalars["Int"]>
  /** Sync state of the PipelineArtifactRegistry */
  state?: Maybe<RegistryState>
}

/** The connection type for PipelineArtifactRegistry. */
export type PipelineArtifactRegistryConnection = {
  __typename?: "PipelineArtifactRegistryConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<PipelineArtifactRegistryEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<PipelineArtifactRegistry>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type PipelineArtifactRegistryEdge = {
  __typename?: "PipelineArtifactRegistryEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<PipelineArtifactRegistry>
}

/** Autogenerated input type of PipelineCancel */
export type PipelineCancelInput = {
  /** The ID of the pipeline to mutate. */
  id: Scalars["CiPipelineID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of PipelineCancel */
export type PipelineCancelPayload = {
  __typename?: "PipelineCancelPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

export enum PipelineConfigSourceEnum {
  /** Unknown source. */
  UnknownSource = "UNKNOWN_SOURCE",
  /** Repository source. */
  RepositorySource = "REPOSITORY_SOURCE",
  /** Auto DevOps source. */
  AutoDevopsSource = "AUTO_DEVOPS_SOURCE",
  /** Webide source. */
  WebideSource = "WEBIDE_SOURCE",
  /** Remote source. */
  RemoteSource = "REMOTE_SOURCE",
  /** External project source. */
  ExternalProjectSource = "EXTERNAL_PROJECT_SOURCE",
  /** Bridge source. */
  BridgeSource = "BRIDGE_SOURCE",
  /** Parameter source. */
  ParameterSource = "PARAMETER_SOURCE",
  /** Compliance source. */
  ComplianceSource = "COMPLIANCE_SOURCE",
}

/** The connection type for Pipeline. */
export type PipelineConnection = {
  __typename?: "PipelineConnection"
  /** Total count of collection. */
  count: Scalars["Int"]
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<PipelineEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<Pipeline>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** Autogenerated input type of PipelineDestroy */
export type PipelineDestroyInput = {
  /** The ID of the pipeline to mutate. */
  id: Scalars["CiPipelineID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of PipelineDestroy */
export type PipelineDestroyPayload = {
  __typename?: "PipelineDestroyPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

/** An edge in a connection. */
export type PipelineEdge = {
  __typename?: "PipelineEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<Pipeline>
}

export type PipelinePermissions = {
  __typename?: "PipelinePermissions"
  /** Indicates the user can perform `admin_pipeline` on this resource */
  adminPipeline: Scalars["Boolean"]
  /** Indicates the user can perform `destroy_pipeline` on this resource */
  destroyPipeline: Scalars["Boolean"]
  /** Indicates the user can perform `update_pipeline` on this resource */
  updatePipeline: Scalars["Boolean"]
}

/** Autogenerated input type of PipelineRetry */
export type PipelineRetryInput = {
  /** The ID of the pipeline to mutate. */
  id: Scalars["CiPipelineID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of PipelineRetry */
export type PipelineRetryPayload = {
  __typename?: "PipelineRetryPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The pipeline after mutation. */
  pipeline?: Maybe<Pipeline>
}

/** Represents vulnerability finding of a security report on the pipeline. */
export type PipelineSecurityReportFinding = {
  __typename?: "PipelineSecurityReportFinding"
  /** Type of the security report that found the vulnerability. */
  confidence?: Maybe<Scalars["String"]>
  /** Description of the vulnerability finding. */
  description?: Maybe<Scalars["String"]>
  /** Identifiers of the vulnerabilit finding. */
  identifiers: Array<VulnerabilityIdentifier>
  /** Location metadata for the vulnerability. Its fields depend on the type of security scan that found the vulnerability. */
  location?: Maybe<VulnerabilityLocation>
  /** Name of the vulnerability finding. */
  name?: Maybe<Scalars["String"]>
  /** The project on which the vulnerability finding was found. */
  project?: Maybe<Project>
  /** Name of the vulnerability finding. */
  projectFingerprint?: Maybe<Scalars["String"]>
  /** Type of the security report that found the vulnerability finding. */
  reportType?: Maybe<VulnerabilityReportType>
  /** Scanner metadata for the vulnerability. */
  scanner?: Maybe<VulnerabilityScanner>
  /** Severity of the vulnerability finding. */
  severity?: Maybe<VulnerabilitySeverity>
  /** URL to the vulnerability's details page. */
  solution?: Maybe<Scalars["String"]>
  /** The finding status. */
  state?: Maybe<VulnerabilityState>
  /** Name of the vulnerability finding. */
  uuid?: Maybe<Scalars["String"]>
}

/** The connection type for PipelineSecurityReportFinding. */
export type PipelineSecurityReportFindingConnection = {
  __typename?: "PipelineSecurityReportFindingConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<PipelineSecurityReportFindingEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<PipelineSecurityReportFinding>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type PipelineSecurityReportFindingEdge = {
  __typename?: "PipelineSecurityReportFindingEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<PipelineSecurityReportFinding>
}

export enum PipelineStatusEnum {
  /** Pipeline has been created. */
  Created = "CREATED",
  /** A resource (for example, a runner) that the pipeline requires to run is unavailable. */
  WaitingForResource = "WAITING_FOR_RESOURCE",
  /** Pipeline is preparing to run. */
  Preparing = "PREPARING",
  /** Pipeline has not started running yet. */
  Pending = "PENDING",
  /** Pipeline is running. */
  Running = "RUNNING",
  /** At least one stage of the pipeline failed. */
  Failed = "FAILED",
  /** Pipeline completed successfully. */
  Success = "SUCCESS",
  /** Pipeline was canceled before completion. */
  Canceled = "CANCELED",
  /** Pipeline was skipped. */
  Skipped = "SKIPPED",
  /** Pipeline needs to be manually started. */
  Manual = "MANUAL",
  /** Pipeline is scheduled to run. */
  Scheduled = "SCHEDULED",
}

export type Project = {
  __typename?: "Project"
  /** Size limit for the repository in bytes. */
  actualRepositorySizeLimit?: Maybe<Scalars["Float"]>
  /** A single Alert Management alert of the project. */
  alertManagementAlert?: Maybe<AlertManagementAlert>
  /** Counts of alerts by status for the project. */
  alertManagementAlertStatusCounts?: Maybe<AlertManagementAlertStatusCountsType>
  /** Alert Management alerts of the project. */
  alertManagementAlerts?: Maybe<AlertManagementAlertConnection>
  /** HTTP Integrations which can receive alerts for the project. */
  alertManagementHttpIntegrations?: Maybe<AlertManagementHttpIntegrationConnection>
  /** Integrations which can receive alerts for the project. */
  alertManagementIntegrations?: Maybe<AlertManagementIntegrationConnection>
  /** Extract alert fields from payload for custom mapping. */
  alertManagementPayloadFields?: Maybe<Array<AlertManagementPayloadAlertField>>
  /** If `only_allow_merge_if_pipeline_succeeds` is true, indicates if merge requests of the project can also be merged with skipped jobs. */
  allowMergeOnSkippedPipeline?: Maybe<Scalars["Boolean"]>
  /** API fuzzing configuration for the project.  */
  apiFuzzingCiConfiguration?: Maybe<ApiFuzzingCiConfiguration>
  /** Indicates the archived status of the project. */
  archived?: Maybe<Scalars["Boolean"]>
  /** Indicates if issues referenced by merge requests and commits within the default branch are closed automatically. */
  autocloseReferencedIssues?: Maybe<Scalars["Boolean"]>
  /** URL to avatar image file of the project. */
  avatarUrl?: Maybe<Scalars["String"]>
  /** A single board of the project. */
  board?: Maybe<Board>
  /** Boards of the project. */
  boards?: Maybe<BoardConnection>
  /** CI/CD settings for the project. */
  ciCdSettings?: Maybe<ProjectCiCdSetting>
  /** Find a single CI/CD template by name. */
  ciTemplate?: Maybe<CiTemplate>
  /** Find a single cluster agent by name. */
  clusterAgent?: Maybe<ClusterAgent>
  /** Cluster agents associated with the project. */
  clusterAgents?: Maybe<ClusterAgentConnection>
  /** Code coverage summary associated with the project. */
  codeCoverageSummary?: Maybe<CodeCoverageSummary>
  /** Compliance frameworks associated with the project. */
  complianceFrameworks?: Maybe<ComplianceFrameworkConnection>
  /** The container expiration policy of the project. */
  containerExpirationPolicy?: Maybe<ContainerExpirationPolicy>
  /** Indicates if the project stores Docker container images in a container registry. */
  containerRegistryEnabled?: Maybe<Scalars["Boolean"]>
  /** Container repositories of the project. */
  containerRepositories?: Maybe<ContainerRepositoryConnection>
  /** Number of container repositories in the project. */
  containerRepositoriesCount: Scalars["Int"]
  /** Timestamp of the project creation. */
  createdAt?: Maybe<Scalars["Time"]>
  /** DAST Profiles associated with the project. */
  dastProfiles?: Maybe<DastProfileConnection>
  /** The DAST scanner profiles associated with the project. */
  dastScannerProfiles?: Maybe<DastScannerProfileConnection>
  /** DAST Site Profile associated with the project. */
  dastSiteProfile?: Maybe<DastSiteProfile>
  /** DAST Site Profiles associated with the project. */
  dastSiteProfiles?: Maybe<DastSiteProfileConnection>
  /** DAST Site Validations associated with the project. */
  dastSiteValidations?: Maybe<DastSiteValidationConnection>
  /** Short description of the project. */
  description?: Maybe<Scalars["String"]>
  /** The GitLab Flavored Markdown rendering of `description` */
  descriptionHtml?: Maybe<Scalars["String"]>
  /** A single environment of the project. */
  environment?: Maybe<Environment>
  /** Environments of the project. */
  environments?: Maybe<EnvironmentConnection>
  /** Number of times the project has been forked. */
  forksCount: Scalars["Int"]
  /** Full path of the project. */
  fullPath: Scalars["ID"]
  /** Grafana integration details for the project. */
  grafanaIntegration?: Maybe<GrafanaIntegration>
  /** Group of the project. */
  group?: Maybe<Group>
  /** URL to connect to the project via HTTPS. */
  httpUrlToRepo?: Maybe<Scalars["String"]>
  /** ID of the project. */
  id: Scalars["ID"]
  /** Status of import background job of the project. */
  importStatus?: Maybe<Scalars["String"]>
  /** Incident Management escalation policies of the project. */
  incidentManagementEscalationPolicies?: Maybe<EscalationPolicyTypeConnection>
  /** Incident Management escalation policy of the project. */
  incidentManagementEscalationPolicy?: Maybe<EscalationPolicyType>
  /** Incident Management On-call schedules of the project. */
  incidentManagementOncallSchedules?: Maybe<IncidentManagementOncallScheduleConnection>
  /** A single issue of the project. */
  issue?: Maybe<Issue>
  /** Counts of issues by status for the project. */
  issueStatusCounts?: Maybe<IssueStatusCountsType>
  /** Issues of the project. */
  issues?: Maybe<IssueConnection>
  /** Indicates if Issues are enabled for the current user */
  issuesEnabled?: Maybe<Scalars["Boolean"]>
  /** Find iteration cadences. */
  iterationCadences?: Maybe<IterationCadenceConnection>
  /** Find iterations. */
  iterations?: Maybe<IterationConnection>
  /** Status of Jira import background job of the project. */
  jiraImportStatus?: Maybe<Scalars["String"]>
  /** Jira imports into the project. */
  jiraImports?: Maybe<JiraImportConnection>
  /** Jobs of a project. This field can only be resolved for one project in any single request. */
  jobs?: Maybe<CiJobConnection>
  /** Indicates if CI/CD pipeline jobs are enabled for the current user. */
  jobsEnabled?: Maybe<Scalars["Boolean"]>
  /** A label available on this project. */
  label?: Maybe<Label>
  /** Labels available on this project. */
  labels?: Maybe<LabelConnection>
  /** Timestamp of the project last activity. */
  lastActivityAt?: Maybe<Scalars["Time"]>
  /** Indicates if the project has Large File Storage (LFS) enabled. */
  lfsEnabled?: Maybe<Scalars["Boolean"]>
  /** A single merge request of the project. */
  mergeRequest?: Maybe<MergeRequest>
  /** Merge requests of the project. */
  mergeRequests?: Maybe<MergeRequestConnection>
  /** Indicates if Merge Requests are enabled for the current user */
  mergeRequestsEnabled?: Maybe<Scalars["Boolean"]>
  /** Indicates if no merge commits should be created and all merges should instead be fast-forwarded, which means that merging is only allowed if the branch could be fast-forwarded. */
  mergeRequestsFfOnlyEnabled?: Maybe<Scalars["Boolean"]>
  /** Milestones of the project. */
  milestones?: Maybe<MilestoneConnection>
  /** Name of the project (without namespace). */
  name: Scalars["String"]
  /** Full name of the project with its namespace. */
  nameWithNamespace: Scalars["String"]
  /** Namespace of the project. */
  namespace?: Maybe<Namespace>
  /** Indicates if merge requests of the project can only be merged when all the discussions are resolved. */
  onlyAllowMergeIfAllDiscussionsAreResolved?: Maybe<Scalars["Boolean"]>
  /** Indicates if merge requests of the project can only be merged with successful jobs. */
  onlyAllowMergeIfPipelineSucceeds?: Maybe<Scalars["Boolean"]>
  /** Number of open issues for the project. */
  openIssuesCount?: Maybe<Scalars["Int"]>
  /** Packages of the project. */
  packages?: Maybe<PackageConnection>
  /** Path of the project. */
  path: Scalars["String"]
  /** The project's path locks. */
  pathLocks?: Maybe<PathLockConnection>
  /** Build pipeline of the project. */
  pipeline?: Maybe<Pipeline>
  /** Pipeline analytics. */
  pipelineAnalytics?: Maybe<PipelineAnalytics>
  /** Build pipelines of the project. */
  pipelines?: Maybe<PipelineConnection>
  /** Indicates if a link to create or view a merge request should display after a push to Git repositories of the project from the command line. */
  printingMergeRequestLinkEnabled?: Maybe<Scalars["Boolean"]>
  /** Members of the project. */
  projectMembers?: Maybe<MemberInterfaceConnection>
  /** Indicates if there is public access to pipelines and job details of the project, including output logs and artifacts. */
  publicJobs?: Maybe<Scalars["Boolean"]>
  /** The project's push rules settings. */
  pushRules?: Maybe<PushRules>
  /** A single release of the project. */
  release?: Maybe<Release>
  /** Releases of the project. */
  releases?: Maybe<ReleaseConnection>
  /** Indicates if `Delete source branch` option should be enabled by default for all new merge requests of the project. */
  removeSourceBranchAfterMerge?: Maybe<Scalars["Boolean"]>
  /** Git repository of the project. */
  repository?: Maybe<Repository>
  /** Size of repository that exceeds the limit in bytes. */
  repositorySizeExcess?: Maybe<Scalars["Float"]>
  /** Indicates if users can request member access to the project. */
  requestAccessEnabled?: Maybe<Scalars["Boolean"]>
  /** Find a single requirement. */
  requirement?: Maybe<Requirement>
  /** Number of requirements for the project by their state. */
  requirementStatesCount?: Maybe<RequirementStatesCount>
  /** Find requirements. */
  requirements?: Maybe<RequirementConnection>
  /** SAST CI configuration for the project. */
  sastCiConfiguration?: Maybe<SastCiConfiguration>
  /** Scan Execution Policies of the project */
  scanExecutionPolicies?: Maybe<ScanExecutionPolicyConnection>
  /** Path to project's security dashboard. */
  securityDashboardPath?: Maybe<Scalars["String"]>
  /** Information about security analyzers used in the project. */
  securityScanners?: Maybe<SecurityScanners>
  /** Detailed version of a Sentry error on the project. */
  sentryDetailedError?: Maybe<SentryDetailedError>
  /** Paginated collection of Sentry errors on the project. */
  sentryErrors?: Maybe<SentryErrorCollection>
  /** E-mail address of the service desk. */
  serviceDeskAddress?: Maybe<Scalars["String"]>
  /** Indicates if the project has service desk enabled. */
  serviceDeskEnabled?: Maybe<Scalars["Boolean"]>
  /** Project services. */
  services?: Maybe<ServiceConnection>
  /** Indicates if shared runners are enabled for the project. */
  sharedRunnersEnabled?: Maybe<Scalars["Boolean"]>
  /** Snippets of the project. */
  snippets?: Maybe<SnippetConnection>
  /** Indicates if Snippets are enabled for the current user */
  snippetsEnabled?: Maybe<Scalars["Boolean"]>
  /** Indicates if `squashReadOnly` is enabled. */
  squashReadOnly: Scalars["Boolean"]
  /** URL to connect to the project via SSH. */
  sshUrlToRepo?: Maybe<Scalars["String"]>
  /** Number of times the project has been starred. */
  starCount: Scalars["Int"]
  /** Statistics of the project. */
  statistics?: Maybe<ProjectStatistics>
  /** The commit message used to apply merge request suggestions. */
  suggestionCommitMessage?: Maybe<Scalars["String"]>
  /**
   * List of project topics (not Git tags). Deprecated in 13.12: Use `topics`.
   * @deprecated Use `topics`. Deprecated in 13.12.
   */
  tagList?: Maybe<Scalars["String"]>
  /** Find a single Terraform state by name. */
  terraformState?: Maybe<TerraformState>
  /** Terraform states associated with the project. */
  terraformStates?: Maybe<TerraformStateConnection>
  /** List of project topics. */
  topics?: Maybe<Array<Scalars["String"]>>
  /** Permissions for the current user on the resource */
  userPermissions: ProjectPermissions
  /** Visibility of the project. */
  visibility?: Maybe<Scalars["String"]>
  /** Vulnerabilities reported on the project. */
  vulnerabilities?: Maybe<VulnerabilityConnection>
  /** Number of vulnerabilities per day for the project. */
  vulnerabilitiesCountByDay?: Maybe<VulnerabilitiesCountByDayConnection>
  /** Vulnerability scanners reported on the project vulnerabilities. */
  vulnerabilityScanners?: Maybe<VulnerabilityScannerConnection>
  /** Counts for each vulnerability severity in the project. */
  vulnerabilitySeveritiesCount?: Maybe<VulnerabilitySeveritiesCount>
  /** Web URL of the project. */
  webUrl?: Maybe<Scalars["String"]>
  /** Indicates if Wikis are enabled for the current user */
  wikiEnabled?: Maybe<Scalars["Boolean"]>
}

export type ProjectAlertManagementAlertArgs = {
  iid?: Maybe<Scalars["String"]>
  statuses?: Maybe<Array<AlertManagementStatus>>
  sort?: Maybe<AlertManagementAlertSort>
  domain?: AlertManagementDomainFilter
  search?: Maybe<Scalars["String"]>
  assigneeUsername?: Maybe<Scalars["String"]>
}

export type ProjectAlertManagementAlertStatusCountsArgs = {
  search?: Maybe<Scalars["String"]>
  assigneeUsername?: Maybe<Scalars["String"]>
}

export type ProjectAlertManagementAlertsArgs = {
  iid?: Maybe<Scalars["String"]>
  statuses?: Maybe<Array<AlertManagementStatus>>
  sort?: Maybe<AlertManagementAlertSort>
  domain?: AlertManagementDomainFilter
  search?: Maybe<Scalars["String"]>
  assigneeUsername?: Maybe<Scalars["String"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type ProjectAlertManagementHttpIntegrationsArgs = {
  id?: Maybe<Scalars["AlertManagementHttpIntegrationID"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type ProjectAlertManagementIntegrationsArgs = {
  id?: Maybe<Scalars["GlobalID"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type ProjectAlertManagementPayloadFieldsArgs = {
  payloadExample: Scalars["String"]
}

export type ProjectBoardArgs = {
  id: Scalars["BoardID"]
}

export type ProjectBoardsArgs = {
  id?: Maybe<Scalars["BoardID"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type ProjectCiTemplateArgs = {
  name: Scalars["String"]
}

export type ProjectClusterAgentArgs = {
  name: Scalars["String"]
}

export type ProjectClusterAgentsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type ProjectComplianceFrameworksArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type ProjectContainerRepositoriesArgs = {
  name?: Maybe<Scalars["String"]>
  sort?: Maybe<ContainerRepositorySort>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type ProjectDastProfilesArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type ProjectDastScannerProfilesArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type ProjectDastSiteProfileArgs = {
  id: Scalars["DastSiteProfileID"]
}

export type ProjectDastSiteProfilesArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type ProjectDastSiteValidationsArgs = {
  normalizedTargetUrls?: Maybe<Array<Scalars["String"]>>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type ProjectEnvironmentArgs = {
  name?: Maybe<Scalars["String"]>
  search?: Maybe<Scalars["String"]>
  states?: Maybe<Array<Scalars["String"]>>
}

export type ProjectEnvironmentsArgs = {
  name?: Maybe<Scalars["String"]>
  search?: Maybe<Scalars["String"]>
  states?: Maybe<Array<Scalars["String"]>>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type ProjectIncidentManagementEscalationPoliciesArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type ProjectIncidentManagementEscalationPolicyArgs = {
  id: Scalars["IncidentManagementEscalationPolicyID"]
}

export type ProjectIncidentManagementOncallSchedulesArgs = {
  iids?: Maybe<Array<Scalars["ID"]>>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type ProjectIssueArgs = {
  iid?: Maybe<Scalars["String"]>
  iids?: Maybe<Array<Scalars["String"]>>
  labelName?: Maybe<Array<Maybe<Scalars["String"]>>>
  milestoneTitle?: Maybe<Array<Maybe<Scalars["String"]>>>
  authorUsername?: Maybe<Scalars["String"]>
  assigneeUsernames?: Maybe<Array<Scalars["String"]>>
  assigneeId?: Maybe<Scalars["String"]>
  createdBefore?: Maybe<Scalars["Time"]>
  createdAfter?: Maybe<Scalars["Time"]>
  updatedBefore?: Maybe<Scalars["Time"]>
  updatedAfter?: Maybe<Scalars["Time"]>
  closedBefore?: Maybe<Scalars["Time"]>
  closedAfter?: Maybe<Scalars["Time"]>
  search?: Maybe<Scalars["String"]>
  types?: Maybe<Array<IssueType>>
  not?: Maybe<NegatedIssueFilterInput>
  state?: Maybe<IssuableState>
  sort?: Maybe<IssueSort>
  iterationId?: Maybe<Array<Maybe<Scalars["ID"]>>>
  iterationWildcardId?: Maybe<IterationWildcardId>
  epicId?: Maybe<Scalars["String"]>
  weight?: Maybe<Scalars["String"]>
}

export type ProjectIssueStatusCountsArgs = {
  iid?: Maybe<Scalars["String"]>
  iids?: Maybe<Array<Scalars["String"]>>
  labelName?: Maybe<Array<Maybe<Scalars["String"]>>>
  milestoneTitle?: Maybe<Array<Maybe<Scalars["String"]>>>
  authorUsername?: Maybe<Scalars["String"]>
  assigneeUsernames?: Maybe<Array<Scalars["String"]>>
  assigneeId?: Maybe<Scalars["String"]>
  createdBefore?: Maybe<Scalars["Time"]>
  createdAfter?: Maybe<Scalars["Time"]>
  updatedBefore?: Maybe<Scalars["Time"]>
  updatedAfter?: Maybe<Scalars["Time"]>
  closedBefore?: Maybe<Scalars["Time"]>
  closedAfter?: Maybe<Scalars["Time"]>
  search?: Maybe<Scalars["String"]>
  types?: Maybe<Array<IssueType>>
  not?: Maybe<NegatedIssueFilterInput>
}

export type ProjectIssuesArgs = {
  iid?: Maybe<Scalars["String"]>
  iids?: Maybe<Array<Scalars["String"]>>
  labelName?: Maybe<Array<Maybe<Scalars["String"]>>>
  milestoneTitle?: Maybe<Array<Maybe<Scalars["String"]>>>
  authorUsername?: Maybe<Scalars["String"]>
  assigneeUsernames?: Maybe<Array<Scalars["String"]>>
  assigneeId?: Maybe<Scalars["String"]>
  createdBefore?: Maybe<Scalars["Time"]>
  createdAfter?: Maybe<Scalars["Time"]>
  updatedBefore?: Maybe<Scalars["Time"]>
  updatedAfter?: Maybe<Scalars["Time"]>
  closedBefore?: Maybe<Scalars["Time"]>
  closedAfter?: Maybe<Scalars["Time"]>
  search?: Maybe<Scalars["String"]>
  types?: Maybe<Array<IssueType>>
  not?: Maybe<NegatedIssueFilterInput>
  state?: Maybe<IssuableState>
  sort?: Maybe<IssueSort>
  iterationId?: Maybe<Array<Maybe<Scalars["ID"]>>>
  iterationWildcardId?: Maybe<IterationWildcardId>
  epicId?: Maybe<Scalars["String"]>
  weight?: Maybe<Scalars["String"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type ProjectIterationCadencesArgs = {
  id?: Maybe<Scalars["IterationsCadenceID"]>
  title?: Maybe<Scalars["String"]>
  durationInWeeks?: Maybe<Scalars["Int"]>
  automatic?: Maybe<Scalars["Boolean"]>
  active?: Maybe<Scalars["Boolean"]>
  includeAncestorGroups?: Maybe<Scalars["Boolean"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type ProjectIterationsArgs = {
  timeframe?: Maybe<Timeframe>
  state?: Maybe<IterationState>
  title?: Maybe<Scalars["String"]>
  id?: Maybe<Scalars["ID"]>
  iid?: Maybe<Scalars["ID"]>
  includeAncestors?: Maybe<Scalars["Boolean"]>
  iterationCadenceIds?: Maybe<Array<Scalars["IterationsCadenceID"]>>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type ProjectJiraImportsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type ProjectJobsArgs = {
  statuses?: Maybe<Array<CiJobStatus>>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type ProjectLabelArgs = {
  title: Scalars["String"]
}

export type ProjectLabelsArgs = {
  searchTerm?: Maybe<Scalars["String"]>
  includeAncestorGroups?: Maybe<Scalars["Boolean"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type ProjectMergeRequestArgs = {
  iid: Scalars["String"]
}

export type ProjectMergeRequestsArgs = {
  iids?: Maybe<Array<Scalars["String"]>>
  sourceBranches?: Maybe<Array<Scalars["String"]>>
  targetBranches?: Maybe<Array<Scalars["String"]>>
  state?: Maybe<MergeRequestState>
  labels?: Maybe<Array<Scalars["String"]>>
  mergedAfter?: Maybe<Scalars["Time"]>
  mergedBefore?: Maybe<Scalars["Time"]>
  milestoneTitle?: Maybe<Scalars["String"]>
  sort?: Maybe<MergeRequestSort>
  not?: Maybe<MergeRequestsResolverNegatedParams>
  assigneeUsername?: Maybe<Scalars["String"]>
  authorUsername?: Maybe<Scalars["String"]>
  reviewerUsername?: Maybe<Scalars["String"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type ProjectMilestonesArgs = {
  timeframe?: Maybe<Timeframe>
  ids?: Maybe<Array<Scalars["ID"]>>
  state?: Maybe<MilestoneStateEnum>
  title?: Maybe<Scalars["String"]>
  searchTitle?: Maybe<Scalars["String"]>
  containingDate?: Maybe<Scalars["Time"]>
  includeAncestors?: Maybe<Scalars["Boolean"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type ProjectPackagesArgs = {
  sort?: Maybe<PackageSort>
  packageName?: Maybe<Scalars["String"]>
  packageType?: Maybe<PackageTypeEnum>
  status?: Maybe<PackageStatus>
  includeVersionless?: Maybe<Scalars["Boolean"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type ProjectPathLocksArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type ProjectPipelineArgs = {
  iid?: Maybe<Scalars["ID"]>
  sha?: Maybe<Scalars["String"]>
}

export type ProjectPipelinesArgs = {
  status?: Maybe<PipelineStatusEnum>
  ref?: Maybe<Scalars["String"]>
  sha?: Maybe<Scalars["String"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type ProjectProjectMembersArgs = {
  search?: Maybe<Scalars["String"]>
  relations?: Maybe<Array<ProjectMemberRelation>>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type ProjectReleaseArgs = {
  tagName: Scalars["String"]
}

export type ProjectReleasesArgs = {
  sort?: Maybe<ReleaseSort>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type ProjectRequirementArgs = {
  sort?: Maybe<Sort>
  state?: Maybe<RequirementState>
  search?: Maybe<Scalars["String"]>
  authorUsername?: Maybe<Array<Scalars["String"]>>
  iid?: Maybe<Scalars["ID"]>
  iids?: Maybe<Array<Scalars["ID"]>>
  lastTestReportState?: Maybe<RequirementStatusFilter>
}

export type ProjectRequirementsArgs = {
  sort?: Maybe<Sort>
  state?: Maybe<RequirementState>
  search?: Maybe<Scalars["String"]>
  authorUsername?: Maybe<Array<Scalars["String"]>>
  iid?: Maybe<Scalars["ID"]>
  iids?: Maybe<Array<Scalars["ID"]>>
  lastTestReportState?: Maybe<RequirementStatusFilter>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type ProjectScanExecutionPoliciesArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type ProjectSentryDetailedErrorArgs = {
  id: Scalars["GitlabErrorTrackingDetailedErrorID"]
}

export type ProjectServicesArgs = {
  active?: Maybe<Scalars["Boolean"]>
  type?: Maybe<ServiceType>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type ProjectSnippetsArgs = {
  ids?: Maybe<Array<Scalars["SnippetID"]>>
  visibility?: Maybe<VisibilityScopesEnum>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type ProjectTerraformStateArgs = {
  name: Scalars["String"]
}

export type ProjectTerraformStatesArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type ProjectVulnerabilitiesArgs = {
  projectId?: Maybe<Array<Scalars["ID"]>>
  reportType?: Maybe<Array<VulnerabilityReportType>>
  severity?: Maybe<Array<VulnerabilitySeverity>>
  state?: Maybe<Array<VulnerabilityState>>
  scanner?: Maybe<Array<Scalars["String"]>>
  scannerId?: Maybe<Array<Scalars["VulnerabilitiesScannerID"]>>
  sort?: Maybe<VulnerabilitySort>
  hasResolution?: Maybe<Scalars["Boolean"]>
  hasIssues?: Maybe<Scalars["Boolean"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type ProjectVulnerabilitiesCountByDayArgs = {
  startDate: Scalars["ISO8601Date"]
  endDate: Scalars["ISO8601Date"]
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type ProjectVulnerabilityScannersArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type ProjectVulnerabilitySeveritiesCountArgs = {
  projectId?: Maybe<Array<Scalars["ID"]>>
  reportType?: Maybe<Array<VulnerabilityReportType>>
  severity?: Maybe<Array<VulnerabilitySeverity>>
  state?: Maybe<Array<VulnerabilityState>>
  scanner?: Maybe<Array<Scalars["String"]>>
}

export type ProjectCiCdSetting = {
  __typename?: "ProjectCiCdSetting"
  /** Whether to keep the latest builds artifacts. */
  keepLatestArtifact?: Maybe<Scalars["Boolean"]>
  /** Whether merge pipelines are enabled. */
  mergePipelinesEnabled?: Maybe<Scalars["Boolean"]>
  /** Whether merge trains are enabled. */
  mergeTrainsEnabled?: Maybe<Scalars["Boolean"]>
  /** Project the CI/CD settings belong to. */
  project?: Maybe<Project>
}

/** The connection type for Project. */
export type ProjectConnection = {
  __typename?: "ProjectConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<ProjectEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<Project>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type ProjectEdge = {
  __typename?: "ProjectEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<Project>
}

/** Represents a Project Membership */
export type ProjectMember = MemberInterface & {
  __typename?: "ProjectMember"
  /** GitLab::Access level. */
  accessLevel?: Maybe<AccessLevel>
  /** Date and time the membership was created. */
  createdAt?: Maybe<Scalars["Time"]>
  /** User that authorized membership. */
  createdBy?: Maybe<UserCore>
  /** Date and time the membership expires. */
  expiresAt?: Maybe<Scalars["Time"]>
  /** ID of the member. */
  id: Scalars["ID"]
  /** Project that User is a member of. */
  project?: Maybe<Project>
  /** Date and time the membership was last updated. */
  updatedAt?: Maybe<Scalars["Time"]>
  /** User that is associated with the member object. */
  user: UserCore
  /** Permissions for the current user on the resource */
  userPermissions: ProjectPermissions
}

/** The connection type for ProjectMember. */
export type ProjectMemberConnection = {
  __typename?: "ProjectMemberConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<ProjectMemberEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<ProjectMember>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type ProjectMemberEdge = {
  __typename?: "ProjectMemberEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<ProjectMember>
}

/** Project member relation */
export enum ProjectMemberRelation {
  /** Direct members */
  Direct = "DIRECT",
  /** Inherited members */
  Inherited = "INHERITED",
  /** Descendants members */
  Descendants = "DESCENDANTS",
  /** Invited Groups members */
  InvitedGroups = "INVITED_GROUPS",
}

export type ProjectPermissions = {
  __typename?: "ProjectPermissions"
  /** Indicates the user can perform `admin_operations` on this resource */
  adminOperations: Scalars["Boolean"]
  /** Indicates the user can perform `admin_path_locks` on this resource */
  adminPathLocks: Scalars["Boolean"]
  /** Indicates the user can perform `admin_project` on this resource */
  adminProject: Scalars["Boolean"]
  /** Indicates the user can perform `admin_remote_mirror` on this resource */
  adminRemoteMirror: Scalars["Boolean"]
  /** Indicates the user can perform `admin_wiki` on this resource */
  adminWiki: Scalars["Boolean"]
  /** Indicates the user can perform `archive_project` on this resource */
  archiveProject: Scalars["Boolean"]
  /** Indicates the user can perform `change_namespace` on this resource */
  changeNamespace: Scalars["Boolean"]
  /** Indicates the user can perform `change_visibility_level` on this resource */
  changeVisibilityLevel: Scalars["Boolean"]
  /** Indicates the user can perform `create_deployment` on this resource */
  createDeployment: Scalars["Boolean"]
  /** Indicates the user can perform `create_design` on this resource */
  createDesign: Scalars["Boolean"]
  /** Indicates the user can perform `create_issue` on this resource */
  createIssue: Scalars["Boolean"]
  /** Indicates the user can perform `create_label` on this resource */
  createLabel: Scalars["Boolean"]
  /** Indicates the user can perform `create_merge_request_from` on this resource */
  createMergeRequestFrom: Scalars["Boolean"]
  /** Indicates the user can perform `create_merge_request_in` on this resource */
  createMergeRequestIn: Scalars["Boolean"]
  /** Indicates the user can perform `create_pages` on this resource */
  createPages: Scalars["Boolean"]
  /** Indicates the user can perform `create_pipeline` on this resource */
  createPipeline: Scalars["Boolean"]
  /** Indicates the user can perform `create_pipeline_schedule` on this resource */
  createPipelineSchedule: Scalars["Boolean"]
  /** Indicates the user can perform `create_snippet` on this resource */
  createSnippet: Scalars["Boolean"]
  /** Indicates the user can perform `create_wiki` on this resource */
  createWiki: Scalars["Boolean"]
  /** Indicates the user can perform `destroy_design` on this resource */
  destroyDesign: Scalars["Boolean"]
  /** Indicates the user can perform `destroy_pages` on this resource */
  destroyPages: Scalars["Boolean"]
  /** Indicates the user can perform `destroy_wiki` on this resource */
  destroyWiki: Scalars["Boolean"]
  /** Indicates the user can perform `download_code` on this resource */
  downloadCode: Scalars["Boolean"]
  /** Indicates the user can perform `download_wiki_code` on this resource */
  downloadWikiCode: Scalars["Boolean"]
  /** Indicates the user can perform `fork_project` on this resource */
  forkProject: Scalars["Boolean"]
  /** Indicates the user can perform `push_code` on this resource */
  pushCode: Scalars["Boolean"]
  /** Indicates the user can perform `push_to_delete_protected_branch` on this resource */
  pushToDeleteProtectedBranch: Scalars["Boolean"]
  /** Indicates the user can perform `read_commit_status` on this resource */
  readCommitStatus: Scalars["Boolean"]
  /** Indicates the user can perform `read_cycle_analytics` on this resource */
  readCycleAnalytics: Scalars["Boolean"]
  /** Indicates the user can perform `read_design` on this resource */
  readDesign: Scalars["Boolean"]
  /** Indicates the user can perform `read_merge_request` on this resource */
  readMergeRequest: Scalars["Boolean"]
  /** Indicates the user can perform `read_pages_content` on this resource */
  readPagesContent: Scalars["Boolean"]
  /** Indicates the user can perform `read_project` on this resource */
  readProject: Scalars["Boolean"]
  /** Indicates the user can perform `read_project_member` on this resource */
  readProjectMember: Scalars["Boolean"]
  /** Indicates the user can perform `read_wiki` on this resource */
  readWiki: Scalars["Boolean"]
  /** Indicates the user can perform `remove_fork_project` on this resource */
  removeForkProject: Scalars["Boolean"]
  /** Indicates the user can perform `remove_pages` on this resource */
  removePages: Scalars["Boolean"]
  /** Indicates the user can perform `remove_project` on this resource */
  removeProject: Scalars["Boolean"]
  /** Indicates the user can perform `rename_project` on this resource */
  renameProject: Scalars["Boolean"]
  /** Indicates the user can perform `request_access` on this resource */
  requestAccess: Scalars["Boolean"]
  /** Indicates the user can perform `update_pages` on this resource */
  updatePages: Scalars["Boolean"]
  /** Indicates the user can perform `update_wiki` on this resource */
  updateWiki: Scalars["Boolean"]
  /** Indicates the user can perform `upload_file` on this resource */
  uploadFile: Scalars["Boolean"]
}

export type ProjectStatistics = {
  __typename?: "ProjectStatistics"
  /** Build artifacts size of the project in bytes. */
  buildArtifactsSize: Scalars["Float"]
  /** Commit count of the project. */
  commitCount: Scalars["Float"]
  /** Large File Storage (LFS) object size of the project in bytes. */
  lfsObjectsSize: Scalars["Float"]
  /** Packages size of the project in bytes. */
  packagesSize: Scalars["Float"]
  /** Repository size of the project in bytes. */
  repositorySize: Scalars["Float"]
  /** Snippets size of the project in bytes. */
  snippetsSize?: Maybe<Scalars["Float"]>
  /** Storage size of the project in bytes. */
  storageSize: Scalars["Float"]
  /** Uploads size of the project in bytes. */
  uploadsSize?: Maybe<Scalars["Float"]>
  /** Wiki size of the project in bytes. */
  wikiSize?: Maybe<Scalars["Float"]>
}

/** The alert condition for Prometheus */
export type PrometheusAlert = {
  __typename?: "PrometheusAlert"
  /** The human-readable text of the alert condition. */
  humanizedText: Scalars["String"]
  /** ID of the alert condition. */
  id: Scalars["ID"]
}

/** Autogenerated input type of PrometheusIntegrationCreate */
export type PrometheusIntegrationCreateInput = {
  /** The project to create the integration in. */
  projectPath: Scalars["ID"]
  /** Whether the integration is receiving alerts. */
  active: Scalars["Boolean"]
  /** Endpoint at which Prometheus can be queried. */
  apiUrl: Scalars["String"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of PrometheusIntegrationCreate */
export type PrometheusIntegrationCreatePayload = {
  __typename?: "PrometheusIntegrationCreatePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The newly created integration. */
  integration?: Maybe<AlertManagementPrometheusIntegration>
}

/** Autogenerated input type of PrometheusIntegrationResetToken */
export type PrometheusIntegrationResetTokenInput = {
  /** The ID of the integration to mutate. */
  id: Scalars["PrometheusServiceID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of PrometheusIntegrationResetToken */
export type PrometheusIntegrationResetTokenPayload = {
  __typename?: "PrometheusIntegrationResetTokenPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The newly created integration. */
  integration?: Maybe<AlertManagementPrometheusIntegration>
}

/** Autogenerated input type of PrometheusIntegrationUpdate */
export type PrometheusIntegrationUpdateInput = {
  /** The ID of the integration to mutate. */
  id: Scalars["PrometheusServiceID"]
  /** Whether the integration is receiving alerts. */
  active?: Maybe<Scalars["Boolean"]>
  /** Endpoint at which Prometheus can be queried. */
  apiUrl?: Maybe<Scalars["String"]>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of PrometheusIntegrationUpdate */
export type PrometheusIntegrationUpdatePayload = {
  __typename?: "PrometheusIntegrationUpdatePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The newly created integration. */
  integration?: Maybe<AlertManagementPrometheusIntegration>
}

/** Autogenerated input type of PromoteToEpic */
export type PromoteToEpicInput = {
  /** The project the issue to mutate is in. */
  projectPath: Scalars["ID"]
  /** The IID of the issue to mutate. */
  iid: Scalars["String"]
  /** The group the promoted epic will belong to. */
  groupPath?: Maybe<Scalars["ID"]>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of PromoteToEpic */
export type PromoteToEpicPayload = {
  __typename?: "PromoteToEpicPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** The epic after issue promotion. */
  epic?: Maybe<Epic>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The issue after mutation. */
  issue?: Maybe<Issue>
}

/** Represents rules that commit pushes must follow. */
export type PushRules = {
  __typename?: "PushRules"
  /** Indicates whether commits not signed through GPG will be rejected. */
  rejectUnsignedCommits: Scalars["Boolean"]
}

/** Pypi metadata */
export type PypiMetadata = {
  __typename?: "PypiMetadata"
  /** ID of the metadatum. */
  id: Scalars["PackagesPypiMetadatumID"]
  /** Required Python version of the Pypi package. */
  requiredPython?: Maybe<Scalars["String"]>
}

export type Query = {
  __typename?: "Query"
  /** CI related settings that apply to the entire instance. */
  ciApplicationSettings?: Maybe<CiApplicationSettings>
  /**
   * Linted and processed contents of a CI config.
   * Should not be requested more than once per request.
   */
  ciConfig?: Maybe<CiConfig>
  /** Find a container repository. */
  containerRepository?: Maybe<ContainerRepositoryDetails>
  /** Fields related to the current license. */
  currentLicense?: Maybe<CurrentLicense>
  /** Get information about current user. */
  currentUser?: Maybe<UserCore>
  /** Fields related to design management. */
  designManagement: DesignManagement
  /** Get configured DevOps adoption namespaces. **BETA** This endpoint is subject to change without notice. */
  devopsAdoptionEnabledNamespaces?: Maybe<DevopsAdoptionSegmentConnection>
  /** Testing endpoint to validate the API with */
  echo: Scalars["String"]
  /** Find a Geo node. */
  geoNode?: Maybe<GeoNode>
  /** Find a group. */
  group?: Maybe<Group>
  /** Fields related to Instance Security Dashboard. */
  instanceSecurityDashboard?: Maybe<InstanceSecurityDashboard>
  /**
   * Get statistics on the instance. Deprecated in 13.10: This was renamed.
   * @deprecated This was renamed. Please use `Query.usageTrendsMeasurements`. Deprecated in 13.10.
   */
  instanceStatisticsMeasurements?: Maybe<UsageTrendsMeasurementConnection>
  /** Find an issue. */
  issue?: Maybe<Issue>
  /** Find an iteration. */
  iteration?: Maybe<Iteration>
  /** Fields related to entries in the license history. */
  licenseHistoryEntries?: Maybe<LicenseHistoryEntryConnection>
  /** Find a merge request. */
  mergeRequest?: Maybe<MergeRequest>
  /** Metadata about GitLab. */
  metadata?: Maybe<Metadata>
  /** Find a milestone. */
  milestone?: Maybe<Milestone>
  /** Find a namespace. */
  namespace?: Maybe<Namespace>
  /** Find a package. */
  package?: Maybe<PackageDetailsType>
  /** Find a project. */
  project?: Maybe<Project>
  /** Find projects visible to the current user. */
  projects?: Maybe<ProjectConnection>
  /** Supported runner platforms. */
  runnerPlatforms?: Maybe<RunnerPlatformConnection>
  /** Runner setup instructions. */
  runnerSetup?: Maybe<RunnerSetup>
  /** Find Snippets visible to the current user. */
  snippets?: Maybe<SnippetConnection>
  /** Get statistics on the instance. */
  usageTrendsMeasurements?: Maybe<UsageTrendsMeasurementConnection>
  /** Find a user. */
  user?: Maybe<UserCore>
  /** Find users. */
  users?: Maybe<UserCoreConnection>
  /** Vulnerabilities reported on projects on the current user's instance security dashboard. */
  vulnerabilities?: Maybe<VulnerabilityConnection>
  /** Number of vulnerabilities per day for the projects on the current user's instance security dashboard. */
  vulnerabilitiesCountByDay?: Maybe<VulnerabilitiesCountByDayConnection>
  /**
   * Number of vulnerabilities per severity level, per day, for the projects on the
   * current user's instance security dashboard.
   *  Deprecated in 13.3: Use of this is not recommended.
   * @deprecated Use of this is not recommended. Please use `Query.vulnerabilitiesCountByDay`. Deprecated in 13.3.
   */
  vulnerabilitiesCountByDayAndSeverity?: Maybe<VulnerabilitiesCountByDayAndSeverityConnection>
  /** Find a vulnerability. */
  vulnerability?: Maybe<Vulnerability>
}

export type QueryCiConfigArgs = {
  projectPath: Scalars["ID"]
  content: Scalars["String"]
  dryRun?: Maybe<Scalars["Boolean"]>
}

export type QueryContainerRepositoryArgs = {
  id: Scalars["ContainerRepositoryID"]
}

export type QueryDevopsAdoptionEnabledNamespacesArgs = {
  displayNamespaceId?: Maybe<Scalars["NamespaceID"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type QueryEchoArgs = {
  text: Scalars["String"]
}

export type QueryGeoNodeArgs = {
  name?: Maybe<Scalars["String"]>
}

export type QueryGroupArgs = {
  fullPath: Scalars["ID"]
}

export type QueryInstanceStatisticsMeasurementsArgs = {
  identifier: MeasurementIdentifier
  recordedAfter?: Maybe<Scalars["Time"]>
  recordedBefore?: Maybe<Scalars["Time"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type QueryIssueArgs = {
  id: Scalars["IssueID"]
}

export type QueryIterationArgs = {
  id: Scalars["IterationID"]
}

export type QueryLicenseHistoryEntriesArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type QueryMergeRequestArgs = {
  id: Scalars["MergeRequestID"]
}

export type QueryMilestoneArgs = {
  id: Scalars["MilestoneID"]
}

export type QueryNamespaceArgs = {
  fullPath: Scalars["ID"]
}

export type QueryPackageArgs = {
  id: Scalars["PackagesPackageID"]
}

export type QueryProjectArgs = {
  fullPath: Scalars["ID"]
}

export type QueryProjectsArgs = {
  membership?: Maybe<Scalars["Boolean"]>
  search?: Maybe<Scalars["String"]>
  ids?: Maybe<Array<Scalars["ID"]>>
  searchNamespaces?: Maybe<Scalars["Boolean"]>
  sort?: Maybe<Scalars["String"]>
  topics?: Maybe<Array<Scalars["String"]>>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type QueryRunnerPlatformsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type QueryRunnerSetupArgs = {
  platform: Scalars["String"]
  architecture: Scalars["String"]
}

export type QuerySnippetsArgs = {
  ids?: Maybe<Array<Scalars["SnippetID"]>>
  visibility?: Maybe<VisibilityScopesEnum>
  authorId?: Maybe<Scalars["UserID"]>
  projectId?: Maybe<Scalars["ProjectID"]>
  type?: Maybe<TypeEnum>
  explore?: Maybe<Scalars["Boolean"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type QueryUsageTrendsMeasurementsArgs = {
  identifier: MeasurementIdentifier
  recordedAfter?: Maybe<Scalars["Time"]>
  recordedBefore?: Maybe<Scalars["Time"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type QueryUserArgs = {
  id?: Maybe<Scalars["UserID"]>
  username?: Maybe<Scalars["String"]>
}

export type QueryUsersArgs = {
  ids?: Maybe<Array<Scalars["ID"]>>
  usernames?: Maybe<Array<Scalars["String"]>>
  sort?: Maybe<Sort>
  search?: Maybe<Scalars["String"]>
  admins?: Maybe<Scalars["Boolean"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type QueryVulnerabilitiesArgs = {
  projectId?: Maybe<Array<Scalars["ID"]>>
  reportType?: Maybe<Array<VulnerabilityReportType>>
  severity?: Maybe<Array<VulnerabilitySeverity>>
  state?: Maybe<Array<VulnerabilityState>>
  scanner?: Maybe<Array<Scalars["String"]>>
  scannerId?: Maybe<Array<Scalars["VulnerabilitiesScannerID"]>>
  sort?: Maybe<VulnerabilitySort>
  hasResolution?: Maybe<Scalars["Boolean"]>
  hasIssues?: Maybe<Scalars["Boolean"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type QueryVulnerabilitiesCountByDayArgs = {
  startDate: Scalars["ISO8601Date"]
  endDate: Scalars["ISO8601Date"]
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type QueryVulnerabilitiesCountByDayAndSeverityArgs = {
  startDate: Scalars["ISO8601Date"]
  endDate: Scalars["ISO8601Date"]
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type QueryVulnerabilityArgs = {
  id: Scalars["VulnerabilityID"]
}

/** Recent failure history of a test case. */
export type RecentFailures = {
  __typename?: "RecentFailures"
  /** Name of the base branch of the project. */
  baseBranch?: Maybe<Scalars["String"]>
  /** Number of times the test case has failed in the past 14 days. */
  count?: Maybe<Scalars["Int"]>
}

/** State of a Geo registry */
export enum RegistryState {
  /** Registry waiting to be synced. */
  Pending = "PENDING",
  /** Registry currently syncing. */
  Started = "STARTED",
  /** Registry that is synced. */
  Synced = "SYNCED",
  /** Registry that failed to sync. */
  Failed = "FAILED",
}

/** Represents a release */
export type Release = {
  __typename?: "Release"
  /** Assets of the release. */
  assets?: Maybe<ReleaseAssets>
  /** User that created the release. */
  author?: Maybe<UserCore>
  /** The commit associated with the release. */
  commit?: Maybe<Commit>
  /** Timestamp of when the release was created. */
  createdAt?: Maybe<Scalars["Time"]>
  /** Description (also known as "release notes") of the release. */
  description?: Maybe<Scalars["String"]>
  /** The GitLab Flavored Markdown rendering of `description` */
  descriptionHtml?: Maybe<Scalars["String"]>
  /** Evidence for the release. */
  evidences?: Maybe<ReleaseEvidenceConnection>
  /** Links of the release. */
  links?: Maybe<ReleaseLinks>
  /** Milestones associated to the release. */
  milestones?: Maybe<MilestoneConnection>
  /** Name of the release. */
  name?: Maybe<Scalars["String"]>
  /** Timestamp of when the release was released. */
  releasedAt?: Maybe<Scalars["Time"]>
  /** Name of the tag associated with the release. */
  tagName?: Maybe<Scalars["String"]>
  /** Relative web path to the tag associated with the release. */
  tagPath?: Maybe<Scalars["String"]>
  /** Indicates the release is an upcoming release. */
  upcomingRelease?: Maybe<Scalars["Boolean"]>
}

/** Represents a release */
export type ReleaseEvidencesArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Represents a release */
export type ReleaseMilestonesArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Represents an asset link associated with a release */
export type ReleaseAssetLink = {
  __typename?: "ReleaseAssetLink"
  /** Direct asset URL of the link. */
  directAssetUrl?: Maybe<Scalars["String"]>
  /** Indicates the link points to an external resource. */
  external?: Maybe<Scalars["Boolean"]>
  /** ID of the link. */
  id: Scalars["ID"]
  /** Type of the link: `other`, `runbook`, `image`, `package`; defaults to `other`. */
  linkType?: Maybe<ReleaseAssetLinkType>
  /** Name of the link. */
  name?: Maybe<Scalars["String"]>
  /** URL of the link. */
  url?: Maybe<Scalars["String"]>
}

/** The connection type for ReleaseAssetLink. */
export type ReleaseAssetLinkConnection = {
  __typename?: "ReleaseAssetLinkConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<ReleaseAssetLinkEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<ReleaseAssetLink>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** Autogenerated input type of ReleaseAssetLinkCreate */
export type ReleaseAssetLinkCreateInput = {
  /** Name of the asset link. */
  name: Scalars["String"]
  /** URL of the asset link. */
  url: Scalars["String"]
  /** Relative path for a direct asset link. */
  directAssetPath?: Maybe<Scalars["String"]>
  /** The type of the asset link. */
  linkType?: Maybe<ReleaseAssetLinkType>
  /** Full path of the project the asset link is associated with. */
  projectPath: Scalars["ID"]
  /** Name of the associated release's tag. */
  tagName: Scalars["String"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of ReleaseAssetLinkCreate */
export type ReleaseAssetLinkCreatePayload = {
  __typename?: "ReleaseAssetLinkCreatePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The asset link after mutation. */
  link?: Maybe<ReleaseAssetLink>
}

/** Autogenerated input type of ReleaseAssetLinkDelete */
export type ReleaseAssetLinkDeleteInput = {
  /** ID of the release asset link to delete. */
  id: Scalars["ReleasesLinkID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of ReleaseAssetLinkDelete */
export type ReleaseAssetLinkDeletePayload = {
  __typename?: "ReleaseAssetLinkDeletePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The deleted release asset link. */
  link?: Maybe<ReleaseAssetLink>
}

/** An edge in a connection. */
export type ReleaseAssetLinkEdge = {
  __typename?: "ReleaseAssetLinkEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<ReleaseAssetLink>
}

/** Fields that are available when modifying a release asset link */
export type ReleaseAssetLinkInput = {
  /** Name of the asset link. */
  name: Scalars["String"]
  /** URL of the asset link. */
  url: Scalars["String"]
  /** Relative path for a direct asset link. */
  directAssetPath?: Maybe<Scalars["String"]>
  /** The type of the asset link. */
  linkType?: Maybe<ReleaseAssetLinkType>
}

/** Type of the link: `other`, `runbook`, `image`, `package` */
export enum ReleaseAssetLinkType {
  /** Other link type */
  Other = "OTHER",
  /** Runbook link type */
  Runbook = "RUNBOOK",
  /** Package link type */
  Package = "PACKAGE",
  /** Image link type */
  Image = "IMAGE",
}

/** Autogenerated input type of ReleaseAssetLinkUpdate */
export type ReleaseAssetLinkUpdateInput = {
  /** ID of the release asset link to update. */
  id: Scalars["ReleasesLinkID"]
  /** Name of the asset link. */
  name?: Maybe<Scalars["String"]>
  /** URL of the asset link. */
  url?: Maybe<Scalars["String"]>
  /** Relative path for a direct asset link. */
  directAssetPath?: Maybe<Scalars["String"]>
  /** The type of the asset link. */
  linkType?: Maybe<ReleaseAssetLinkType>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of ReleaseAssetLinkUpdate */
export type ReleaseAssetLinkUpdatePayload = {
  __typename?: "ReleaseAssetLinkUpdatePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The asset link after mutation. */
  link?: Maybe<ReleaseAssetLink>
}

/** A container for all assets associated with a release */
export type ReleaseAssets = {
  __typename?: "ReleaseAssets"
  /** Number of assets of the release. */
  count?: Maybe<Scalars["Int"]>
  /** Asset links of the release. */
  links?: Maybe<ReleaseAssetLinkConnection>
  /** Sources of the release. */
  sources?: Maybe<ReleaseSourceConnection>
}

/** A container for all assets associated with a release */
export type ReleaseAssetsLinksArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** A container for all assets associated with a release */
export type ReleaseAssetsSourcesArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Fields that are available when modifying release assets */
export type ReleaseAssetsInput = {
  /** A list of asset links to associate to the release. */
  links?: Maybe<Array<ReleaseAssetLinkInput>>
}

/** The connection type for Release. */
export type ReleaseConnection = {
  __typename?: "ReleaseConnection"
  /** Total count of collection. */
  count: Scalars["Int"]
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<ReleaseEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<Release>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** Autogenerated input type of ReleaseCreate */
export type ReleaseCreateInput = {
  /** Full path of the project the release is associated with. */
  projectPath: Scalars["ID"]
  /** Name of the tag to associate with the release. */
  tagName: Scalars["String"]
  /** The commit SHA or branch name to use if creating a new tag. */
  ref?: Maybe<Scalars["String"]>
  /** Name of the release. */
  name?: Maybe<Scalars["String"]>
  /** Description (also known as "release notes") of the release. */
  description?: Maybe<Scalars["String"]>
  /** The date when the release will be/was ready. Defaults to the current time. */
  releasedAt?: Maybe<Scalars["Time"]>
  /** The title of each milestone the release is associated with. GitLab Premium customers can specify group milestones. */
  milestones?: Maybe<Array<Scalars["String"]>>
  /** Assets associated to the release. */
  assets?: Maybe<ReleaseAssetsInput>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of ReleaseCreate */
export type ReleaseCreatePayload = {
  __typename?: "ReleaseCreatePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The release after mutation. */
  release?: Maybe<Release>
}

/** Autogenerated input type of ReleaseDelete */
export type ReleaseDeleteInput = {
  /** Full path of the project the release is associated with. */
  projectPath: Scalars["ID"]
  /** Name of the tag associated with the release to delete. */
  tagName: Scalars["String"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of ReleaseDelete */
export type ReleaseDeletePayload = {
  __typename?: "ReleaseDeletePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The deleted release. */
  release?: Maybe<Release>
}

/** An edge in a connection. */
export type ReleaseEdge = {
  __typename?: "ReleaseEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<Release>
}

/** Evidence for a release */
export type ReleaseEvidence = {
  __typename?: "ReleaseEvidence"
  /** Timestamp when the evidence was collected. */
  collectedAt?: Maybe<Scalars["Time"]>
  /** URL from where the evidence can be downloaded. */
  filepath?: Maybe<Scalars["String"]>
  /** ID of the evidence. */
  id: Scalars["ID"]
  /** SHA1 ID of the evidence hash. */
  sha?: Maybe<Scalars["String"]>
}

/** The connection type for ReleaseEvidence. */
export type ReleaseEvidenceConnection = {
  __typename?: "ReleaseEvidenceConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<ReleaseEvidenceEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<ReleaseEvidence>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type ReleaseEvidenceEdge = {
  __typename?: "ReleaseEvidenceEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<ReleaseEvidence>
}

export type ReleaseLinks = {
  __typename?: "ReleaseLinks"
  /** HTTP URL of the issues page, filtered by this release and `state=closed`. */
  closedIssuesUrl?: Maybe<Scalars["String"]>
  /** HTTP URL of the merge request page , filtered by this release and `state=closed`. */
  closedMergeRequestsUrl?: Maybe<Scalars["String"]>
  /** HTTP URL of the release's edit page. */
  editUrl?: Maybe<Scalars["String"]>
  /** HTTP URL of the merge request page , filtered by this release and `state=merged`. */
  mergedMergeRequestsUrl?: Maybe<Scalars["String"]>
  /** HTTP URL of the issues page, filtered by this release and `state=open`. */
  openedIssuesUrl?: Maybe<Scalars["String"]>
  /** HTTP URL of the merge request page, filtered by this release and `state=open`. */
  openedMergeRequestsUrl?: Maybe<Scalars["String"]>
  /** HTTP URL of the release. */
  selfUrl?: Maybe<Scalars["String"]>
}

/** Values for sorting releases */
export enum ReleaseSort {
  /** Created at descending order. */
  CreatedDesc = "CREATED_DESC",
  /** Created at ascending order. */
  CreatedAsc = "CREATED_ASC",
  /** Released at by descending order. */
  ReleasedAtDesc = "RELEASED_AT_DESC",
  /** Released at by ascending order. */
  ReleasedAtAsc = "RELEASED_AT_ASC",
}

/** Represents the source code attached to a release in a particular format */
export type ReleaseSource = {
  __typename?: "ReleaseSource"
  /** Format of the source. */
  format?: Maybe<Scalars["String"]>
  /** Download URL of the source. */
  url?: Maybe<Scalars["String"]>
}

/** The connection type for ReleaseSource. */
export type ReleaseSourceConnection = {
  __typename?: "ReleaseSourceConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<ReleaseSourceEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<ReleaseSource>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type ReleaseSourceEdge = {
  __typename?: "ReleaseSourceEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<ReleaseSource>
}

/** Autogenerated input type of ReleaseUpdate */
export type ReleaseUpdateInput = {
  /** Full path of the project the release is associated with. */
  projectPath: Scalars["ID"]
  /** Name of the tag associated with the release. */
  tagName: Scalars["String"]
  /** Name of the release. */
  name?: Maybe<Scalars["String"]>
  /** Description (release notes) of the release. */
  description?: Maybe<Scalars["String"]>
  /** The release date. */
  releasedAt?: Maybe<Scalars["Time"]>
  /** The title of each milestone the release is associated with. GitLab Premium customers can specify group milestones. */
  milestones?: Maybe<Array<Scalars["String"]>>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of ReleaseUpdate */
export type ReleaseUpdatePayload = {
  __typename?: "ReleaseUpdatePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The release after mutation. */
  release?: Maybe<Release>
}

/** Autogenerated input type of RemoveAwardEmoji */
export type RemoveAwardEmojiInput = {
  /** The global ID of the awardable resource. */
  awardableId: Scalars["AwardableID"]
  /** The emoji name. */
  name: Scalars["String"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of RemoveAwardEmoji */
export type RemoveAwardEmojiPayload = {
  __typename?: "RemoveAwardEmojiPayload"
  /** The award emoji after mutation. */
  awardEmoji?: Maybe<AwardEmoji>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

/** Autogenerated input type of RemoveProjectFromSecurityDashboard */
export type RemoveProjectFromSecurityDashboardInput = {
  /** ID of the project to remove from the Instance Security Dashboard. */
  id: Scalars["ProjectID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of RemoveProjectFromSecurityDashboard */
export type RemoveProjectFromSecurityDashboardPayload = {
  __typename?: "RemoveProjectFromSecurityDashboardPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

/** Autogenerated input type of RepositionImageDiffNote */
export type RepositionImageDiffNoteInput = {
  /** The global ID of the DiffNote to update. */
  id: Scalars["DiffNoteID"]
  /** The position of this note on a diff. */
  position: UpdateDiffImagePositionInput
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of RepositionImageDiffNote */
export type RepositionImageDiffNotePayload = {
  __typename?: "RepositionImageDiffNotePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The note after mutation. */
  note?: Maybe<Note>
}

export type Repository = {
  __typename?: "Repository"
  /** Blobs contained within the repository */
  blobs?: Maybe<RepositoryBlobConnection>
  /** Names of branches available in this repository that match the search pattern. */
  branchNames?: Maybe<Array<Scalars["String"]>>
  /** Shows a disk path of the repository. */
  diskPath?: Maybe<Scalars["String"]>
  /** Indicates repository has no visible content. */
  empty: Scalars["Boolean"]
  /** Indicates a corresponding Git repository exists on disk. */
  exists: Scalars["Boolean"]
  /** Default branch of the repository. */
  rootRef?: Maybe<Scalars["String"]>
  /** Tree of the repository. */
  tree?: Maybe<Tree>
}

export type RepositoryBlobsArgs = {
  paths: Array<Scalars["String"]>
  ref?: Maybe<Scalars["String"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type RepositoryBranchNamesArgs = {
  searchPattern: Scalars["String"]
  offset: Scalars["Int"]
  limit: Scalars["Int"]
}

export type RepositoryTreeArgs = {
  path?: Maybe<Scalars["String"]>
  ref?: Maybe<Scalars["String"]>
  recursive?: Maybe<Scalars["Boolean"]>
}

export type RepositoryBlob = {
  __typename?: "RepositoryBlob"
  /** Whether the current user can modify the blob. */
  canModifyBlob?: Maybe<Scalars["Boolean"]>
  /** Web path to edit the blob in the old-style editor. */
  editBlobPath?: Maybe<Scalars["String"]>
  /** Web path to download the raw blob via external storage, if enabled. */
  externalStorageUrl?: Maybe<Scalars["String"]>
  /** The expected format of the blob based on the extension. */
  fileType?: Maybe<Scalars["String"]>
  /** Web path to edit this blob using a forked project. */
  forkAndEditPath?: Maybe<Scalars["String"]>
  /** ID of the blob. */
  id: Scalars["ID"]
  /** Web path to edit this blob in the Web IDE. */
  ideEditPath?: Maybe<Scalars["String"]>
  /** Web path to edit this blob in the Web IDE using a forked project. */
  ideForkAndEditPath?: Maybe<Scalars["String"]>
  /** LFS OID of the blob. */
  lfsOid?: Maybe<Scalars["String"]>
  /** Blob mode. */
  mode?: Maybe<Scalars["String"]>
  /** Blob name. */
  name?: Maybe<Scalars["String"]>
  /** OID of the blob. */
  oid: Scalars["String"]
  /** Path of the blob. */
  path: Scalars["String"]
  /** Blob plain highlighted data. */
  plainData?: Maybe<Scalars["String"]>
  /** The raw content of the blob. */
  rawBlob?: Maybe<Scalars["String"]>
  /** Web path to download the raw blob. */
  rawPath?: Maybe<Scalars["String"]>
  /** Size (in bytes) of the blob, or the blob target if stored externally. */
  rawSize?: Maybe<Scalars["Int"]>
  /** The raw content of the blob, if the blob is text data. */
  rawTextBlob?: Maybe<Scalars["String"]>
  /** Web path to replace the blob content. */
  replacePath?: Maybe<Scalars["String"]>
  /** Blob content rich viewer. */
  richViewer?: Maybe<BlobViewer>
  /** Blob content simple viewer. */
  simpleViewer: BlobViewer
  /** Size (in bytes) of the blob. */
  size?: Maybe<Scalars["Int"]>
  /** Whether the blob's content is stored externally (for instance, in LFS). */
  storedExternally?: Maybe<Scalars["Boolean"]>
  /** Web path of the blob. */
  webPath?: Maybe<Scalars["String"]>
}

/** The connection type for RepositoryBlob. */
export type RepositoryBlobConnection = {
  __typename?: "RepositoryBlobConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<RepositoryBlobEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<RepositoryBlob>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type RepositoryBlobEdge = {
  __typename?: "RepositoryBlobEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<RepositoryBlob>
}

/** Represents a requirement */
export type Requirement = {
  __typename?: "Requirement"
  /** Author of the requirement. */
  author: UserCore
  /** Timestamp of when the requirement was created. */
  createdAt: Scalars["Time"]
  /** Description of the requirement. */
  description?: Maybe<Scalars["String"]>
  /** The GitLab Flavored Markdown rendering of `description` */
  descriptionHtml?: Maybe<Scalars["String"]>
  /** ID of the requirement. */
  id: Scalars["ID"]
  /** Internal ID of the requirement. */
  iid: Scalars["ID"]
  /** Indicates if latest test report was created by user. */
  lastTestReportManuallyCreated?: Maybe<Scalars["Boolean"]>
  /** Latest requirement test report state. */
  lastTestReportState?: Maybe<TestReportState>
  /** Project to which the requirement belongs. */
  project: Project
  /** State of the requirement. */
  state: RequirementState
  /** Test reports of the requirement. */
  testReports?: Maybe<TestReportConnection>
  /** Title of the requirement. */
  title?: Maybe<Scalars["String"]>
  /** The GitLab Flavored Markdown rendering of `title` */
  titleHtml?: Maybe<Scalars["String"]>
  /** Timestamp of when the requirement was last updated. */
  updatedAt: Scalars["Time"]
  /** Permissions for the current user on the resource */
  userPermissions: RequirementPermissions
}

/** Represents a requirement */
export type RequirementTestReportsArgs = {
  sort?: Maybe<Sort>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** The connection type for Requirement. */
export type RequirementConnection = {
  __typename?: "RequirementConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<RequirementEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<Requirement>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type RequirementEdge = {
  __typename?: "RequirementEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<Requirement>
}

/** Check permissions for the current user on a requirement */
export type RequirementPermissions = {
  __typename?: "RequirementPermissions"
  /** Indicates the user can perform `admin_requirement` on this resource */
  adminRequirement: Scalars["Boolean"]
  /** Indicates the user can perform `create_requirement` on this resource */
  createRequirement: Scalars["Boolean"]
  /** Indicates the user can perform `destroy_requirement` on this resource */
  destroyRequirement: Scalars["Boolean"]
  /** Indicates the user can perform `read_requirement` on this resource */
  readRequirement: Scalars["Boolean"]
  /** Indicates the user can perform `update_requirement` on this resource */
  updateRequirement: Scalars["Boolean"]
}

/** State of a requirement */
export enum RequirementState {
  Opened = "OPENED",
  Archived = "ARCHIVED",
}

/** Counts of requirements by their state */
export type RequirementStatesCount = {
  __typename?: "RequirementStatesCount"
  /** Number of archived requirements. */
  archived?: Maybe<Scalars["Int"]>
  /** Number of opened requirements. */
  opened?: Maybe<Scalars["Int"]>
}

/** Status of a requirement based on last test report */
export enum RequirementStatusFilter {
  Passed = "PASSED",
  Failed = "FAILED",
  /** Requirements without any test report. */
  Missing = "MISSING",
}

export type ResolvableInterface = {
  /** Indicates if the object can be resolved. */
  resolvable: Scalars["Boolean"]
  /** Indicates if the object is resolved. */
  resolved: Scalars["Boolean"]
  /** Timestamp of when the object was resolved. */
  resolvedAt?: Maybe<Scalars["Time"]>
  /** User who resolved the object. */
  resolvedBy?: Maybe<UserCore>
}

/** Autogenerated input type of RevertVulnerabilityToDetected */
export type RevertVulnerabilityToDetectedInput = {
  /** ID of the vulnerability to be reverted. */
  id: Scalars["VulnerabilityID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of RevertVulnerabilityToDetected */
export type RevertVulnerabilityToDetectedPayload = {
  __typename?: "RevertVulnerabilityToDetectedPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The vulnerability after revert. */
  vulnerability?: Maybe<Vulnerability>
}

export type RootStorageStatistics = {
  __typename?: "RootStorageStatistics"
  /** The CI artifacts size in bytes. */
  buildArtifactsSize: Scalars["Float"]
  /** The LFS objects size in bytes. */
  lfsObjectsSize: Scalars["Float"]
  /** The packages size in bytes. */
  packagesSize: Scalars["Float"]
  /** The CI pipeline artifacts size in bytes. */
  pipelineArtifactsSize: Scalars["Float"]
  /** The Git repository size in bytes. */
  repositorySize: Scalars["Float"]
  /** The snippets size in bytes. */
  snippetsSize: Scalars["Float"]
  /** The total storage in bytes. */
  storageSize: Scalars["Float"]
  /** The uploads size in bytes. */
  uploadsSize: Scalars["Float"]
  /** The wiki size in bytes. */
  wikiSize: Scalars["Float"]
}

/** Autogenerated input type of RunDASTScan */
export type RunDastScanInput = {
  /** The project the DAST scan belongs to. */
  projectPath: Scalars["ID"]
  /** The URL of the target to be scanned. */
  targetUrl: Scalars["String"]
  /** The branch to be associated with the scan. */
  branch: Scalars["String"]
  /** The type of scan to be run. */
  scanType: DastScanTypeEnum
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of RunDASTScan */
export type RunDastScanPayload = {
  __typename?: "RunDASTScanPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** URL of the pipeline that was created. */
  pipelineUrl?: Maybe<Scalars["String"]>
}

export type RunnerArchitecture = {
  __typename?: "RunnerArchitecture"
  /** Download location for the runner for the platform architecture. */
  downloadLocation: Scalars["String"]
  /** Name of the runner platform architecture. */
  name: Scalars["String"]
}

/** The connection type for RunnerArchitecture. */
export type RunnerArchitectureConnection = {
  __typename?: "RunnerArchitectureConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<RunnerArchitectureEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<RunnerArchitecture>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type RunnerArchitectureEdge = {
  __typename?: "RunnerArchitectureEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<RunnerArchitecture>
}

export type RunnerPlatform = {
  __typename?: "RunnerPlatform"
  /** Runner architectures supported for the platform. */
  architectures?: Maybe<RunnerArchitectureConnection>
  /** Human readable name of the runner platform. */
  humanReadableName: Scalars["String"]
  /** Name slug of the runner platform. */
  name: Scalars["String"]
}

export type RunnerPlatformArchitecturesArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** The connection type for RunnerPlatform. */
export type RunnerPlatformConnection = {
  __typename?: "RunnerPlatformConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<RunnerPlatformEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<RunnerPlatform>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type RunnerPlatformEdge = {
  __typename?: "RunnerPlatformEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<RunnerPlatform>
}

export type RunnerSetup = {
  __typename?: "RunnerSetup"
  /** Instructions for installing the runner on the specified architecture. */
  installInstructions: Scalars["String"]
  /** Instructions for registering the runner. */
  registerInstructions?: Maybe<Scalars["String"]>
}

/** Represents a CI configuration of SAST */
export type SastCiConfiguration = {
  __typename?: "SastCiConfiguration"
  /** List of analyzers entities attached to SAST configuration. */
  analyzers?: Maybe<SastCiConfigurationAnalyzersEntityConnection>
  /** List of global entities related to SAST configuration. */
  global?: Maybe<SastCiConfigurationEntityConnection>
  /** List of pipeline entities related to SAST configuration. */
  pipeline?: Maybe<SastCiConfigurationEntityConnection>
}

/** Represents a CI configuration of SAST */
export type SastCiConfigurationAnalyzersArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Represents a CI configuration of SAST */
export type SastCiConfigurationGlobalArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Represents a CI configuration of SAST */
export type SastCiConfigurationPipelineArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Represents an analyzer entity in SAST CI configuration */
export type SastCiConfigurationAnalyzersEntity = {
  __typename?: "SastCiConfigurationAnalyzersEntity"
  /** Analyzer description that is displayed on the form. */
  description?: Maybe<Scalars["String"]>
  /** Indicates whether an analyzer is enabled. */
  enabled?: Maybe<Scalars["Boolean"]>
  /** Analyzer label used in the config UI. */
  label?: Maybe<Scalars["String"]>
  /** Name of the analyzer. */
  name?: Maybe<Scalars["String"]>
  /** List of supported variables. */
  variables?: Maybe<SastCiConfigurationEntityConnection>
}

/** Represents an analyzer entity in SAST CI configuration */
export type SastCiConfigurationAnalyzersEntityVariablesArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** The connection type for SastCiConfigurationAnalyzersEntity. */
export type SastCiConfigurationAnalyzersEntityConnection = {
  __typename?: "SastCiConfigurationAnalyzersEntityConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<SastCiConfigurationAnalyzersEntityEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<SastCiConfigurationAnalyzersEntity>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type SastCiConfigurationAnalyzersEntityEdge = {
  __typename?: "SastCiConfigurationAnalyzersEntityEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<SastCiConfigurationAnalyzersEntity>
}

/** Represents the analyzers entity in SAST CI configuration */
export type SastCiConfigurationAnalyzersEntityInput = {
  /** Name of analyzer. */
  name: Scalars["String"]
  /** State of the analyzer. */
  enabled: Scalars["Boolean"]
  /** List of variables for the analyzer. */
  variables?: Maybe<Array<SastCiConfigurationEntityInput>>
}

/** Represents an entity in SAST CI configuration */
export type SastCiConfigurationEntity = {
  __typename?: "SastCiConfigurationEntity"
  /** Default value that is used if value is empty. */
  defaultValue?: Maybe<Scalars["String"]>
  /** Entity description that is displayed on the form. */
  description?: Maybe<Scalars["String"]>
  /** CI keyword of entity. */
  field?: Maybe<Scalars["String"]>
  /** Label for entity used in the form. */
  label?: Maybe<Scalars["String"]>
  /** Different possible values of the field. */
  options?: Maybe<SastCiConfigurationOptionsEntityConnection>
  /** Size of the UI component. */
  size?: Maybe<SastUiComponentSize>
  /** Type of the field value. */
  type?: Maybe<Scalars["String"]>
  /** Current value of the entity. */
  value?: Maybe<Scalars["String"]>
}

/** Represents an entity in SAST CI configuration */
export type SastCiConfigurationEntityOptionsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** The connection type for SastCiConfigurationEntity. */
export type SastCiConfigurationEntityConnection = {
  __typename?: "SastCiConfigurationEntityConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<SastCiConfigurationEntityEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<SastCiConfigurationEntity>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type SastCiConfigurationEntityEdge = {
  __typename?: "SastCiConfigurationEntityEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<SastCiConfigurationEntity>
}

/** Represents an entity in SAST CI configuration */
export type SastCiConfigurationEntityInput = {
  /** CI keyword of entity. */
  field: Scalars["String"]
  /** Default value that is used if value is empty. */
  defaultValue: Scalars["String"]
  /** Current value of the entity. */
  value: Scalars["String"]
}

/** Represents a CI configuration of SAST */
export type SastCiConfigurationInput = {
  /** List of global entities related to SAST configuration. */
  global?: Maybe<Array<SastCiConfigurationEntityInput>>
  /** List of pipeline entities related to SAST configuration. */
  pipeline?: Maybe<Array<SastCiConfigurationEntityInput>>
  /** List of analyzers and related variables for the SAST configuration. */
  analyzers?: Maybe<Array<SastCiConfigurationAnalyzersEntityInput>>
}

/** Represents an entity for options in SAST CI configuration */
export type SastCiConfigurationOptionsEntity = {
  __typename?: "SastCiConfigurationOptionsEntity"
  /** Label of option entity. */
  label?: Maybe<Scalars["String"]>
  /** Value of option entity. */
  value?: Maybe<Scalars["String"]>
}

/** The connection type for SastCiConfigurationOptionsEntity. */
export type SastCiConfigurationOptionsEntityConnection = {
  __typename?: "SastCiConfigurationOptionsEntityConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<SastCiConfigurationOptionsEntityEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<SastCiConfigurationOptionsEntity>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type SastCiConfigurationOptionsEntityEdge = {
  __typename?: "SastCiConfigurationOptionsEntityEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<SastCiConfigurationOptionsEntity>
}

/** Size of UI component in SAST configuration page */
export enum SastUiComponentSize {
  /** The size of UI component in SAST configuration page is small. */
  Small = "SMALL",
  /** The size of UI component in SAST configuration page is medium. */
  Medium = "MEDIUM",
  /** The size of UI component in SAST configuration page is large. */
  Large = "LARGE",
}

/** Represents the security scan information */
export type Scan = {
  __typename?: "Scan"
  /** List of errors. */
  errors: Array<Scalars["String"]>
  /** Name of the scan. */
  name: Scalars["String"]
}

/** The connection type for Scan. */
export type ScanConnection = {
  __typename?: "ScanConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<ScanEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<Scan>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type ScanEdge = {
  __typename?: "ScanEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<Scan>
}

/** Represents the scan execution policy */
export type ScanExecutionPolicy = {
  __typename?: "ScanExecutionPolicy"
  /** Description of the policy. */
  description: Scalars["String"]
  /** Indicates whether this policy is enabled. */
  enabled: Scalars["Boolean"]
  /** Name of the policy. */
  name: Scalars["String"]
  /** Timestamp of when the policy YAML was last updated. */
  updatedAt: Scalars["Time"]
  /** YAML definition of the policy. */
  yaml: Scalars["String"]
}

/** The connection type for ScanExecutionPolicy. */
export type ScanExecutionPolicyConnection = {
  __typename?: "ScanExecutionPolicyConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<ScanExecutionPolicyEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<ScanExecutionPolicy>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type ScanExecutionPolicyEdge = {
  __typename?: "ScanExecutionPolicyEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<ScanExecutionPolicy>
}

/** Represents a resource scanned by a security scan */
export type ScannedResource = {
  __typename?: "ScannedResource"
  /** The HTTP request method used to access the URL. */
  requestMethod?: Maybe<Scalars["String"]>
  /** The URL scanned by the scanner. */
  url?: Maybe<Scalars["String"]>
}

/** The connection type for ScannedResource. */
export type ScannedResourceConnection = {
  __typename?: "ScannedResourceConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<ScannedResourceEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<ScannedResource>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type ScannedResourceEdge = {
  __typename?: "ScannedResourceEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<ScannedResource>
}

/** Represents summary of a security report */
export type SecurityReportSummary = {
  __typename?: "SecurityReportSummary"
  /** Aggregated counts for the `api_fuzzing` scan */
  apiFuzzing?: Maybe<SecurityReportSummarySection>
  /** Aggregated counts for the `container_scanning` scan */
  containerScanning?: Maybe<SecurityReportSummarySection>
  /** Aggregated counts for the `coverage_fuzzing` scan */
  coverageFuzzing?: Maybe<SecurityReportSummarySection>
  /** Aggregated counts for the `dast` scan */
  dast?: Maybe<SecurityReportSummarySection>
  /** Aggregated counts for the `dependency_scanning` scan */
  dependencyScanning?: Maybe<SecurityReportSummarySection>
  /** Aggregated counts for the `sast` scan */
  sast?: Maybe<SecurityReportSummarySection>
  /** Aggregated counts for the `secret_detection` scan */
  secretDetection?: Maybe<SecurityReportSummarySection>
}

/** Represents a section of a summary of a security report */
export type SecurityReportSummarySection = {
  __typename?: "SecurityReportSummarySection"
  /** A list of the first 20 scanned resources. */
  scannedResources?: Maybe<ScannedResourceConnection>
  /** Total number of scanned resources. */
  scannedResourcesCount?: Maybe<Scalars["Int"]>
  /** Path to download all the scanned resources in CSV format. */
  scannedResourcesCsvPath?: Maybe<Scalars["String"]>
  /** List of security scans ran for the type. */
  scans: ScanConnection
  /** Total number of vulnerabilities. */
  vulnerabilitiesCount?: Maybe<Scalars["Int"]>
}

/** Represents a section of a summary of a security report */
export type SecurityReportSummarySectionScannedResourcesArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Represents a section of a summary of a security report */
export type SecurityReportSummarySectionScansArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export enum SecurityReportTypeEnum {
  /** SAST scan report */
  Sast = "SAST",
  /** DAST scan report */
  Dast = "DAST",
  /** DEPENDENCY SCANNING scan report */
  DependencyScanning = "DEPENDENCY_SCANNING",
  /** CONTAINER SCANNING scan report */
  ContainerScanning = "CONTAINER_SCANNING",
  /** SECRET DETECTION scan report */
  SecretDetection = "SECRET_DETECTION",
  /** COVERAGE FUZZING scan report */
  CoverageFuzzing = "COVERAGE_FUZZING",
  /** API FUZZING scan report */
  ApiFuzzing = "API_FUZZING",
}

/** The type of the security scanner */
export enum SecurityScannerType {
  Sast = "SAST",
  Dast = "DAST",
  DependencyScanning = "DEPENDENCY_SCANNING",
  ContainerScanning = "CONTAINER_SCANNING",
  SecretDetection = "SECRET_DETECTION",
  CoverageFuzzing = "COVERAGE_FUZZING",
  ApiFuzzing = "API_FUZZING",
}

/** Represents a list of security scanners */
export type SecurityScanners = {
  __typename?: "SecurityScanners"
  /** List of analyzers which are available for the project. */
  available?: Maybe<Array<SecurityScannerType>>
  /** List of analyzers which are enabled for the project. */
  enabled?: Maybe<Array<SecurityScannerType>>
  /** List of analyzers which ran successfully in the latest pipeline. */
  pipelineRun?: Maybe<Array<SecurityScannerType>>
}

/** A Sentry error */
export type SentryDetailedError = {
  __typename?: "SentryDetailedError"
  /** Count of occurrences. */
  count: Scalars["Int"]
  /** Culprit of the error. */
  culprit: Scalars["String"]
  /** External Base URL of the Sentry Instance. */
  externalBaseUrl: Scalars["String"]
  /** External URL of the error. */
  externalUrl: Scalars["String"]
  /** Commit the error was first seen. */
  firstReleaseLastCommit?: Maybe<Scalars["String"]>
  /** Release short version the error was first seen. */
  firstReleaseShortVersion?: Maybe<Scalars["String"]>
  /** Release version the error was first seen. */
  firstReleaseVersion?: Maybe<Scalars["String"]>
  /** Timestamp when the error was first seen. */
  firstSeen: Scalars["Time"]
  /** Last 24hr stats of the error. */
  frequency: Array<SentryErrorFrequency>
  /** GitLab commit SHA attributed to the Error based on the release version. */
  gitlabCommit?: Maybe<Scalars["String"]>
  /** Path to the GitLab page for the GitLab commit attributed to the error. */
  gitlabCommitPath?: Maybe<Scalars["String"]>
  /** URL of GitLab Issue. */
  gitlabIssuePath?: Maybe<Scalars["String"]>
  /** ID (global ID) of the error. */
  id: Scalars["ID"]
  /** Commit the error was last seen. */
  lastReleaseLastCommit?: Maybe<Scalars["String"]>
  /** Release short version the error was last seen. */
  lastReleaseShortVersion?: Maybe<Scalars["String"]>
  /** Release version the error was last seen. */
  lastReleaseVersion?: Maybe<Scalars["String"]>
  /** Timestamp when the error was last seen. */
  lastSeen: Scalars["Time"]
  /** Sentry metadata message of the error. */
  message?: Maybe<Scalars["String"]>
  /** ID (Sentry ID) of the error. */
  sentryId: Scalars["String"]
  /** ID of the project (Sentry project). */
  sentryProjectId: Scalars["ID"]
  /** Name of the project affected by the error. */
  sentryProjectName: Scalars["String"]
  /** Slug of the project affected by the error. */
  sentryProjectSlug: Scalars["String"]
  /** Short ID (Sentry ID) of the error. */
  shortId: Scalars["String"]
  /** Status of the error. */
  status: SentryErrorStatus
  /** Tags associated with the Sentry Error. */
  tags: SentryErrorTags
  /** Title of the error. */
  title: Scalars["String"]
  /** Type of the error. */
  type: Scalars["String"]
  /** Count of users affected by the error. */
  userCount: Scalars["Int"]
}

/** A Sentry error. A simplified version of SentryDetailedError */
export type SentryError = {
  __typename?: "SentryError"
  /** Count of occurrences. */
  count: Scalars["Int"]
  /** Culprit of the error. */
  culprit: Scalars["String"]
  /** External URL of the error. */
  externalUrl: Scalars["String"]
  /** Timestamp when the error was first seen. */
  firstSeen: Scalars["Time"]
  /** Last 24hr stats of the error. */
  frequency: Array<SentryErrorFrequency>
  /** ID (global ID) of the error. */
  id: Scalars["ID"]
  /** Timestamp when the error was last seen. */
  lastSeen: Scalars["Time"]
  /** Sentry metadata message of the error. */
  message?: Maybe<Scalars["String"]>
  /** ID (Sentry ID) of the error. */
  sentryId: Scalars["String"]
  /** ID of the project (Sentry project). */
  sentryProjectId: Scalars["ID"]
  /** Name of the project affected by the error. */
  sentryProjectName: Scalars["String"]
  /** Slug of the project affected by the error. */
  sentryProjectSlug: Scalars["String"]
  /** Short ID (Sentry ID) of the error. */
  shortId: Scalars["String"]
  /** Status of the error. */
  status: SentryErrorStatus
  /** Title of the error. */
  title: Scalars["String"]
  /** Type of the error. */
  type: Scalars["String"]
  /** Count of users affected by the error. */
  userCount: Scalars["Int"]
}

/** An object containing a collection of Sentry errors, and a detailed error */
export type SentryErrorCollection = {
  __typename?: "SentryErrorCollection"
  /** Detailed version of a Sentry error on the project. */
  detailedError?: Maybe<SentryDetailedError>
  /** Stack Trace of Sentry Error. */
  errorStackTrace?: Maybe<SentryErrorStackTrace>
  /** Collection of Sentry Errors. */
  errors?: Maybe<SentryErrorConnection>
  /** External URL for Sentry. */
  externalUrl?: Maybe<Scalars["String"]>
}

/** An object containing a collection of Sentry errors, and a detailed error */
export type SentryErrorCollectionDetailedErrorArgs = {
  id: Scalars["GitlabErrorTrackingDetailedErrorID"]
}

/** An object containing a collection of Sentry errors, and a detailed error */
export type SentryErrorCollectionErrorStackTraceArgs = {
  id: Scalars["GitlabErrorTrackingDetailedErrorID"]
}

/** An object containing a collection of Sentry errors, and a detailed error */
export type SentryErrorCollectionErrorsArgs = {
  searchTerm?: Maybe<Scalars["String"]>
  sort?: Maybe<Scalars["String"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** The connection type for SentryError. */
export type SentryErrorConnection = {
  __typename?: "SentryErrorConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<SentryErrorEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<SentryError>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type SentryErrorEdge = {
  __typename?: "SentryErrorEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<SentryError>
}

export type SentryErrorFrequency = {
  __typename?: "SentryErrorFrequency"
  /** Count of errors received since the previously recorded time. */
  count: Scalars["Int"]
  /** Time the error frequency stats were recorded. */
  time: Scalars["Time"]
}

/** An object containing a stack trace entry for a Sentry error */
export type SentryErrorStackTrace = {
  __typename?: "SentryErrorStackTrace"
  /** Time the stack trace was received by Sentry. */
  dateReceived: Scalars["String"]
  /** ID of the Sentry error. */
  issueId: Scalars["String"]
  /** Stack trace entries for the Sentry error. */
  stackTraceEntries: Array<SentryErrorStackTraceEntry>
}

/** An object context for a Sentry error stack trace */
export type SentryErrorStackTraceContext = {
  __typename?: "SentryErrorStackTraceContext"
  /** Code number of the context. */
  code: Scalars["String"]
  /** Line number of the context. */
  line: Scalars["Int"]
}

/** An object containing a stack trace entry for a Sentry error */
export type SentryErrorStackTraceEntry = {
  __typename?: "SentryErrorStackTraceEntry"
  /** Function in which the Sentry error occurred. */
  col?: Maybe<Scalars["String"]>
  /** File in which the Sentry error occurred. */
  fileName?: Maybe<Scalars["String"]>
  /** Function in which the Sentry error occurred. */
  function?: Maybe<Scalars["String"]>
  /** Function in which the Sentry error occurred. */
  line?: Maybe<Scalars["String"]>
  /** Context of the Sentry error. */
  traceContext?: Maybe<Array<SentryErrorStackTraceContext>>
}

/** State of a Sentry error */
export enum SentryErrorStatus {
  /** Error has been resolved. */
  Resolved = "RESOLVED",
  /** Error has been ignored until next release. */
  ResolvedInNextRelease = "RESOLVED_IN_NEXT_RELEASE",
  /** Error is unresolved. */
  Unresolved = "UNRESOLVED",
  /** Error has been ignored. */
  Ignored = "IGNORED",
}

/** State of a Sentry error */
export type SentryErrorTags = {
  __typename?: "SentryErrorTags"
  /** Severity level of the Sentry Error. */
  level?: Maybe<Scalars["String"]>
  /** Logger of the Sentry Error. */
  logger?: Maybe<Scalars["String"]>
}

export type Service = {
  /** Indicates if the service is active. */
  active?: Maybe<Scalars["Boolean"]>
  /** Class name of the service. */
  type?: Maybe<Scalars["String"]>
}

/** The connection type for Service. */
export type ServiceConnection = {
  __typename?: "ServiceConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<ServiceEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<Service>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type ServiceEdge = {
  __typename?: "ServiceEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<Service>
}

export enum ServiceType {
  /** AsanaService type */
  AsanaService = "ASANA_SERVICE",
  /** AssemblaService type */
  AssemblaService = "ASSEMBLA_SERVICE",
  /** BambooService type */
  BambooService = "BAMBOO_SERVICE",
  /** BugzillaService type */
  BugzillaService = "BUGZILLA_SERVICE",
  /** BuildkiteService type */
  BuildkiteService = "BUILDKITE_SERVICE",
  /** CampfireService type */
  CampfireService = "CAMPFIRE_SERVICE",
  /** ConfluenceService type */
  ConfluenceService = "CONFLUENCE_SERVICE",
  /** CustomIssueTrackerService type */
  CustomIssueTrackerService = "CUSTOM_ISSUE_TRACKER_SERVICE",
  /** DatadogService type */
  DatadogService = "DATADOG_SERVICE",
  /** DiscordService type */
  DiscordService = "DISCORD_SERVICE",
  /** DroneCiService type */
  DroneCiService = "DRONE_CI_SERVICE",
  /** EmailsOnPushService type */
  EmailsOnPushService = "EMAILS_ON_PUSH_SERVICE",
  /** EwmService type */
  EwmService = "EWM_SERVICE",
  /** ExternalWikiService type */
  ExternalWikiService = "EXTERNAL_WIKI_SERVICE",
  /** FlowdockService type */
  FlowdockService = "FLOWDOCK_SERVICE",
  /** GithubService type */
  GithubService = "GITHUB_SERVICE",
  /** GitlabSlackApplicationService type */
  GitlabSlackApplicationService = "GITLAB_SLACK_APPLICATION_SERVICE",
  /** HangoutsChatService type */
  HangoutsChatService = "HANGOUTS_CHAT_SERVICE",
  /** IrkerService type */
  IrkerService = "IRKER_SERVICE",
  /** JenkinsService type */
  JenkinsService = "JENKINS_SERVICE",
  /** JiraService type */
  JiraService = "JIRA_SERVICE",
  /** MattermostService type */
  MattermostService = "MATTERMOST_SERVICE",
  /** MattermostSlashCommandsService type */
  MattermostSlashCommandsService = "MATTERMOST_SLASH_COMMANDS_SERVICE",
  /** MicrosoftTeamsService type */
  MicrosoftTeamsService = "MICROSOFT_TEAMS_SERVICE",
  /** PackagistService type */
  PackagistService = "PACKAGIST_SERVICE",
  /** PipelinesEmailService type */
  PipelinesEmailService = "PIPELINES_EMAIL_SERVICE",
  /** PivotaltrackerService type */
  PivotaltrackerService = "PIVOTALTRACKER_SERVICE",
  /** PrometheusService type */
  PrometheusService = "PROMETHEUS_SERVICE",
  /** PushoverService type */
  PushoverService = "PUSHOVER_SERVICE",
  /** RedmineService type */
  RedmineService = "REDMINE_SERVICE",
  /** SlackService type */
  SlackService = "SLACK_SERVICE",
  /** SlackSlashCommandsService type */
  SlackSlashCommandsService = "SLACK_SLASH_COMMANDS_SERVICE",
  /** TeamcityService type */
  TeamcityService = "TEAMCITY_SERVICE",
  /** UnifyCircuitService type */
  UnifyCircuitService = "UNIFY_CIRCUIT_SERVICE",
  /** WebexTeamsService type */
  WebexTeamsService = "WEBEX_TEAMS_SERVICE",
  /** YoutrackService type */
  YoutrackService = "YOUTRACK_SERVICE",
}

/** Represents a snippet entry */
export type Snippet = Noteable & {
  __typename?: "Snippet"
  /** The owner of the snippet. */
  author?: Maybe<UserCore>
  /**
   * Snippet blob. Deprecated in 13.3: Use `blobs`.
   * @deprecated Use `blobs`. Deprecated in 13.3.
   */
  blob: SnippetBlob
  /** Snippet blobs. */
  blobs?: Maybe<SnippetBlobConnection>
  /** Timestamp this snippet was created. */
  createdAt: Scalars["Time"]
  /** Description of the snippet. */
  description?: Maybe<Scalars["String"]>
  /** The GitLab Flavored Markdown rendering of `description` */
  descriptionHtml?: Maybe<Scalars["String"]>
  /** All discussions on this noteable. */
  discussions: DiscussionConnection
  /** File Name of the snippet. */
  fileName?: Maybe<Scalars["String"]>
  /** HTTP URL to the snippet repository. */
  httpUrlToRepo?: Maybe<Scalars["String"]>
  /** ID of the snippet. */
  id: Scalars["SnippetID"]
  /** All notes on this noteable. */
  notes: NoteConnection
  /** The project the snippet is associated with. */
  project?: Maybe<Project>
  /** Raw URL of the snippet. */
  rawUrl: Scalars["String"]
  /** SSH URL to the snippet repository. */
  sshUrlToRepo?: Maybe<Scalars["String"]>
  /** Title of the snippet. */
  title: Scalars["String"]
  /** Timestamp this snippet was updated. */
  updatedAt: Scalars["Time"]
  /** Permissions for the current user on the resource */
  userPermissions: SnippetPermissions
  /** Visibility Level of the snippet. */
  visibilityLevel: VisibilityLevelsEnum
  /** Web URL of the snippet. */
  webUrl: Scalars["String"]
}

/** Represents a snippet entry */
export type SnippetBlobsArgs = {
  paths?: Maybe<Array<Scalars["String"]>>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Represents a snippet entry */
export type SnippetDiscussionsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Represents a snippet entry */
export type SnippetNotesArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Represents the snippet blob */
export type SnippetBlob = {
  __typename?: "SnippetBlob"
  /** Shows whether the blob is binary. */
  binary: Scalars["Boolean"]
  /** Blob external storage. */
  externalStorage?: Maybe<Scalars["String"]>
  /** Blob mode. */
  mode?: Maybe<Scalars["String"]>
  /** Blob name. */
  name?: Maybe<Scalars["String"]>
  /** Blob path. */
  path?: Maybe<Scalars["String"]>
  /** Blob plain highlighted data. */
  plainData?: Maybe<Scalars["String"]>
  /** Blob raw content endpoint path. */
  rawPath: Scalars["String"]
  /** Shows whether the blob is rendered as text. */
  renderedAsText: Scalars["Boolean"]
  /** Blob highlighted data. */
  richData?: Maybe<Scalars["String"]>
  /** Blob content rich viewer. */
  richViewer?: Maybe<SnippetBlobViewer>
  /** Blob content simple viewer. */
  simpleViewer: SnippetBlobViewer
  /** Blob size. */
  size: Scalars["Int"]
}

/** Type of a snippet blob input action */
export enum SnippetBlobActionEnum {
  /** Create a snippet blob. */
  Create = "create",
  /** Update a snippet blob. */
  Update = "update",
  /** Delete a snippet blob. */
  Delete = "delete",
  /** Move a snippet blob. */
  Move = "move",
}

/** Represents an action to perform over a snippet file */
export type SnippetBlobActionInputType = {
  /** Type of input action. */
  action: SnippetBlobActionEnum
  /** Previous path of the snippet file. */
  previousPath?: Maybe<Scalars["String"]>
  /** Path of the snippet file. */
  filePath: Scalars["String"]
  /** Snippet file content. */
  content?: Maybe<Scalars["String"]>
}

/** The connection type for SnippetBlob. */
export type SnippetBlobConnection = {
  __typename?: "SnippetBlobConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<SnippetBlobEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<SnippetBlob>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type SnippetBlobEdge = {
  __typename?: "SnippetBlobEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<SnippetBlob>
}

/** Represents how the blob content should be displayed */
export type SnippetBlobViewer = {
  __typename?: "SnippetBlobViewer"
  /** Shows whether the blob should be displayed collapsed. */
  collapsed: Scalars["Boolean"]
  /** Content file type. */
  fileType: Scalars["String"]
  /** Shows whether the blob content is loaded asynchronously. */
  loadAsync: Scalars["Boolean"]
  /** Loading partial name. */
  loadingPartialName: Scalars["String"]
  /** Error rendering the blob content. */
  renderError?: Maybe<Scalars["String"]>
  /** Shows whether the blob is too large to be displayed. */
  tooLarge: Scalars["Boolean"]
  /** Type of blob viewer. */
  type: BlobViewersType
}

/** The connection type for Snippet. */
export type SnippetConnection = {
  __typename?: "SnippetConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<SnippetEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<Snippet>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type SnippetEdge = {
  __typename?: "SnippetEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<Snippet>
}

export type SnippetPermissions = {
  __typename?: "SnippetPermissions"
  /** Indicates the user can perform `admin_snippet` on this resource */
  adminSnippet: Scalars["Boolean"]
  /** Indicates the user can perform `award_emoji` on this resource */
  awardEmoji: Scalars["Boolean"]
  /** Indicates the user can perform `create_note` on this resource */
  createNote: Scalars["Boolean"]
  /** Indicates the user can perform `read_snippet` on this resource */
  readSnippet: Scalars["Boolean"]
  /** Indicates the user can perform `report_snippet` on this resource */
  reportSnippet: Scalars["Boolean"]
  /** Indicates the user can perform `update_snippet` on this resource */
  updateSnippet: Scalars["Boolean"]
}

/** Represents the Geo sync and verification state of a snippet repository */
export type SnippetRepositoryRegistry = {
  __typename?: "SnippetRepositoryRegistry"
  /** Timestamp when the SnippetRepositoryRegistry was created */
  createdAt?: Maybe<Scalars["Time"]>
  /** ID of the SnippetRepositoryRegistry */
  id: Scalars["ID"]
  /** Error message during sync of the SnippetRepositoryRegistry */
  lastSyncFailure?: Maybe<Scalars["String"]>
  /** Timestamp of the most recent successful sync of the SnippetRepositoryRegistry */
  lastSyncedAt?: Maybe<Scalars["Time"]>
  /** Timestamp after which the SnippetRepositoryRegistry should be resynced */
  retryAt?: Maybe<Scalars["Time"]>
  /** Number of consecutive failed sync attempts of the SnippetRepositoryRegistry */
  retryCount?: Maybe<Scalars["Int"]>
  /** ID of the Snippet Repository. */
  snippetRepositoryId: Scalars["ID"]
  /** Sync state of the SnippetRepositoryRegistry */
  state?: Maybe<RegistryState>
}

/** The connection type for SnippetRepositoryRegistry. */
export type SnippetRepositoryRegistryConnection = {
  __typename?: "SnippetRepositoryRegistryConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<SnippetRepositoryRegistryEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<SnippetRepositoryRegistry>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type SnippetRepositoryRegistryEdge = {
  __typename?: "SnippetRepositoryRegistryEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<SnippetRepositoryRegistry>
}

/** Common sort values */
export enum Sort {
  /** Updated at descending order. */
  UpdatedDesc = "UPDATED_DESC",
  /** Updated at ascending order. */
  UpdatedAsc = "UPDATED_ASC",
  /** Created at descending order. */
  CreatedDesc = "CREATED_DESC",
  /** Created at ascending order. */
  CreatedAsc = "CREATED_ASC",
}

export type StatusAction = {
  __typename?: "StatusAction"
  /** Title for the button, for example: Retry this job. */
  buttonTitle?: Maybe<Scalars["String"]>
  /** Icon used in the action button. */
  icon?: Maybe<Scalars["String"]>
  /** Method for the action, for example: :post. */
  method?: Maybe<Scalars["String"]>
  /** Path for the action. */
  path?: Maybe<Scalars["String"]>
  /** Title for the action, for example: Retry. */
  title?: Maybe<Scalars["String"]>
}

export type Submodule = Entry & {
  __typename?: "Submodule"
  /** Flat path of the entry. */
  flatPath: Scalars["String"]
  /** ID of the entry. */
  id: Scalars["ID"]
  /** Name of the entry. */
  name: Scalars["String"]
  /** Path of the entry. */
  path: Scalars["String"]
  /** Last commit SHA for the entry. */
  sha: Scalars["String"]
  /** Tree URL for the sub-module. */
  treeUrl?: Maybe<Scalars["String"]>
  /** Type of tree entry. */
  type: EntryType
  /** Web URL for the sub-module. */
  webUrl?: Maybe<Scalars["String"]>
}

/** The connection type for Submodule. */
export type SubmoduleConnection = {
  __typename?: "SubmoduleConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<SubmoduleEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<Submodule>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type SubmoduleEdge = {
  __typename?: "SubmoduleEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<Submodule>
}

export type Subscription = {
  __typename?: "Subscription"
  /** Triggered when the assignees of an issuable are updated. */
  issuableAssigneesUpdated?: Maybe<Issuable>
}

export type SubscriptionIssuableAssigneesUpdatedArgs = {
  issuableId: Scalars["IssuableID"]
}

/** Completion status of tasks */
export type TaskCompletionStatus = {
  __typename?: "TaskCompletionStatus"
  /** Number of completed tasks. */
  completedCount: Scalars["Int"]
  /** Number of total tasks. */
  count: Scalars["Int"]
}

export type TerraformState = {
  __typename?: "TerraformState"
  /** Timestamp the Terraform state was created. */
  createdAt: Scalars["Time"]
  /** ID of the Terraform state. */
  id: Scalars["ID"]
  /** The latest version of the Terraform state. */
  latestVersion?: Maybe<TerraformStateVersion>
  /** Timestamp the Terraform state was locked. */
  lockedAt?: Maybe<Scalars["Time"]>
  /** The user currently holding a lock on the Terraform state. */
  lockedByUser?: Maybe<UserCore>
  /** Name of the Terraform state. */
  name: Scalars["String"]
  /** Timestamp the Terraform state was updated. */
  updatedAt: Scalars["Time"]
}

/** The connection type for TerraformState. */
export type TerraformStateConnection = {
  __typename?: "TerraformStateConnection"
  /** Total count of collection. */
  count: Scalars["Int"]
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<TerraformStateEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<TerraformState>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** Autogenerated input type of TerraformStateDelete */
export type TerraformStateDeleteInput = {
  /** Global ID of the Terraform state. */
  id: Scalars["TerraformStateID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of TerraformStateDelete */
export type TerraformStateDeletePayload = {
  __typename?: "TerraformStateDeletePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

/** An edge in a connection. */
export type TerraformStateEdge = {
  __typename?: "TerraformStateEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<TerraformState>
}

/** Autogenerated input type of TerraformStateLock */
export type TerraformStateLockInput = {
  /** Global ID of the Terraform state. */
  id: Scalars["TerraformStateID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of TerraformStateLock */
export type TerraformStateLockPayload = {
  __typename?: "TerraformStateLockPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

/** Autogenerated input type of TerraformStateUnlock */
export type TerraformStateUnlockInput = {
  /** Global ID of the Terraform state. */
  id: Scalars["TerraformStateID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of TerraformStateUnlock */
export type TerraformStateUnlockPayload = {
  __typename?: "TerraformStateUnlockPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

export type TerraformStateVersion = {
  __typename?: "TerraformStateVersion"
  /** Timestamp the version was created. */
  createdAt: Scalars["Time"]
  /** The user that created this version. */
  createdByUser?: Maybe<UserCore>
  /** URL for downloading the version's JSON file. */
  downloadPath?: Maybe<Scalars["String"]>
  /** ID of the Terraform state version. */
  id: Scalars["ID"]
  /** The job that created this version. */
  job?: Maybe<CiJob>
  /** Serial number of the version. */
  serial?: Maybe<Scalars["Int"]>
  /** Timestamp the version was updated. */
  updatedAt: Scalars["Time"]
}

/** Represents the Geo sync and verification state of a terraform state version */
export type TerraformStateVersionRegistry = {
  __typename?: "TerraformStateVersionRegistry"
  /** Timestamp when the TerraformStateVersionRegistry was created */
  createdAt?: Maybe<Scalars["Time"]>
  /** ID of the TerraformStateVersionRegistry */
  id: Scalars["ID"]
  /** Error message during sync of the TerraformStateVersionRegistry */
  lastSyncFailure?: Maybe<Scalars["String"]>
  /** Timestamp of the most recent successful sync of the TerraformStateVersionRegistry */
  lastSyncedAt?: Maybe<Scalars["Time"]>
  /** Timestamp after which the TerraformStateVersionRegistry should be resynced */
  retryAt?: Maybe<Scalars["Time"]>
  /** Number of consecutive failed sync attempts of the TerraformStateVersionRegistry */
  retryCount?: Maybe<Scalars["Int"]>
  /** Sync state of the TerraformStateVersionRegistry */
  state?: Maybe<RegistryState>
  /** ID of the terraform state version. */
  terraformStateVersionId: Scalars["ID"]
}

/** The connection type for TerraformStateVersionRegistry. */
export type TerraformStateVersionRegistryConnection = {
  __typename?: "TerraformStateVersionRegistryConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<TerraformStateVersionRegistryEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<TerraformStateVersionRegistry>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type TerraformStateVersionRegistryEdge = {
  __typename?: "TerraformStateVersionRegistryEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<TerraformStateVersionRegistry>
}

/** Test case in pipeline test report. */
export type TestCase = {
  __typename?: "TestCase"
  /** URL of the test case attachment file. */
  attachmentUrl?: Maybe<Scalars["String"]>
  /** Classname of the test case. */
  classname?: Maybe<Scalars["String"]>
  /** Test case execution time in seconds. */
  executionTime?: Maybe<Scalars["Float"]>
  /** Path to the file of the test case. */
  file?: Maybe<Scalars["String"]>
  /** Name of the test case. */
  name?: Maybe<Scalars["String"]>
  /** Recent failure history of the test case on the base branch. */
  recentFailures?: Maybe<RecentFailures>
  /** Stack trace of the test case. */
  stackTrace?: Maybe<Scalars["String"]>
  /** Status of the test case (error, failed, success, skipped). */
  status?: Maybe<TestCaseStatus>
  /** System output of the test case. */
  systemOutput?: Maybe<Scalars["String"]>
}

/** The connection type for TestCase. */
export type TestCaseConnection = {
  __typename?: "TestCaseConnection"
  /** Total count of collection. */
  count: Scalars["Int"]
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<TestCaseEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<TestCase>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type TestCaseEdge = {
  __typename?: "TestCaseEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<TestCase>
}

export enum TestCaseStatus {
  /** Test case that has a status of error. */
  Error = "error",
  /** Test case that has a status of failed. */
  Failed = "failed",
  /** Test case that has a status of success. */
  Success = "success",
  /** Test case that has a status of skipped. */
  Skipped = "skipped",
}

/** Represents a requirement test report */
export type TestReport = {
  __typename?: "TestReport"
  /** Author of the test report. */
  author?: Maybe<UserCore>
  /** Timestamp of when the test report was created. */
  createdAt: Scalars["Time"]
  /** ID of the test report. */
  id: Scalars["ID"]
  /** State of the test report. */
  state: TestReportState
}

/** The connection type for TestReport. */
export type TestReportConnection = {
  __typename?: "TestReportConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<TestReportEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<TestReport>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type TestReportEdge = {
  __typename?: "TestReportEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<TestReport>
}

/** State of a test report */
export enum TestReportState {
  Passed = "PASSED",
  Failed = "FAILED",
}

/** Test report for a pipeline */
export type TestReportSummary = {
  __typename?: "TestReportSummary"
  /** Test suites belonging to a pipeline test report. */
  testSuites: TestSuiteSummaryConnection
  /** Total report statistics for a pipeline test report. */
  total: TestReportTotal
}

/** Test report for a pipeline */
export type TestReportSummaryTestSuitesArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Total test report statistics. */
export type TestReportTotal = {
  __typename?: "TestReportTotal"
  /** Total number of the test cases. */
  count?: Maybe<Scalars["Int"]>
  /** Total number of test cases that had an error. */
  error?: Maybe<Scalars["Int"]>
  /** Total number of test cases that failed. */
  failed?: Maybe<Scalars["Int"]>
  /** Total number of test cases that were skipped. */
  skipped?: Maybe<Scalars["Int"]>
  /** Total number of test cases that succeeded. */
  success?: Maybe<Scalars["Int"]>
  /** Test suite error message. */
  suiteError?: Maybe<Scalars["String"]>
  /** Total duration of the tests. */
  time?: Maybe<Scalars["Float"]>
}

/** Test suite in a pipeline test report. */
export type TestSuite = {
  __typename?: "TestSuite"
  /** Total number of test cases that had an error. */
  errorCount?: Maybe<Scalars["Int"]>
  /** Total number of test cases that failed in the test suite. */
  failedCount?: Maybe<Scalars["Int"]>
  /** Name of the test suite. */
  name?: Maybe<Scalars["String"]>
  /** Total number of test cases that were skipped in the test suite. */
  skippedCount?: Maybe<Scalars["Int"]>
  /** Total number of test cases that succeeded in the test suite. */
  successCount?: Maybe<Scalars["Int"]>
  /** Test suite error message. */
  suiteError?: Maybe<Scalars["String"]>
  /** Test cases in the test suite. */
  testCases?: Maybe<TestCaseConnection>
  /** Total number of the test cases in the test suite. */
  totalCount?: Maybe<Scalars["Int"]>
  /** Total duration of the tests in the test suite. */
  totalTime?: Maybe<Scalars["Float"]>
}

/** Test suite in a pipeline test report. */
export type TestSuiteTestCasesArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Test suite summary in a pipeline test report. */
export type TestSuiteSummary = {
  __typename?: "TestSuiteSummary"
  /** IDs of the builds used to run the test suite. */
  buildIds?: Maybe<Array<Scalars["ID"]>>
  /** Total number of test cases that had an error. */
  errorCount?: Maybe<Scalars["Int"]>
  /** Total number of test cases that failed in the test suite. */
  failedCount?: Maybe<Scalars["Int"]>
  /** Name of the test suite. */
  name?: Maybe<Scalars["String"]>
  /** Total number of test cases that were skipped in the test suite. */
  skippedCount?: Maybe<Scalars["Int"]>
  /** Total number of test cases that succeeded in the test suite. */
  successCount?: Maybe<Scalars["Int"]>
  /** Test suite error message. */
  suiteError?: Maybe<Scalars["String"]>
  /** Total number of the test cases in the test suite. */
  totalCount?: Maybe<Scalars["Int"]>
  /** Total duration of the tests in the test suite. */
  totalTime?: Maybe<Scalars["Float"]>
}

/** The connection type for TestSuiteSummary. */
export type TestSuiteSummaryConnection = {
  __typename?: "TestSuiteSummaryConnection"
  /** Total count of collection. */
  count: Scalars["Int"]
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<TestSuiteSummaryEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<TestSuiteSummary>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type TestSuiteSummaryEdge = {
  __typename?: "TestSuiteSummaryEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<TestSuiteSummary>
}

/** Represents the time report stats for timeboxes */
export type TimeReportStats = {
  __typename?: "TimeReportStats"
  /** Completed issues metrics. */
  complete?: Maybe<TimeboxMetrics>
  /** Incomplete issues metrics. */
  incomplete?: Maybe<TimeboxMetrics>
  /** Total issues metrics. */
  total?: Maybe<TimeboxMetrics>
}

/** Represents measured stats metrics for timeboxes */
export type TimeboxMetrics = {
  __typename?: "TimeboxMetrics"
  /** The count metric. */
  count: Scalars["Int"]
  /** The weight metric. */
  weight: Scalars["Int"]
}

/** Represents a historically accurate report about the timebox */
export type TimeboxReport = {
  __typename?: "TimeboxReport"
  /** Daily scope and completed totals for burnup charts. */
  burnupTimeSeries?: Maybe<Array<BurnupChartDailyTotals>>
  /** Represents the time report stats for the timebox. */
  stats?: Maybe<TimeReportStats>
}

export type TimeboxReportInterface = {
  /** Historically accurate report about the timebox. */
  report?: Maybe<TimeboxReport>
}

/** A time-frame defined as a closed inclusive range of two dates */
export type Timeframe = {
  /** The start of the range. */
  start: Scalars["Date"]
  /** The end of the range. */
  end: Scalars["Date"]
}

export type Timelog = {
  __typename?: "Timelog"
  /** The issue that logged time was added to. */
  issue?: Maybe<Issue>
  /** The merge request that logged time was added to. */
  mergeRequest?: Maybe<MergeRequest>
  /** The note where the quick action to add the logged time was executed. */
  note?: Maybe<Note>
  /** Timestamp of when the time tracked was spent at. */
  spentAt?: Maybe<Scalars["Time"]>
  /** The time spent displayed in seconds. */
  timeSpent: Scalars["Int"]
  /** The user that logged the time. */
  user: UserCore
}

/** The connection type for Timelog. */
export type TimelogConnection = {
  __typename?: "TimelogConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<TimelogEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<Timelog>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type TimelogEdge = {
  __typename?: "TimelogEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<Timelog>
}

/** Representing a to-do entry */
export type Todo = {
  __typename?: "Todo"
  /** Action of the to-do item. */
  action: TodoActionEnum
  /** The author of this to-do item. */
  author: UserCore
  /** Body of the to-do item. */
  body: Scalars["String"]
  /** Timestamp this to-do item was created. */
  createdAt: Scalars["Time"]
  /** Group this to-do item is associated with. */
  group?: Maybe<Group>
  /** ID of the to-do item. */
  id: Scalars["ID"]
  /** The project this to-do item is associated with. */
  project?: Maybe<Project>
  /** State of the to-do item. */
  state: TodoStateEnum
  /** Target type of the to-do item. */
  targetType: TodoTargetEnum
}

export enum TodoActionEnum {
  /** User was assigned. */
  Assigned = "assigned",
  /** User was mentioned. */
  Mentioned = "mentioned",
  /** Build triggered by the user failed. */
  BuildFailed = "build_failed",
  /** User added a TODO. */
  Marked = "marked",
  /** User was set as an approver. */
  ApprovalRequired = "approval_required",
  /** Merge request authored by the user could not be merged. */
  Unmergeable = "unmergeable",
  /** User was directly addressed. */
  DirectlyAddressed = "directly_addressed",
  /** Merge request authored by the user was removed from the merge train. */
  MergeTrainRemoved = "merge_train_removed",
  /** Review was requested from the user. */
  ReviewRequested = "review_requested",
}

/** The connection type for Todo. */
export type TodoConnection = {
  __typename?: "TodoConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<TodoEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<Todo>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** Autogenerated input type of TodoCreate */
export type TodoCreateInput = {
  /** The global ID of the to-do item's parent. Issues, merge requests, designs and epics are supported. */
  targetId: Scalars["TodoableID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of TodoCreate */
export type TodoCreatePayload = {
  __typename?: "TodoCreatePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The to-do item created. */
  todo?: Maybe<Todo>
}

/** An edge in a connection. */
export type TodoEdge = {
  __typename?: "TodoEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<Todo>
}

/** Autogenerated input type of TodoMarkDone */
export type TodoMarkDoneInput = {
  /** The global ID of the to-do item to mark as done. */
  id: Scalars["TodoID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of TodoMarkDone */
export type TodoMarkDonePayload = {
  __typename?: "TodoMarkDonePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The requested to-do item. */
  todo: Todo
}

/** Autogenerated input type of TodoRestore */
export type TodoRestoreInput = {
  /** The global ID of the to-do item to restore. */
  id: Scalars["TodoID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated input type of TodoRestoreMany */
export type TodoRestoreManyInput = {
  /** The global IDs of the to-do items to restore (a maximum of 50 is supported at once). */
  ids: Array<Scalars["TodoID"]>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of TodoRestoreMany */
export type TodoRestoreManyPayload = {
  __typename?: "TodoRestoreManyPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** Updated to-do items. */
  todos: Array<Todo>
  /**
   * The IDs of the updated to-do items. Deprecated in 13.2: Use to-do items.
   * @deprecated Use to-do items. Deprecated in 13.2.
   */
  updatedIds: Array<Scalars["TodoID"]>
}

/** Autogenerated return type of TodoRestore */
export type TodoRestorePayload = {
  __typename?: "TodoRestorePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The requested to-do item. */
  todo: Todo
}

export enum TodoStateEnum {
  /** The state of the todo is pending. */
  Pending = "pending",
  /** The state of the todo is done. */
  Done = "done",
}

export enum TodoTargetEnum {
  /** A Commit. */
  Commit = "COMMIT",
  /** An Issue. */
  Issue = "ISSUE",
  /** A MergeRequest. */
  Mergerequest = "MERGEREQUEST",
  /** A Design. */
  Design = "DESIGN",
  /** An Alert. */
  Alert = "ALERT",
  /** An Epic. */
  Epic = "EPIC",
}

/** Autogenerated input type of TodosMarkAllDone */
export type TodosMarkAllDoneInput = {
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of TodosMarkAllDone */
export type TodosMarkAllDonePayload = {
  __typename?: "TodosMarkAllDonePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** Updated to-do items. */
  todos: Array<Todo>
  /**
   * IDs of the updated to-do items. Deprecated in 13.2: Use to-do items.
   * @deprecated Use to-do items. Deprecated in 13.2.
   */
  updatedIds: Array<Scalars["TodoID"]>
}

/** Autogenerated input type of ToggleAwardEmoji */
export type ToggleAwardEmojiInput = {
  /** The global ID of the awardable resource. */
  awardableId: Scalars["AwardableID"]
  /** The emoji name. */
  name: Scalars["String"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of ToggleAwardEmoji */
export type ToggleAwardEmojiPayload = {
  __typename?: "ToggleAwardEmojiPayload"
  /** The award emoji after mutation. */
  awardEmoji?: Maybe<AwardEmoji>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** Indicates the status of the emoji. True if the toggle awarded the emoji, and false if the toggle removed the emoji. */
  toggledOn: Scalars["Boolean"]
}

export type Tree = {
  __typename?: "Tree"
  /** Blobs of the tree. */
  blobs: BlobConnection
  /** Last commit for the tree. */
  lastCommit?: Maybe<Commit>
  /** Sub-modules of the tree. */
  submodules: SubmoduleConnection
  /** Trees of the tree. */
  trees: TreeEntryConnection
}

export type TreeBlobsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type TreeSubmodulesArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type TreeTreesArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Represents a directory */
export type TreeEntry = Entry & {
  __typename?: "TreeEntry"
  /** Flat path of the entry. */
  flatPath: Scalars["String"]
  /** ID of the entry. */
  id: Scalars["ID"]
  /** Name of the entry. */
  name: Scalars["String"]
  /** Path of the entry. */
  path: Scalars["String"]
  /** Last commit SHA for the entry. */
  sha: Scalars["String"]
  /** Type of tree entry. */
  type: EntryType
  /** Web path for the tree entry (directory). */
  webPath?: Maybe<Scalars["String"]>
  /** Web URL for the tree entry (directory). */
  webUrl?: Maybe<Scalars["String"]>
}

/** The connection type for TreeEntry. */
export type TreeEntryConnection = {
  __typename?: "TreeEntryConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<TreeEntryEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<TreeEntry>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type TreeEntryEdge = {
  __typename?: "TreeEntryEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<TreeEntry>
}

export enum TypeEnum {
  /** Snippet created independent of any project. */
  Personal = "personal",
  /** Snippet related to a specific project. */
  Project = "project",
}

/** Autogenerated input type of UpdateAlertStatus */
export type UpdateAlertStatusInput = {
  /** The project the alert to mutate is in. */
  projectPath: Scalars["ID"]
  /** The IID of the alert to mutate. */
  iid: Scalars["String"]
  /** The status to set the alert. */
  status: AlertManagementStatus
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of UpdateAlertStatus */
export type UpdateAlertStatusPayload = {
  __typename?: "UpdateAlertStatusPayload"
  /** The alert after mutation. */
  alert?: Maybe<AlertManagementAlert>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The issue created after mutation. */
  issue?: Maybe<Issue>
  /** The to-do item after mutation. */
  todo?: Maybe<Todo>
}

/** Autogenerated input type of UpdateBoardEpicUserPreferences */
export type UpdateBoardEpicUserPreferencesInput = {
  /** The board global ID. */
  boardId: Scalars["BoardID"]
  /** ID of an epic to set preferences for. */
  epicId: Scalars["EpicID"]
  /** Whether the epic should be collapsed in the board. */
  collapsed: Scalars["Boolean"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of UpdateBoardEpicUserPreferences */
export type UpdateBoardEpicUserPreferencesPayload = {
  __typename?: "UpdateBoardEpicUserPreferencesPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** User preferences for the epic in the board after mutation. */
  epicUserPreferences?: Maybe<BoardEpicUserPreferences>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

/** Autogenerated input type of UpdateBoard */
export type UpdateBoardInput = {
  /** The board name. */
  name?: Maybe<Scalars["String"]>
  /** Whether or not backlog list is hidden. */
  hideBacklogList?: Maybe<Scalars["Boolean"]>
  /** Whether or not closed list is hidden. */
  hideClosedList?: Maybe<Scalars["Boolean"]>
  /** The board global ID. */
  id: Scalars["BoardID"]
  /** ID of user to be assigned to the board. */
  assigneeId?: Maybe<Scalars["UserID"]>
  /** ID of milestone to be assigned to the board. */
  milestoneId?: Maybe<Scalars["MilestoneID"]>
  /** ID of iteration to be assigned to the board. */
  iterationId?: Maybe<Scalars["IterationID"]>
  /** Weight value to be assigned to the board. */
  weight?: Maybe<Scalars["Int"]>
  /** Labels of the issue. */
  labels?: Maybe<Array<Scalars["String"]>>
  /** IDs of labels to be added to the board. */
  labelIds?: Maybe<Array<Scalars["LabelID"]>>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated input type of UpdateBoardList */
export type UpdateBoardListInput = {
  /** Position of list within the board. */
  position?: Maybe<Scalars["Int"]>
  /** Indicates if the list is collapsed for this user. */
  collapsed?: Maybe<Scalars["Boolean"]>
  /** Global ID of the list. */
  listId: Scalars["ListID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of UpdateBoardList */
export type UpdateBoardListPayload = {
  __typename?: "UpdateBoardListPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** Mutated list. */
  list?: Maybe<BoardList>
}

/** Autogenerated return type of UpdateBoard */
export type UpdateBoardPayload = {
  __typename?: "UpdateBoardPayload"
  /** The board after mutation. */
  board?: Maybe<Board>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

/** Autogenerated input type of UpdateComplianceFramework */
export type UpdateComplianceFrameworkInput = {
  /** The global ID of the compliance framework to update. */
  id: Scalars["ComplianceManagementFrameworkID"]
  /** Parameters to update the compliance framework with. */
  params: ComplianceFrameworkInput
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of UpdateComplianceFramework */
export type UpdateComplianceFrameworkPayload = {
  __typename?: "UpdateComplianceFrameworkPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** The compliance framework after mutation. */
  complianceFramework?: Maybe<ComplianceFramework>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

/** Autogenerated input type of UpdateContainerExpirationPolicy */
export type UpdateContainerExpirationPolicyInput = {
  /** The project path where the container expiration policy is located. */
  projectPath: Scalars["ID"]
  /** Indicates whether this container expiration policy is enabled. */
  enabled?: Maybe<Scalars["Boolean"]>
  /** This container expiration policy schedule. */
  cadence?: Maybe<ContainerExpirationPolicyCadenceEnum>
  /** Tags older that this will expire. */
  olderThan?: Maybe<ContainerExpirationPolicyOlderThanEnum>
  /** Number of tags to retain. */
  keepN?: Maybe<ContainerExpirationPolicyKeepEnum>
  /** Tags with names matching this regex pattern will expire. */
  nameRegex?: Maybe<Scalars["UntrustedRegexp"]>
  /** Tags with names matching this regex pattern will be preserved. */
  nameRegexKeep?: Maybe<Scalars["UntrustedRegexp"]>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of UpdateContainerExpirationPolicy */
export type UpdateContainerExpirationPolicyPayload = {
  __typename?: "UpdateContainerExpirationPolicyPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** The container expiration policy after mutation. */
  containerExpirationPolicy?: Maybe<ContainerExpirationPolicy>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

export type UpdateDiffImagePositionInput = {
  /** X position of the note. */
  x?: Maybe<Scalars["Int"]>
  /** Y position of the note. */
  y?: Maybe<Scalars["Int"]>
  /** Total width of the image. */
  width?: Maybe<Scalars["Int"]>
  /** Total height of the image. */
  height?: Maybe<Scalars["Int"]>
}

/** Autogenerated input type of UpdateEpicBoardList */
export type UpdateEpicBoardListInput = {
  /** Position of list within the board. */
  position?: Maybe<Scalars["Int"]>
  /** Indicates if the list is collapsed for this user. */
  collapsed?: Maybe<Scalars["Boolean"]>
  /** Global ID of the epic list. */
  listId: Scalars["BoardsEpicListID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of UpdateEpicBoardList */
export type UpdateEpicBoardListPayload = {
  __typename?: "UpdateEpicBoardListPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** Mutated epic list. */
  list?: Maybe<EpicList>
}

/** Autogenerated input type of UpdateEpic */
export type UpdateEpicInput = {
  /** The IID of the epic to mutate. */
  iid: Scalars["ID"]
  /** The group the epic to mutate is in. */
  groupPath: Scalars["ID"]
  /** The title of the epic. */
  title?: Maybe<Scalars["String"]>
  /** The description of the epic. */
  description?: Maybe<Scalars["String"]>
  /** Indicates if the epic is confidential. */
  confidential?: Maybe<Scalars["Boolean"]>
  /** The start date of the epic. */
  startDateFixed?: Maybe<Scalars["String"]>
  /** The end date of the epic. */
  dueDateFixed?: Maybe<Scalars["String"]>
  /** Indicates start date should be sourced from start_date_fixed field not the issue milestones. */
  startDateIsFixed?: Maybe<Scalars["Boolean"]>
  /** Indicates end date should be sourced from due_date_fixed field not the issue milestones. */
  dueDateIsFixed?: Maybe<Scalars["Boolean"]>
  /** The IDs of labels to be added to the epic. */
  addLabelIds?: Maybe<Array<Scalars["ID"]>>
  /** The IDs of labels to be removed from the epic. */
  removeLabelIds?: Maybe<Array<Scalars["ID"]>>
  /** State event for the epic. */
  stateEvent?: Maybe<EpicStateEvent>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of UpdateEpic */
export type UpdateEpicPayload = {
  __typename?: "UpdateEpicPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** The epic after mutation. */
  epic?: Maybe<Epic>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

/** Autogenerated input type of UpdateImageDiffNote */
export type UpdateImageDiffNoteInput = {
  /** The global ID of the note to update. */
  id: Scalars["NoteID"]
  /** Content of the note. */
  body?: Maybe<Scalars["String"]>
  /** The position of this note on a diff. */
  position?: Maybe<UpdateDiffImagePositionInput>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of UpdateImageDiffNote */
export type UpdateImageDiffNotePayload = {
  __typename?: "UpdateImageDiffNotePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The note after mutation. */
  note?: Maybe<Note>
}

/** Autogenerated input type of UpdateIssue */
export type UpdateIssueInput = {
  /** The project the issue to mutate is in. */
  projectPath: Scalars["ID"]
  /** The IID of the issue to mutate. */
  iid: Scalars["String"]
  /** Description of the issue. */
  description?: Maybe<Scalars["String"]>
  /** Due date of the issue. */
  dueDate?: Maybe<Scalars["ISO8601Date"]>
  /** Indicates the issue is confidential. */
  confidential?: Maybe<Scalars["Boolean"]>
  /** Indicates discussion is locked on the issue. */
  locked?: Maybe<Scalars["Boolean"]>
  /** Type of the issue. */
  type?: Maybe<IssueType>
  /** Title of the issue. */
  title?: Maybe<Scalars["String"]>
  /** The ID of the milestone to assign to the issue. On update milestone will be removed if set to null. */
  milestoneId?: Maybe<Scalars["ID"]>
  /** The IDs of labels to be added to the issue. */
  addLabelIds?: Maybe<Array<Scalars["ID"]>>
  /** The IDs of labels to be removed from the issue. */
  removeLabelIds?: Maybe<Array<Scalars["ID"]>>
  /** Close or reopen an issue. */
  stateEvent?: Maybe<IssueStateEvent>
  /** The desired health status. */
  healthStatus?: Maybe<HealthStatus>
  /** The weight of the issue. */
  weight?: Maybe<Scalars["Int"]>
  /** The ID of the parent epic. NULL when removing the association. */
  epicId?: Maybe<Scalars["EpicID"]>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of UpdateIssue */
export type UpdateIssuePayload = {
  __typename?: "UpdateIssuePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The issue after mutation. */
  issue?: Maybe<Issue>
}

/** Autogenerated input type of UpdateIteration */
export type UpdateIterationInput = {
  /** Group of the iteration. */
  groupPath: Scalars["ID"]
  /** Global ID of the iteration. */
  id: Scalars["ID"]
  /** Title of the iteration. */
  title?: Maybe<Scalars["String"]>
  /** Description of the iteration. */
  description?: Maybe<Scalars["String"]>
  /** Start date of the iteration. */
  startDate?: Maybe<Scalars["String"]>
  /** End date of the iteration. */
  dueDate?: Maybe<Scalars["String"]>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of UpdateIteration */
export type UpdateIterationPayload = {
  __typename?: "UpdateIterationPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** Updated iteration. */
  iteration?: Maybe<Iteration>
}

/** Autogenerated input type of UpdateNamespacePackageSettings */
export type UpdateNamespacePackageSettingsInput = {
  /** The namespace path where the namespace package setting is located. */
  namespacePath: Scalars["ID"]
  /** Indicates whether duplicate Maven packages are allowed for this namespace. */
  mavenDuplicatesAllowed?: Maybe<Scalars["Boolean"]>
  /** When maven_duplicates_allowed is false, you can publish duplicate packages with names that match this regex. Otherwise, this setting has no effect. */
  mavenDuplicateExceptionRegex?: Maybe<Scalars["UntrustedRegexp"]>
  /** Indicates whether duplicate generic packages are allowed for this namespace. */
  genericDuplicatesAllowed?: Maybe<Scalars["Boolean"]>
  /** When generic_duplicates_allowed is false, you can publish duplicate packages with names that match this regex. Otherwise, this setting has no effect. */
  genericDuplicateExceptionRegex?: Maybe<Scalars["UntrustedRegexp"]>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of UpdateNamespacePackageSettings */
export type UpdateNamespacePackageSettingsPayload = {
  __typename?: "UpdateNamespacePackageSettingsPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The namespace package setting after mutation. */
  packageSettings?: Maybe<PackageSettings>
}

/** Autogenerated input type of UpdateNote */
export type UpdateNoteInput = {
  /** The global ID of the note to update. */
  id: Scalars["NoteID"]
  /** Content of the note. */
  body?: Maybe<Scalars["String"]>
  /** The confidentiality flag of a note. Default is false. */
  confidential?: Maybe<Scalars["Boolean"]>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of UpdateNote */
export type UpdateNotePayload = {
  __typename?: "UpdateNotePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The note after mutation. */
  note?: Maybe<Note>
}

/** Autogenerated input type of UpdateRequirement */
export type UpdateRequirementInput = {
  /** Title of the requirement. */
  title?: Maybe<Scalars["String"]>
  /** Description of the requirement. */
  description?: Maybe<Scalars["String"]>
  /** Full project path the requirement is associated with. */
  projectPath: Scalars["ID"]
  /** State of the requirement. */
  state?: Maybe<RequirementState>
  /** The IID of the requirement to update. */
  iid: Scalars["String"]
  /** Creates a test report for the requirement with the given state. */
  lastTestReportState?: Maybe<TestReportState>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of UpdateRequirement */
export type UpdateRequirementPayload = {
  __typename?: "UpdateRequirementPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** Requirement after mutation. */
  requirement?: Maybe<Requirement>
}

/** Autogenerated input type of UpdateSnippet */
export type UpdateSnippetInput = {
  /** The global ID of the snippet to update. */
  id: Scalars["SnippetID"]
  /** Title of the snippet. */
  title?: Maybe<Scalars["String"]>
  /** Description of the snippet. */
  description?: Maybe<Scalars["String"]>
  /** The visibility level of the snippet. */
  visibilityLevel?: Maybe<VisibilityLevelsEnum>
  /** Actions to perform over the snippet repository and blobs. */
  blobActions?: Maybe<Array<SnippetBlobActionInputType>>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of UpdateSnippet */
export type UpdateSnippetPayload = {
  __typename?: "UpdateSnippetPayload"
  /**
   * The CAPTCHA site key which must be used to render a challenge for the user to solve to obtain a valid captchaResponse value. Included only when an operation was not completed because "NeedsCaptchaResponse" is true. Deprecated in 13.11: Use spam protection with HTTP headers instead.
   * @deprecated Use spam protection with HTTP headers instead. Deprecated in 13.11.
   */
  captchaSiteKey?: Maybe<Scalars["String"]>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /**
   * Indicates whether the operation was detected as possible spam and not completed. If CAPTCHA is enabled, the request must be resubmitted with a valid CAPTCHA response and spam_log_id included for the operation to be completed. Included only when an operation was not completed because "NeedsCaptchaResponse" is true. Deprecated in 13.11: Use spam protection with HTTP headers instead.
   * @deprecated Use spam protection with HTTP headers instead. Deprecated in 13.11.
   */
  needsCaptchaResponse?: Maybe<Scalars["Boolean"]>
  /** The snippet after mutation. */
  snippet?: Maybe<Snippet>
  /**
   * Indicates whether the operation was detected as definite spam. There is no option to resubmit the request with a CAPTCHA response. Deprecated in 13.11: Use spam protection with HTTP headers instead.
   * @deprecated Use spam protection with HTTP headers instead. Deprecated in 13.11.
   */
  spam?: Maybe<Scalars["Boolean"]>
  /**
   * The spam log ID which must be passed along with a valid CAPTCHA response for an operation to be completed. Included only when an operation was not completed because "NeedsCaptchaResponse" is true. Deprecated in 13.11: Use spam protection with HTTP headers instead.
   * @deprecated Use spam protection with HTTP headers instead. Deprecated in 13.11.
   */
  spamLogId?: Maybe<Scalars["Int"]>
}

/** Represents a recorded measurement (object count) for the Admins */
export type UsageTrendsMeasurement = {
  __typename?: "UsageTrendsMeasurement"
  /** Object count. */
  count: Scalars["Int"]
  /** The type of objects being measured. */
  identifier: MeasurementIdentifier
  /** The time the measurement was recorded. */
  recordedAt?: Maybe<Scalars["Time"]>
}

/** The connection type for UsageTrendsMeasurement. */
export type UsageTrendsMeasurementConnection = {
  __typename?: "UsageTrendsMeasurementConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<UsageTrendsMeasurementEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<UsageTrendsMeasurement>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type UsageTrendsMeasurementEdge = {
  __typename?: "UsageTrendsMeasurementEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<UsageTrendsMeasurement>
}

/** Representation of a GitLab user. */
export type User = {
  /** Merge requests assigned to the user. */
  assignedMergeRequests?: Maybe<MergeRequestConnection>
  /** Merge requests authored by the user. */
  authoredMergeRequests?: Maybe<MergeRequestConnection>
  /** URL of the user's avatar. */
  avatarUrl?: Maybe<Scalars["String"]>
  /** Indicates if the user is a bot. */
  bot: Scalars["Boolean"]
  /** User callouts that belong to the user. */
  callouts?: Maybe<UserCalloutConnection>
  /**
   * User email. Deprecated in 13.7: This was renamed.
   * @deprecated This was renamed. Please use `User.publicEmail`. Deprecated in 13.7.
   */
  email?: Maybe<Scalars["String"]>
  /** Group count for the user. */
  groupCount?: Maybe<Scalars["Int"]>
  /** Group memberships of the user. */
  groupMemberships?: Maybe<GroupMemberConnection>
  /** ID of the user. */
  id: Scalars["ID"]
  /** The location of the user. */
  location?: Maybe<Scalars["String"]>
  /** Human-readable name of the user. */
  name: Scalars["String"]
  /** Project memberships of the user. */
  projectMemberships?: Maybe<ProjectMemberConnection>
  /** User's public email. */
  publicEmail?: Maybe<Scalars["String"]>
  /** Merge requests assigned to the user for review. */
  reviewRequestedMergeRequests?: Maybe<MergeRequestConnection>
  /** Snippets authored by the user. */
  snippets?: Maybe<SnippetConnection>
  /** Projects starred by the user. */
  starredProjects?: Maybe<ProjectConnection>
  /** State of the user. */
  state: UserState
  /** User status. */
  status?: Maybe<UserStatus>
  /** To-do items of the user. */
  todos?: Maybe<TodoConnection>
  /** Permissions for the current user on the resource. */
  userPermissions: UserPermissions
  /** Username of the user. Unique within this instance of GitLab. */
  username: Scalars["String"]
  /** Web path of the user. */
  webPath: Scalars["String"]
  /** Web URL of the user. */
  webUrl: Scalars["String"]
}

/** Representation of a GitLab user. */
export type UserAssignedMergeRequestsArgs = {
  iids?: Maybe<Array<Scalars["String"]>>
  sourceBranches?: Maybe<Array<Scalars["String"]>>
  targetBranches?: Maybe<Array<Scalars["String"]>>
  state?: Maybe<MergeRequestState>
  labels?: Maybe<Array<Scalars["String"]>>
  mergedAfter?: Maybe<Scalars["Time"]>
  mergedBefore?: Maybe<Scalars["Time"]>
  milestoneTitle?: Maybe<Scalars["String"]>
  sort?: Maybe<MergeRequestSort>
  not?: Maybe<MergeRequestsResolverNegatedParams>
  projectPath?: Maybe<Scalars["String"]>
  projectId?: Maybe<Scalars["ProjectID"]>
  authorUsername?: Maybe<Scalars["String"]>
  reviewerUsername?: Maybe<Scalars["String"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Representation of a GitLab user. */
export type UserAuthoredMergeRequestsArgs = {
  iids?: Maybe<Array<Scalars["String"]>>
  sourceBranches?: Maybe<Array<Scalars["String"]>>
  targetBranches?: Maybe<Array<Scalars["String"]>>
  state?: Maybe<MergeRequestState>
  labels?: Maybe<Array<Scalars["String"]>>
  mergedAfter?: Maybe<Scalars["Time"]>
  mergedBefore?: Maybe<Scalars["Time"]>
  milestoneTitle?: Maybe<Scalars["String"]>
  sort?: Maybe<MergeRequestSort>
  not?: Maybe<MergeRequestsResolverNegatedParams>
  projectPath?: Maybe<Scalars["String"]>
  projectId?: Maybe<Scalars["ProjectID"]>
  assigneeUsername?: Maybe<Scalars["String"]>
  reviewerUsername?: Maybe<Scalars["String"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Representation of a GitLab user. */
export type UserCalloutsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Representation of a GitLab user. */
export type UserGroupMembershipsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Representation of a GitLab user. */
export type UserProjectMembershipsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Representation of a GitLab user. */
export type UserReviewRequestedMergeRequestsArgs = {
  iids?: Maybe<Array<Scalars["String"]>>
  sourceBranches?: Maybe<Array<Scalars["String"]>>
  targetBranches?: Maybe<Array<Scalars["String"]>>
  state?: Maybe<MergeRequestState>
  labels?: Maybe<Array<Scalars["String"]>>
  mergedAfter?: Maybe<Scalars["Time"]>
  mergedBefore?: Maybe<Scalars["Time"]>
  milestoneTitle?: Maybe<Scalars["String"]>
  sort?: Maybe<MergeRequestSort>
  not?: Maybe<MergeRequestsResolverNegatedParams>
  projectPath?: Maybe<Scalars["String"]>
  projectId?: Maybe<Scalars["ProjectID"]>
  authorUsername?: Maybe<Scalars["String"]>
  assigneeUsername?: Maybe<Scalars["String"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Representation of a GitLab user. */
export type UserSnippetsArgs = {
  ids?: Maybe<Array<Scalars["SnippetID"]>>
  visibility?: Maybe<VisibilityScopesEnum>
  type?: Maybe<TypeEnum>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Representation of a GitLab user. */
export type UserStarredProjectsArgs = {
  search?: Maybe<Scalars["String"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Representation of a GitLab user. */
export type UserTodosArgs = {
  action?: Maybe<Array<TodoActionEnum>>
  authorId?: Maybe<Array<Scalars["ID"]>>
  projectId?: Maybe<Array<Scalars["ID"]>>
  groupId?: Maybe<Array<Scalars["ID"]>>
  state?: Maybe<Array<TodoStateEnum>>
  type?: Maybe<Array<TodoTargetEnum>>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

export type UserCallout = {
  __typename?: "UserCallout"
  /** Date when the callout was dismissed. */
  dismissedAt?: Maybe<Scalars["Time"]>
  /** Name of the feature that the callout is for. */
  featureName: UserCalloutFeatureNameEnum
}

/** The connection type for UserCallout. */
export type UserCalloutConnection = {
  __typename?: "UserCalloutConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<UserCalloutEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<UserCallout>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** Autogenerated input type of UserCalloutCreate */
export type UserCalloutCreateInput = {
  /** The feature name you want to dismiss the callout for. */
  featureName: Scalars["String"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of UserCalloutCreate */
export type UserCalloutCreatePayload = {
  __typename?: "UserCalloutCreatePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The user callout dismissed. */
  userCallout: UserCallout
}

/** An edge in a connection. */
export type UserCalloutEdge = {
  __typename?: "UserCalloutEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<UserCallout>
}

/** Name of the feature that the callout is for. */
export enum UserCalloutFeatureNameEnum {
  /** Callout feature name for gke_cluster_integration. */
  GkeClusterIntegration = "GKE_CLUSTER_INTEGRATION",
  /** Callout feature name for gcp_signup_offer. */
  GcpSignupOffer = "GCP_SIGNUP_OFFER",
  /** Callout feature name for cluster_security_warning. */
  ClusterSecurityWarning = "CLUSTER_SECURITY_WARNING",
  /** Callout feature name for ultimate_trial. */
  UltimateTrial = "ULTIMATE_TRIAL",
  /** Callout feature name for geo_enable_hashed_storage. */
  GeoEnableHashedStorage = "GEO_ENABLE_HASHED_STORAGE",
  /** Callout feature name for geo_migrate_hashed_storage. */
  GeoMigrateHashedStorage = "GEO_MIGRATE_HASHED_STORAGE",
  /** Callout feature name for canary_deployment. */
  CanaryDeployment = "CANARY_DEPLOYMENT",
  /** Callout feature name for gold_trial_billings. */
  GoldTrialBillings = "GOLD_TRIAL_BILLINGS",
  /** Callout feature name for suggest_popover_dismissed. */
  SuggestPopoverDismissed = "SUGGEST_POPOVER_DISMISSED",
  /** Callout feature name for tabs_position_highlight. */
  TabsPositionHighlight = "TABS_POSITION_HIGHLIGHT",
  /** Callout feature name for threat_monitoring_info. */
  ThreatMonitoringInfo = "THREAT_MONITORING_INFO",
  /** Callout feature name for account_recovery_regular_check. */
  AccountRecoveryRegularCheck = "ACCOUNT_RECOVERY_REGULAR_CHECK",
  /** Callout feature name for service_templates_deprecated_callout. */
  ServiceTemplatesDeprecatedCallout = "SERVICE_TEMPLATES_DEPRECATED_CALLOUT",
  /** Callout feature name for web_ide_alert_dismissed. */
  WebIdeAlertDismissed = "WEB_IDE_ALERT_DISMISSED",
  /** Callout feature name for active_user_count_threshold. */
  ActiveUserCountThreshold = "ACTIVE_USER_COUNT_THRESHOLD",
  /** Callout feature name for buy_pipeline_minutes_notification_dot. */
  BuyPipelineMinutesNotificationDot = "BUY_PIPELINE_MINUTES_NOTIFICATION_DOT",
  /** Callout feature name for personal_access_token_expiry. */
  PersonalAccessTokenExpiry = "PERSONAL_ACCESS_TOKEN_EXPIRY",
  /** Callout feature name for suggest_pipeline. */
  SuggestPipeline = "SUGGEST_PIPELINE",
  /** Callout feature name for customize_homepage. */
  CustomizeHomepage = "CUSTOMIZE_HOMEPAGE",
  /** Callout feature name for feature_flags_new_version. */
  FeatureFlagsNewVersion = "FEATURE_FLAGS_NEW_VERSION",
  /** Callout feature name for registration_enabled_callout. */
  RegistrationEnabledCallout = "REGISTRATION_ENABLED_CALLOUT",
  /** Callout feature name for new_user_signups_cap_reached. */
  NewUserSignupsCapReached = "NEW_USER_SIGNUPS_CAP_REACHED",
  /** Callout feature name for unfinished_tag_cleanup_callout. */
  UnfinishedTagCleanupCallout = "UNFINISHED_TAG_CLEANUP_CALLOUT",
  /** Callout feature name for eoa_bronze_plan_banner. */
  EoaBronzePlanBanner = "EOA_BRONZE_PLAN_BANNER",
  /** Callout feature name for pipeline_needs_banner. */
  PipelineNeedsBanner = "PIPELINE_NEEDS_BANNER",
  /** Callout feature name for pipeline_needs_hover_tip. */
  PipelineNeedsHoverTip = "PIPELINE_NEEDS_HOVER_TIP",
  /** Callout feature name for web_ide_ci_environments_guidance. */
  WebIdeCiEnvironmentsGuidance = "WEB_IDE_CI_ENVIRONMENTS_GUIDANCE",
}

/** Core represention of a GitLab user. */
export type UserCore = User & {
  __typename?: "UserCore"
  /** Merge requests assigned to the user. */
  assignedMergeRequests?: Maybe<MergeRequestConnection>
  /** Merge requests authored by the user. */
  authoredMergeRequests?: Maybe<MergeRequestConnection>
  /** URL of the user's avatar. */
  avatarUrl?: Maybe<Scalars["String"]>
  /** Indicates if the user is a bot. */
  bot: Scalars["Boolean"]
  /** User callouts that belong to the user. */
  callouts?: Maybe<UserCalloutConnection>
  /**
   * User email. Deprecated in 13.7: This was renamed.
   * @deprecated This was renamed. Please use `User.publicEmail`. Deprecated in 13.7.
   */
  email?: Maybe<Scalars["String"]>
  /** Group count for the user. */
  groupCount?: Maybe<Scalars["Int"]>
  /** Group memberships of the user. */
  groupMemberships?: Maybe<GroupMemberConnection>
  /** ID of the user. */
  id: Scalars["ID"]
  /** The location of the user. */
  location?: Maybe<Scalars["String"]>
  /** Human-readable name of the user. */
  name: Scalars["String"]
  /** Project memberships of the user. */
  projectMemberships?: Maybe<ProjectMemberConnection>
  /** User's public email. */
  publicEmail?: Maybe<Scalars["String"]>
  /** Merge requests assigned to the user for review. */
  reviewRequestedMergeRequests?: Maybe<MergeRequestConnection>
  /** Snippets authored by the user. */
  snippets?: Maybe<SnippetConnection>
  /** Projects starred by the user. */
  starredProjects?: Maybe<ProjectConnection>
  /** State of the user. */
  state: UserState
  /** User status. */
  status?: Maybe<UserStatus>
  /** To-do items of the user. */
  todos?: Maybe<TodoConnection>
  /** Permissions for the current user on the resource. */
  userPermissions: UserPermissions
  /** Username of the user. Unique within this instance of GitLab. */
  username: Scalars["String"]
  /** Web path of the user. */
  webPath: Scalars["String"]
  /** Web URL of the user. */
  webUrl: Scalars["String"]
}

/** Core represention of a GitLab user. */
export type UserCoreAssignedMergeRequestsArgs = {
  iids?: Maybe<Array<Scalars["String"]>>
  sourceBranches?: Maybe<Array<Scalars["String"]>>
  targetBranches?: Maybe<Array<Scalars["String"]>>
  state?: Maybe<MergeRequestState>
  labels?: Maybe<Array<Scalars["String"]>>
  mergedAfter?: Maybe<Scalars["Time"]>
  mergedBefore?: Maybe<Scalars["Time"]>
  milestoneTitle?: Maybe<Scalars["String"]>
  sort?: Maybe<MergeRequestSort>
  not?: Maybe<MergeRequestsResolverNegatedParams>
  projectPath?: Maybe<Scalars["String"]>
  projectId?: Maybe<Scalars["ProjectID"]>
  authorUsername?: Maybe<Scalars["String"]>
  reviewerUsername?: Maybe<Scalars["String"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Core represention of a GitLab user. */
export type UserCoreAuthoredMergeRequestsArgs = {
  iids?: Maybe<Array<Scalars["String"]>>
  sourceBranches?: Maybe<Array<Scalars["String"]>>
  targetBranches?: Maybe<Array<Scalars["String"]>>
  state?: Maybe<MergeRequestState>
  labels?: Maybe<Array<Scalars["String"]>>
  mergedAfter?: Maybe<Scalars["Time"]>
  mergedBefore?: Maybe<Scalars["Time"]>
  milestoneTitle?: Maybe<Scalars["String"]>
  sort?: Maybe<MergeRequestSort>
  not?: Maybe<MergeRequestsResolverNegatedParams>
  projectPath?: Maybe<Scalars["String"]>
  projectId?: Maybe<Scalars["ProjectID"]>
  assigneeUsername?: Maybe<Scalars["String"]>
  reviewerUsername?: Maybe<Scalars["String"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Core represention of a GitLab user. */
export type UserCoreCalloutsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Core represention of a GitLab user. */
export type UserCoreGroupMembershipsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Core represention of a GitLab user. */
export type UserCoreProjectMembershipsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Core represention of a GitLab user. */
export type UserCoreReviewRequestedMergeRequestsArgs = {
  iids?: Maybe<Array<Scalars["String"]>>
  sourceBranches?: Maybe<Array<Scalars["String"]>>
  targetBranches?: Maybe<Array<Scalars["String"]>>
  state?: Maybe<MergeRequestState>
  labels?: Maybe<Array<Scalars["String"]>>
  mergedAfter?: Maybe<Scalars["Time"]>
  mergedBefore?: Maybe<Scalars["Time"]>
  milestoneTitle?: Maybe<Scalars["String"]>
  sort?: Maybe<MergeRequestSort>
  not?: Maybe<MergeRequestsResolverNegatedParams>
  projectPath?: Maybe<Scalars["String"]>
  projectId?: Maybe<Scalars["ProjectID"]>
  authorUsername?: Maybe<Scalars["String"]>
  assigneeUsername?: Maybe<Scalars["String"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Core represention of a GitLab user. */
export type UserCoreSnippetsArgs = {
  ids?: Maybe<Array<Scalars["SnippetID"]>>
  visibility?: Maybe<VisibilityScopesEnum>
  type?: Maybe<TypeEnum>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Core represention of a GitLab user. */
export type UserCoreStarredProjectsArgs = {
  search?: Maybe<Scalars["String"]>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Core represention of a GitLab user. */
export type UserCoreTodosArgs = {
  action?: Maybe<Array<TodoActionEnum>>
  authorId?: Maybe<Array<Scalars["ID"]>>
  projectId?: Maybe<Array<Scalars["ID"]>>
  groupId?: Maybe<Array<Scalars["ID"]>>
  state?: Maybe<Array<TodoStateEnum>>
  type?: Maybe<Array<TodoTargetEnum>>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** The connection type for UserCore. */
export type UserCoreConnection = {
  __typename?: "UserCoreConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<UserCoreEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<UserCore>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type UserCoreEdge = {
  __typename?: "UserCoreEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<UserCore>
}

/**
 * Information about a merge request given a specific user.
 *
 * This object has two parts to its state: a `User` and a `MergeRequest`. All
 * fields relate to interactions between the two entities.
 */
export type UserMergeRequestInteraction = {
  __typename?: "UserMergeRequestInteraction"
  /** Approval rules that apply to this user for this merge request. */
  applicableApprovalRules?: Maybe<Array<ApprovalRule>>
  /** Whether this user has approved this merge request. */
  approved: Scalars["Boolean"]
  /** Whether this user can merge this merge request. */
  canMerge: Scalars["Boolean"]
  /** Whether this user can update this merge request. */
  canUpdate: Scalars["Boolean"]
  /** The state of the review by this user. */
  reviewState?: Maybe<MergeRequestReviewState>
  /** Whether this user has provided a review for this merge request. */
  reviewed: Scalars["Boolean"]
}

export type UserPermissions = {
  __typename?: "UserPermissions"
  /** Indicates the user can perform `create_snippet` on this resource */
  createSnippet: Scalars["Boolean"]
}

/** Possible states of a user */
export enum UserState {
  /** The user is active and is able to use the system. */
  Active = "active",
  /** The user has been blocked and is prevented from using the system. */
  Blocked = "blocked",
  /** The user is no longer active and is unable to use the system. */
  Deactivated = "deactivated",
}

export type UserStatus = {
  __typename?: "UserStatus"
  /** User availability status. */
  availability: AvailabilityEnum
  /** String representation of emoji. */
  emoji?: Maybe<Scalars["String"]>
  /** User status message. */
  message?: Maybe<Scalars["String"]>
  /** HTML of the user status message */
  messageHtml?: Maybe<Scalars["String"]>
}

export enum VisibilityLevelsEnum {
  /** Private visibility level. */
  Private = "private",
  /** Internal visibility level. */
  Internal = "internal",
  /** Public visibility level. */
  Public = "public",
}

export enum VisibilityScopesEnum {
  /** The snippet is visible only to the snippet creator. */
  Private = "private",
  /** The snippet is visible for any logged in user except external users. */
  Internal = "internal",
  /** The snippet can be accessed without any authentication. */
  Public = "public",
}

/** Represents the count of vulnerabilities by severity on a particular day. This data is retained for 365 days */
export type VulnerabilitiesCountByDay = {
  __typename?: "VulnerabilitiesCountByDay"
  /** Total number of vulnerabilities on a particular day with critical severity */
  critical: Scalars["Int"]
  /** Date for the count. */
  date: Scalars["ISO8601Date"]
  /** Total number of vulnerabilities on a particular day with high severity */
  high: Scalars["Int"]
  /** Total number of vulnerabilities on a particular day with info severity */
  info: Scalars["Int"]
  /** Total number of vulnerabilities on a particular day with low severity */
  low: Scalars["Int"]
  /** Total number of vulnerabilities on a particular day with medium severity */
  medium: Scalars["Int"]
  /** Total number of vulnerabilities on a particular day. */
  total: Scalars["Int"]
  /** Total number of vulnerabilities on a particular day with unknown severity */
  unknown: Scalars["Int"]
}

/** Represents the number of vulnerabilities for a particular severity on a particular day. This data is retained for 365 days */
export type VulnerabilitiesCountByDayAndSeverity = {
  __typename?: "VulnerabilitiesCountByDayAndSeverity"
  /** Number of vulnerabilities. */
  count?: Maybe<Scalars["Int"]>
  /** Date for the count. */
  day?: Maybe<Scalars["ISO8601Date"]>
  /** Severity of the counted vulnerabilities. */
  severity?: Maybe<VulnerabilitySeverity>
}

/** The connection type for VulnerabilitiesCountByDayAndSeverity. */
export type VulnerabilitiesCountByDayAndSeverityConnection = {
  __typename?: "VulnerabilitiesCountByDayAndSeverityConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<VulnerabilitiesCountByDayAndSeverityEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<VulnerabilitiesCountByDayAndSeverity>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type VulnerabilitiesCountByDayAndSeverityEdge = {
  __typename?: "VulnerabilitiesCountByDayAndSeverityEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<VulnerabilitiesCountByDayAndSeverity>
}

/** The connection type for VulnerabilitiesCountByDay. */
export type VulnerabilitiesCountByDayConnection = {
  __typename?: "VulnerabilitiesCountByDayConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<VulnerabilitiesCountByDayEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<VulnerabilitiesCountByDay>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type VulnerabilitiesCountByDayEdge = {
  __typename?: "VulnerabilitiesCountByDayEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<VulnerabilitiesCountByDay>
}

/** Represents a vulnerability */
export type Vulnerability = Noteable & {
  __typename?: "Vulnerability"
  /** Timestamp of when the vulnerability state was changed to confirmed. */
  confirmedAt?: Maybe<Scalars["Time"]>
  /** The user that confirmed the vulnerability. */
  confirmedBy?: Maybe<UserCore>
  /** Description of the vulnerability. */
  description?: Maybe<Scalars["String"]>
  /** Details of the vulnerability. */
  details: Array<VulnerabilityDetail>
  /** Timestamp of when the vulnerability was first detected. */
  detectedAt: Scalars["Time"]
  /** All discussions on this noteable. */
  discussions: DiscussionConnection
  /** Timestamp of when the vulnerability state was changed to dismissed. */
  dismissedAt?: Maybe<Scalars["Time"]>
  /** The user that dismissed the vulnerability. */
  dismissedBy?: Maybe<UserCore>
  /** List of external issue links related to the vulnerability. */
  externalIssueLinks: VulnerabilityExternalIssueLinkConnection
  /** Indicates whether there is a solution available for this vulnerability. */
  hasSolutions?: Maybe<Scalars["Boolean"]>
  /** GraphQL ID of the vulnerability. */
  id: Scalars["ID"]
  /** Identifiers of the vulnerability. */
  identifiers: Array<VulnerabilityIdentifier>
  /** List of issue links related to the vulnerability. */
  issueLinks: VulnerabilityIssueLinkConnection
  /** Location metadata for the vulnerability. Its fields depend on the type of security scan that found the vulnerability. */
  location?: Maybe<VulnerabilityLocation>
  /** Merge request that fixes the vulnerability. */
  mergeRequest?: Maybe<MergeRequest>
  /** All notes on this noteable. */
  notes: NoteConnection
  /** Primary identifier of the vulnerability. */
  primaryIdentifier?: Maybe<VulnerabilityIdentifier>
  /** The project on which the vulnerability was found. */
  project?: Maybe<Project>
  /** Type of the security report that found the vulnerability (SAST, DEPENDENCY_SCANNING, CONTAINER_SCANNING, DAST, SECRET_DETECTION, COVERAGE_FUZZING, API_FUZZING). `Scan Type` in the UI. */
  reportType?: Maybe<VulnerabilityReportType>
  /** Timestamp of when the vulnerability state was changed to resolved. */
  resolvedAt?: Maybe<Scalars["Time"]>
  /** The user that resolved the vulnerability. */
  resolvedBy?: Maybe<UserCore>
  /** Indicates whether the vulnerability is fixed on the default branch or not. */
  resolvedOnDefaultBranch: Scalars["Boolean"]
  /** Scanner metadata for the vulnerability. */
  scanner?: Maybe<VulnerabilityScanner>
  /** Severity of the vulnerability (INFO, UNKNOWN, LOW, MEDIUM, HIGH, CRITICAL) */
  severity?: Maybe<VulnerabilitySeverity>
  /** State of the vulnerability (DETECTED, CONFIRMED, RESOLVED, DISMISSED) */
  state?: Maybe<VulnerabilityState>
  /** Title of the vulnerability. */
  title?: Maybe<Scalars["String"]>
  /** Number of user notes attached to the vulnerability. */
  userNotesCount: Scalars["Int"]
  /** Permissions for the current user on the resource */
  userPermissions: VulnerabilityPermissions
  /** URL to the vulnerability's details page. */
  vulnerabilityPath?: Maybe<Scalars["String"]>
}

/** Represents a vulnerability */
export type VulnerabilityDiscussionsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Represents a vulnerability */
export type VulnerabilityExternalIssueLinksArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Represents a vulnerability */
export type VulnerabilityIssueLinksArgs = {
  linkType?: Maybe<VulnerabilityIssueLinkType>
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Represents a vulnerability */
export type VulnerabilityNotesArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Autogenerated input type of VulnerabilityConfirm */
export type VulnerabilityConfirmInput = {
  /** ID of the vulnerability to be confirmed. */
  id: Scalars["VulnerabilityID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of VulnerabilityConfirm */
export type VulnerabilityConfirmPayload = {
  __typename?: "VulnerabilityConfirmPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The vulnerability after state change. */
  vulnerability?: Maybe<Vulnerability>
}

/** The connection type for Vulnerability. */
export type VulnerabilityConnection = {
  __typename?: "VulnerabilityConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<VulnerabilityEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<Vulnerability>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** Represents a vulnerability detail field. The fields with data will depend on the vulnerability detail type */
export type VulnerabilityDetail =
  | VulnerabilityDetailBase
  | VulnerabilityDetailBoolean
  | VulnerabilityDetailCode
  | VulnerabilityDetailCommit
  | VulnerabilityDetailDiff
  | VulnerabilityDetailFileLocation
  | VulnerabilityDetailInt
  | VulnerabilityDetailList
  | VulnerabilityDetailMarkdown
  | VulnerabilityDetailModuleLocation
  | VulnerabilityDetailTable
  | VulnerabilityDetailText
  | VulnerabilityDetailUrl

/** Represents the vulnerability details base */
export type VulnerabilityDetailBase = {
  __typename?: "VulnerabilityDetailBase"
  /** Description of the field. */
  description?: Maybe<Scalars["String"]>
  /** Name of the field. */
  fieldName?: Maybe<Scalars["String"]>
  /** Name of the field. */
  name?: Maybe<Scalars["String"]>
}

/** Represents the vulnerability details boolean value */
export type VulnerabilityDetailBoolean = {
  __typename?: "VulnerabilityDetailBoolean"
  /** Description of the field. */
  description?: Maybe<Scalars["String"]>
  /** Name of the field. */
  fieldName?: Maybe<Scalars["String"]>
  /** Name of the field. */
  name?: Maybe<Scalars["String"]>
  /** Value of the field. */
  value: Scalars["Boolean"]
}

/** Represents the vulnerability details code field */
export type VulnerabilityDetailCode = {
  __typename?: "VulnerabilityDetailCode"
  /** Description of the field. */
  description?: Maybe<Scalars["String"]>
  /** Name of the field. */
  fieldName?: Maybe<Scalars["String"]>
  /** Language of the code. */
  lang?: Maybe<Scalars["String"]>
  /** Name of the field. */
  name?: Maybe<Scalars["String"]>
  /** Source code. */
  value: Scalars["String"]
}

/** Represents the vulnerability details commit field */
export type VulnerabilityDetailCommit = {
  __typename?: "VulnerabilityDetailCommit"
  /** Description of the field. */
  description?: Maybe<Scalars["String"]>
  /** Name of the field. */
  fieldName?: Maybe<Scalars["String"]>
  /** Name of the field. */
  name?: Maybe<Scalars["String"]>
  /** The commit SHA value. */
  value: Scalars["String"]
}

/** Represents the vulnerability details diff field */
export type VulnerabilityDetailDiff = {
  __typename?: "VulnerabilityDetailDiff"
  /** Value of the field after the change. */
  after: Scalars["String"]
  /** Value of the field before the change. */
  before: Scalars["String"]
  /** Description of the field. */
  description?: Maybe<Scalars["String"]>
  /** Name of the field. */
  fieldName?: Maybe<Scalars["String"]>
  /** Name of the field. */
  name?: Maybe<Scalars["String"]>
}

/** Represents the vulnerability details location within a file in the project */
export type VulnerabilityDetailFileLocation = {
  __typename?: "VulnerabilityDetailFileLocation"
  /** Description of the field. */
  description?: Maybe<Scalars["String"]>
  /** Name of the field. */
  fieldName?: Maybe<Scalars["String"]>
  /** File name. */
  fileName: Scalars["String"]
  /** End line number of the file location. */
  lineEnd: Scalars["Int"]
  /** Start line number of the file location. */
  lineStart: Scalars["Int"]
  /** Name of the field. */
  name?: Maybe<Scalars["String"]>
}

/** Represents the vulnerability details integer value */
export type VulnerabilityDetailInt = {
  __typename?: "VulnerabilityDetailInt"
  /** Description of the field. */
  description?: Maybe<Scalars["String"]>
  /** Name of the field. */
  fieldName?: Maybe<Scalars["String"]>
  /** Name of the field. */
  name?: Maybe<Scalars["String"]>
  /** Value of the field. */
  value: Scalars["Int"]
}

/** Represents the vulnerability details list value */
export type VulnerabilityDetailList = {
  __typename?: "VulnerabilityDetailList"
  /** Description of the field. */
  description?: Maybe<Scalars["String"]>
  /** Name of the field. */
  fieldName?: Maybe<Scalars["String"]>
  /** List of details. */
  items: Array<VulnerabilityDetail>
  /** Name of the field. */
  name?: Maybe<Scalars["String"]>
}

/** Represents the vulnerability details Markdown field */
export type VulnerabilityDetailMarkdown = {
  __typename?: "VulnerabilityDetailMarkdown"
  /** Description of the field. */
  description?: Maybe<Scalars["String"]>
  /** Name of the field. */
  fieldName?: Maybe<Scalars["String"]>
  /** Name of the field. */
  name?: Maybe<Scalars["String"]>
  /** Value of the Markdown field. */
  value: Scalars["String"]
}

/** Represents the vulnerability details location within a file in the project */
export type VulnerabilityDetailModuleLocation = {
  __typename?: "VulnerabilityDetailModuleLocation"
  /** Description of the field. */
  description?: Maybe<Scalars["String"]>
  /** Name of the field. */
  fieldName?: Maybe<Scalars["String"]>
  /** Module name. */
  moduleName: Scalars["String"]
  /** Name of the field. */
  name?: Maybe<Scalars["String"]>
  /** Offset of the module location. */
  offset: Scalars["Int"]
}

/** Represents the vulnerability details table value */
export type VulnerabilityDetailTable = {
  __typename?: "VulnerabilityDetailTable"
  /** Description of the field. */
  description?: Maybe<Scalars["String"]>
  /** Name of the field. */
  fieldName?: Maybe<Scalars["String"]>
  /** Table headers. */
  headers: Array<VulnerabilityDetail>
  /** Name of the field. */
  name?: Maybe<Scalars["String"]>
  /** Table rows. */
  rows: Array<VulnerabilityDetail>
}

/** Represents the vulnerability details text field */
export type VulnerabilityDetailText = {
  __typename?: "VulnerabilityDetailText"
  /** Description of the field. */
  description?: Maybe<Scalars["String"]>
  /** Name of the field. */
  fieldName?: Maybe<Scalars["String"]>
  /** Name of the field. */
  name?: Maybe<Scalars["String"]>
  /** Value of the text field. */
  value: Scalars["String"]
}

/** Represents the vulnerability details URL field */
export type VulnerabilityDetailUrl = {
  __typename?: "VulnerabilityDetailUrl"
  /** Description of the field. */
  description?: Maybe<Scalars["String"]>
  /** Name of the field. */
  fieldName?: Maybe<Scalars["String"]>
  /** Href of the URL. */
  href: Scalars["String"]
  /** Name of the field. */
  name?: Maybe<Scalars["String"]>
  /** Text of the URL. */
  text?: Maybe<Scalars["String"]>
}

/** Autogenerated input type of VulnerabilityDismiss */
export type VulnerabilityDismissInput = {
  /** ID of the vulnerability to be dismissed. */
  id: Scalars["VulnerabilityID"]
  /** Comment why vulnerability should be dismissed. */
  comment?: Maybe<Scalars["String"]>
  /** Reason why vulnerability should be dismissed. */
  dismissalReason?: Maybe<VulnerabilityDismissalReason>
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of VulnerabilityDismiss */
export type VulnerabilityDismissPayload = {
  __typename?: "VulnerabilityDismissPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The vulnerability after dismissal. */
  vulnerability?: Maybe<Vulnerability>
}

/** The dismissal reason of the Vulnerability */
export enum VulnerabilityDismissalReason {
  /** The vulnerability is known, and has not been remediated or mitigated, but is considered to be an acceptable business risk. */
  AcceptableRisk = "ACCEPTABLE_RISK",
  /** An error in reporting in which a test result incorrectly indicates the presence of a vulnerability in a system when the vulnerability is not present. */
  FalsePositive = "FALSE_POSITIVE",
  /** A management, operational, or technical control (that is, safeguard or countermeasure) employed by an organization that provides equivalent or comparable protection for an information system. */
  MitigatingControl = "MITIGATING_CONTROL",
  /** The finding is not a vulnerability because it is part of a test or is test data. */
  UsedInTests = "USED_IN_TESTS",
  /** The vulnerability is known, and has not been remediated or mitigated, but is considered to be in a part of the application that will not be updated. */
  NotApplicable = "NOT_APPLICABLE",
}

/** An edge in a connection. */
export type VulnerabilityEdge = {
  __typename?: "VulnerabilityEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<Vulnerability>
}

/** Represents an external issue link of a vulnerability */
export type VulnerabilityExternalIssueLink = {
  __typename?: "VulnerabilityExternalIssueLink"
  /** The external issue attached to the issue link. */
  externalIssue?: Maybe<ExternalIssue>
  /** GraphQL ID of the external issue link. */
  id: Scalars["VulnerabilitiesExternalIssueLinkID"]
  /** Type of the external issue link. */
  linkType: VulnerabilityExternalIssueLinkType
}

/** The connection type for VulnerabilityExternalIssueLink. */
export type VulnerabilityExternalIssueLinkConnection = {
  __typename?: "VulnerabilityExternalIssueLinkConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<VulnerabilityExternalIssueLinkEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<VulnerabilityExternalIssueLink>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** Autogenerated input type of VulnerabilityExternalIssueLinkCreate */
export type VulnerabilityExternalIssueLinkCreateInput = {
  /** ID of the vulnerability. */
  id: Scalars["VulnerabilityID"]
  /** Type of the external issue link. */
  linkType: VulnerabilityExternalIssueLinkType
  /** External tracker type of the external issue link. */
  externalTracker: VulnerabilityExternalIssueLinkExternalTracker
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of VulnerabilityExternalIssueLinkCreate */
export type VulnerabilityExternalIssueLinkCreatePayload = {
  __typename?: "VulnerabilityExternalIssueLinkCreatePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The created external issue link. */
  externalIssueLink?: Maybe<VulnerabilityExternalIssueLink>
}

/** Autogenerated input type of VulnerabilityExternalIssueLinkDestroy */
export type VulnerabilityExternalIssueLinkDestroyInput = {
  /** The global ID of the vulnerability external issue link. */
  id: Scalars["VulnerabilitiesExternalIssueLinkID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of VulnerabilityExternalIssueLinkDestroy */
export type VulnerabilityExternalIssueLinkDestroyPayload = {
  __typename?: "VulnerabilityExternalIssueLinkDestroyPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
}

/** An edge in a connection. */
export type VulnerabilityExternalIssueLinkEdge = {
  __typename?: "VulnerabilityExternalIssueLinkEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<VulnerabilityExternalIssueLink>
}

/** The external tracker of the external issue link related to a vulnerability */
export enum VulnerabilityExternalIssueLinkExternalTracker {
  /** Jira external tracker */
  Jira = "JIRA",
}

/** The type of the external issue link related to a vulnerability */
export enum VulnerabilityExternalIssueLinkType {
  /** Created link type */
  Created = "CREATED",
}

/** The grade of the vulnerable project */
export enum VulnerabilityGrade {
  A = "A",
  B = "B",
  C = "C",
  D = "D",
  F = "F",
}

/** Represents a vulnerability identifier */
export type VulnerabilityIdentifier = {
  __typename?: "VulnerabilityIdentifier"
  /** External ID of the vulnerability identifier. */
  externalId?: Maybe<Scalars["String"]>
  /** External type of the vulnerability identifier. */
  externalType?: Maybe<Scalars["String"]>
  /** Name of the vulnerability identifier. */
  name?: Maybe<Scalars["String"]>
  /** URL of the vulnerability identifier. */
  url?: Maybe<Scalars["String"]>
}

/** Represents an issue link of a vulnerability */
export type VulnerabilityIssueLink = {
  __typename?: "VulnerabilityIssueLink"
  /** GraphQL ID of the vulnerability. */
  id: Scalars["ID"]
  /** The issue attached to issue link. */
  issue: Issue
  /** Type of the issue link. */
  linkType: VulnerabilityIssueLinkType
}

/** The connection type for VulnerabilityIssueLink. */
export type VulnerabilityIssueLinkConnection = {
  __typename?: "VulnerabilityIssueLinkConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<VulnerabilityIssueLinkEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<VulnerabilityIssueLink>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type VulnerabilityIssueLinkEdge = {
  __typename?: "VulnerabilityIssueLinkEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<VulnerabilityIssueLink>
}

/** The type of the issue link related to a vulnerability */
export enum VulnerabilityIssueLinkType {
  Related = "RELATED",
  Created = "CREATED",
}

/** Represents a vulnerability location. The fields with data will depend on the vulnerability report type */
export type VulnerabilityLocation =
  | VulnerabilityLocationContainerScanning
  | VulnerabilityLocationCoverageFuzzing
  | VulnerabilityLocationDast
  | VulnerabilityLocationDependencyScanning
  | VulnerabilityLocationSast
  | VulnerabilityLocationSecretDetection

/** Represents the location of a vulnerability found by a container security scan */
export type VulnerabilityLocationContainerScanning = {
  __typename?: "VulnerabilityLocationContainerScanning"
  /** Dependency containing the vulnerability. */
  dependency?: Maybe<VulnerableDependency>
  /** Name of the vulnerable container image. */
  image?: Maybe<Scalars["String"]>
  /** Operating system that runs on the vulnerable container image. */
  operatingSystem?: Maybe<Scalars["String"]>
}

/** Represents the location of a vulnerability found by a Coverage Fuzzing scan */
export type VulnerabilityLocationCoverageFuzzing = {
  __typename?: "VulnerabilityLocationCoverageFuzzing"
  /** Blob path to the vulnerable file. */
  blobPath?: Maybe<Scalars["String"]>
  /** Number of the last relevant line in the vulnerable file. */
  endLine?: Maybe<Scalars["String"]>
  /** Path to the vulnerable file. */
  file?: Maybe<Scalars["String"]>
  /** Number of the first relevant line in the vulnerable file. */
  startLine?: Maybe<Scalars["String"]>
  /** Class containing the vulnerability. */
  vulnerableClass?: Maybe<Scalars["String"]>
  /** Method containing the vulnerability. */
  vulnerableMethod?: Maybe<Scalars["String"]>
}

/** Represents the location of a vulnerability found by a DAST scan */
export type VulnerabilityLocationDast = {
  __typename?: "VulnerabilityLocationDast"
  /** Domain name of the vulnerable request. */
  hostname?: Maybe<Scalars["String"]>
  /** Query parameter for the URL on which the vulnerability occurred. */
  param?: Maybe<Scalars["String"]>
  /** URL path and query string of the vulnerable request. */
  path?: Maybe<Scalars["String"]>
  /** HTTP method of the vulnerable request. */
  requestMethod?: Maybe<Scalars["String"]>
}

/** Represents the location of a vulnerability found by a dependency security scan */
export type VulnerabilityLocationDependencyScanning = {
  __typename?: "VulnerabilityLocationDependencyScanning"
  /** Blob path to the vulnerable file. */
  blobPath?: Maybe<Scalars["String"]>
  /** Dependency containing the vulnerability. */
  dependency?: Maybe<VulnerableDependency>
  /** Path to the vulnerable file. */
  file?: Maybe<Scalars["String"]>
}

/** Represents the location of a vulnerability found by a SAST scan */
export type VulnerabilityLocationSast = {
  __typename?: "VulnerabilityLocationSast"
  /** Blob path to the vulnerable file. */
  blobPath?: Maybe<Scalars["String"]>
  /** Number of the last relevant line in the vulnerable file. */
  endLine?: Maybe<Scalars["String"]>
  /** Path to the vulnerable file. */
  file?: Maybe<Scalars["String"]>
  /** Number of the first relevant line in the vulnerable file. */
  startLine?: Maybe<Scalars["String"]>
  /** Class containing the vulnerability. */
  vulnerableClass?: Maybe<Scalars["String"]>
  /** Method containing the vulnerability. */
  vulnerableMethod?: Maybe<Scalars["String"]>
}

/** Represents the location of a vulnerability found by a secret detection scan */
export type VulnerabilityLocationSecretDetection = {
  __typename?: "VulnerabilityLocationSecretDetection"
  /** Blob path to the vulnerable file. */
  blobPath?: Maybe<Scalars["String"]>
  /** Number of the last relevant line in the vulnerable file. */
  endLine?: Maybe<Scalars["String"]>
  /** Path to the vulnerable file. */
  file?: Maybe<Scalars["String"]>
  /** Number of the first relevant line in the vulnerable file. */
  startLine?: Maybe<Scalars["String"]>
  /** Class containing the vulnerability. */
  vulnerableClass?: Maybe<Scalars["String"]>
  /** Method containing the vulnerability. */
  vulnerableMethod?: Maybe<Scalars["String"]>
}

/** Check permissions for the current user on a vulnerability */
export type VulnerabilityPermissions = {
  __typename?: "VulnerabilityPermissions"
  /** Indicates the user can perform `admin_vulnerability` on this resource */
  adminVulnerability: Scalars["Boolean"]
  /** Indicates the user can perform `admin_vulnerability_external_issue_link` on this resource */
  adminVulnerabilityExternalIssueLink: Scalars["Boolean"]
  /** Indicates the user can perform `admin_vulnerability_issue_link` on this resource */
  adminVulnerabilityIssueLink: Scalars["Boolean"]
  /** Indicates the user can perform `create_vulnerability` on this resource */
  createVulnerability: Scalars["Boolean"]
  /** Indicates the user can perform `create_vulnerability_export` on this resource */
  createVulnerabilityExport: Scalars["Boolean"]
  /** Indicates the user can perform `create_vulnerability_feedback` on this resource */
  createVulnerabilityFeedback: Scalars["Boolean"]
  /** Indicates the user can perform `destroy_vulnerability_feedback` on this resource */
  destroyVulnerabilityFeedback: Scalars["Boolean"]
  /** Indicates the user can perform `read_vulnerability_feedback` on this resource */
  readVulnerabilityFeedback: Scalars["Boolean"]
  /** Indicates the user can perform `update_vulnerability_feedback` on this resource */
  updateVulnerabilityFeedback: Scalars["Boolean"]
}

/** The type of the security scan that found the vulnerability */
export enum VulnerabilityReportType {
  Sast = "SAST",
  DependencyScanning = "DEPENDENCY_SCANNING",
  ContainerScanning = "CONTAINER_SCANNING",
  Dast = "DAST",
  SecretDetection = "SECRET_DETECTION",
  CoverageFuzzing = "COVERAGE_FUZZING",
  ApiFuzzing = "API_FUZZING",
}

/** Autogenerated input type of VulnerabilityResolve */
export type VulnerabilityResolveInput = {
  /** ID of the vulnerability to be resolved. */
  id: Scalars["VulnerabilityID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of VulnerabilityResolve */
export type VulnerabilityResolvePayload = {
  __typename?: "VulnerabilityResolvePayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The vulnerability after state change. */
  vulnerability?: Maybe<Vulnerability>
}

/** Autogenerated input type of VulnerabilityRevertToDetected */
export type VulnerabilityRevertToDetectedInput = {
  /** ID of the vulnerability to be reverted. */
  id: Scalars["VulnerabilityID"]
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
}

/** Autogenerated return type of VulnerabilityRevertToDetected */
export type VulnerabilityRevertToDetectedPayload = {
  __typename?: "VulnerabilityRevertToDetectedPayload"
  /** A unique identifier for the client performing the mutation. */
  clientMutationId?: Maybe<Scalars["String"]>
  /** Errors encountered during execution of the mutation. */
  errors: Array<Scalars["String"]>
  /** The vulnerability after revert. */
  vulnerability?: Maybe<Vulnerability>
}

/** Represents a vulnerability scanner */
export type VulnerabilityScanner = {
  __typename?: "VulnerabilityScanner"
  /** External ID of the vulnerability scanner. */
  externalId?: Maybe<Scalars["String"]>
  /** ID of the scanner. */
  id?: Maybe<Scalars["ID"]>
  /** Name of the vulnerability scanner. */
  name?: Maybe<Scalars["String"]>
  /** Type of the vulnerability report. */
  reportType?: Maybe<VulnerabilityReportType>
  /** Vendor of the vulnerability scanner. */
  vendor?: Maybe<Scalars["String"]>
}

/** The connection type for VulnerabilityScanner. */
export type VulnerabilityScannerConnection = {
  __typename?: "VulnerabilityScannerConnection"
  /** A list of edges. */
  edges?: Maybe<Array<Maybe<VulnerabilityScannerEdge>>>
  /** A list of nodes. */
  nodes?: Maybe<Array<Maybe<VulnerabilityScanner>>>
  /** Information to aid in pagination. */
  pageInfo: PageInfo
}

/** An edge in a connection. */
export type VulnerabilityScannerEdge = {
  __typename?: "VulnerabilityScannerEdge"
  /** A cursor for use in pagination. */
  cursor: Scalars["String"]
  /** The item at the end of the edge. */
  node?: Maybe<VulnerabilityScanner>
}

/** Represents vulnerability counts by severity */
export type VulnerabilitySeveritiesCount = {
  __typename?: "VulnerabilitySeveritiesCount"
  /** Number of vulnerabilities of CRITICAL severity of the project */
  critical?: Maybe<Scalars["Int"]>
  /** Number of vulnerabilities of HIGH severity of the project */
  high?: Maybe<Scalars["Int"]>
  /** Number of vulnerabilities of INFO severity of the project */
  info?: Maybe<Scalars["Int"]>
  /** Number of vulnerabilities of LOW severity of the project */
  low?: Maybe<Scalars["Int"]>
  /** Number of vulnerabilities of MEDIUM severity of the project */
  medium?: Maybe<Scalars["Int"]>
  /** Number of vulnerabilities of UNKNOWN severity of the project */
  unknown?: Maybe<Scalars["Int"]>
}

/** The severity of the vulnerability */
export enum VulnerabilitySeverity {
  Info = "INFO",
  Unknown = "UNKNOWN",
  Low = "LOW",
  Medium = "MEDIUM",
  High = "HIGH",
  Critical = "CRITICAL",
}

/** Vulnerability sort values */
export enum VulnerabilitySort {
  /** Severity in descending order. */
  SeverityDesc = "severity_desc",
  /** Severity in ascending order. */
  SeverityAsc = "severity_asc",
  /** Title in descending order. */
  TitleDesc = "title_desc",
  /** Title in ascending order. */
  TitleAsc = "title_asc",
  /** Detection timestamp in descending order. */
  DetectedDesc = "detected_desc",
  /** Detection timestamp in ascending order. */
  DetectedAsc = "detected_asc",
  /** Report Type in descending order. */
  ReportTypeDesc = "report_type_desc",
  /** Report Type in ascending order. */
  ReportTypeAsc = "report_type_asc",
  /** State in descending order. */
  StateDesc = "state_desc",
  /** State in ascending order. */
  StateAsc = "state_asc",
}

/** The state of the vulnerability */
export enum VulnerabilityState {
  Detected = "DETECTED",
  Confirmed = "CONFIRMED",
  Resolved = "RESOLVED",
  Dismissed = "DISMISSED",
}

/** Represents a vulnerable dependency. Used in vulnerability location data */
export type VulnerableDependency = {
  __typename?: "VulnerableDependency"
  /** The package associated with the vulnerable dependency. */
  package?: Maybe<VulnerablePackage>
  /** The version of the vulnerable dependency. */
  version?: Maybe<Scalars["String"]>
}

/** Represents a vulnerable package. Used in vulnerability dependency data */
export type VulnerablePackage = {
  __typename?: "VulnerablePackage"
  /** The name of the vulnerable package. */
  name?: Maybe<Scalars["String"]>
}

/** Represents vulnerability letter grades with associated projects */
export type VulnerableProjectsByGrade = {
  __typename?: "VulnerableProjectsByGrade"
  /** Number of projects within this grade. */
  count: Scalars["Int"]
  /** Grade based on the highest severity vulnerability present. */
  grade: VulnerabilityGrade
  /** Projects within this grade. */
  projects: ProjectConnection
}

/** Represents vulnerability letter grades with associated projects */
export type VulnerableProjectsByGradeProjectsArgs = {
  after?: Maybe<Scalars["String"]>
  before?: Maybe<Scalars["String"]>
  first?: Maybe<Scalars["Int"]>
  last?: Maybe<Scalars["Int"]>
}

/** Weight ID wildcard values */
export enum WeightWildcardId {
  /** No weight is assigned. */
  None = "NONE",
  /** Weight is assigned. */
  Any = "ANY",
}

export type CommitFilesMutationVariables = Exact<{
  input: CommitCreateInput
}>

export type CommitFilesMutation = { __typename?: "Mutation" } & {
  commitCreate?: Maybe<
    { __typename?: "CommitCreatePayload" } & Pick<
      CommitCreatePayload,
      "errors" | "content"
    > & {
        commit?: Maybe<{ __typename?: "Commit" } & Pick<Commit, "id" | "sha">>
      }
  >
}

export type ContainerRepositoryTagsQueryVariables = Exact<{
  repositoryId: Scalars["ContainerRepositoryID"]
  afterTagCursor?: Maybe<Scalars["String"]>
}>

export type ContainerRepositoryTagsQuery = { __typename?: "Query" } & {
  containerRepository?: Maybe<
    { __typename?: "ContainerRepositoryDetails" } & {
      tags?: Maybe<
        { __typename?: "ContainerRepositoryTagConnection" } & {
          pageInfo: { __typename?: "PageInfo" } & Pick<
            PageInfo,
            "endCursor" | "hasNextPage"
          >
          nodes?: Maybe<
            Array<
              Maybe<
                { __typename?: "ContainerRepositoryTag" } & Pick<
                  ContainerRepositoryTag,
                  "name"
                >
              >
            >
          >
        }
      >
    }
  >
}

export type ContainerRepositoriesQueryVariables = Exact<{
  fullPath: Scalars["ID"]
}>

export type ContainerRepositoriesQuery = { __typename?: "Query" } & {
  project?: Maybe<
    { __typename?: "Project" } & {
      containerRepositories?: Maybe<
        { __typename?: "ContainerRepositoryConnection" } & {
          pageInfo: { __typename?: "PageInfo" } & Pick<
            PageInfo,
            "endCursor" | "hasNextPage"
          >
          nodes?: Maybe<
            Array<
              Maybe<
                { __typename?: "ContainerRepository" } & Pick<
                  ContainerRepository,
                  "id" | "path"
                >
              >
            >
          >
        }
      >
    }
  >
}

export type FileBlobsQueryVariables = Exact<{
  fullPath: Scalars["ID"]
  paths: Array<Scalars["String"]> | Scalars["String"]
}>

export type FileBlobsQuery = { __typename?: "Query" } & {
  project?: Maybe<
    { __typename?: "Project" } & {
      repository?: Maybe<
        { __typename?: "Repository" } & {
          blobs?: Maybe<
            { __typename?: "RepositoryBlobConnection" } & {
              pageInfo: { __typename?: "PageInfo" } & Pick<
                PageInfo,
                "endCursor" | "hasNextPage"
              >
              nodes?: Maybe<
                Array<
                  Maybe<
                    { __typename?: "RepositoryBlob" } & Pick<
                      RepositoryBlob,
                      "path" | "mode" | "rawBlob"
                    >
                  >
                >
              >
            }
          >
        }
      >
    }
  >
}

export type ProjectQueryVariables = Exact<{
  fullPath: Scalars["ID"]
  blobCursor?: Maybe<Scalars["String"]>
}>

export type ProjectQuery = { __typename?: "Query" } & {
  project?: Maybe<
    { __typename?: "Project" } & {
      repository?: Maybe<
        { __typename?: "Repository" } & {
          tree?: Maybe<
            { __typename?: "Tree" } & {
              blobs: { __typename?: "BlobConnection" } & {
                pageInfo: { __typename?: "PageInfo" } & Pick<
                  PageInfo,
                  "endCursor" | "hasNextPage"
                >
                nodes?: Maybe<
                  Array<
                    Maybe<{ __typename?: "Blob" } & Pick<Blob, "name" | "path">>
                  >
                >
              }
            }
          >
        }
      >
    }
  >
}

export type ProjectsQueryVariables = Exact<{
  group: Scalars["String"]
}>

export type ProjectsQuery = { __typename?: "Query" } & {
  projects?: Maybe<
    { __typename?: "ProjectConnection" } & {
      pageInfo: { __typename?: "PageInfo" } & Pick<
        PageInfo,
        "endCursor" | "hasNextPage"
      >
      nodes?: Maybe<
        Array<
          Maybe<
            { __typename?: "Project" } & Pick<
              Project,
              "fullPath" | "archived"
            > & {
                repository?: Maybe<
                  { __typename?: "Repository" } & Pick<Repository, "rootRef">
                >
              }
          >
        >
      >
    }
  >
}

export const CommitFilesDocument = gql`
  mutation commitFiles($input: CommitCreateInput!) {
    commitCreate(input: $input) {
      errors
      commit {
        id
        sha
      }
      content
    }
  }
`
export const ContainerRepositoryTagsDocument = gql`
  query containerRepositoryTags(
    $repositoryId: ContainerRepositoryID!
    $afterTagCursor: String
  ) {
    containerRepository(id: $repositoryId) {
      tags(after: $afterTagCursor) {
        pageInfo {
          endCursor
          hasNextPage
        }
        nodes {
          name
        }
      }
    }
  }
`
export const ContainerRepositoriesDocument = gql`
  query containerRepositories($fullPath: ID!) {
    project(fullPath: $fullPath) {
      containerRepositories {
        pageInfo {
          endCursor
          hasNextPage
        }
        nodes {
          id
          path
        }
      }
    }
  }
`
export const FileBlobsDocument = gql`
  query fileBlobs($fullPath: ID!, $paths: [String!]!) {
    project(fullPath: $fullPath) {
      repository {
        blobs(paths: $paths) {
          pageInfo {
            endCursor
            hasNextPage
          }
          nodes {
            path
            mode
            rawBlob
          }
        }
      }
    }
  }
`
export const ProjectDocument = gql`
  query project($fullPath: ID!, $blobCursor: String) {
    project(fullPath: $fullPath) {
      repository {
        tree(path: "", recursive: true) {
          blobs(after: $blobCursor) {
            pageInfo {
              endCursor
              hasNextPage
            }
            nodes {
              name
              path
            }
          }
        }
      }
    }
  }
`
export const ProjectsDocument = gql`
  query projects($group: String!) {
    projects(search: $group, membership: true, searchNamespaces: true) {
      pageInfo {
        endCursor
        hasNextPage
      }
      nodes {
        fullPath
        archived
        repository {
          rootRef
        }
      }
    }
  }
`

export type SdkFunctionWrapper = <T>(
  action: (requestHeaders?: Record<string, string>) => Promise<T>,
  operationName: string,
) => Promise<T>

const defaultWrapper: SdkFunctionWrapper = (action, _operationName) => action()

export function getSdk(
  client: GraphQLClient,
  withWrapper: SdkFunctionWrapper = defaultWrapper,
) {
  return {
    commitFiles(
      variables: CommitFilesMutationVariables,
      requestHeaders?: Dom.RequestInit["headers"],
    ): Promise<CommitFilesMutation> {
      return withWrapper(
        (wrappedRequestHeaders) =>
          client.request<CommitFilesMutation>(CommitFilesDocument, variables, {
            ...requestHeaders,
            ...wrappedRequestHeaders,
          }),
        "commitFiles",
      )
    },
    containerRepositoryTags(
      variables: ContainerRepositoryTagsQueryVariables,
      requestHeaders?: Dom.RequestInit["headers"],
    ): Promise<ContainerRepositoryTagsQuery> {
      return withWrapper(
        (wrappedRequestHeaders) =>
          client.request<ContainerRepositoryTagsQuery>(
            ContainerRepositoryTagsDocument,
            variables,
            { ...requestHeaders, ...wrappedRequestHeaders },
          ),
        "containerRepositoryTags",
      )
    },
    containerRepositories(
      variables: ContainerRepositoriesQueryVariables,
      requestHeaders?: Dom.RequestInit["headers"],
    ): Promise<ContainerRepositoriesQuery> {
      return withWrapper(
        (wrappedRequestHeaders) =>
          client.request<ContainerRepositoriesQuery>(
            ContainerRepositoriesDocument,
            variables,
            { ...requestHeaders, ...wrappedRequestHeaders },
          ),
        "containerRepositories",
      )
    },
    fileBlobs(
      variables: FileBlobsQueryVariables,
      requestHeaders?: Dom.RequestInit["headers"],
    ): Promise<FileBlobsQuery> {
      return withWrapper(
        (wrappedRequestHeaders) =>
          client.request<FileBlobsQuery>(FileBlobsDocument, variables, {
            ...requestHeaders,
            ...wrappedRequestHeaders,
          }),
        "fileBlobs",
      )
    },
    project(
      variables: ProjectQueryVariables,
      requestHeaders?: Dom.RequestInit["headers"],
    ): Promise<ProjectQuery> {
      return withWrapper(
        (wrappedRequestHeaders) =>
          client.request<ProjectQuery>(ProjectDocument, variables, {
            ...requestHeaders,
            ...wrappedRequestHeaders,
          }),
        "project",
      )
    },
    projects(
      variables: ProjectsQueryVariables,
      requestHeaders?: Dom.RequestInit["headers"],
    ): Promise<ProjectsQuery> {
      return withWrapper(
        (wrappedRequestHeaders) =>
          client.request<ProjectsQuery>(ProjectsDocument, variables, {
            ...requestHeaders,
            ...wrappedRequestHeaders,
          }),
        "projects",
      )
    },
  }
}
export type Sdk = ReturnType<typeof getSdk>
