import { gitlabSdk } from "./client"
import { Maybe, ContainerRepository } from "./generated/graphql"

export async function findContainerRepositories(image: string) {
  const pathSections = image.replace("registry.gitlab.com/", "").split("/")

  if (pathSections.length < 2) {
    throw new Error("Image name could not be gitlab registry image")
  }

  return containerQueryRecursive(pathSections)
}

async function containerQueryRecursive(
  pathSections: string[],
  index = 2,
): Promise<
  Array<
    Maybe<
      {
        __typename?: "ContainerRepository" | undefined
      } & Pick<ContainerRepository, "path" | "id">
    >
  >
> {
  if (pathSections.length < index) {
    throw new Error(
      `Container image: ${pathSections.join("/")} not found in projects`,
    )
  }

  const searchPaths = pathSections.slice(0, index)

  const fullPath = searchPaths.join("/")

  const response = await gitlabSdk.containerRepositories({ fullPath })
  if (response.project) {
    return response.project.containerRepositories?.nodes ?? []
  }

  return containerQueryRecursive(pathSections, index + 1)
}
