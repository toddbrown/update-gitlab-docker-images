import { gitlabSdk } from "./client"
import { RepositoryBlob } from "./generated/graphql"
import { imageVersionSplit } from "./gitlabImageRegex"

export async function getImagesForProject(fullPath: string) {
  try {
    const files = await getFiles(fullPath)
    if (files) {
      const response = await findImagesForProject(fullPath, files)
      return { ...response, fullPath }
    }

    return
  } catch (error: unknown) {
    throw error
  }
}

async function getFiles(fullPath: string, blobCursor = "") {
  const filesToUpdate: string[] = []
  try {
    const response = await gitlabSdk.project({
      fullPath,
      blobCursor,
    })

    if (!response.project?.repository?.tree?.blobs.nodes) {
      return
    }

    for (const node of response.project.repository.tree.blobs.nodes) {
      if (node?.name.endsWith(".yml")) {
        filesToUpdate.push(node.path)
      }

      if (node?.name.endsWith("Dockerfile")) {
        filesToUpdate.push(node.path)
      }
    }

    if (response.project.repository.tree.blobs.pageInfo.hasNextPage) {
      const files = await getFiles(
        fullPath,
        response.project.repository.tree.blobs.pageInfo.endCursor ?? "",
      )
      if (files) {
        filesToUpdate.push(...files)
      }
    }
  } catch (error: unknown) {
    throw error
  }

  return filesToUpdate
}

async function findImagesForProject(fullPath: string, files: string[]) {
  const images = new Set<string>()
  const projectFiles = new Map<string, string>()

  try {
    const response = await gitlabSdk.fileBlobs({ fullPath, paths: files })
    if (response.project?.repository?.blobs?.nodes) {
      for (const blob of response.project.repository.blobs.nodes) {
        if (blob?.rawBlob) {
          for (const image of findImageInFile(blob)) {
            images.add(image)
            projectFiles.set(blob.path, blob.rawBlob)
          }
        }
      }
    }
  } catch (error: unknown) {
    throw error
  }

  return { images, projectFiles }
}

function findImageInFile(
  fileBlob: {
    __typename?: "RepositoryBlob" | undefined
  } & Pick<RepositoryBlob, "mode" | "rawBlob" | "path">,
) {
  const images: string[] = []

  if (!fileBlob.rawBlob) {
    return images
  }

  const lines = fileBlob.rawBlob.split("\n")
  for (const line of lines) {
    const possibleMatch = line.matchAll(imageVersionSplit)
    for (const match of possibleMatch) {
      if (match.length > 0) {
        images.push(match[1])
      }
    }
  }

  return images
}
