import { gitlabSdk } from "./client"
import { findContainerRepositories } from "./findContainerRepositories"
import { findMaxVersionForContainerRepository } from "./findMaxVersionForContainerRepository"
import { getImagesForProject } from "./getImagesForProject"
import { imageVersionSplit } from "./gitlabImageRegex"
import * as graphql from "./generated/graphql"

interface ProjectConfig {
  fullPath: string
  images: Set<string>
  projectFiles: Map<string, string>
  defaultBranch: string
}

async function getImagesForGroup(group: string) {
  // Get projects to update
  const repsonse = await gitlabSdk.projects({ group })
  const projectPromises = repsonse.projects?.nodes?.map((project) => {
    if (project && !project.archived) {
      return getImagesForProject(project.fullPath)
    }

    return
  })

  if (!projectPromises) {
    return
  }

  const allImages = new Map<
    string,
    { repostioryId?: string; maxVersion?: string }
  >()
  const allProjects: ProjectConfig[] = []

  // Get images contained in projects
  for await (const project of projectPromises) {
    if (project) {
      for (const image of project.images.keys()) {
        allImages.set(image, {})
      }

      const defaultBranch =
        repsonse.projects?.nodes?.find(
          (node) => node?.fullPath === project.fullPath,
        )?.repository?.rootRef ?? ""
      allProjects.push({ ...project, defaultBranch })
    }
  }

  const containerImageRepositoryTuple: string[][] = []

  // Find container repositories for images
  for (const [image, containerRepositoryId] of allImages.entries()) {
    if (!containerRepositoryId.repostioryId) {
      const containerRepositories = await findContainerRepositories(image)

      for (const containerRepository of containerRepositories) {
        const containerURI = `registry.gitlab.com/${String(
          containerRepository?.path,
        )}`

        if (containerRepository && allImages.has(containerURI)) {
          allImages.set(containerURI, { repostioryId: containerRepository.id })
          containerImageRepositoryTuple.push([
            containerURI,
            containerRepository.id,
          ])
        }
      }
    }
  }

  // Find max versions of images
  const maxVersionPromise = containerImageRepositoryTuple.map(
    async ([, containerRepository]) =>
      findMaxVersionForContainerRepository(containerRepository),
  )

  let index = 0
  for await (const maxVersion of maxVersionPromise) {
    const containerURI = containerImageRepositoryTuple[index][0]
    const image = allImages.get(containerURI)
    allImages.set(containerURI, { ...image, maxVersion })
    index += 1
  }

  for (const project of allProjects.values()) {
    projectUpdate(project, allImages)
  }

  // console.log(allImages)
}

function projectUpdate(
  project: ProjectConfig,
  allImages: Map<string, { repostioryId?: string; maxVersion?: string }>,
) {
  const updatedFiles = new Map<string, string>()
  const updatedImages = new Map<string, string>()

  for (const [fileName, fileBlob] of project.projectFiles.entries()) {
    const newLines: string[] = []
    let fileUpdated = false

    for (const line of fileBlob.split("\n")) {
      const matches = line.matchAll(imageVersionSplit)
      let matched = false
      for (const match of matches) {
        if (match.length > 0) {
          const image = allImages.get(match[1])
          if (image?.maxVersion && image.maxVersion !== match[3]) {
            matched = true
            fileUpdated = true
            newLines.push(
              line.replace(match[0], `${match[1]}:${image?.maxVersion}`),
            )
            updatedImages.set(match[1], image.maxVersion)
          }
        }
      }
      if (!matched) {
        newLines.push(line)
      }
    }
    if (fileUpdated) {
      updatedFiles.set(fileName, newLines.join("\n"))
    }
  }

  if (updatedFiles.size === 0) {
    return
  }

  const commitActions: graphql.CommitAction[] = []
  for (const [fileName, fileBlob] of updatedFiles.entries()) {
    const commitAction: graphql.CommitAction = {
      action: graphql.CommitActionMode.Update,
      filePath: fileName,
      content: fileBlob,
    }
    commitActions.push(commitAction)
  }

  // const response = await gitlabSdk.commitFiles({
  //   input: {
  //     actions: commitActions,
  //     projectPath: project.fullPath,
  //     branch: "chore/update-gitlab-images",
  //     startBranch: project.defaultBranch,
  //     message: "chore(deps): update gitlab images",
  //   },
  // })

  console.log(commitActions)
}

const group = "appliedsystems/devops/templates"

console.time("updateImagesForProject")
getImagesForGroup(group).then(
  () => {
    console.timeEnd("updateImagesForProject")
  },
  (error: unknown) => {
    console.error(error)
  },
)
