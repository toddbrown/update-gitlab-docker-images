import { GraphQLClient } from "graphql-request"
import { getSdk } from "./generated/graphql"
import dotenv from "dotenv"

dotenv.config()
const client = new GraphQLClient("https://gitlab.com/api/graphql")
client.setHeader("Authorization", `Bearer ${String(process.env.GITLAB_TOKEN)}`)
export const gitlabSdk = getSdk(client)
