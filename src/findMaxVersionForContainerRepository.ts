import * as semver from "semver"
import { gitlabSdk } from "./client"

export async function findMaxVersionForContainerRepository(
  containerRepositoryId: string,
) {
  const versions = (await findImageVersionTags(containerRepositoryId)).map(
    (version) => version?.name ?? "",
  )
  let maxVersion = "v0.0.0"

  for (const version of versions) {
    if (semver.gt(version, maxVersion)) {
      maxVersion = version
    }
  }

  return maxVersion
}

async function findImageVersionTags(repositoryId: string, cursor = "") {
  const results = await gitlabSdk.containerRepositoryTags({
    repositoryId,
    afterTagCursor: cursor,
  })

  if (!results.containerRepository?.tags?.nodes) {
    throw new Error("No container respository nodes for: " + repositoryId)
  }

  const imageTags = results.containerRepository.tags.nodes.filter((imageTag) =>
    Boolean(semver.valid(imageTag?.name)),
  )

  if (
    results.containerRepository?.tags?.pageInfo.hasNextPage &&
    results.containerRepository.tags.pageInfo.endCursor
  ) {
    const recursiveResults = await findImageVersionTags(
      repositoryId,
      results.containerRepository.tags.pageInfo.endCursor ?? "",
    )
    imageTags.push(...recursiveResults)
  }

  return imageTags
}
